const gulp = require('gulp');
const zip = require('gulp-zip');

gulp.task('default', () => {
    return gulp.src('build/**/*')
        .pipe(zip('wos-client.zip'))
        .pipe(gulp.dest('release'));
});