<?php
ini_set('display_errors', 1);
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header('Content-Type: application/json;charset=utf-8');

$from = "noreply@edupool.in";
$data = json_decode(file_get_contents('php://input'));
// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$headers .= 'From: ' . $from . "\r\n";

$footer = '<div class="footer-container" style="width:100%;margin-top:10px;">
    <hr/>
    <p style="text-align:center;">
        <span style="font-size:14px"> Don’t forget to follow us on-</span>
    </p>
    <p style="text-align:center; height:25px;">
        <!--Facebook icon-->
         <a href="https://www.facebook.com/edupoolofficial/" target="_blank">
             <img alt="" src="https://image.ibb.co/n4azeo/icons8_facebook_48.png" height="100%"/>
         </a>&nbsp; &nbsp; &nbsp;

         <!--Google+ icon-->
         <a href="https://plus.google.com/u/1/113448798628110106140" target="_blank">
             <img alt="" src="https://image.ibb.co/gOEgs8/icons8_google_plus_squared_48.png" height="100%" />
         </a>&nbsp; &nbsp;&nbsp;

         <!--Twitter icon-->
         <a href="https://twitter.com/edupoolofficial" target="_blank">
             <img alt="" src="https://image.ibb.co/hbe35T/icons8_twitter_squared_48.png" height="100%"/>
         </a>&nbsp; &nbsp;&nbsp;

         <!--Linkedin icon-->
         <a href="https://www.linkedin.com/in/edupoolofficial" target="_blank">
             <img alt="" src="https://image.ibb.co/mSK35T/icons8_linkedin_48.png" height="100%"  />
         </a>&nbsp; &nbsp;&nbsp;

          <!--Instagram icon-->
         <a href="https://www.instagram.com/edupoolofficial" target="_blank">
             <img alt="" src="https://image.ibb.co/cpD2zo/instagram.png" height="100%" />
         </a>&nbsp; &nbsp;&nbsp;

          <!--Youtube icon-->
         <a href="https://www.youtube.com/channel/UCFwr0kCl5fnkAZII4t6LdWQ" target="_blank">
             <img alt="" src="https://image.ibb.co/konqkT/icons8_play_button_48.png" height="100%"/>
         </a>
     </p></div>';


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////---------------------------------------- MAIL TO ADMIN ---------------------------------------------/////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$to = "chaturvedi0209@gmail.com,dharmendra.nagda16@gmail.com,info.edupool@gmail.com";
$subject = "Admission for " . $data->college;
$message1 = '
<html>
    <head>
    <title></title>
    <style>
        body {
            margin: 0;
            color:black;
        }

        .container {
            position: relative;
            margin: 10px auto ;
            padding-right: 15px;
            padding-left: 15px;
        }
        .heading-website{
            position:absolute;
            top:22px;
            display:inline;
        }
        .heading-container{
            position:relative;
            width:100%;
            background:#2F4F4F;
            margin-bottom: 10px;
        }

        a{
            text-decoration: none;
        }

        ol li{
            margin-bottom:10px;
        }
    </style>
    </head>
    <body class="bd-docs" data-target=".bd-sidenav-active" data-spy="scroll">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-9 pull-md-3 bd-content">
                    <div class="bd-example" data-example-id="">
                        <div class="heading-container">
                            <img src="https://i.imgur.com/mUKcRsI.png" />
                        </div>
                        <h4><b>' . $data->name . ' </b> has applied for admission in ' . $data->college . ' in ' . $data->course . ' on ' . $data->date . '</h4>
                        <h5>
                            The details are -
                        </h5>

                        <ol>
                            <li>Student\'s Name : ' . $data->name . '</li>
                            <li>Email : ' . $data->email . '</li>
                            <li>Mobile : ' . $data->mobile . '</li>
                            <li>College Applied: ' . $data->college . '</li>
                            <li>Course Applied: ' . $data->course . '</li>
                            <li>Student\'s Location : ' . $data->location . '</li>
                        </ol>

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
';

mail($to, $subject, $message1, $headers);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////---------------------------------------- MAIL TO STUDENT ---------------------------------------------/////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$to = $data->email;
$subject = "Greetings from Edupool";

$message2 = '
<html>
    <head>
    <title></title>
    <style>
        body {
            margin: 0;
            color:black;
        }
        .container {
            position: relative;
            margin: 10px auto ;
            padding-right: 15px;
            padding-left: 15px;
        }
        .heading-website{
            position:absolute;
            top:22px;
            display:inline;
        }
        .heading-container{
            position:relative;
            width:100%;
            background:#2F4F4F;
            margin-bottom: 10px;
        }
        a{
            text-decoration: none;
        }
    </style>
    </head>
    <body class="bd-docs" data-target=".bd-sidenav-active" data-spy="scroll">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-9 pull-md-3 bd-content">
                <div class="bd-example" data-example-id="">
                    <div class="heading-container">
                      <img  src="https://i.imgur.com/mUKcRsI.png" />
                    </div>
                    <h2 style="color:teal">Greetings from Edupool</h2>
                    <div>
                        Hello <b>' . $data->name . '</b>,
                    </div>
                    <div style="text-align:justify;margin-top:5px;">
                            You just applied for admission in ' . $data->college . ' in ' . $data->course . '. We have sent your details to
                        college. The college admission cell will contact you soon for further admission process. For any help, you can
                        mail us at
                        <a href="mailto:query.edupool@gmail.com?subject=Regarding admission enquiry for ' . $data->college . ' for course ' . $data->course . ' from edupool.in">query.edupool@gmail.com</a>
                        or call us at <a href="tel:+917877511631">+91 7877511631</a>. We will be glad to serve to better.
                        Keep visiting <a href="https://edupool.in" target="_blank">Edupool</a>
                    </div>

                    ' . $footer . '
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>';

mail($to, $subject, $message2, $headers);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////---------------------------------------- MAIL TO COLLEGE ---------------------------------------------/////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$to = $data->collegeEmail;
$subject = "Greetings from Edupool";

$message3 = '
<html>
    <head>
    <title></title>
    <style>
        body {
            margin: 0;
            color:black;
        }

        .container {
            position: relative;
            margin: 10px auto ;
            padding-right: 15px;
            padding-left: 15px;
        }
        .heading-website{
            position:absolute;
            top:22px;
            display:inline;
        }
        .heading-container{
            position:relative;
            width:100%;
            background:#2F4F4F;
            margin-bottom: 10px;
        }
        ol li{
            margin-bottom:10px;
        }
        a{
            text-decoration: none;
        }
    </style>
    </head>
    <body class="bd-docs" data-target=".bd-sidenav-active" data-spy="scroll">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-9 pull-md-3 bd-content">
                <div class="bd-example" data-example-id="">

                        <div class="heading-container">
                            <img src="https://i.imgur.com/mUKcRsI.png" />
                        </div>

                        <div style="text-align:justify;margin-bottom:5px;">
                            Hello <b>' . $data->college . '</b>
                        </div>

                        Bellow mentioned student has applied for admission in your college. The details are -

                        <ol>
                            <li>Student\'s Name : ' . $data->name . '</li>
                            <li>Email : ' . $data->email . '</li>
                            <li>Mobile : ' . $data->mobile . '</li>
                            <li>Course Applied: ' . $data->course . '</li>
                            <li>Student\'s Location : ' . $data->location . '</li>
                        </ol>

                         <div style="text-align:justify;">
                            This mail is generated as you are listed with edupool.in. This student has applied for Admission in your
                            college. Kindly, contact the student for further admission process. For more details, mail us at
                            <a href="mailto:query.edupool@gmail.com?subject=Regarding admission enquiry from edupool.in">query.edupool@gmail.com</a>
                            or call us at <a href="tel:+917877511631">+91 7877511631</a>.
                            Visit (College URL) to check details of your college.
                        </div>

                         ' . $footer . '

                </div>
            </div>
        </div>
    </div>
    </body>
</html>';

mail($to, $subject, $message3, $headers);

header("Status: 200", "Content-Type:application/json");
echo json_encode($subject);
?>