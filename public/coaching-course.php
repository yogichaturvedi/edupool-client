<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: *");
    header('Content-Type: application/json;charset=utf-8');
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        echo file_get_contents('coaching-course.json');
    }
    else if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $contents = file_get_contents('php://input');
        file_put_contents('coaching-course.json', $contents);
        echo $contents;
    }
?>
