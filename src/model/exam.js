/**
 * Created by Yogesh Chaturvedi on 23-06-2017.
 */
import {observable} from 'mobx';

class ExamModal {
    @observable basicDetails = {
        title: "",
        description: "",
        organizer : "",
        imageUrl: ""
    };

    @observable notice = {
        message1: "",
        message2: ""
    };
    @observable numberOfVacancies = "";

    @observable importantDates = [];

    @observable criteria = {
        educational: "",
        age: ""
    };

    @observable fees = "";

    @observable selectionProcedure = "";

    @observable howToApply = {
        message: "",
        instructions: []
    };

    @observable officialWebsite = {
        message: "",
        link: ""
    };

    @observable tags = [];
}

export default ExamModal;