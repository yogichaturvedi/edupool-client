/**
 * Created by Yogesh Chaturvedi on 23-06-2017.
 */
import {observable} from 'mobx';
import Moment from "moment";

class EventModal {
    @observable basicDetails = {
        title: "",
        description: "",
        imageUrl: "",
        website: "",
        startDate: {
            date: Moment(),
            time: ""
        },
        endDate: {
            date: Moment(),
            time: ""
        },
        fees: "",
        venue: {
            line1: "", line2: "", city: "", state: "", pincode: ""
        },
        organizer: "",
        category: "",
    };
    @observable coverImage = "";
    @observable activities = [];
    @observable socialLinks = [];
    @observable termsAndConditions = [];
    @observable faqs = [];
    @observable similar = {
        category: [],
        city: [],
        state: []
    };
    @observable tags = [];
}

export default EventModal;