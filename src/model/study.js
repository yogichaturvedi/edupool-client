/**
 * Created by Yogesh Chaturvedi on 23-06-2017.
 */
import {observable} from 'mobx';

class Study {
    @observable title = "";
    @observable description = "";
    @observable links = [];
    @observable tags = [];
}

export default Study;