/**
 * Created by Amit on 16-03-2018.
 */
import {observable, action } from 'mobx';
import {admin_details} from "../util/util";

class ArticleNews {
    @observable title = "";
    @observable show = true;
    @observable shortDescription = "";
    @observable thumbnail = "";
    @observable content = "";
    @observable comment = [];
    @observable tags = [];
    @observable category = "Article";
    @observable auditInfo = admin_details;

    @action
    reInitialize() {
        this.type = "";
        this.title = "";
        this.show = true;
        this.shortDescription = "";
        this.thumbnail = "";
        this.content = "";
        this.comment = [];
        this.tags = [];
        this.category = "Article";
        this.auditInfo = admin_details;
    }
}

export default ArticleNews;