/**
 * Created by Yogesh Chaturvedi on 23-06-2017.
 */
import {observable} from 'mobx';

class EventResultModal {
    @observable basicDetails = {
        title: "",
        description: "",
        boardOrUniversity: "",
        imageUrl: "",
        website: "",
        createdOn: ""
    };
    @observable termsAndConditions=[];
    @observable faqs=[];
    @observable tags = [];
    @observable similar = {
        boardOrUniversity : []
    };
}

export default EventResultModal;