/**
 * Created by Amit on 14-06-2018.
 */

import {observable} from 'mobx';
import _ from 'lodash';

class Stream {
    @observable id = "";
    @observable name = "";
    @observable value = "";
    // @observable available = false;
    @observable subStream = {};

    constructor(props) {
        this.id = props.id;
        this.name = props.name;
        this.value = props.value;
        // this.available = props.available ? props.available : this.available;

        let $this = this;
        _.each(Object.keys(props.subStream), subStreamName => {
            $this.subStream[subStreamName] = new SubStream(props.subStream[subStreamName])
        });
    }

    addSubStream = (subStreamName) => {
        this.subStream[subStreamName] = new SubStream({
            "id": Object.keys(this.subStream).length + 1,
            "name": subStreamName,
            "value": subStreamName.replace(/,/g, ''),
        });
    };

    addSubStreams =  (subStreams) => {
        let $this = this;
        _.each(subStreams, ss => {
            $this.subStream[ss.value] = new SubStream(ss)
        });
    };

    removeSubStream = (subStream) => {
        delete this.subStream[subStream];
    };
}

class SubStream {
    @observable id = "";
    @observable name = "";
    @observable value = "";
    @observable subjects = [];
    // @observable available = false;

    constructor(props) {
        this.id = props.id;
        this.name = props.name;
        this.value = props.value;
        // this.available = props.available ? props.available : this.available;

        let subjects = props.subjects ? props.subjects : this.subjects;
        this.subjects = _.map(subjects, subject => {
            return new Subject(subject);
        });
    }

    updateSubjects = (subject) => {
        let matchedSubject = _.find(this.subjects, {name: subject.name});
        if(matchedSubject) {
            matchedSubject.name = subject.name;
            matchedSubject.fee = subject.fee;
            matchedSubject.duration = subject.duration;
        }
        else{
            this.subjects.push(new Subject(subject));
        }
    }
}

class Subject {
    @observable name = "";
    @observable fee = "";
    @observable duration = "";

    constructor(props) {
        this.name = props.name;
        this.fee = props.fee ? props.fee : this.fee;
        this.duration = props.duration ? props.duration : this.duration;
    }
}


class Course {
    @observable id = "";
    @observable name = "";
    @observable value = "";
    @observable stream = {};
    // @observable subStream = {};

    constructor(props) {
        this.id = props.id;
        this.name = props.name;
        this.value = props.value;

        let $this = this;
        if(!_.isEmpty(props.stream)) {
            _.each(Object.keys(props.stream), streamName => {
                $this.stream[streamName] = new Stream(props.stream[streamName]);
            });
        }
    }

    addStream = (stream) => {
        this.stream[stream.value] = new Stream(stream);
    };

    removeStream = (stream) => {
        delete this.stream[stream.value];
    };
}

export default Course;