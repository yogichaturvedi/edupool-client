/**
 * Created by Yogesh Chaturvedi on 23-06-2017.
 */
import {observable} from 'mobx';

class RatingReview {
    @observable name = {value: '', valid: true, error: ""};
    @observable mobileNumber = {value: '', valid: true, error: ""};
    @observable otp = {value: '', valid: true, error: ""};
    @observable rating = {value: '', valid: true, error: ""};
    @observable review = {value: '', valid: true, error: ""};
}

export default RatingReview;