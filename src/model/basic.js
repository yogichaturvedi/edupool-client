/**
 * Created by Yogesh Chaturvedi on 23-06-2017.
 */
import {observable} from 'mobx';
import _ from "lodash";
import {admin_details} from "../util/util";

class BasicModal {
    @observable basicDetails = {
        title: "",
        description: "",
        imageUrl: "",
        website: "",
        mobileNumber: "",
        email: "",
        address: {
            line1 : "", line2 : "", city:"", state : "", pincode:""
        },
        level : ""
    };

    @observable featured = false;

    @observable admissionOpen = false;

    @observable gallery = [];

    @observable aboutUs = {
        message1: "",
        message2: ""
    };

    @observable course = {
        message: "",
        courses: [],
        courseList: []
    };

    @observable hostelAndBuses = {
        hostel: "",
        buses: "",
    };

    @observable contactUs = {
        mobileNumber: "",
        email: "",
        website: "",
        addresses: [],
    };

    @observable rating = {
        value : 0 , max : 5, totalUsers :0
    };
    @observable events = [];

    @observable socialLinks = [];

    @observable tags = [];

    @observable auditInfo = admin_details;

    @observable reviewed = false;

    @observable similar = {
        state : [],
        city : []
    };

    areBasicDetailsEmpty(){
        return _.some(this.basicDetails, (detail)=>{
            return detail === "";
        });
    }

    isGalleryEmpty(){
        if(this.gallery.length === 0){
            return true;
        }
        return _.some(this.gallery, (image)=>{
            return image === "";
        })
    }
    getInvalidGalleryImages(){
        let invalidImageIndexes = [];
        _.forEach(this.gallery, (image, index)=>{
            if(!this.isAddedImageUrlValid(index)){
                invalidImageIndexes.push(index+1);
           }
        });
        return invalidImageIndexes;
    }

    isAddedImageUrlValid(imageIndex){
        let image = this.gallery[imageIndex];
        return image !== "" && (image.endsWith('.jpg') || image.endsWith('.png') || image.endsWith('.svg'));
    }

    isCourseListEmpty(){
        return this.course.courseList.length === 0;
    }

    isAboutUsEmpty(){
        return this.aboutUs.message1 === "" || this.aboutUs.message2 === "";
    }

    areEventsEmpty(){
        return this.events.length === 0;
    }

    areSocialLinksEmpty(){
        return this.socialLinks.length === 0;
    }

    isHostelEmpty(){
        return this.hostelAndBuses.hostel === "";
    }

    areBusesEmpty(){
        return this.hostelAndBuses.buses === ""
    }
}

export default BasicModal;