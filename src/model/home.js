import {observable} from 'mobx';

class HomeInfoModel {
    @observable aboutUs = "";
    @observable infoEmail = "";
    @observable queryEmail = "";
    @observable contactNumbers = [];
    @observable socialLinks = [];
    @observable address = "";
    @observable images = [];
}

export default HomeInfoModel;