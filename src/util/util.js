/**
 * Created by Amit on 17-09-2017.
 */

import {browserHistory} from "react-router";

let user = JSON.parse(localStorage.getItem('user'));
export const admin_details = {
    name: user ? user[0].username : 'Edupool Admin',
    email: user ? user[0].email : 'edupoolindia@gmail.com',
    institute: '-',
    mobile: user ? user[0].mobileNumber : '9876543210'
};

export const MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

export const schoolLevels = [
    {
        "key": 1,
        "text": "Play School",
        "value": "Play School"
    },
    {
        "key": 2,
        "text": "Pre-Primary",
        "value": "Pre-Primary"
    },
    {
        "key": 3,
        "text": "Primary",
        "value": "Primary"
    },
    {
        "key": 4,
        "text": "Secondary",
        "value": "Secondary"
    },
    {
        "key": 5,
        "text": "Sr. Secondary",
        "value": "Sr. Secondary"
    }
];

export const socialSitesInfo = {
    facebook: {link: "https://www.facebook.com/edupoolofficial/"},
    twitter: {link: "https://twitter.com/edupoolofficial"},
    google: {link: "https://plus.google.com/u/1/113448798628110106140"},
    linkedin: {link: "https://www.linkedin.com/in/edupoolofficial"},
    instagram: {link: "https://www.instagram.com/edupoolofficial"},
    youtube: {link: "https://www.youtube.com/channel/UCFwr0kCl5fnkAZII4t6LdWQ"}
};


export function redirectToPage(page, queryParams) {
    if (queryParams) {
        browserHistory.push(`${page}?${queryParams}`);
    }
    else {
        browserHistory.push(page)
    }
}

export function toTitleCase(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}


export function getFormattedTime(date) {
    return new Date(date).toLocaleDateString();
}

export function getParsedTime(time) {
    if (!time) {
        return "00:00 AM";
    }
    let timeSplit = time.split(':'),
        hours,
        minutes,
        meridian;
    hours = timeSplit[0];
    minutes = timeSplit[1];
    if (hours > 12) {
        meridian = 'PM';
        hours -= 12;
    } else if (hours < 12) {
        meridian = 'AM';
        if (hours === 0) {
            hours = 12;
        }
    } else {
        meridian = 'PM';
    }
    return (hours + ':' + minutes + ' ' + meridian);

}

export function getParsedDate(date) {
    let dateObject = new Date(date);
    let month = MONTHS[dateObject.getMonth()];
    return `${dateObject.getDate()} ${month}, ${dateObject.getFullYear()}`;
}

export function isEmailValid(email) {
    /*eslint no-useless-escape: "off"*/
    let validEmailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return validEmailRegex.test(email);
}

export function copyToClipboard(context, textToCopy) {
    let wrapper = document.getElementById(context);
    let input = document.createElement("input");
    wrapper.appendChild(input);
    input.value = textToCopy;
    // input.style.visibility =  'hidden';
    input.style.position = 'absolute';
    input.style.left = '-99999999px';
    input.focus();
    document.execCommand("SelectAll");
    document.execCommand("Copy");
    wrapper.removeChild(input);
}
