/**
 * Created by Amit on 27-06-2017.
 */
import _ from "lodash";
import StateCities from "../static-data/state-city.json";

export let statesAndCities = [
    {
        "key": "state-1",
        "text": "Andaman and Nicobar",
        "value": "Andaman and Nicobar",
        "cities": [
            {
                "key": "city-1",
                "name": "Nicobar",
                "state": "Andaman and Nicobar"
            },
            {
                "key": "city-2",
                "name": "North & Middle Andaman",
                "state": "Andaman and Nicobar"
            },
            {
                "key": "city-3",
                "name": "South Andaman",
                "state": "Andaman and Nicobar"
            }
        ]
    },
    {
        "key": "state-2",
        "text": "Andhra Pradesh",
        "value": "Andhra Pradesh",
        "cities": [
            {
                "key": "city-4",
                "name": "Anantpur",
                "state": "Andhra Pradesh"
            },
            {
                "key": "city-5",
                "name": "Chittoor",
                "state": "Andhra Pradesh"
            },
            {
                "key": "city-6",
                "name": "East Godavari",
                "state": "Andhra Pradesh"
            },
            {
                "key": "city-7",
                "name": "Guntur",
                "state": "Andhra Pradesh"
            },
            {
                "key": "city-8",
                "name": "Kadapa",
                "state": "Andhra Pradesh"
            },
            {
                "key": "city-9",
                "name": "Krishna",
                "state": "Andhra Pradesh"
            },
            {
                "key": "city-10",
                "name": "Kurnool",
                "state": "Andhra Pradesh"
            },
            {
                "key": "city-11",
                "name": "Prakasam",
                "state": "Andhra Pradesh"
            },
            {
                "key": "city-12",
                "name": "Sri Potti Sriramulu Nellore",
                "state": "Andhra Pradesh"
            },
            {
                "key": "city-13",
                "name": "Visakhapatnam",
                "state": "Andhra Pradesh"
            },
            {
                "key": "city-14",
                "name": "Vizianagaram",
                "state": "Andhra Pradesh"
            },
            {
                "key": "city-15",
                "name": "West Godavari",
                "state": "Andhra Pradesh"
            },
            {
                "key": "city-16",
                "name": "Srikakulam",
                "state": "Andhra Pradesh"
            }
        ]
    },
    {
        "key": "state-3",
        "text": "Arunachal Pradesh",
        "value": "Arunachal Pradesh",
        "cities": [
            {
                "key": "city-17",
                "name": "Anjaw",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-18",
                "name": "Changlang",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-19",
                "name": "East Kameng",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-20",
                "name": "East Siang",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-21",
                "name": "Kamle",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-22",
                "name": "Kra Daadi",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-23",
                "name": "Kurung Kumey",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-24",
                "name": "Lohit",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-25",
                "name": "Longding",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-26",
                "name": "Lower Dibang Valley",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-27",
                "name": "Lower Siang",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-28",
                "name": "Lower Subansari",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-29",
                "name": "Namsai",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-30",
                "name": "Papum Pare",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-31",
                "name": "Siang",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-32",
                "name": "Tawang",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-33",
                "name": "Tirap",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-34",
                "name": "Upper Dibang Valley",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-35",
                "name": "Upper Siang",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-36",
                "name": "Upper Subansiri",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-37",
                "name": "West Kameng",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-38",
                "name": "West Siang",
                "state": "Arunachal Pradesh"
            },
            {
                "key": "city-39",
                "name": "Bomdila",
                "state": "Arunachal Pradesh"
            }
        ]
    },
    {
        "key": "state-4",
        "text": "Assam",
        "value": "Assam",
        "cities": [
            {
                "key": "city-40",
                "name": "Baksa",
                "state": "Assam"
            },
            {
                "key": "city-41",
                "name": "Barpeta",
                "state": "Assam"
            },
            {
                "key": "city-42",
                "name": "Bishwanath",
                "state": "Assam"
            },
            {
                "key": "city-43",
                "name": "Bongaigaon",
                "state": "Assam"
            },
            {
                "key": "city-44",
                "name": "Cachar",
                "state": "Assam"
            },
            {
                "key": "city-45",
                "name": "Charaideo",
                "state": "Assam"
            },
            {
                "key": "city-46",
                "name": "Chirang",
                "state": "Assam"
            },
            {
                "key": "city-47",
                "name": "Darrang",
                "state": "Assam"
            },
            {
                "key": "city-48",
                "name": "Dhemaji",
                "state": "Assam"
            },
            {
                "key": "city-49",
                "name": "Dhubri",
                "state": "Assam"
            },
            {
                "key": "city-50",
                "name": "Dibrugarh",
                "state": "Assam"
            },
            {
                "key": "city-51",
                "name": "Dima Hasao",
                "state": "Assam"
            },
            {
                "key": "city-52",
                "name": "Goalpara",
                "state": "Assam"
            },
            {
                "key": "city-53",
                "name": "Golaghat",
                "state": "Assam"
            },
            {
                "key": "city-54",
                "name": "Hailakandi",
                "state": "Assam"
            },
            {
                "key": "city-55",
                "name": "Hojai",
                "state": "Assam"
            },
            {
                "key": "city-56",
                "name": "Jorhat",
                "state": "Assam"
            },
            {
                "key": "city-57",
                "name": "Kamrup",
                "state": "Assam"
            },
            {
                "key": "city-58",
                "name": "Kamrup Metropolitan",
                "state": "Assam"
            },
            {
                "key": "city-59",
                "name": "Karbi Anglong",
                "state": "Assam"
            },
            {
                "key": "city-60",
                "name": "Karimganj",
                "state": "Assam"
            },
            {
                "key": "city-61",
                "name": "Kokrajhar",
                "state": "Assam"
            },
            {
                "key": "city-62",
                "name": "Lakhimpur",
                "state": "Assam"
            },
            {
                "key": "city-63",
                "name": "Majuli",
                "state": "Assam"
            },
            {
                "key": "city-64",
                "name": "Morigaon",
                "state": "Assam"
            },
            {
                "key": "city-65",
                "name": "Nagaon",
                "state": "Assam"
            },
            {
                "key": "city-66",
                "name": "Nalbari",
                "state": "Assam"
            },
            {
                "key": "city-67",
                "name": "Sivasagar",
                "state": "Assam"
            },
            {
                "key": "city-68",
                "name": "South Salmara- Mankachar",
                "state": "Assam"
            },
            {
                "key": "city-69",
                "name": "Sonitpur",
                "state": "Assam"
            },
            {
                "key": "city-70",
                "name": "Tinsukia",
                "state": "Assam"
            },
            {
                "key": "city-71",
                "name": "Udalguri",
                "state": "Assam"
            },
            {
                "key": "city-72",
                "name": "West Karbi Anglong",
                "state": "Assam"
            }
        ]
    },
    {
        "key": "state-5",
        "text": "Bihar",
        "value": "Bihar",
        "cities": [
            {
                "key": "city-73",
                "name": "Araria",
                "state": "Bihar"
            },
            {
                "key": "city-74",
                "name": "Arwal",
                "state": "Bihar"
            },
            {
                "key": "city-75",
                "name": "Aurangabad",
                "state": "Bihar"
            },
            {
                "key": "city-76",
                "name": "Banka",
                "state": "Bihar"
            },
            {
                "key": "city-77",
                "name": "Begusarai",
                "state": "Bihar"
            },
            {
                "key": "city-78",
                "name": "Bhagalpur",
                "state": "Bihar"
            },
            {
                "key": "city-79",
                "name": "Bhojpur",
                "state": "Bihar"
            },
            {
                "key": "city-80",
                "name": "Buxar",
                "state": "Bihar"
            },
            {
                "key": "city-81",
                "name": "Darbhanga",
                "state": "Bihar"
            },
            {
                "key": "city-82",
                "name": "East Champaran",
                "state": "Bihar"
            },
            {
                "key": "city-83",
                "name": "Gaya",
                "state": "Bihar"
            },
            {
                "key": "city-84",
                "name": "Gopalganj",
                "state": "Bihar"
            },
            {
                "key": "city-85",
                "name": "Jamui",
                "state": "Bihar"
            },
            {
                "key": "city-86",
                "name": "Jehanabad",
                "state": "Bihar"
            },
            {
                "key": "city-87",
                "name": "Kaimur",
                "state": "Bihar"
            },
            {
                "key": "city-88",
                "name": "Katihar",
                "state": "Bihar"
            },
            {
                "key": "city-89",
                "name": "Khagaria",
                "state": "Bihar"
            },
            {
                "key": "city-90",
                "name": "Kishanganj",
                "state": "Bihar"
            },
            {
                "key": "city-91",
                "name": "Lakhisarai",
                "state": "Bihar"
            },
            {
                "key": "city-92",
                "name": "Madhepura",
                "state": "Bihar"
            },
            {
                "key": "city-93",
                "name": "Madhubani",
                "state": "Bihar"
            },
            {
                "key": "city-94",
                "name": "Munger",
                "state": "Bihar"
            },
            {
                "key": "city-95",
                "name": "Muzaffarpur",
                "state": "Bihar"
            },
            {
                "key": "city-96",
                "name": "Nalanda",
                "state": "Bihar"
            },
            {
                "key": "city-97",
                "name": "Nawada",
                "state": "Bihar"
            },
            {
                "key": "city-98",
                "name": "Patna",
                "state": "Bihar"
            },
            {
                "key": "city-99",
                "name": "Purnia",
                "state": "Bihar"
            },
            {
                "key": "city-100",
                "name": "Rohtas",
                "state": "Bihar"
            },
            {
                "key": "city-101",
                "name": "Saharsa",
                "state": "Bihar"
            },
            {
                "key": "city-102",
                "name": "Samastipur",
                "state": "Bihar"
            },
            {
                "key": "city-103",
                "name": "Saran",
                "state": "Bihar"
            },
            {
                "key": "city-104",
                "name": "Sheikhpura",
                "state": "Bihar"
            },
            {
                "key": "city-105",
                "name": "Sheohar",
                "state": "Bihar"
            },
            {
                "key": "city-106",
                "name": "Sitamarhi",
                "state": "Bihar"
            },
            {
                "key": "city-107",
                "name": "Siwan",
                "state": "Bihar"
            },
            {
                "key": "city-108",
                "name": "Supaul",
                "state": "Bihar"
            },
            {
                "key": "city-109",
                "name": "Vaishali",
                "state": "Bihar"
            },
            {
                "key": "city-110",
                "name": "West Champaran",
                "state": "Bihar"
            }
        ]
    },
    {
        "key": "state-6",
        "text": "Chandigarh",
        "value": "Chandigarh",
        "cities": [
            {
                "key": "city-111",
                "name": "Chandigarh",
                "state": "Chandigarh"
            }
        ]
    },
    {
        "key": "state-7",
        "text": "Chhattisgarh",
        "value": "Chhattisgarh",
        "cities": [
            {
                "key": "city-112",
                "name": "Balod",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-113",
                "name": "Baloda Bazar",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-114",
                "name": "Balrampur",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-115",
                "name": "Bastar",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-116",
                "name": "Bemetara",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-117",
                "name": "Bijapur",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-118",
                "name": "Bilaspur",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-119",
                "name": "Dantewada",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-120",
                "name": "Dhamtari",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-121",
                "name": "Durg",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-122",
                "name": "Gariaband",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-123",
                "name": "Janjgir-Champa",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-124",
                "name": "Jashpur",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-125",
                "name": "Kabirdham",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-126",
                "name": "Kanker",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-127",
                "name": "Kondagaon",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-128",
                "name": "Korba",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-129",
                "name": "Koriya",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-130",
                "name": "Mahasamund",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-131",
                "name": "Mungeli",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-132",
                "name": "Narayanpur",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-133",
                "name": "Raigarh",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-134",
                "name": "Raipur",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-135",
                "name": "Rajnandgaon",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-136",
                "name": "Sukma",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-137",
                "name": "Surajpur",
                "state": "Chhattisgarh"
            },
            {
                "key": "city-138",
                "name": "Surguja",
                "state": "Chhattisgarh"
            }
        ]
    },
    {
        "key": "state-8",
        "text": "Dadra & Nagar Haveli",
        "value": "Dadra & Nagar Haveli",
        "cities": [
            {
                "key": "city-139",
                "name": "Dadra & Nagar Haveli",
                "state": "Dadra & Nagar Haveli"
            }
        ]
    },
    {
        "key": "state-9",
        "text": "Daman & Diu",
        "value": "Daman & Diu",
        "cities": [
            {
                "key": "city-140",
                "name": "Daman",
                "state": "Daman & Diu"
            },
            {
                "key": "city-141",
                "name": "Diu",
                "state": "Daman & Diu"
            }
        ]
    },
    {
        "key": "state-10",
        "text": "Delhi",
        "value": "Delhi",
        "cities": [
            {
                "key": "city-142",
                "name": "Central Delhi",
                "state": "Delhi"
            },
            {
                "key": "city-143",
                "name": "East Delhi",
                "state": "Delhi"
            },
            {
                "key": "city-144",
                "name": "New Delhi",
                "state": "Delhi"
            },
            {
                "key": "city-145",
                "name": "North Delhi",
                "state": "Delhi"
            },
            {
                "key": "city-146",
                "name": "North East Delhi",
                "state": "Delhi"
            },
            {
                "key": "city-147",
                "name": "North West Delhi",
                "state": "Delhi"
            },
            {
                "key": "city-148",
                "name": "Shahdara",
                "state": "Delhi"
            },
            {
                "key": "city-149",
                "name": "South Delhi",
                "state": "Delhi"
            },
            {
                "key": "city-150",
                "name": "South East Delhi",
                "state": "Delhi"
            },
            {
                "key": "city-151",
                "name": "South West Delhi",
                "state": "Delhi"
            },
            {
                "key": "city-152",
                "name": "West Delhi",
                "state": "Delhi"
            }
        ]
    },
    {
        "key": "state-11",
        "text": "Goa",
        "value": "Goa",
        "cities": [
            {
                "key": "city-153",
                "name": "North Goa",
                "state": "Goa"
            },
            {
                "key": "city-154",
                "name": "South Goa",
                "state": "Goa"
            }
        ]
    },
    {
        "key": "state-12",
        "text": "Gujarat",
        "value": "Gujarat",
        "cities": [
            {
                "key": "city-155",
                "name": "Ahmedabad",
                "state": "Gujarat"
            },
            {
                "key": "city-156",
                "name": "Amreli",
                "state": "Gujarat"
            },
            {
                "key": "city-157",
                "name": "Anand",
                "state": "Gujarat"
            },
            {
                "key": "city-158",
                "name": "Aravalli",
                "state": "Gujarat"
            },
            {
                "key": "city-159",
                "name": "Banaskantha",
                "state": "Gujarat"
            },
            {
                "key": "city-160",
                "name": "Bharuch",
                "state": "Gujarat"
            },
            {
                "key": "city-161",
                "name": "Bhavnagar",
                "state": "Gujarat"
            },
            {
                "key": "city-162",
                "name": "Botad",
                "state": "Gujarat"
            },
            {
                "key": "city-163",
                "name": "Chhota Udaipur",
                "state": "Gujarat"
            },
            {
                "key": "city-164",
                "name": "Dahod",
                "state": "Gujarat"
            },
            {
                "key": "city-165",
                "name": "Dang",
                "state": "Gujarat"
            },
            {
                "key": "city-166",
                "name": "Devbhoomi Dwarka",
                "state": "Gujarat"
            },
            {
                "key": "city-167",
                "name": "Gandhinagar",
                "state": "Gujarat"
            },
            {
                "key": "city-168",
                "name": "Gir Somnath",
                "state": "Gujarat"
            },
            {
                "key": "city-169",
                "name": "Jamnagar",
                "state": "Gujarat"
            },
            {
                "key": "city-170",
                "name": "Junagadh",
                "state": "Gujarat"
            },
            {
                "key": "city-171",
                "name": "Kheda",
                "state": "Gujarat"
            },
            {
                "key": "city-172",
                "name": "Kutch",
                "state": "Gujarat"
            },
            {
                "key": "city-173",
                "name": "Mahisagar",
                "state": "Gujarat"
            },
            {
                "key": "city-174",
                "name": "Mehsana",
                "state": "Gujarat"
            },
            {
                "key": "city-175",
                "name": "Morbi",
                "state": "Gujarat"
            },
            {
                "key": "city-176",
                "name": "Narmada",
                "state": "Gujarat"
            },
            {
                "key": "city-177",
                "name": "Navsari",
                "state": "Gujarat"
            },
            {
                "key": "city-178",
                "name": "Panchmahal",
                "state": "Gujarat"
            },
            {
                "key": "city-179",
                "name": "Patan",
                "state": "Gujarat"
            },
            {
                "key": "city-180",
                "name": "Porbandar",
                "state": "Gujarat"
            },
            {
                "key": "city-181",
                "name": "Rajkot",
                "state": "Gujarat"
            },
            {
                "key": "city-182",
                "name": "Sabarkantha",
                "state": "Gujarat"
            },
            {
                "key": "city-183",
                "name": "Surat",
                "state": "Gujarat"
            },
            {
                "key": "city-184",
                "name": "Surendranagar",
                "state": "Gujarat"
            },
            {
                "key": "city-185",
                "name": "Tapi",
                "state": "Gujarat"
            },
            {
                "key": "city-186",
                "name": "Vadodara",
                "state": "Gujarat"
            },
            {
                "key": "city-187",
                "name": "Valsad",
                "state": "Gujarat"
            }
        ]
    },
    {
        "key": "state-13",
        "text": "Haryana",
        "value": "Haryana",
        "cities": [
            {
                "key": "city-188",
                "name": "Ambala",
                "state": "Haryana"
            },
            {
                "key": "city-189",
                "name": "Bhiwani",
                "state": "Haryana"
            },
            {
                "key": "city-190",
                "name": "Charkhi Dadri",
                "state": "Haryana"
            },
            {
                "key": "city-191",
                "name": "Faridabad",
                "state": "Haryana"
            },
            {
                "key": "city-192",
                "name": "Fatehabad",
                "state": "Haryana"
            },
            {
                "key": "city-193",
                "name": "Gurugram",
                "state": "Haryana"
            },
            {
                "key": "city-194",
                "name": "Hissar",
                "state": "Haryana"
            },
            {
                "key": "city-195",
                "name": "Jhajjar",
                "state": "Haryana"
            },
            {
                "key": "city-196",
                "name": "Jind",
                "state": "Haryana"
            },
            {
                "key": "city-197",
                "name": "Kaithal",
                "state": "Haryana"
            },
            {
                "key": "city-198",
                "name": "Karnal",
                "state": "Haryana"
            },
            {
                "key": "city-199",
                "name": "Kurukshetra \t",
                "state": "Haryana"
            },
            {
                "key": "city-200",
                "name": "Mahendragarh",
                "state": "Haryana"
            },
            {
                "key": "city-201",
                "name": "Nuh",
                "state": "Haryana"
            },
            {
                "key": "city-202",
                "name": "Palwal",
                "state": "Haryana"
            },
            {
                "key": "city-203",
                "name": "Panchkula",
                "state": "Haryana"
            },
            {
                "key": "city-204",
                "name": "Panipat",
                "state": "Haryana"
            },
            {
                "key": "city-205",
                "name": "Rewai",
                "state": "Haryana"
            },
            {
                "key": "city-206",
                "name": "Rohtak",
                "state": "Haryana"
            },
            {
                "key": "city-207",
                "name": "Sirsa",
                "state": "Haryana"
            },
            {
                "key": "city-208",
                "name": "Sonipat",
                "state": "Haryana"
            },
            {
                "key": "city-209",
                "name": "Yamuna Nagar",
                "state": "Haryana"
            }
        ]
    },
    {
        "key": "state-14",
        "text": "Himachal Pradesh",
        "value": "Himachal Pradesh",
        "cities": [
            {
                "key": "city-210",
                "name": "Bilaspur",
                "state": "Himachal Pradesh"
            },
            {
                "key": "city-211",
                "name": "Chamba",
                "state": "Himachal Pradesh"
            },
            {
                "key": "city-212",
                "name": "Hamirpur",
                "state": "Himachal Pradesh"
            },
            {
                "key": "city-213",
                "name": "Kangra",
                "state": "Himachal Pradesh"
            },
            {
                "key": "city-214",
                "name": "Kinnaur",
                "state": "Himachal Pradesh"
            },
            {
                "key": "city-215",
                "name": "Kullu",
                "state": "Himachal Pradesh"
            },
            {
                "key": "city-216",
                "name": "Lahaul & Spiti",
                "state": "Himachal Pradesh"
            },
            {
                "key": "city-217",
                "name": "Mandi",
                "state": "Himachal Pradesh"
            },
            {
                "key": "city-218",
                "name": "Shimla",
                "state": "Himachal Pradesh"
            },
            {
                "key": "city-219",
                "name": "Sirmaur",
                "state": "Himachal Pradesh"
            },
            {
                "key": "city-220",
                "name": "Solan",
                "state": "Himachal Pradesh"
            },
            {
                "key": "city-221",
                "name": "Una",
                "state": "Himachal Pradesh"
            }
        ]
    },
    {
        "key": "state-15",
        "text": "Jammu & Kashmir",
        "value": "Jammu & Kashmir",
        "cities": [
            {
                "key": "city-222",
                "name": "Anantnag",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-223",
                "name": "Bandipora",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-224",
                "name": "Baramulla",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-225",
                "name": "Badgam",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-226",
                "name": "Doda",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-227",
                "name": "Ganderbal",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-228",
                "name": "Jammu",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-229",
                "name": "Kargil",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-230",
                "name": "Kathua",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-231",
                "name": "Kishtwar",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-232",
                "name": "Kulgam",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-233",
                "name": "Kupwara",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-234",
                "name": "Leh",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-235",
                "name": "Poonch",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-236",
                "name": "Pulwama",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-237",
                "name": "Rajouri",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-238",
                "name": "Ramban",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-239",
                "name": "Reasi",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-240",
                "name": "Samba",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-241",
                "name": "Shopian",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-242",
                "name": "Srinagar",
                "state": "Jammu & Kashmir"
            },
            {
                "key": "city-243",
                "name": "Udhampur",
                "state": "Jammu & Kashmir"
            }
        ]
    },
    {
        "key": "state-16",
        "text": "Jharkhand",
        "value": "Jharkhand",
        "cities": [
            {
                "key": "city-244",
                "name": "Bokaro",
                "state": "Jharkhand"
            },
            {
                "key": "city-245",
                "name": "Chatra",
                "state": "Jharkhand"
            },
            {
                "key": "city-246",
                "name": "Deoghar",
                "state": "Jharkhand"
            },
            {
                "key": "city-247",
                "name": "Dhandad",
                "state": "Jharkhand"
            },
            {
                "key": "city-248",
                "name": "Dumka",
                "state": "Jharkhand"
            },
            {
                "key": "city-249",
                "name": "East Singhbhum",
                "state": "Jharkhand"
            },
            {
                "key": "city-250",
                "name": "Garhwa",
                "state": "Jharkhand"
            },
            {
                "key": "city-251",
                "name": "Giridih",
                "state": "Jharkhand"
            },
            {
                "key": "city-252",
                "name": "Godda",
                "state": "Jharkhand"
            },
            {
                "key": "city-253",
                "name": "Gumla",
                "state": "Jharkhand"
            },
            {
                "key": "city-254",
                "name": "Hazaribag",
                "state": "Jharkhand"
            },
            {
                "key": "city-255",
                "name": "Jamtara",
                "state": "Jharkhand"
            },
            {
                "key": "city-256",
                "name": "Khunti",
                "state": "Jharkhand"
            },
            {
                "key": "city-257",
                "name": "Koderma",
                "state": "Jharkhand"
            },
            {
                "key": "city-258",
                "name": "Latehar",
                "state": "Jharkhand"
            },
            {
                "key": "city-259",
                "name": "Lohardaga",
                "state": "Jharkhand"
            },
            {
                "key": "city-260",
                "name": "Pakur",
                "state": "Jharkhand"
            },
            {
                "key": "city-261",
                "name": "Palamu",
                "state": "Jharkhand"
            },
            {
                "key": "city-262",
                "name": "Ramgarh",
                "state": "Jharkhand"
            },
            {
                "key": "city-263",
                "name": "Ranchi",
                "state": "Jharkhand"
            },
            {
                "key": "city-264",
                "name": "Sahibganj",
                "state": "Jharkhand"
            },
            {
                "key": "city-265",
                "name": "Seraikela Kharsawan",
                "state": "Jharkhand"
            },
            {
                "key": "city-266",
                "name": "Simdega",
                "state": "Jharkhand"
            },
            {
                "key": "city-267",
                "name": "West Singhbhum",
                "state": "Jharkhand"
            }
        ]
    },
    {
        "key": "state-17",
        "text": "Karnataka",
        "value": "Karnataka",
        "cities": [
            {
                "key": "city-268",
                "name": "Bagalkot",
                "state": "Karnataka"
            },
            {
                "key": "city-269",
                "name": "Ballari",
                "state": "Karnataka"
            },
            {
                "key": "city-270",
                "name": "Belagavi",
                "state": "Karnataka"
            },
            {
                "key": "city-271",
                "name": "Bengaluru",
                "state": "Karnataka"
            },
            {
                "key": "city-272",
                "name": "Bidar",
                "state": "Karnataka"
            },
            {
                "key": "city-273",
                "name": "Chamarajnagar",
                "state": "Karnataka"
            },
            {
                "key": "city-274",
                "name": "Chikkaballapur",
                "state": "Karnataka"
            },
            {
                "key": "city-275",
                "name": "Chikkamagaluru",
                "state": "Karnataka"
            },
            {
                "key": "city-276",
                "name": "Chitradurga",
                "state": "Karnataka"
            },
            {
                "key": "city-277",
                "name": "Dakshina Kannada",
                "state": "Karnataka"
            },
            {
                "key": "city-278",
                "name": "Davanagere",
                "state": "Karnataka"
            },
            {
                "key": "city-279",
                "name": "Dharwad",
                "state": "Karnataka"
            },
            {
                "key": "city-280",
                "name": "Gadag",
                "state": "Karnataka"
            },
            {
                "key": "city-281",
                "name": "Hassan",
                "state": "Karnataka"
            },
            {
                "key": "city-282",
                "name": "Haveri",
                "state": "Karnataka"
            },
            {
                "key": "city-283",
                "name": "Kalaburagi",
                "state": "Karnataka"
            },
            {
                "key": "city-284",
                "name": "Koduga",
                "state": "Karnataka"
            },
            {
                "key": "city-285",
                "name": "Kolar",
                "state": "Karnataka"
            },
            {
                "key": "city-286",
                "name": "Koppal",
                "state": "Karnataka"
            },
            {
                "key": "city-287",
                "name": "Mandya",
                "state": "Karnataka"
            },
            {
                "key": "city-288",
                "name": "Mysuru",
                "state": "Karnataka"
            },
            {
                "key": "city-289",
                "name": "Raichuru",
                "state": "Karnataka"
            },
            {
                "key": "city-290",
                "name": "Ramanagara",
                "state": "Karnataka"
            },
            {
                "key": "city-291",
                "name": "Shivamogga",
                "state": "Karnataka"
            },
            {
                "key": "city-292",
                "name": "Tumakuru",
                "state": "Karnataka"
            },
            {
                "key": "city-293",
                "name": "Udupi",
                "state": "Karnataka"
            },
            {
                "key": "city-294",
                "name": "Uttara Kannada",
                "state": "Karnataka"
            },
            {
                "key": "city-295",
                "name": "Vijyapura",
                "state": "Karnataka"
            },
            {
                "key": "city-296",
                "name": "Yadgir",
                "state": "Karnataka"
            }
        ]
    },
    {
        "key": "state-18",
        "text": "Kerala",
        "value": "Kerala",
        "cities": [
            {
                "key": "city-297",
                "name": "Alappuzha",
                "state": "Kerala"
            },
            {
                "key": "city-298",
                "name": "Ernakulam",
                "state": "Kerala"
            },
            {
                "key": "city-299",
                "name": "Idukki",
                "state": "Kerala"
            },
            {
                "key": "city-300",
                "name": "Kannur",
                "state": "Kerala"
            },
            {
                "key": "city-301",
                "name": "Kasaragod",
                "state": "Kerala"
            },
            {
                "key": "city-302",
                "name": "Kollam",
                "state": "Kerala"
            },
            {
                "key": "city-303",
                "name": "Kottayam",
                "state": "Kerala"
            },
            {
                "key": "city-304",
                "name": "Kozhikode",
                "state": "Kerala"
            },
            {
                "key": "city-305",
                "name": "Malappuram",
                "state": "Kerala"
            },
            {
                "key": "city-306",
                "name": "Palakkad",
                "state": "Kerala"
            },
            {
                "key": "city-307",
                "name": "Pathanamthitta",
                "state": "Kerala"
            },
            {
                "key": "city-308",
                "name": "Thrissur",
                "state": "Kerala"
            },
            {
                "key": "city-309",
                "name": "Thiruvananthapuram",
                "state": "Kerala"
            },
            {
                "key": "city-310",
                "name": "Wayand",
                "state": "Kerala"
            }
        ]
    },
    {
        "key": "state-19",
        "text": "Lakshadweep",
        "value": "Lakshadweep",
        "cities": [
            {
                "key": "city-311",
                "name": "Lakshadweep",
                "state": "Lakshadweep"
            }
        ]
    },
    {
        "key": "state-20",
        "text": "Madhya Pradesh",
        "value": "Madhya Pradesh",
        "cities": [
            {
                "key": "city-312",
                "name": "Agar Malwa",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-313",
                "name": "Alirajpur",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-314",
                "name": "Anuppur",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-315",
                "name": "Ashok Nagar",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-316",
                "name": "Balaghat",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-317",
                "name": "Barwani",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-318",
                "name": "Betul",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-319",
                "name": "Bhind",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-320",
                "name": "Bhopal",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-321",
                "name": "Burhanpur",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-322",
                "name": "Chhatarpur",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-323",
                "name": "Chhindwara",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-324",
                "name": "Damoh",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-325",
                "name": "Datia",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-326",
                "name": "Dewas",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-327",
                "name": "Dhar",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-328",
                "name": "Dindori",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-329",
                "name": "Guna",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-330",
                "name": "Gwalior",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-331",
                "name": "Harda",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-332",
                "name": "Hoshangabad",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-333",
                "name": "Indore",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-334",
                "name": "Jabalpur",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-335",
                "name": "Jhabua",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-336",
                "name": "Katni",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-337",
                "name": "Khandwa",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-338",
                "name": "Khargone",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-339",
                "name": "Mandla",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-340",
                "name": "Mandsaur",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-341",
                "name": "Morena",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-342",
                "name": "Narsinghpur",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-343",
                "name": "Neemuch",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-344",
                "name": "Panna",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-345",
                "name": "Raisen",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-346",
                "name": "Rajgarh",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-347",
                "name": "Ratlam",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-348",
                "name": "Rewa",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-349",
                "name": "Sagar",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-350",
                "name": "Satna",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-351",
                "name": "Sehore",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-352",
                "name": "Seoni",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-353",
                "name": "Shahdol",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-354",
                "name": "Shajapur",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-355",
                "name": "Sheopur",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-356",
                "name": "Shivpuri",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-357",
                "name": "Sidhi",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-358",
                "name": "Singrauli",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-359",
                "name": "Tikamgarh",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-360",
                "name": "Ujjain",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-361",
                "name": "Umaria",
                "state": "Madhya Pradesh"
            },
            {
                "key": "city-362",
                "name": "Vidisha",
                "state": "Madhya Pradesh"
            }
        ]
    },
    {
        "key": "state-21",
        "text": "Maharashtra",
        "value": "Maharashtra",
        "cities": [
            {
                "key": "city-363",
                "name": "Ahmednagar",
                "state": "Maharashtra"
            },
            {
                "key": "city-364",
                "name": "Akola",
                "state": "Maharashtra"
            },
            {
                "key": "city-365",
                "name": "Amravati",
                "state": "Maharashtra"
            },
            {
                "key": "city-366",
                "name": "Aurangabad",
                "state": "Maharashtra"
            },
            {
                "key": "city-367",
                "name": "Beed",
                "state": "Maharashtra"
            },
            {
                "key": "city-368",
                "name": "Bhandara",
                "state": "Maharashtra"
            },
            {
                "key": "city-369",
                "name": "Buldhana",
                "state": "Maharashtra"
            },
            {
                "key": "city-370",
                "name": "Chandrapur",
                "state": "Maharashtra"
            },
            {
                "key": "city-371",
                "name": "Dhule",
                "state": "Maharashtra"
            },
            {
                "key": "city-372",
                "name": "Gadchiroli",
                "state": "Maharashtra"
            },
            {
                "key": "city-373",
                "name": "Gondia",
                "state": "Maharashtra"
            },
            {
                "key": "city-374",
                "name": "Hingoli",
                "state": "Maharashtra"
            },
            {
                "key": "city-375",
                "name": "Jalgaon",
                "state": "Maharashtra"
            },
            {
                "key": "city-376",
                "name": "Jalna",
                "state": "Maharashtra"
            },
            {
                "key": "city-377",
                "name": "Kolhapur",
                "state": "Maharashtra"
            },
            {
                "key": "city-378",
                "name": "Latur",
                "state": "Maharashtra"
            },
            {
                "key": "city-379",
                "name": "Mumbai",
                "state": "Maharashtra"
            },
            {
                "key": "city-380",
                "name": "Nanded",
                "state": "Maharashtra"
            },
            {
                "key": "city-381",
                "name": "Nandurbar",
                "state": "Maharashtra"
            },
            {
                "key": "city-382",
                "name": "Nagpur",
                "state": "Maharashtra"
            },
            {
                "key": "city-383",
                "name": "Nashik",
                "state": "Maharashtra"
            },
            {
                "key": "city-384",
                "name": "Osmanabad",
                "state": "Maharashtra"
            },
            {
                "key": "city-385",
                "name": "Palghar",
                "state": "Maharashtra"
            },
            {
                "key": "city-386",
                "name": "Parbhani",
                "state": "Maharashtra"
            },
            {
                "key": "city-387",
                "name": "Pune",
                "state": "Maharashtra"
            },
            {
                "key": "city-388",
                "name": "Raigad",
                "state": "Maharashtra"
            },
            {
                "key": "city-389",
                "name": "Ratnagiri",
                "state": "Maharashtra"
            },
            {
                "key": "city-390",
                "name": "Sangli",
                "state": "Maharashtra"
            },
            {
                "key": "city-391",
                "name": "Satara",
                "state": "Maharashtra"
            },
            {
                "key": "city-392",
                "name": "Sindhudurg",
                "state": "Maharashtra"
            },
            {
                "key": "city-393",
                "name": "Solapur",
                "state": "Maharashtra"
            },
            {
                "key": "city-394",
                "name": "Thane",
                "state": "Maharashtra"
            },
            {
                "key": "city-395",
                "name": "Wardha",
                "state": "Maharashtra"
            },
            {
                "key": "city-396",
                "name": "Washim",
                "state": "Maharashtra"
            },
            {
                "key": "city-397",
                "name": "Yavatmal",
                "state": "Maharashtra"
            }
        ]
    },
    {
        "key": "state-22",
        "text": "Manipur",
        "value": "Manipur",
        "cities": [
            {
                "key": "city-398",
                "name": "Bishnupur",
                "state": "Manipur"
            },
            {
                "key": "city-399",
                "name": "Thoubal",
                "state": "Manipur"
            },
            {
                "key": "city-400",
                "name": "Imphal East",
                "state": "Manipur"
            },
            {
                "key": "city-401",
                "name": "Imphal West",
                "state": "Manipur"
            },
            {
                "key": "city-402",
                "name": "Senapati",
                "state": "Manipur"
            },
            {
                "key": "city-403",
                "name": "Ukhrul",
                "state": "Manipur"
            },
            {
                "key": "city-404",
                "name": "Chandel",
                "state": "Manipur"
            },
            {
                "key": "city-405",
                "name": "Churachandpur",
                "state": "Manipur"
            },
            {
                "key": "city-406",
                "name": "Tamenglong",
                "state": "Manipur"
            },
            {
                "key": "city-407",
                "name": "Jiribam",
                "state": "Manipur"
            },
            {
                "key": "city-408",
                "name": "Kangpokpi (Sadar Hills)",
                "state": "Manipur"
            },
            {
                "key": "city-409",
                "name": "Kakching",
                "state": "Manipur"
            },
            {
                "key": "city-410",
                "name": "Tengnoupal",
                "state": "Manipur"
            },
            {
                "key": "city-411",
                "name": "Kamjong",
                "state": "Manipur"
            },
            {
                "key": "city-412",
                "name": "Noney",
                "state": "Manipur"
            },
            {
                "key": "city-413",
                "name": "Pherzawl",
                "state": "Manipur"
            }
        ]
    },
    {
        "key": "state-23",
        "text": "Meghalaya",
        "value": "Meghalaya",
        "cities": [
            {
                "key": "city-414",
                "name": "East Gharo Hills",
                "state": "Meghalaya"
            },
            {
                "key": "city-415",
                "name": "East Khasi Hills",
                "state": "Meghalaya"
            },
            {
                "key": "city-416",
                "name": "East Jaintia Hills",
                "state": "Meghalaya"
            },
            {
                "key": "city-417",
                "name": "North Garo Hills",
                "state": "Meghalaya"
            },
            {
                "key": "city-418",
                "name": "Ri Bhoi",
                "state": "Meghalaya"
            },
            {
                "key": "city-419",
                "name": "South Garo Hills",
                "state": "Meghalaya"
            },
            {
                "key": "city-420",
                "name": "South West Garo Hills",
                "state": "Meghalaya"
            },
            {
                "key": "city-421",
                "name": "South West Khasi Hills",
                "state": "Meghalaya"
            },
            {
                "key": "city-422",
                "name": "West Jaintia Hills",
                "state": "Meghalaya"
            },
            {
                "key": "city-423",
                "name": "West Garo Hills",
                "state": "Meghalaya"
            },
            {
                "key": "city-424",
                "name": "West Khasi Hills",
                "state": "Meghalaya"
            }
        ]
    },
    {
        "key": "state-24",
        "text": "Mizoram",
        "value": "Mizoram",
        "cities": [
            {
                "key": "city-425",
                "name": "Aizawl",
                "state": "Mizoram"
            },
            {
                "key": "city-426",
                "name": "Champhai",
                "state": "Mizoram"
            },
            {
                "key": "city-427",
                "name": "Kolasib",
                "state": "Mizoram"
            },
            {
                "key": "city-428",
                "name": "Lawngtlai",
                "state": "Mizoram"
            },
            {
                "key": "city-429",
                "name": "Lunglei",
                "state": "Mizoram"
            },
            {
                "key": "city-430",
                "name": "Mamit",
                "state": "Mizoram"
            },
            {
                "key": "city-431",
                "name": "Saiha",
                "state": "Mizoram"
            },
            {
                "key": "city-432",
                "name": "Serchhip",
                "state": "Mizoram"
            }
        ]
    },
    {
        "key": "state-25",
        "text": "Nagaland",
        "value": "Nagaland",
        "cities": [
            {
                "key": "city-433",
                "name": "Dimapur",
                "state": "Nagaland"
            },
            {
                "key": "city-434",
                "name": "Kiphire",
                "state": "Nagaland"
            },
            {
                "key": "city-435",
                "name": "Kohima",
                "state": "Nagaland"
            },
            {
                "key": "city-436",
                "name": "Longleng",
                "state": "Nagaland"
            },
            {
                "key": "city-437",
                "name": "Mokokchung",
                "state": "Nagaland"
            },
            {
                "key": "city-438",
                "name": "Mon",
                "state": "Nagaland"
            },
            {
                "key": "city-439",
                "name": "Peren",
                "state": "Nagaland"
            },
            {
                "key": "city-440",
                "name": "Phek",
                "state": "Nagaland"
            },
            {
                "key": "city-441",
                "name": "Tuensang",
                "state": "Nagaland"
            },
            {
                "key": "city-442",
                "name": "Wokha",
                "state": "Nagaland"
            },
            {
                "key": "city-443",
                "name": "Zunheboto",
                "state": "Nagaland"
            }
        ]
    },
    {
        "key": "state-26",
        "text": "Odisha",
        "value": "Odisha",
        "cities": [
            {
                "key": "city-444",
                "name": "Angul",
                "state": "Odisha"
            },
            {
                "key": "city-445",
                "name": "Boudh",
                "state": "Odisha"
            },
            {
                "key": "city-446",
                "name": "Bhadrak",
                "state": "Odisha"
            },
            {
                "key": "city-447",
                "name": "Balangir",
                "state": "Odisha"
            },
            {
                "key": "city-448",
                "name": "Bargarh",
                "state": "Odisha"
            },
            {
                "key": "city-449",
                "name": "Balasore",
                "state": "Odisha"
            },
            {
                "key": "city-450",
                "name": "Cuttack",
                "state": "Odisha"
            },
            {
                "key": "city-451",
                "name": "Debagarh",
                "state": "Odisha"
            },
            {
                "key": "city-452",
                "name": "Dhenkanal",
                "state": "Odisha"
            },
            {
                "key": "city-453",
                "name": "Ganjam",
                "state": "Odisha"
            },
            {
                "key": "city-454",
                "name": "Gajapati",
                "state": "Odisha"
            },
            {
                "key": "city-455",
                "name": "Jharsuguda",
                "state": "Odisha"
            },
            {
                "key": "city-456",
                "name": "Jajpur",
                "state": "Odisha"
            },
            {
                "key": "city-457",
                "name": "Jagatsinghpur",
                "state": "Odisha"
            },
            {
                "key": "city-458",
                "name": "Khordha",
                "state": "Odisha"
            },
            {
                "key": "city-459",
                "name": "Kendujhar",
                "state": "Odisha"
            },
            {
                "key": "city-460",
                "name": "Kalahandi",
                "state": "Odisha"
            },
            {
                "key": "city-461",
                "name": "Kandhamal",
                "state": "Odisha"
            },
            {
                "key": "city-462",
                "name": "Koraput",
                "state": "Odisha"
            },
            {
                "key": "city-463",
                "name": "Kendrapara",
                "state": "Odisha"
            },
            {
                "key": "city-464",
                "name": "Malkangiri",
                "state": "Odisha"
            },
            {
                "key": "city-465",
                "name": "Mayurbhanj",
                "state": "Odisha"
            },
            {
                "key": "city-466",
                "name": "Nabarangpur",
                "state": "Odisha"
            },
            {
                "key": "city-467",
                "name": "Nuapada",
                "state": "Odisha"
            },
            {
                "key": "city-468",
                "name": "Nayagarh",
                "state": "Odisha"
            },
            {
                "key": "city-469",
                "name": "Puri",
                "state": "Odisha"
            },
            {
                "key": "city-470",
                "name": "Rayagada",
                "state": "Odisha"
            },
            {
                "key": "city-471",
                "name": "Sambalpur",
                "state": "Odisha"
            },
            {
                "key": "city-472",
                "name": "Subarnapur",
                "state": "Odisha"
            },
            {
                "key": "city-473",
                "name": "Sundargarh",
                "state": "Odisha"
            }
        ]
    },
    {
        "key": "state-27",
        "text": "Puducherry",
        "value": "Puducherry",
        "cities": [
            {
                "key": "city-474",
                "name": "Karaikal",
                "state": "Puducherry"
            },
            {
                "key": "city-475",
                "name": "Mahe",
                "state": "Puducherry"
            },
            {
                "key": "city-476",
                "name": "Pondicherry",
                "state": "Puducherry"
            },
            {
                "key": "city-477",
                "name": "Yanam",
                "state": "Puducherry"
            }
        ]
    },
    {
        "key": "state-28",
        "text": "Punjab",
        "value": "Punjab",
        "cities": [
            {
                "key": "city-478",
                "name": "Amritsar",
                "state": "Punjab"
            },
            {
                "key": "city-479",
                "name": "Barnala",
                "state": "Punjab"
            },
            {
                "key": "city-480",
                "name": "Bathinda",
                "state": "Punjab"
            },
            {
                "key": "city-481",
                "name": "Firozpur",
                "state": "Punjab"
            },
            {
                "key": "city-482",
                "name": "Faridkot",
                "state": "Punjab"
            },
            {
                "key": "city-483",
                "name": "Fatehgarh Saheb",
                "state": "Punjab"
            },
            {
                "key": "city-484",
                "name": "Fazilka",
                "state": "Punjab"
            },
            {
                "key": "city-485",
                "name": "Gurdaspur",
                "state": "Punjab"
            },
            {
                "key": "city-486",
                "name": "Hoshiarpur",
                "state": "Punjab"
            },
            {
                "key": "city-487",
                "name": "Jalandhar",
                "state": "Punjab"
            },
            {
                "key": "city-488",
                "name": "Kapurthala",
                "state": "Punjab"
            },
            {
                "key": "city-489",
                "name": "Ludhiana",
                "state": "Punjab"
            },
            {
                "key": "city-490",
                "name": "Mansa",
                "state": "Punjab"
            },
            {
                "key": "city-491",
                "name": "Moga",
                "state": "Punjab"
            },
            {
                "key": "city-492",
                "name": "Sri Muktar Sahib",
                "state": "Punjab"
            },
            {
                "key": "city-493",
                "name": "Pathankot",
                "state": "Punjab"
            },
            {
                "key": "city-494",
                "name": "Patiala",
                "state": "Punjab"
            },
            {
                "key": "city-495",
                "name": "Rupnagar",
                "state": "Punjab"
            },
            {
                "key": "city-496",
                "name": "Mohali",
                "state": "Punjab"
            },
            {
                "key": "city-497",
                "name": "Sangrur",
                "state": "Punjab"
            },
            {
                "key": "city-498",
                "name": "Shahid Bhagat Singh Nagar",
                "state": "Punjab"
            },
            {
                "key": "city-499",
                "name": "Tarn Taran",
                "state": "Punjab"
            }
        ]
    },
    {
        "key": "state-29",
        "text": "Rajasthan",
        "value": "Rajasthan",
        "cities": [
            {
                "key": "city-500",
                "name": "Ajmer",
                "state": "Rajasthan"
            },
            {
                "key": "city-501",
                "name": "Alwar",
                "state": "Rajasthan"
            },
            {
                "key": "city-502",
                "name": "Bikaner",
                "state": "Rajasthan"
            },
            {
                "key": "city-503",
                "name": "Barmer",
                "state": "Rajasthan"
            },
            {
                "key": "city-504",
                "name": "Banswara",
                "state": "Rajasthan"
            },
            {
                "key": "city-505",
                "name": "Bharatpur",
                "state": "Rajasthan"
            },
            {
                "key": "city-506",
                "name": "Baran",
                "state": "Rajasthan"
            },
            {
                "key": "city-507",
                "name": "Bundi",
                "state": "Rajasthan"
            },
            {
                "key": "city-508",
                "name": "Bhilwara",
                "state": "Rajasthan"
            },
            {
                "key": "city-509",
                "name": "Churu",
                "state": "Rajasthan"
            },
            {
                "key": "city-510",
                "name": "Chittorgarh",
                "state": "Rajasthan"
            },
            {
                "key": "city-511",
                "name": "Dausa",
                "state": "Rajasthan"
            },
            {
                "key": "city-512",
                "name": "Dholpur",
                "state": "Rajasthan"
            },
            {
                "key": "city-513",
                "name": "Dungarpur",
                "state": "Rajasthan"
            },
            {
                "key": "city-514",
                "name": "Ganganagar",
                "state": "Rajasthan"
            },
            {
                "key": "city-515",
                "name": "Hanumangarh",
                "state": "Rajasthan"
            },
            {
                "key": "city-516",
                "name": "Jhunjhunu",
                "state": "Rajasthan"
            },
            {
                "key": "city-517",
                "name": "Jalore",
                "state": "Rajasthan"
            },
            {
                "key": "city-518",
                "name": "Jodhpur",
                "state": "Rajasthan"
            },
            {
                "key": "city-519",
                "name": "Jaipur",
                "state": "Rajasthan"
            },
            {
                "key": "city-520",
                "name": "Jaisalmer",
                "state": "Rajasthan"
            },
            {
                "key": "city-521",
                "name": "Jhalawar",
                "state": "Rajasthan"
            },
            {
                "key": "city-522",
                "name": "Karauli",
                "state": "Rajasthan"
            },
            {
                "key": "city-523",
                "name": "Kota",
                "state": "Rajasthan"
            },
            {
                "key": "city-524",
                "name": "Nagaur",
                "state": "Rajasthan"
            },
            {
                "key": "city-525",
                "name": "Pali",
                "state": "Rajasthan"
            },
            {
                "key": "city-526",
                "name": "Pratapgarh",
                "state": "Rajasthan"
            },
            {
                "key": "city-527",
                "name": "Rajsamand",
                "state": "Rajasthan"
            },
            {
                "key": "city-528",
                "name": "Sikar",
                "state": "Rajasthan"
            },
            {
                "key": "city-529",
                "name": "Sawai Madhopur",
                "state": "Rajasthan"
            },
            {
                "key": "city-530",
                "name": "Sirohi",
                "state": "Rajasthan"
            },
            {
                "key": "city-531",
                "name": "Tonk",
                "state": "Rajasthan"
            },
            {
                "key": "city-532",
                "name": "Udaipur",
                "state": "Rajasthan"
            }
        ]
    },
    {
        "key": "state-30",
        "text": "Sikkim",
        "value": "Sikkim",
        "cities": [
            {
                "key": "city-533",
                "name": "Gangtok",
                "state": "Sikkim"
            },
            {
                "key": "city-534",
                "name": "Mangan",
                "state": "Sikkim"
            },
            {
                "key": "city-535",
                "name": "Namchi",
                "state": "Sikkim"
            },
            {
                "key": "city-536",
                "name": "Geyzing",
                "state": "Sikkim"
            }
        ]
    },
    {
        "key": "state-31",
        "text": "Tamil Nadu",
        "value": "Tamil Nadu",
        "cities": [
            {
                "key": "city-537",
                "name": "Ariyalur",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-538",
                "name": "Chennai",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-539",
                "name": "Coimbatore",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-540",
                "name": "Cuddalore",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-541",
                "name": "Dharmapuri",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-542",
                "name": "Dindigul",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-543",
                "name": "Erode",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-544",
                "name": "Kanchipuram",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-545",
                "name": "Kanyakumari",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-546",
                "name": "Karur",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-547",
                "name": "Krishnagiri",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-548",
                "name": "Madurai",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-549",
                "name": "Nagapattinam",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-550",
                "name": "Nilgiris",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-551",
                "name": "Namakkal",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-552",
                "name": "Perambalur",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-553",
                "name": "Pudukkottai",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-554",
                "name": "Ramannathapuram",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-555",
                "name": "Salem",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-556",
                "name": "Sivaganga",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-557",
                "name": "Tirupur",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-558",
                "name": "Tiruchirappalli",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-559",
                "name": "Theni",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-560",
                "name": "Tirunelveli",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-561",
                "name": "Thanjavur",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-562",
                "name": "Thoothukudi",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-563",
                "name": "Tiruvallur",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-564",
                "name": "Tiruvarur",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-565",
                "name": "Tiruvannamalai",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-566",
                "name": "Vellore",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-567",
                "name": "Viluppuram",
                "state": "Tamil Nadu"
            },
            {
                "key": "city-568",
                "name": "Virudhunagar",
                "state": "Tamil Nadu"
            }
        ]
    },
    {
        "key": "state-32",
        "text": "Telangana",
        "value": "Telangana",
        "cities": [
            {
                "key": "city-569",
                "name": "Adilabad",
                "state": "Telangana"
            },
            {
                "key": "city-570",
                "name": "Komaram Bheem Asifabad",
                "state": "Telangana"
            },
            {
                "key": "city-571",
                "name": "Bhadradri Kothagudem",
                "state": "Telangana"
            },
            {
                "key": "city-572",
                "name": "Hyderabad",
                "state": "Telangana"
            },
            {
                "key": "city-573",
                "name": "Jagtial",
                "state": "Telangana"
            },
            {
                "key": "city-574",
                "name": "Jangaon",
                "state": "Telangana"
            },
            {
                "key": "city-575",
                "name": "Jayashankar Bhupalpally",
                "state": "Telangana"
            },
            {
                "key": "city-576",
                "name": "Jogulamba Gadwal",
                "state": "Telangana"
            },
            {
                "key": "city-577",
                "name": "Kamareddy",
                "state": "Telangana"
            },
            {
                "key": "city-578",
                "name": "Karimnagar",
                "state": "Telangana"
            },
            {
                "key": "city-579",
                "name": "Khammam",
                "state": "Telangana"
            },
            {
                "key": "city-580",
                "name": "Mahabubabad",
                "state": "Telangana"
            },
            {
                "key": "city-581",
                "name": "Mahabubnagar",
                "state": "Telangana"
            },
            {
                "key": "city-582",
                "name": "Mancherial",
                "state": "Telangana"
            },
            {
                "key": "city-583",
                "name": "Medak",
                "state": "Telangana"
            },
            {
                "key": "city-584",
                "name": "Medchal",
                "state": "Telangana"
            },
            {
                "key": "city-585",
                "name": "Nalgonda",
                "state": "Telangana"
            },
            {
                "key": "city-586",
                "name": "Nagarkurnool",
                "state": "Telangana"
            },
            {
                "key": "city-587",
                "name": "Nirmal",
                "state": "Telangana"
            },
            {
                "key": "city-588",
                "name": "Nizamabad",
                "state": "Telangana"
            },
            {
                "key": "city-589",
                "name": "Peddapalli",
                "state": "Telangana"
            },
            {
                "key": "city-590",
                "name": "Rajanna Sircilla",
                "state": "Telangana"
            },
            {
                "key": "city-591",
                "name": "Ranga Reddy",
                "state": "Telangana"
            },
            {
                "key": "city-592",
                "name": "Sangareddy",
                "state": "Telangana"
            },
            {
                "key": "city-593",
                "name": "Siddipet",
                "state": "Telangana"
            },
            {
                "key": "city-594",
                "name": "Suryapet",
                "state": "Telangana"
            },
            {
                "key": "city-595",
                "name": "Vikarabad",
                "state": "Telangana"
            },
            {
                "key": "city-596",
                "name": "Wanaparthy",
                "state": "Telangana"
            },
            {
                "key": "city-597",
                "name": "Warangal",
                "state": "Telangana"
            },
            {
                "key": "city-598",
                "name": "Yadadri Bhuvanagiri",
                "state": "Telangana"
            }
        ]
    },
    {
        "key": "state-33",
        "text": "Tripura",
        "value": "Tripura",
        "cities": [
            {
                "key": "city-599",
                "name": "Dhalai",
                "state": "Tripura"
            },
            {
                "key": "city-600",
                "name": "Gomati",
                "state": "Tripura"
            },
            {
                "key": "city-601",
                "name": "Khowai",
                "state": "Tripura"
            },
            {
                "key": "city-602",
                "name": "North Tripura",
                "state": "Tripura"
            },
            {
                "key": "city-603",
                "name": "Sepahijala",
                "state": "Tripura"
            },
            {
                "key": "city-604",
                "name": "South Tripura",
                "state": "Tripura"
            },
            {
                "key": "city-605",
                "name": "Unokoti",
                "state": "Tripura"
            },
            {
                "key": "city-606",
                "name": "West Tripura",
                "state": "Tripura"
            }
        ]
    },
    {
        "key": "state-34",
        "text": "Uttar Pradesh",
        "value": "Uttar Pradesh",
        "cities": [
            {
                "key": "city-607",
                "name": "Agra",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-608",
                "name": "Aligarh",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-609",
                "name": "Allahabad",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-610",
                "name": "Ambedkar Nagar",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-611",
                "name": "Amethi",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-612",
                "name": "Amroha",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-613",
                "name": "Auraiya",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-614",
                "name": "Azamgarh",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-615",
                "name": "Bagpat",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-616",
                "name": "Bahraich",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-617",
                "name": "Ballia",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-618",
                "name": "Balrampur",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-619",
                "name": "Banda",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-620",
                "name": "Barabanki",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-621",
                "name": "Bareilly",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-622",
                "name": "Basti",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-623",
                "name": "Bijnor",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-624",
                "name": "Budaun",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-625",
                "name": "Bulandshahr",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-626",
                "name": "Chandauli",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-627",
                "name": "Chitrakoot",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-628",
                "name": "Deoria",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-629",
                "name": "Etah",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-630",
                "name": "Etawah",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-631",
                "name": "Faizabad",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-632",
                "name": "Farrukhabad",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-633",
                "name": "Fatehpur",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-634",
                "name": "Firozabad",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-635",
                "name": "Noida",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-636",
                "name": "Ghaziabad",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-637",
                "name": "Ghazipur",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-638",
                "name": "Gonda",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-639",
                "name": "Gorakhpur",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-640",
                "name": "Hamirpur",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-641",
                "name": "Hapur",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-642",
                "name": "Hardoi",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-643",
                "name": "Hathras",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-644",
                "name": "Jalaun",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-645",
                "name": "Jaunpur",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-646",
                "name": "Jhansi",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-647",
                "name": "Kannauj",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-648",
                "name": "Kanpur Dehat",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-649",
                "name": "Kanpur",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-650",
                "name": "Kasganj",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-651",
                "name": "Kaushambi",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-652",
                "name": "Kushinagar",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-653",
                "name": "Lakhimpur Kheri",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-654",
                "name": "Lalitpur",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-655",
                "name": "Lucknow",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-656",
                "name": "Maharajganj",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-657",
                "name": "Mahoba",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-658",
                "name": "Mainpuri",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-659",
                "name": "Mathura",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-660",
                "name": "Mau",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-661",
                "name": "Meerut",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-662",
                "name": "Mirzapur",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-663",
                "name": "Moradabad",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-664",
                "name": "Muzaffarnagar",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-665",
                "name": "Pilibhit",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-666",
                "name": "Pratapgarh",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-667",
                "name": "Raebareli",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-668",
                "name": "Raipur",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-669",
                "name": "Saharanpur",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-670",
                "name": "Sambhal",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-671",
                "name": "Sant Kabir Nagar",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-672",
                "name": "Bhadohi",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-673",
                "name": "Shahjahanpur",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-674",
                "name": "Shamli",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-675",
                "name": "Shravasti",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-676",
                "name": "Siddharthnagar",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-677",
                "name": "Sitapur",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-678",
                "name": "Sonbhadra",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-679",
                "name": "Sultanpur",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-680",
                "name": "Unnao",
                "state": "Uttar Pradesh"
            },
            {
                "key": "city-681",
                "name": "Varanasi",
                "state": "Uttar Pradesh"
            }
        ]
    },
    {
        "key": "state-35",
        "text": "Uttarakhand",
        "value": "Uttarakhand",
        "cities": [
            {
                "key": "city-682",
                "name": "Almora",
                "state": "Uttarakhand"
            },
            {
                "key": "city-683",
                "name": "Bageshwar",
                "state": "Uttarakhand"
            },
            {
                "key": "city-684",
                "name": "Chamoli",
                "state": "Uttarakhand"
            },
            {
                "key": "city-685",
                "name": "Champawat",
                "state": "Uttarakhand"
            },
            {
                "key": "city-686",
                "name": "Dehradun",
                "state": "Uttarakhand"
            },
            {
                "key": "city-687",
                "name": "Haridwar",
                "state": "Uttarakhand"
            },
            {
                "key": "city-688",
                "name": "Nainital",
                "state": "Uttarakhand"
            },
            {
                "key": "city-689",
                "name": "Pauri Garhwal",
                "state": "Uttarakhand"
            },
            {
                "key": "city-690",
                "name": "Pithoragarh",
                "state": "Uttarakhand"
            },
            {
                "key": "city-691",
                "name": "Rudraprayag",
                "state": "Uttarakhand"
            },
            {
                "key": "city-692",
                "name": "Tehri Garhwal",
                "state": "Uttarakhand"
            },
            {
                "key": "city-693",
                "name": "Udham Singh Nagar",
                "state": "Uttarakhand"
            },
            {
                "key": "city-694",
                "name": "Uttarkashi",
                "state": "Uttarakhand"
            }
        ]
    },
    {
        "key": "state-36",
        "text": "West Bengal",
        "value": "West Bengal",
        "cities": [
            {
                "key": "city-695",
                "name": "Alipurduar",
                "state": "West Bengal"
            },
            {
                "key": "city-696",
                "name": "Bankura",
                "state": "West Bengal"
            },
            {
                "key": "city-697",
                "name": "Paschim Bardhaman",
                "state": "West Bengal"
            },
            {
                "key": "city-698",
                "name": "Purba Bardhaman",
                "state": "West Bengal"
            },
            {
                "key": "city-699",
                "name": "Birbhum",
                "state": "West Bengal"
            },
            {
                "key": "city-700",
                "name": "Cooch Behar",
                "state": "West Bengal"
            },
            {
                "key": "city-701",
                "name": "Dakshin Dinajpur",
                "state": "West Bengal"
            },
            {
                "key": "city-702",
                "name": "Darjeeling",
                "state": "West Bengal"
            },
            {
                "key": "city-703",
                "name": "Hooghly",
                "state": "West Bengal"
            },
            {
                "key": "city-704",
                "name": "Howrah",
                "state": "West Bengal"
            },
            {
                "key": "city-705",
                "name": "Jalpaiguri",
                "state": "West Bengal"
            },
            {
                "key": "city-706",
                "name": "Jhargram",
                "state": "West Bengal"
            },
            {
                "key": "city-707",
                "name": "Kalimpong",
                "state": "West Bengal"
            },
            {
                "key": "city-708",
                "name": "Kolkata",
                "state": "West Bengal"
            },
            {
                "key": "city-709",
                "name": "Maldah",
                "state": "West Bengal"
            },
            {
                "key": "city-710",
                "name": "Mushidabad",
                "state": "West Bengal"
            },
            {
                "key": "city-711",
                "name": "Nadia",
                "state": "West Bengal"
            },
            {
                "key": "city-712",
                "name": "North 24 Parganas",
                "state": "West Bengal"
            },
            {
                "key": "city-713",
                "name": "Paschim Medinipur",
                "state": "West Bengal"
            },
            {
                "key": "city-714",
                "name": "Purba Medinipur",
                "state": "West Bengal"
            },
            {
                "key": "city-715",
                "name": "Purulia",
                "state": "West Bengal"
            },
            {
                "key": "city-716",
                "name": "South 24 Parganas",
                "state": "West Bengal"
            },
            {
                "key": "city-717",
                "name": "Uttar Dinajpur",
                "state": "West Bengal"
            }
        ]
    }
];

export function getStates() {
    return _.map(statesAndCities, (item) => {
        return {key: item.key, text: item.text, value: item.value};
    });
}

export function getCities(state) {
    let cities = [];
    let matchedState = _.find(statesAndCities, (stateAndCity) => {
        return stateAndCity.value === state;
    });
    if (matchedState) {
        cities = _.map(matchedState.cities, (city) => {
            return {key: city.key, text: city.name, value: city.name}
        });
    }
    return cities;
}

export function prepareStateCityJson() {
    // {
    //     "cityKey": "State-0",
    //     "text": "Andaman and Nicobar Islands",
    //     "value": "Andaman and Nicobar Islands",
    //     "cities": [
    //     {
    //         "cityKey": "334",
    //         "name": "Port Blair",
    //         "state": "Andaman and Nicobar Islands"
    //     }
    // ]
    // }
    let array = [];
    let obj = {key: "", text: "", value: "", cities: []};
    let obj2 = {key: "", name: "", state: ""};
    let stateKey = 1;
    let cityKey = 1;
    _.each(StateCities, (item1) => {
        let contains = _.find(array, (item2) => {
            return item2.value === item1.state;
        });
        if (contains) {
            contains.cities.push({
                key: "city-" + cityKey,
                name: item1.name,
                state: item1.state
            });
        }
        else {
            array.push(
                {
                    key: "state-" + stateKey,
                    text: item1.state,
                    value: item1.state, cities: [
                        {
                            key: "city-" + cityKey,
                            name: item1.name,
                            state: item1.state
                        }
                    ]
                }
            );
            stateKey += 1;
        }
        cityKey += 1;
    });
    console.log(JSON.stringify(array));
}

// prepareStateCityJson();