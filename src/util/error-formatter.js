import _ from 'lodash';

export function errorMessage(error) {
    if (error) {
        if (_.isString(error)) {
            return error;
        }
        else if (_.isString(error.message)) {
            return error.message;
        }
        else if (error.data) {
            if (_.isString(error.data.message)) {
                return error.data.message;
            }
            else if (_.isString(error.data)) {
                return error.data;
            }
        }
        else if (error.messages && error.messages.length > 0) {
            if (_.isString(error.messages[0].message)) {
                return error.messages[0].message;
            }
            else if (_.isString(error.messages[0].code)) {
                return error.messages[0].code;
            }
        }
        else {
            return JSON.stringify(error);
        }
    }
    else {
        return JSON.stringify(error);
    }
}