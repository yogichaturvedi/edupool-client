import httpStatus from 'http-status';
import fetch from '../core/fetch';

async function callApi(apiObject, resolver = null) {

    const fetchObject = {};
    fetchObject.method = apiObject.method ? apiObject.method : 'get';
    fetchObject.body = apiObject.body ? JSON.stringify(apiObject.body) : null;

    fetchObject.headers = {
        'Content-Type': 'application/json'
    };
    fetchObject.headers = apiObject.headers ? {...fetchObject.headers, ...apiObject.headers} : {...fetchObject.headers};
    if(apiObject.mode){
        fetchObject.mode = apiObject.mode;
    }
    const url = `${apiObject.endPoint}`;
    const fetchResult = await fetch(url, fetchObject);
    return new Promise(async (resolve, reject) => {
        if (resolver !== null) {
            let resolvedData = resolver(fetchResult, fetchResult.headers);
            if (resolvedData) {
                resolve(resolvedData);
            }
            else {
                const error = await fetchResult.json();
                return reject(error);
            }
        }
        else {
            if(fetchResult.status === 0){
                return "OK";
            }
            if((fetchResult.status >= httpStatus.OK && fetchResult.status <= httpStatus.MULTIPLE_CHOICES)){
            return resolve(fetchResult.json());
        }
            if(fetchResult.status === httpStatus.BAD_REQUEST){
                const response = await fetchResult.json();
                return reject(response);
            }
            const errorResponse = new Error(fetchResult.statusText);
            return reject(errorResponse);
        }
    });
}

export default {callApi};
