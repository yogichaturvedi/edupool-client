import ApiService from "./api-service";
import Constants from "../util/constants";
class CommonApiService {
    type = "school";

    constructor(type) {
        this.type = type;
    }

    async fetchBasicData(limit) {
        let user = localStorage.getItem("user");
        try {
            let limitQuery = limit ? ("&$limit=" + limit) : "";
            let reviewedQuery = "";
            if (this.type === 'school' || this.type === 'college' || this.type === 'coaching') {
                reviewedQuery = "&$select=reviewed";
                if (!user) {
                    reviewedQuery += "&reviewed=true";
                }
            }
            let endPoint = Constants.API + this.type
                + "?$select=id&$select=basicDetails&$select=createdBy&$select=createdAt&$select=updatedAt"
                + "&$sort[createdAt]=-1" + limitQuery + reviewedQuery;

            if (this.type === 'event') {
                endPoint += '&$select=tags';
            }

            return await ApiService.callApi({
                method: 'GET',
                authentication: false,
                endPoint: endPoint
            });
        }
        catch (error) {
            console.log(error);
            throw error;
        }
    }

    async fetchAdmissionOpenBasicData() {
        let user = localStorage.getItem("user");
        try {
            let reviewedQuery = "";
            if (this.type === 'school' || this.type === 'college' || this.type === 'coaching') {
                reviewedQuery = "&$select=reviewed";
                if (!user) {
                    reviewedQuery += "&reviewed=true";
                }
            }
            let endPoint = Constants.API + this.type
                + "?$select=id&$select=basicDetails&$select=createdBy&$select=createdAt&$select=updatedAt&$select=admissionOpen&$select=course"
                + "&$sort[createdAt]=-1" + reviewedQuery;
            endPoint += "&admissionOpen=true";

            return await ApiService.callApi({
                method: 'GET',
                authentication: false,
                endPoint: endPoint
            });
        }
        catch (error) {
            console.log(error);
            throw error;
        }
    }

    getCourses = async () => {
        try {
            return await ApiService.callApi({
                method: 'GET',
                authentication: false,
                endPoint: 'https://edupool.in/college-course.php'
            });
        }
        catch (error) {
            console.log(error);
            throw error;
        }
    };

    getCoachingCourses = async () => {
        try {
            return await ApiService.callApi({
                method: 'GET',
                authentication: false,
                endPoint: 'https://edupool.in/coaching-course.php'
            });
        }
        catch (error) {
            console.log(error);
            throw error;
        }
    };


    saveCourses = async (type, courses) => {
        try {
            return await ApiService.callApi({
                method: 'POST',
                authentication: false,
                endPoint: `https://edupool.in/${type}-course.php`,
                body: courses
            }).then(function (res) {
                return res;
            });
        }
        catch (error) {
            console.log(error);
            throw error;
        }
    };

    async saveData(dataToSave) {
        let endPoint = Constants.API + this.type;
        let user = JSON.parse(localStorage.getItem('user'));
        user = user ? user[0].username : 'Edupool India';
        return await ApiService.callApi({
            method: 'POST',
            authentication: false,
            endPoint: endPoint,
            body: {...dataToSave, createdBy: (dataToSave.createdBy || user)}
        }, function (response, headers) {
            if (response.status === 201) {
                return true;
            }
        }).then(function (res) {
            return res;
        });
    }

    async updateData(dataToUpdate, id) {
        let endPoint = Constants.API + this.type + "/" + id;
        let user = JSON.parse(localStorage.getItem('user'));
        return await ApiService.callApi({
            method: 'PATCH',
            authentication: false,
            endPoint: endPoint,
            body: {...dataToUpdate, updatedBy: user ? user[0].username : 'Edupool India'}
        }).then(function (res) {
            return res;
        });
    }

    async getById(id) {
        let endPoint = Constants.API + this.type + "/" + id;
        return await ApiService.callApi({
            method: 'GET',
            authentication: false,
            endPoint: endPoint
        }).then(function (res) {
            return res;
        });
    }

    async deleteData(id) {
        try {
            let endPoint = Constants.API + this.type + "/" + id;
            const response = await ApiService.callApi({
                method: 'DELETE',
                authentication: false,
                endPoint: endPoint
            });
            console.log(response);
            return response;
        }
        catch (error) {
            console.log(error);
            throw error.message ? error.message : error;
        }
    }

    async fetchBasicDataWithQuery(query) {
        try {
            let endPoint = Constants.API + this.type
                + query;
            let response = await ApiService.callApi({
                method: 'GET',
                authentication: false,
                endPoint: endPoint
            });
            console.log(response);
            return response;
        }
        catch (error) {
            console.log(error);
            throw error;
        }
    }

    async search(keywords) {
        try {
            let endPoint = Constants.API + "search/" + this.type
                + "?keywords=" + keywords;
            let response = await ApiService.callApi({
                method: 'GET',
                authentication: false,
                endPoint: endPoint
            });
            console.log(response);
            return response;
        }
        catch (error) {
            console.log(error);
            throw error;
        }
    }

     applyNow = async (dataToSave) => {
        try {
            let endPoint = 'https://edupool.in/admission-mail.php';
            return await ApiService.callApi({
                method: 'POST',
                authentication: false,
                endPoint: endPoint,
                body: dataToSave,
                mode:"no-cors"
            });
        }
        catch (error) {
            console.log(error);
            throw error;
        }
    }

    enquiryNow = async (dataToSave) => {
        try {
            let endPoint = 'https://edupool.in/coaching-enquiry-mail.php';
            return await ApiService.callApi({
                method: 'POST',
                authentication: false,
                endPoint: endPoint,
                body: dataToSave,
                mode:"no-cors"
            });
        }
        catch (error) {
            console.log(error);
            throw error;
        }
    }
}

export default CommonApiService;
