/**
 * Created by Amit on 16-03-2018.
 */
import ApiService from "./api-service";
import _ from 'lodash';
import Constants from "../util/constants";

const baseUrl = Constants.API + "blog";

class ArticleNewsService {

    async all(type, limit) {
        let user = localStorage.getItem("user");
        try {
            let queryParams = {
                limit : limit ? ("&$limit=" + limit) : "",
                show : user ? '' : '&show=true',
                type : `&type=${type}`
            };
            let endPoint = baseUrl + `?$select=id&$select=type&$select=title&$select=shortDescription` +
                `&$select=thumbnail&$select=category&$select=show` +
                `&$select=createdAt&$select=createdBy&$select=tags`;
            endPoint += queryParams.limit + queryParams.show + queryParams.type +"&$sort[createdAt]=-1";

            return await ApiService.callApi({
                method: 'GET',
                authentication: false,
                endPoint: endPoint
            });
        }
        catch (error) {
            console.log(error);
            throw error;
        }
    }

    async save(dataToSave) {
        let user = JSON.parse(localStorage.getItem('user'));
        user = user ? user[0].username : 'Edupool India';
        return await ApiService.callApi({
            method: 'POST',
            authentication: false,
            endPoint: baseUrl,
            body: {...dataToSave, createdBy: (dataToSave.createdBy || user)}
        }, function (response, headers) {
            if (response.status === 201) {
                return true;
            }
        }).then(function (res) {
            return res;
        });
    }

    async update(dataToUpdate, id) {
        let user = JSON.parse(localStorage.getItem('user'));
        return await ApiService.callApi({
            method: 'PATCH',
            authentication: false,
            endPoint: `${baseUrl}/${id}`,
            body: {...dataToUpdate, updatedBy: user ? user[0].username : 'Edupool India'}
        }).then(function (res) {
            return res;
        });
    }

    async get(id) {
        return await ApiService.callApi({
            method: 'GET',
            authentication: false,
            endPoint: `${baseUrl}/${id}`,
        }).then(function (res) {
            return res;
        });
    }

    async delete(id) {
        try {
            const response = await ApiService.callApi({
                method: 'DELETE',
                authentication: false,
                endPoint: `${baseUrl}/${id}`,
            });
            console.log(response);
            return response;
        }
        catch (error) {
            console.log(error);
            throw error.message ? error.message : error;
        }
    }

    async search(type, keywords) {
        try {
            let endPoint = `${Constants.API}search/blog?keywords=${keywords}`;
            let searchResults = await ApiService.callApi({
                method: 'GET',
                authentication: false,
                endPoint: endPoint
            });
            return _.filter(searchResults, result => result.type === type);
        }
        catch (error) {
            console.log(error);
            throw error;
        }
    }
}

export default ArticleNewsService;