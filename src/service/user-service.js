import ApiService from "./api-service";
import Constants from "../util/constants";

class UserService {

    async authenticate(email, password) {
        let endPoint = Constants.API +  "verify-user";
        return await ApiService.callApi({
            method: 'POST',
            authentication: false,
            endPoint: endPoint,
            body: {email: email, password: password}
        }).then(function (res) {
            return res;
        });
    }
}

export default new UserService();