import * as firebase from "firebase";

let config = {
    apiKey: "AIzaSyBdxrBBFzyRjb2s_xLhAweGW2YTRv3YXxc",
    authDomain: "edu-pool-client.firebaseapp.com",
    databaseURL: "edu-pool-client.firebaseio.com",
    projectId: "edu-pool-client",
    storageBucket: "edu-pool-client.appspot.com",
    messagingSenderId: "1073253814511"
};

class FirebaseService {
    constructor() {
        firebase.initializeApp(config)
    }

    async sendCode(phoneNumber) {
        // firebase.auth().languageCode = 'it';
        // window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');

        let appVerifier = window.recaptchaVerifier;
        return firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier).then(function (confirmationResult) {
            // SMS sent. Prompt user to type the code from the message, then sign the
            // user in with confirmationResult.confirm(code).
            window.confirmationResult = confirmationResult;
            return true;
        }).catch(function (error) {
          console.log(error);
        });
    }

    async verifyCode(code) {
        return window.confirmationResult.confirm(code).then(function (result) {
            // User signed in successfully.
            return result.user;
            // ...
        }).catch(function (error) {
            // User couldn't sign in (bad verification code?)
            // ...
            console.log(error);
        });
    }


    getCurrentUser(){
        return firebase.auth().currentUser;
    }

    async googleSignIn(){
        let provider = new firebase.auth.GoogleAuthProvider();
        provider.setCustomParameters({
            'login_hint': 'user@example.com'
        });
        return firebase.auth().signInWithPopup(provider).then(function(result) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            // let token = result.credential.accessToken;
            // The signed-in user info.
            return {
                name: result.user.displayName,
                email: result.user.email,
                imageUrl: result.user.photoURL,
                profileLink: result.additionalUserInfo.profile.link
            };
            // ...
        }).catch(function(error) {
            // Handle Errors here.
            // let errorCode = error.code;
            // let errorMessage = error.message;
            // // The email of the user's account used.
            // let email = error.email;
            // // The firebase.auth.AuthCredential type that was used.
            // let credential = error.credential;
            throw error;
        });
    }

    async googleSignOut() {
        return firebase
            .auth()
            .signOut()
            .catch(function (error) {
                throw error
            });
    }
}

export default new FirebaseService();