/**
 * Created by Amit on 01-10-2017.
 */

const TopRated = {
    school : [
        {
            id : '9527df84-f274-4aeb-8d86-e3eb3d1c521d',
            title : 'Central Academy, Sardarpura, Udaipur',
            imageUrl : 'https://preview.ibb.co/dVOxzF/test_logo.gif?'
        },
        {
            id : '21b0714b-15d5-4fa9-8a33-23530b36532e',
            title : 'L-Soldiers Sr. Sec. School',
            imageUrl : 'https://lsoldierschool.com/wp-content/uploads/2011/12/L-soldiers-public-school.jpg?'
        },
        {
            id : '570f6a1e-b0ae-4aca-9252-04f0d52bb3e8',
            title : 'Mount View School',
            imageUrl : 'http://mountviewschool.com/images/mount.png?'
        }
    ],
    college : [
        {
            id : '8574fff0-ae5f-48af-82c0-15daa12435e2',
            title : 'SS College of Engineering',
            imageUrl : 'http://www.ssengineeringcollege.org/images/maingate-sscollege.jpg?'
        },
        {
            id : '550d6a16-9044-4f17-bc29-aed1aec55366',
            title : 'Pacific University',
            imageUrl : 'http://www.pstr.ac.in/images/logo1.png?'
        }
    ],
    coaching : [
        {
            id : 'ddb9138c-00a4-4c24-8dd3-68479096abf8',
            title : 'Ascent Career Point Udaipur',
            imageUrl : 'https://preview.ibb.co/mmwe8k/alogo.png?'
        },
        {
            id : '36670a8b-a8b5-4068-a62a-eed7ff4b3779',
            title : 'Mind Space Coaching Classes',
            imageUrl : 'https://preview.ibb.co/mnGw3k/IMG_20170618_WA0004.jpg?'
        }
    ]
};

export default TopRated;