/**
 * Created by Yogesh Chaturvedi on 11-06-2017.
 */
import React, {Component} from 'react';
import {observer} from 'mobx-react';
import List from "../../component/list";
import {observable} from 'mobx';
import ExamStore from "../store/exam-store";
import _ from "lodash";
import Loading from "../../component/loading/index";
import CommonApiService from "../../service/common-api";
import UiStore from "../store/ui-store";
import App_Const from "../../constants/constants";
import {errorMessage} from "../../util/error-formatter";
import AppError from "../../component/error/index";
import SEO from "../../component/seo/index";
import SEO_INFO from "../../static-data/seo";
import { ListHeader } from '../../component/component';
import '../index.scss';
import ConfirmDelete from "../../component/modal/confirm-delete";

@observer
class Exam extends Component {

    @observable basicDetails = [];

    constructor(props) {
        super(props);
        this.retry = this.retry.bind(this);
        this.fetchBasicData = this.fetchBasicData.bind(this);
        this.deleteDetail = this.deleteDetail.bind(this);
        this.retry = this.retry.bind(this);
        this.state = {
            confirmDelete: {
                show: false, title: "", examToDelete: {}
            }
        };
        UiStore.error.present = false;
        UiStore.error.message = "";

    }

    componentWillMount() {
        localStorage.setItem("activeTab", "exam");
        this.fetchBasicData(this.props.location.query.search);
    }

    componentWillReceiveProps(props) {
        this.fetchBasicData(props.location.query.search);
    }

    retry() {
        UiStore.loading = false;
        UiStore.error.present = false;
        UiStore.error.message = "";
        this.fetchBasicData(this.props.location.query.search);
    }

    async fetchBasicData(queryString = "") {
        try {
            UiStore.loading = true;
            let response = await  ExamStore.fetchBasicData(queryString);
            this.basicDetails = _.map(response, (basicDetail) => {
                return {
                    id: basicDetail.id,
                    createdAt: basicDetail.createdAt,
                    createdBy: basicDetail.createdBy,
                    ...basicDetail.basicDetails
                }
            });
            UiStore.loading = false;
        }
        catch (error) {
            if (error.message === App_Const.failedToFetch) {
                UiStore.loading = false;
                UiStore.error.present = true;
                UiStore.error.message = App_Const.failedToFetch;
            }
        }
    }

    async deleteDetail(id) {
        let examToDelete = _.find(this.basicDetails, detail => detail.id === id);
        this.setState({
            confirmDelete : {
                show: true,
                title: `Delete Exam`,
                examToDelete
            }
        });
    }

    deleteExam = async (id) => {
        try {
            await new CommonApiService('exam').deleteData(id);
            this.props.showToastr('success', 'Delete', 'Exam deleted successfully.');
            return await this.fetchBasicData(this.props.location.query.search);
        }
        catch (error) {
            this.props.showToastr('error', 'Delete', errorMessage(error));
        }
    };


    render() {
        return (
            <div>
                {
                    UiStore.error.present ?
                        <div>
                            <AppError retry={this.retry} error={UiStore.error}/>
                        </div>
                        :
                        <div>
                            <div className={"app-background page-exam"}/>
                            {
                                UiStore.loading ? <Loading/> :
                                    <div>
                                        { ListHeader('exam') }

                                        <ConfirmDelete
                                            show={this.state.confirmDelete.show}
                                            dataToDelete={this.state.confirmDelete.examToDelete}
                                            title={this.state.confirmDelete.title}
                                            deleteData={this.deleteExam}
                                            close={() => this.setState({confirmDelete: { show: false, title: "", examToDelete: {}}})}/>

                                        <List title="Here are the new vacancies"
                                              icon="newspaper"
                                              admin={this.props.admin}
                                              deleteDetail={this.deleteDetail}
                                              basicDetails={this.basicDetails}
                                              type="exam"
                                              {...this.props}/>
                                    </div>
                            }
                        </div>
                }
                <SEO page="exam"
                     title={SEO_INFO.exam.title}
                     description={SEO_INFO.exam.description}
                     image={SEO_INFO.exam.image}/>
            </div>
        );
    }
}

export default Exam;