/**
 * Created by Yogesh Chaturvedi on 11-06-2017.
 */
import React, {Component} from 'react';
import {observable} from 'mobx';
import {observer} from 'mobx-react';
import List from "../../component/list";
import BasicStore from "../store/basic-store";
import Loading from "../../component/loading/index";
import AppError from "../../component/error/index";
import UiStore from '../store/ui-store'
import CommonApiService from "../../service/common-api";
import _ from "lodash";
import App_Const from "../../constants/constants";
import {errorMessage} from "../../util/error-formatter";
import SEO from "../../component/seo/index";
import SEO_INFO from "../../static-data/seo";
import { ListHeader } from '../../component/component';
import '../index.scss';
import ConfirmDelete from "../../component/modal/confirm-delete";

@observer
class School extends Component {

    @observable basicDetails = [];

    constructor(props) {
        super(props);
        this.fetchBasicData = this.fetchBasicData.bind(this);
        this.deleteDetail = this.deleteDetail.bind(this);
        this.retry = this.retry.bind(this);
        this.state = {
            confirmDelete: {
                show: false, title: "", schoolToDelete: {}
            }
        };
        UiStore.error.present = false;
        UiStore.error.message = "";
    }

    componentWillMount() {
        localStorage.setItem("activeTab", "school");
        this.basicStore = new BasicStore("school");
        this.fetchBasicData(this.props.location.query.search);
    }

    componentWillReceiveProps(props) {
        this.fetchBasicData(props.location.query.search);
    }

    async deleteDetail(id) {
        let schoolToDelete = _.find(this.basicDetails, detail => detail.id === id);
        this.setState({
            confirmDelete : {
                show: true,
                title: `Delete School`,
                schoolToDelete: schoolToDelete
            }
        });
    }

    deleteSchool = async (id) => {
        try {
            await new CommonApiService('school').deleteData(id);
            this.props.showToastr('success', 'Delete', 'School deleted successfully.');
            return await this.fetchBasicData();
        }
        catch (error) {
            this.props.showToastr('error', 'Delete', errorMessage(error));
        }
    };

    async fetchBasicData(queryString = "") {
        try {
            UiStore.loading = true;
            let schools = await  this.basicStore.fetchBasicData(queryString);
            // const response = SchoolFakeData;
            console.log(schools);
            this.basicDetails = _.map(schools, (school) => {
                let result = _.extend(school, school.basicDetails);
                delete result.basicDetails;
                return result;
            });
            UiStore.loading = false;
        }
        catch (error) {
            UiStore.loading = false;
            UiStore.error.present = true;
            if (error.message === App_Const.failedToFetch) {
                UiStore.error.message = App_Const.failedToFetch;
            }
            else {
                UiStore.error.message = errorMessage(error);
            }
        }
    }

    retry() {
        UiStore.loading = false;
        UiStore.error.present = false;
        UiStore.error.message = "";
        this.fetchBasicData(this.props.location.query.search);
    }

    getSearchKeyWord = () => {
        const keyword = this.props.location.query.keyword;
        if(keyword){
            return keyword.split("-").join(" ");
        }
        return null;
    };
    getKeyWord = () => {
        return this.props.location.query.keyword.split("-").join("");
    };

    render() {
        return (
            <div>
                {
                    UiStore.error.present ?
                        <div>
                            <AppError retry={this.retry} error={UiStore.error}/>
                        </div>
                        :
                        <div>
                            <div className={"app-background page-school"}/>
                            {
                                UiStore.loading ? <Loading/> :
                                    <div>
                                        { ListHeader('school', this.getSearchKeyWord()) }
                                        <ConfirmDelete
                                            show={this.state.confirmDelete.show}
                                            dataToDelete={this.state.confirmDelete.schoolToDelete}
                                            title={this.state.confirmDelete.title}
                                            deleteData={this.deleteSchool}
                                            close={() => this.setState({confirmDelete: { show: false, title: "", schoolToDelete: {}}})}/>

                                        <List title="Find best school with us"
                                              icon="building"
                                              type="school"
                                              admin={this.props.admin}
                                              showToastr={this.props.showToastr}
                                              deleteDetail={this.deleteDetail}
                                              basicDetails={this.basicDetails}
                                              {...this.props}/>
                                    </div>
                            }
                        </div>
                }
                {

                    this.props.location.query.keyword ?
                        <SEO page="school"
                             title={SEO_INFO.school[this.getKeyWord()].title}
                             description={SEO_INFO.school[this.getKeyWord()].description}
                             image={SEO_INFO.school.image}/>
                        :
                        <SEO page="school"
                             title={SEO_INFO.school.title}
                             description={SEO_INFO.school.description}
                             image={SEO_INFO.school.image}/>
                }
            </div>
        );
    }
}

export default School;

