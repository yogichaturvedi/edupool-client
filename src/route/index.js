/**
 * Created by Yogesh Chaturvedi on 11-06-2017.
 */
import React, {Component} from 'react';
import {browserHistory, IndexRoute, Route, Router} from 'react-router';
import Login from './user/login';
import Fake from './user/fake';
import Header from '../component/header';
import App from '../component/app';
import ForgotPassword from './user/forgot-password';
import SignUp from './user/signup';
import Home from "./home/index";
import School from "./school/index";
import College from "./college/index";
import Exam from "./exam/index";
import Result from "./result/index";
import StudyMaterial from "./study/index";
import Event from "./event/index";
import DetailView from "../component/detail-view/index";
import Coaching from "./coaching/index";
// import Add from "./admin/add";
// import Edit from "./admin/edit";
import AboutUs from "./about-us/index";
import ResultDetailView from "../component/detail-view/result-detail-view";
import NotFound from "../component/not-found/index";
import UiStore from './store/ui-store'
import ExamDetail from "../component/detail-view/exam";
import EventDetail from "../component/detail-view/event";
import ArticleAndNews from "./article-news/index";
import ArticleNewsDetail from '../component/detail-view/article-news'
import AddEditArticleNews from "../component/admin-component/add-edit/article-news";
import ManageCourse from "../component/admin-component/component/manage-course";

class Routes extends Component {

    constructor(props) {
        super(props);
        this.handleLoginRedirect = this.handleLoginRedirect.bind(this);
        this.handleRouteEnter = this.handleRouteEnter.bind(this);
        this.handleRouteChange = this.handleRouteChange.bind(this);
        this.handleProtectedRoute = this.handleProtectedRoute.bind(this);
    }

    handleLoginRedirect(nextState, replace) {
        if (localStorage.getItem("isUserLoggedIn") === 'true') {
            localStorage.setItem('activeTab', '/home');
            replace('/home');
        }
    }

    handleRouteEnter(nextRoute, replace) {
        let activeTab = localStorage.getItem("activeTab");
        if (activeTab !== 'home') {
            browserHistory.push("/home");
        }
    }

    handleRouteChange(previousRoute, nextRoute, replace) {
        this.handleProtectedRoute(nextRoute, replace);
    }

    handleProtectedRoute(nextRoute, replace) {
        let nextroute = localStorage.getItem("activeTab") ? localStorage.getItem("activeTab") : "home";
        replace("/" + nextroute);
    }

    redirectToDahboard() {
        browserHistory.push("/");
    }

    onRouteChange(){
        UiStore.reset();
    }

    render() {
        return (
            <Router history={browserHistory} {...this.props} onUpdate={() => window.scrollTo(0, 0)}>
                <Route path="/" component={App}  onChange={this.onRouteChange}>
                    <IndexRoute component={Fake}/>
                    <Route path="/" component={Header}>
                        {/*<Route component={Role}>*/}
                        <Route path="/home" component={Home}/>
                        <Route exact path="/school">
                            <IndexRoute component={School}/>
                            <Route path=":id" component={DetailView}/>
                        </Route>
                        <Route path="/college">
                            <IndexRoute component={College}/>
                            <Route path=":id" component={DetailView}/>
                        </Route>
                        <Route path="/coaching">
                            <IndexRoute component={Coaching}/>
                            <Route path=":id" component={DetailView}/>
                        </Route>
                        <Route path="/exam">
                            <IndexRoute component={Exam}/>
                            <Route path=":id" component={ExamDetail}/>
                        </Route>
                        <Route path="/result">
                            <IndexRoute component={Result}/>
                            <Route path=":id" component={ResultDetailView}/>
                        </Route>
                        <Route path="/event">
                            <IndexRoute component={Event}/>
                            <Route path=":id" component={EventDetail}/>
                        </Route>
                        <Route path="/study-material" component={StudyMaterial}/>
                        <Route exact path="/article">
                            <IndexRoute component={ArticleAndNews}/>
                            <Route path="add" component={AddEditArticleNews}/>
                            <Route path="edit/:id" component={AddEditArticleNews}/>
                            <Route path="view/:id" component={ArticleNewsDetail}/>
                        </Route>
                        <Route exact path="/news">
                            <IndexRoute component={ArticleAndNews}/>
                            <Route path="add" component={AddEditArticleNews}/>
                            <Route path="edit/:id" component={AddEditArticleNews}/>
                            <Route path="view/:id" component={ArticleNewsDetail}/>
                        </Route>
                        {/*<Route path="/add" component={Add}/>*/}
                        {/*<Route path="/edit" component={Edit}/>*/}
                        <Route path="/manage-course" component={ManageCourse}/>
                        <Route path="/about-us" component={AboutUs}/>
                    </Route>
                    <Route path="/verify-user" component={ForgotPassword}/>
                    <Route path="/signup" component={SignUp}/>
                    <Route path="/oolala" component={Login} onEnter={this.handleLoginRedirect}/>
                    <Route path="*" component={NotFound} status={404}/>
                </Route>
            </Router>

        );
    }

}

export default Routes;