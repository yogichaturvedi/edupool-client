import React from "react";
import './style.scss';
import {Button, Form, Grid, Input} from "semantic-ui-react";
import Slider from "react-slick";
import _ from "lodash";
import {browserHistory} from "react-router";
import Store from "../../../route/store/store";
import {isEmailValid} from "../../../util/util";
import LazyLoad from 'react-lazyload';

const userDetailsSliderSettings = {
    dots: false,
    autoplay: false,
    arrows: false,
    swipeToSlide: false,
    draggable: false,
    swipe: false,
    accessibility: false
};

class JoinUs extends React.Component {

    constructor(props) {
        super(props);
        this.resetUser = this.resetUser.bind(this);
        this.isErrorPresent = true;
        this.state = {
            showAddDetailsForm: false,
            user: {name: '', email: '', instituteName: '', mobile: '', instituteType: 'school'},
            error: {email: false, mobile: false, name: false, instituteName: false}
        };
    }

    onFormDataChange(field, value) {
        let user = this.state.user;
        user[field] = value;
        this.setState({user: user});
        this.validateForm(field);
    }

    validateAndSubmitDetails(e) {
        Store.update(this.state.user);
        let $this = this;
        _.each(['name', 'email', 'mobile', 'instituteName'], (field) => {
            $this.validateForm(field);
        });
        if (!this.isErrorPresent) {
            browserHistory.push("/add?type=" + this.state.user.instituteType);
        }
    }

    moveToSlide(slideNumber) {
        this.refs.slider.slickGoTo(slideNumber);
        this.resetUser();
    }

    resetUser() {
        this.setState({
            user: {name: '', email: '', instituteName: '', mobile: '', instituteType: 'school'},
            error: {email: false, mobile: false, name: false, instituteName: false}
        });
    }

    onInstituteTypeChange(e, value) {
        let user = this.state.user;
        user.instituteType = value;
        this.setState({user: user});
    }

    validateForm(field) {
        let error = this.state.error;
        if (field === 'email') {
            error.email = !isEmailValid(this.state.user.email);
            this.setState(this.state.error);
        }
        else if (field === 'name') {
            error.name = (this.state.user.name.trim() === "");
            this.setState(this.state.error);
        }
        else if (field === 'mobile') {
            error.mobile = !(this.state.user.mobile).match(/^\d{10}$/);
            this.setState(this.state.error);
        }
        else if (field === 'instituteName') {
            error.instituteName = (this.state.user.instituteName.trim() === "");
        }

        this.setState({error: error});
        let $this = this;
        this.isErrorPresent = _.some(['name', 'email', 'mobile', 'instituteName'], (errorName) => {
            if (this.state.user[errorName].trim() === "") {
                return true;
            }
            return $this.state.error[errorName] === true;
        });
    }

    render() {
        return (
            <Grid columns={2} style={{margin: 0}} className="join-us-container">
                <LazyLoad>
                    <Grid.Column computer={8} mobile={16} className="left-container"/>
                </LazyLoad>
                <Grid.Column computer={8} mobile={16} className="right-container bg-extra-dark-gray">
                    <div className="add_details_form_container">
                        <Slider ref='slider' {...userDetailsSliderSettings}>
                            <div className="add_here_text">
                                <div className="heading">
                                    You are few steps<br/>away from getting listed<br/>with us
                                </div>
                                <div className="description-container">
                                    Haven't found your organisation yet ? No worries, If you represent a School, College
                                    and Coaching institute and interested in partnering with us, just press the below
                                    button and fill the form with required information.
                                </div>
                                <Button size="mini" className="animated-button"
                                        onClick={() => this.moveToSlide(1)}>Click
                                    Here</Button>
                            </div>
                            <div className="add_here_form">
                                <Form onSubmit={(e) => this.validateAndSubmitDetails(e)}>
                                    <div className="heading text-uppercase">Enter Your details here</div>
                                    <Form.Input className={this.state.error.name ? 'error' : ''}
                                                name="name"
                                                size="medium"
                                                value={this.state.user.name}
                                                placeholder="Name"
                                                onChange={(e) => this.onFormDataChange(e.target.name, e.target.value)}/>

                                    <Form.Input className={this.state.error.instituteName ? 'error' : ''}
                                                name="instituteName"
                                                size="medium"
                                                value={this.state.user.instituteName}
                                                placeholder="Institute Name"
                                                onChange={(e) => this.onFormDataChange(e.target.name, e.target.value)}/>
                                    <Form.Field className={this.state.error.email ? 'error' : ''}>
                                        <Input name="email"
                                               size="medium"
                                               value={this.state.user.email}
                                               placeholder="Email"
                                               onChange={(e) => this.onFormDataChange(e.target.name, e.target.value)}/>
                                    </Form.Field>
                                    <Form.Field
                                        className={this.state.error.mobile ? 'input-without-controls error' : 'input-without-controls'}>
                                        <Input name="mobile"
                                               size="medium"
                                               type="number"
                                               value={this.state.user.mobile}
                                               placeholder="Contact Number"
                                               onChange={(e) => this.onFormDataChange(e.target.name, e.target.value)}/>
                                    </Form.Field>
                                    <Form.Field>
                                        <Grid columns={3}>
                                            <Grid.Column width={5}>
                                                <div
                                                    className="custom_checkbox">
                                                    <input type="radio"
                                                           name="institute_type_radio"
                                                           id="institute_type_radio_1"
                                                           checked={this.state.user.instituteType === 'school'}
                                                           onChange={(e) => this.onInstituteTypeChange(e, 'school')}/>
                                                    <label
                                                        htmlFor="institute_type_radio_1">School</label>
                                                    <div className="check">
                                                        <div
                                                            className="inside"/>
                                                    </div>
                                                </div>
                                            </Grid.Column>
                                            <Grid.Column width={5}>
                                                <div
                                                    className="custom_checkbox">
                                                    <input type="radio"
                                                           name="institute_type_radio"
                                                           id="institute_type_radio_2"
                                                           checked={this.state.user.instituteType === 'college'}
                                                           onChange={(e) => this.onInstituteTypeChange(e, 'college')}/>
                                                    <label
                                                        htmlFor="institute_type_radio_2">College</label>
                                                    <div className="check">
                                                        <div
                                                            className="inside"/>
                                                    </div>
                                                </div>
                                            </Grid.Column>
                                            <Grid.Column width={6}>
                                                <div
                                                    className="custom_checkbox">
                                                    <input type="radio"
                                                           name="institute_type_radio"
                                                           id="institute_type_radio_3"
                                                           checked={this.state.user.instituteType === 'coaching'}
                                                           onChange={(e) => this.onInstituteTypeChange(e, 'coaching')}/>
                                                    <label
                                                        htmlFor="institute_type_radio_3">Coaching</label>
                                                    <div className="check">
                                                        <div
                                                            className="inside"/>
                                                    </div>
                                                </div>
                                            </Grid.Column>
                                        </Grid>
                                    </Form.Field>
                                </Form>
                                <Form>
                                    <Grid columns={2}>
                                        <Grid.Column>
                                            <Button content='Cancel' fluid
                                                    size="tiny"
                                                    onClick={() => this.moveToSlide(0)}/>
                                        </Grid.Column>
                                        <Grid.Column>
                                            <Button content='Proceed' fluid
                                                    size="tiny"
                                                    onClick={(e) => {
                                                        this.validateAndSubmitDetails(e)
                                                    }}/>
                                        </Grid.Column>
                                    </Grid>
                                </Form>
                            </div>
                        </Slider>
                    </div>
                </Grid.Column>
            </Grid>
        )
    }
}

export default JoinUs;