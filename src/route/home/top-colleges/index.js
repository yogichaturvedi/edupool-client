import React from "react";
import './style.scss';
import {Grid, Icon, Transition} from "semantic-ui-react";
import {Link} from "react-router";
import CustomImage from "../../../component/image/index";
import LazyLoad from 'react-lazyload';

class TopColleges extends React.Component {

    transitionDuration = [ 1000, 1000, 2000, 2000 ];

    redirectLink = (basicDetail)=>{
        return `/college/${basicDetail.id}`;
    };

    getFormattedTime(date) {
        return new Date(date).toLocaleDateString();
    }

    getAddress = address => (address.city ? address.city + ", " + address.state : address.state) || 'Not Available';

    render() {
        return (
            <Grid columns={2} style={{margin: 0}} className="top-colleges-container">
                <Grid.Column computer={8} mobile={16} className="left-container"/>
                <Grid.Column computer={8} mobile={16} className="right-container">
                    <div>
                        <div className="heading text-extra-dark-gray">Top Featured Colleges Of India
                        </div>
                        <div className="description">The best search engine to find the best and top-rated Universities
                            and Colleges in India. Find the detailed information regarding the admission process,
                            courses offered, fee structure, verified rating and feedback of the thousands of colleges
                            and universities in India.
                        </div>
                        <Grid columns={2} className="college-list" padded>
                            <Grid.Row columns={2}>
                                {
                                    this.props.colleges.map((college, index) => {
                                        return <Grid.Column key={index} className="college" computer={8} mobile={16}>
                                            <LazyLoad once>
                                                <Transition animation='vertical flip' duration={this.transitionDuration[index]} transitionOnMount={true}>
                                                    <div>
                                                        <div className="container">
                                                            <div className="image-container">
                                                                <CustomImage src={college.imageUrl + "?"} width="100%" height="100%" alt={college.title}/>
                                                            </div>
                                                            <div className="overlay-content-container">
                                                                <div className="location max-1-line" title={this.getAddress(college.address)}>
                                                                    <Icon name="marker" size="small"/><span>{this.getAddress(college.address)}</span>
                                                                </div>
                                                                <Link className="title max-2-lines" to={this.redirectLink(college)} title={college.title}>
                                                                    {college.title}
                                                                </Link>
                                                            </div>
                                                        </div>
                                                        <div className="description max-2-lines">{college.description}</div>
                                                    </div>
                                                </Transition>
                                            </LazyLoad>
                                        </Grid.Column>
                                    })
                                }
                            </Grid.Row>
                        </Grid>
                    </div>
                </Grid.Column>
            </Grid>

        )
    }
}

export default TopColleges;