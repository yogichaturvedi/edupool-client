/**
 * Created by Amit on 12-03-2018.
 */
import React from "react";
import {Grid, Icon, Segment} from "semantic-ui-react";
import {Link} from "react-router";
import FooterInfo from "../../../static-data/footer.json";
import _ from "lodash";
import './style.scss';

class Footer extends React.Component {

    goToSocialSite = link => window.open(link, '_blank');

    constructor(props) {
        super(props);
        this.state = {active: FooterInfo.socialSites[0]}
        this.switchClass = this.switchClass.bind(this);
    }

    componentDidMount() {
        this.switchClass();
    }

    componentWillUnmount() {
        if (this.timout) {
            clearTimeout(this.timout);
        }
    }

    switchClass() {
        let $this = this;
        if (this.state.active.name !== FooterInfo.socialSites[FooterInfo.socialSites.length - 1].name) {
            let currentIndex = _.findIndex(FooterInfo.socialSites, {'name': this.state.active.name});
            currentIndex += 1;
            this.setState({active: FooterInfo.socialSites[currentIndex]});
        }
        else {
            this.setState({active: FooterInfo.socialSites[0]});
        }
        this.timout = setTimeout(function () {
            $this.switchClass();
        }, 1000)
    }

    render() {
        return (
            <Segment className="home-footer-container">
                <Grid columns={4}>
                    <Grid.Column computer={4} mobile={16}>
                        <h3>
                            <Link to={'/college?search=mba&keyword=Top-MBA-Colleges'}>Top MBA Colleges</Link>
                        </h3>
                        <h3>
                            <Link to={'/college?search=bcom&keyword=Top-BCom-Colleges'}>Top B.Com Colleges</Link>
                        </h3>
                        <h3>
                            <Link to={'/college?search=btech&keyword=Top-BTech-Colleges'}>Top BTech Colleges</Link>
                        </h3>
                        <h3>
                            <Link to={'/college?search=mca&keyword=Top-MCA-Colleges'}>Top MCA Colleges</Link>
                        </h3>
                    </Grid.Column>
                    <Grid.Column computer={4} mobile={16}>
                        <h3>
                            <Link to={'/college?search=udaipur&keyword=Colleges-In-Udaipur'}>Colleges In Udaipur</Link>
                        </h3>
                        <h3>
                            <Link to={'/college?search=delhi&keyword=Colleges-In-Delhi'}>Colleges In Delhi/NCR</Link>
                        </h3>
                        <h3>
                            <Link to={'/college?search=mumbai&keyword=Colleges-In-Mumbai'}>Colleges In Mumbai</Link>
                        </h3>
                        <h3>
                            <Link to={'/college?search=jaipur&keyword=Colleges-In-Jaipur'}>Colleges In Jaipur</Link>
                        </h3>
                    </Grid.Column>
                    <Grid.Column computer={4} mobile={16}>
                        <h3>
                            <Link to={'/school?search=cbse&keyword=Top-CBSE-Schools'}>Top CBSE Schools</Link>
                        </h3>
                        <h3>
                            <Link to={'/school?search=pre primary&keyword=Top-Pre-Primary-Schools'}>Top Pre-Primary
                                Schools</Link>
                        </h3>
                        <h3>
                            <Link to={'/school?search=primary&keyword=Top-Primary-Schools'}>Top Primary Schools</Link>
                        </h3>
                        <h3>
                            <Link to={'/school?search=boarding&keyword=Top-Boarding-Schools'}>Top Boarding Schools</Link>
                        </h3>
                    </Grid.Column>
                    <Grid.Column computer={3} mobile={16}>
                        <h3>
                            <Link>Our Team</Link>
                        </h3>
                        <h3>
                            <Link to={'/about-us'}>About Us</Link>
                        </h3>
                        <div style={{color: 'rgba(255, 255, 255, .7)', margin: 10}}>
                            Follow Us On
                        </div>
                        <div className="display-inline-block vertical-align-middle social-icons-wrapper">
                            {
                                FooterInfo.socialSites.map((socialSite, index) => {
                                    return <Icon key={index}
                                                 className={"social-link-" + socialSite.name + " " + (this.state.active.name === socialSite.name ? "selected" : "")}
                                                 name={socialSite.icon} size="large"
                                                 onClick={(e) => this.goToSocialSite(socialSite.link)}
                                                 inverted/>
                                })
                            }
                        </div>
                    </Grid.Column>
                </Grid>
            </Segment>
        )
    }
}

export default Footer;