import React from "react";
import {Accordion, Divider, Grid, Icon, Message} from "semantic-ui-react";
import './style.scss';
import {Link} from "react-router";
import Slider from "react-slick";
import {getParsedDate} from "../../../util/util";
import LazyLoad from 'react-lazyload';
import CustomImage from "../../../component/image/index";

const CarouselSettings = {
    autoPlaySpeed: 7000,
    autoplay: true,
    speed: 700,
    arrows: false,
    vertical: true,
    infinite: true,
    slidesToScroll: 1,
};

// @lazyload()
class ArticleNews extends React.Component {

    transitionDuration = [1000, 1500, 2000, 2500];

    redirectLink = (type, id) => {
        return `/${type}/view/${id}`;
    };


    getArticles = (articles) => {
        return this.props.article.map((article, i) => {
            return {
                title: {
                    content:
                        <div className="title-wrapper text-extra-dark-gray">
                            <span className="index alt-font">{`0${i + 1}`}</span>
                            <div className="heading alt-font max-1-line">{article.title}</div>
                            <Icon color="black" className="arrows" name="angle down"/>
                            <Icon color="black" className="arrows" name="angle up"/></div>,
                    key: `title-${i}`,
                },
                content: {
                    content: <div className="content-wrapper">
                        <Link to={this.redirectLink('article', article.id)}>
                            <Message info icon>
                                {/*<Image as="icon" src={article.imageUrl}/>*/}
                                <CustomImage src={article.imageUrl + "?"} height="100%" width="100%" alt={article.title}/>
                                <Message.Content>
                                    <p className="max-3-lines">{article.description}</p>
                                    <div className="created-info">
                                        <Icon name="user"
                                              size="small"/>&nbsp;&nbsp;{article.createdBy}&nbsp;&nbsp;&nbsp;
                                        <Icon name="calendar alternate outline"
                                              size="small"/>&nbsp;&nbsp;{getParsedDate(article.createdAt)}
                                    </div>
                                </Message.Content>

                            </Message>
                        </Link>
                    </div>,
                    key: `content-${i}`,
                }
            }
        })
    };

    render() {
        return (
            <Grid divided>
                <Grid.Row width="equal" columns={2}>
                    <Grid.Column className="article-wrapper" computer={8} mobile={16}>
                        <h5 className="heading-wrapper">
                            Latest Articles
                            <span className="description">Find all type of article with us having categories like education, medical, entertainment, food, culture and many more</span>
                        </h5>
                        <LazyLoad once>
                            <Accordion panels={this.getArticles(this.props.article)}/>
                        </LazyLoad>
                    </Grid.Column>
                    <Grid.Column className="news-wrapper" computer={8} mobile={16}>
                        <h5 className="heading-wrapper">
                            Latest News
                            <span className="description">Find all type of news with us having categories like education, medical, entertainment, food, culture and many more</span>
                        </h5>
                        <Slider {...CarouselSettings}
                                slidesToShow={this.props.news.length < 4 ? this.props.news.length : 4}>
                            {
                                this.props.news.map((news, index) => {
                                    return <div className="info" key={index}>
                                        <Grid>
                                            <Grid.Column width={2}>
                                                <LazyLoad once>
                                                    <Link to={this.redirectLink('news', news.id)}>
                                                        <CustomImage style={{display: 'inline'}} src={news.imageUrl + "?"} height={40} width={40} alt={news.title}/>
                                                    </Link>
                                                </LazyLoad>
                                            </Grid.Column>
                                            <Grid.Column width={14} style={{paddingBottom: 0, paddingTop: 0}}>
                                                <Link to={this.redirectLink('news', news.id)}>
                                                    <div className="title max-1-line">{news.title}</div>
                                                </Link>
                                                <div className="created-info">
                                                    <Icon name="user" size="small"/>
                                                    &nbsp;&nbsp;{news.createdBy}&nbsp;&nbsp;&nbsp;
                                                    <Icon name="calendar alternate outline" size="small"/>
                                                    &nbsp;&nbsp;{getParsedDate(news.createdAt)}
                                                </div>
                                                <p className="description max-2-lines">{news.description}</p>
                                                <Divider/>
                                            </Grid.Column>
                                        </Grid>
                                    </div>
                                })
                            }
                        </Slider>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}

export default ArticleNews;