import React from "react";
import {Grid, Transition} from "semantic-ui-react";
import {Link} from "react-router";
import CustomImage from "../../../component/image/index";
import LazyLoad from 'react-lazyload';
import './style.scss';

class TopExams extends React.Component {

    transitionDuration = [ 1000, 1000, 2000, 2000 ];

    redirectLink = (basicDetail)=>{
        return `/exam/${basicDetail.id}`;
    };
    getFormattedTime(date) {
        return new Date(date).toLocaleDateString();
    }
    render() {
        return (
            <Grid columns={2} style={{margin: 0}} className="top-exams-container" reversed='mobile vertically'>
                <Grid.Column computer={8} mobile={16} className="left-container">
                    {/*<div className="heading text-extra-dark-gray">Top Featured Exams Of India*/}
                    {/*</div>*/}
                    {/*<div className="description">The best search engine to find the best and top-rated Universities*/}
                        {/*and Exams in India. Find the detailed information regarding the admission process,*/}
                        {/*courses offered, fee structure, verified rating and feedback of the thousands of exams*/}
                        {/*and universities in India.*/}
                    {/*</div>*/}
                    <Grid columns={2} className="exam-list" padded>
                        <Grid.Row columns={2} centered>
                            {
                                this.props.exams.map((exam, index) => {
                                    return <Grid.Column key={index} className="exam" computer={8} mobile={16}>
                                        <LazyLoad once>
                                            <Transition animation='vertical flip' duration={this.transitionDuration[index]} transitionOnMount={true}>
                                                <div>
                                                    <div className="container">
                                                        <div className="image-container">
                                                            <CustomImage src={exam.imageUrl + "?"} width="100%" height="100%" alt={exam.title}/>
                                                            {/*<img src={exam.imageUrl}/>*/}
                                                        </div>
                                                        <div className="overlay-content-container">
                                                            <Link className="title max-2-lines" to={this.redirectLink(exam)} title={exam.title}>
                                                                {exam.title}
                                                            </Link>
                                                        </div>
                                                    </div>
                                                    <div className="description-wrapper">
                                                        <div className="exam-date text-uppercase">
                                                            {this.getFormattedTime(exam.createdAt) + " | " + exam.createdBy}
                                                        </div>
                                                        <div className="description max-3-lines">{exam.description}</div>
                                                    </div>
                                                </div>
                                            </Transition>
                                        </LazyLoad>
                                    </Grid.Column>
                                })
                            }
                        </Grid.Row>
                    </Grid>
                </Grid.Column>
                <Grid.Column computer={8} mobile={16} className="right-container">
                    <div className="overlay-content-container">
                        <div className="heading text-extra-dark-gray">Top Featured Exams Of India</div>
                        <div className="description">
                            The best search engine to find the best and top-rated Universities
                            and Exams in India. Find the detailed information regarding the admission process,
                            courses offered, fee structure, verified rating and feedback of the thousands of exams
                            and universities in India.
                        </div>
                    </div>
                </Grid.Column>
            </Grid>

        )
    }
}

export default TopExams;