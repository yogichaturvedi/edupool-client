import React from "react";
import {Grid, Icon, Transition} from "semantic-ui-react";
import {Link} from "react-router";
import CustomImage from "../../../component/image/index";
import LazyLoad from 'react-lazyload';
import './style.scss';

class TopCoachings extends React.Component {

    transitionDuration = [ 1000, 1000, 2000, 2000 ];

    redirectLink = (basicDetail) => {
        return `/coaching/${basicDetail.id}`;
    };

    getFormattedTime(date) {
        return new Date(date).toLocaleDateString();
    }

    getAddress = address => (address.city ? address.city + ", " + address.state : address.state) || 'Not Available';

    render() {
        return (
            <div className="coachings-list-container">
                <Grid textAlign='center' className="heading-wrapper">
                    <Grid.Row textAlign="center" centered>
                        <Grid.Column computer={12} mobile={16} textAlign="center">
                            <div className="heading text-extra-dark-gray">Top Featured Coachings Of India</div>
                            <div className="description">
                                Edupool.in provides the extensive information of various Government Exam, Competitive Exam, College and School coaching institutions in India.
                                Explore the verified coaching institutions in India by Courses offered, fee, location, result etc.
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <Grid columns={2} className="coachings-list" padded>
                    <Grid.Row columns={2} textAlign="center" centered>
                        {
                            this.props.coachings.map((coaching, index) => {
                                return <Grid.Column className="coaching-column" key={index} computer={8} mobile={16}>
                                    <LazyLoad once>
                                        <Transition animation='horizontal flip' duration={this.transitionDuration[index]} transitionOnMount={true}>
                                            <Grid columns={2}>
                                                <Grid.Column mobile={16} computer={6} className="coaching-image-column">
                                                    <div className="coaching-image">
                                                        <CustomImage src={coaching.imageUrl + "?"} width="100%" height="160px" alt={coaching.title}/>
                                                        <div className="location max-1-line" title={this.getAddress(coaching.address)}>
                                                            <Icon name="marker" size="small"/><span>{this.getAddress(coaching.address)}</span>
                                                        </div>
                                                    </div>
                                                </Grid.Column>
                                                <Grid.Column mobile={16} computer={10} className="coaching-description-column">
                                                    <div className="coaching-title  max-1-lines">
                                                        <Link to={this.redirectLink(coaching)} title={coaching.title}>
                                                            {coaching.title}
                                                        </Link>
                                                    </div>
                                                    <div className="coaching-date text-uppercase">
                                                        {this.getFormattedTime(coaching.createdAt) + " | " + coaching.createdBy}
                                                    </div>
                                                    <p className="coaching-description computerOnly max-3-lines">
                                                        {coaching.description}
                                                    </p>
                                                </Grid.Column>
                                            </Grid>
                                        </Transition>
                                    </LazyLoad>
                                </Grid.Column>
                            })
                        }
                    </Grid.Row>
                </Grid>
            </div>
        )
    }
}

export default TopCoachings;