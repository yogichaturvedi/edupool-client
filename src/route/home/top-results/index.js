import React from "react";
import {Card, Feed, Transition} from "semantic-ui-react";
import {Link} from "react-router";
import LazyLoad from 'react-lazyload';
import './style.scss';
import CustomImage from "../../../component/image/index";

class TopResults extends React.Component {

    transitionDuration = [1000, 1000, 2500, 2500 ];

    redirectLink = (basicDetail) => {
        return `/result/${basicDetail.id}`;
    };

    getFormattedTime(date) {
        return new Date(date).toLocaleString();
    }

    render() {
        return (
            <div className="results-list-container">
                <div className="results-list">
                    <Card color="blue" raised fluid>
                        <Card.Content className="result-list-header">
                            <Card.Header>
                                Latest Results Of India
                            </Card.Header>
                            <Card.Description>
                                The best search engine to find the best and top-rated results in India. Find the detailed information regarding the admission process,
                                fee structure, verified rating in India.
                            </Card.Description>
                        </Card.Content>
                        <Card.Content>
                            <Feed>
                                {
                                    this.props.results.map((result, index) => {
                                        return <LazyLoad offset={100} key={index} once>
                                            <Transition animation={ ((index+1) % 2 === 0) ? 'fly right' : 'fly left'} duration={2500} transitionOnMount={true} style={{display:'flex !important'}}>
                                                <Feed.Event key={index}>
                                                    <Feed.Label
                                                        image={<CustomImage src={result.imageUrl + "?"} width="100%" height="100%" alt={result.title} circular/>}/>
                                                    <Feed.Content>
                                                        <Feed.Date content={this.getFormattedTime(result.createdAt) + " | " + result.createdBy}/>
                                                        <Feed.Summary>
                                                            <Link to={this.redirectLink(result)} title={result.title}>
                                                                {result.title}
                                                            </Link>
                                                        </Feed.Summary>
                                                    </Feed.Content>
                                                </Feed.Event>
                                            </Transition>
                                        </LazyLoad>
                                    })
                                }
                            </Feed>
                        </Card.Content>
                    </Card>
                </div>
            </div>
        )
    }
}

export default TopResults;