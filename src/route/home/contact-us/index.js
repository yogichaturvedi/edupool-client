import React from "react";
import {Button, Form, Grid, Input, Segment, TextArea} from "semantic-ui-react";
import './style.scss';
import {browserHistory} from "react-router";
import ContactUsStore from "../../store/contact-us-store";
import _ from "lodash";
import {observable} from "mobx";
import CustomImage from "../../../component/image/index";
import LazyLoad from 'react-lazyload';

class ContactForm extends React.Component {
    @observable isErrorPresent = true;

    constructor(props) {
        super(props);
        this.state = {
            user: {name: '', email: '', subject: '', mobile: '', message: ''},
            error: {email: false, mobile: false, name: false, subject: false}
        };
    }

    getFormattedTime(date) {
        return new Date(date).toLocaleDateString();
    }

    onFormDataChange(field, value) {
        let user = this.state.user;
        user[field] = value;
        this.setState({ user : user});
        this.validateForm(field);
    }

    validateForm(field) {
        let error = this.state.error;
        if (field === 'email') {
            error.email = !this.isEmailValid(this.state.user.email);
            this.setState(this.state.error);
        }
        else if (field === 'name') {
            error.name = (this.state.user.name.trim() === "");
            this.setState(this.state.error);
        }
        else if (field === 'mobile') {
            error.mobile = !(this.state.user.mobile).match(/^\d{10}$/);
            this.setState(this.state.error);
        }
        else if (field === 'subject') {
            error.subject = (this.state.user.subject.trim() === "");
        }
        this.setState({error : error});
        let $this = this;
        this.isErrorPresent = _.some(['name', 'email', 'mobile', 'subject'], (errorName) => {
            if (this.state.user[errorName].trim() === "") {
                return true;
            }
            return $this.state.error[errorName] === true;
        });
    }

    submitDetails() {
        let $this = this;
        //Validate Details
        _.each(['name', 'email', 'mobile', 'subject'], (field) => {
            $this.validateForm(field);
        });
        if (!this.isErrorPresent) {
            ContactUsStore.send(this.state.user);
            this.setState({user: {name: '', email: '', subject: '', mobile: '', message: ''}})
        }
    }

    isEmailValid(email) {
        // eslint-disable-next-line
        let validEmailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return validEmailRegex.test(email);
    }

    render() {
        return (
            <div className="contact-form">
                <Grid columns={2} padded>
                    <Grid.Column computer={8} mobile={16}>
                        <Segment className="contact-form-container1">
                            <Form onSubmit={(e) => this.submitDetails(e)}>
                                <h3 style={{ color: 'black', textAlign: 'left', marginBottom: "25px"}}>Get A Quote</h3>
                                <Form.Field className={this.state.error.name ? 'error' : ''}>
                                    <Input name="name" size="large"
                                           value={this.state.user.name}
                                           placeholder="Name*"
                                           onChange={(e) => this.onFormDataChange(e.target.name, e.target.value)}/>
                                    {
                                        this.state.error.name && <div className="error-message">
                                            Name cannot be blank.
                                        </div>
                                    }
                                </Form.Field>
                                <Form.Field className={this.state.error.email ? 'error' : ''}>
                                    <Input name="email" size="large"
                                           value={this.state.user.email}
                                           placeholder="Email*"
                                           onChange={(e) => this.onFormDataChange(e.target.name, e.target.value)}/>
                                    {
                                        this.state.error.email && <div className="error-message">
                                            Enter valid email.
                                        </div>
                                    }
                                </Form.Field>
                                <Form.Field
                                    className={this.state.error.mobile ? 'input-without-controls error' : 'input-without-controls'}>
                                    <Input name="mobile" size="large" type="number"
                                           value={this.state.user.mobile}
                                           placeholder="Contact Number"
                                           onChange={(e) => this.onFormDataChange(e.target.name, e.target.value)}/>
                                    {
                                        this.state.error.mobile && <div className="error-message">
                                            Enter valid mobile number.
                                        </div>
                                    }
                                </Form.Field>
                                <Form.Field className={this.state.error.subject ? 'error' : ''}>
                                    <Input name="subject" size="large"
                                           value={this.state.user.subject}
                                           placeholder="Subject"
                                           onChange={(e) => this.onFormDataChange(e.target.name, e.target.value)}/>
                                    {
                                        this.state.error.subject && <div className="error-message">
                                            Subject cannot be blank.
                                        </div>
                                    }
                                </Form.Field>
                                <Form.Field>
                                    <TextArea name="message"
                                              value={this.state.user.message}
                                              placeholder="Your Message"
                                              rows={5}
                                              style={{resize: "none"}}
                                              onChange={(e) => this.onFormDataChange(e.target.name, e.target.value)}/>
                                </Form.Field>
                                <Form.Field>
                                    <Button content='Send Message' color="black"/>
                                </Form.Field>
                            </Form>
                        </Segment>
                    </Grid.Column>
                    <Grid.Column computer={8} mobile={16}>
                        <Segment className="contact-form-container2">
                            <div className="contact-form-image-container">
                                <LazyLoad offset={100} height={300} once>
                                    <CustomImage src="https://preview.ibb.co/kZEqhm/cole_keister_291568.jpg" alt="contact-us" width="100%" height="100%"/>
                                </LazyLoad>
                            </div>
                            <h3 style={{color: 'black', textAlign: 'left'}}>Quick Overview</h3>
                            <p style={{whiteSpace: "unset"}}>
                                EduPool is an online portal which provides the information of all the educational
                                institutions like schools, colleges and coaching in India. It is the best platform to
                                search for the best rated and verified schools, colleges and coaching in India.</p>
                            <Button content='About Company' color="black" onClick={() => {browserHistory.push("/about-us")}}/>
                        </Segment>
                    </Grid.Column>
                </Grid>
            </div>

        )
    }
}

export default ContactForm;