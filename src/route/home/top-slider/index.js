/**
 * Created by Yogesh Chaturvedi on 11-06-2017.
 */
import React, {Component} from 'react';
import TopSliderInfo from "../../../static-data/home/slider-top.json";
import LazyLoad from 'react-lazyload';
import {Carousel} from 'react-bootstrap';
import '../../../bootstrap-slider.scss';

class TopSlider extends Component {
    render() {
        return (
            <div>
                <Carousel className="slider-body" controls={false} pauseOnHover={false}>
                    {
                        TopSliderInfo.map((slider, index) => {
                            return <Carousel.Item animateIn={false} animateOut={false} key={index}>
                                <LazyLoad once>
                                <div key={"slider-image-" + index}
                                     style={{backgroundImage: 'url(' + require("../../../assets/image/" + slider.background) + ')'}}
                                     className="home-top-slider-content">
                                </div>
                                </LazyLoad>
                                <div className="slide-caption">
                                    {slider.name}
                                </div>

                            </Carousel.Item>
                        })
                    }
                </Carousel>
            </div>
        )
    }
}

export default TopSlider;

