import React from "react";
import {Grid, Icon, Segment, Transition} from "semantic-ui-react";
import './style.scss';
import {Link} from "react-router";
import CustomImage from "../../../component/image/index";
import LazyLoad from 'react-lazyload';

// @lazyload()
class RecentEvents extends React.Component {

    transitionDuration = [ 1000, 1500, 2000, 2500 ];

    getFormattedTime(date) {
        return new Date(date).toLocaleDateString();
    }

    redirectLink = (basicDetail)=>{
        return `/event/${basicDetail.id}`;
    };

    getAddress = address => (address.city ? address.city + ", " + address.state : address.state) || 'Not Available';

    render() {
        return (
            <div>
                <Grid textAlign='center' style={{padding: "70px 0 50px 0", width: "100%"}}>
                    <Grid.Row textAlign="center" centered>
                        <Grid.Column computer={2} mobile={6} textAlign="center">
                            <div className="text-outside-line-full">Latest Events</div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <Grid columns={4} className="recent-event-container" padded>
                    <Grid.Row columns={4} textAlign="center" centered>
                        {
                            this.props.events.map((event, index) => {
                                return <Grid.Column className="event-column" textAlign="center" key={index} computer={4} mobile={16}>
                                    <LazyLoad height={160} once>
                                        <Transition animation='horizontal flip' duration={this.transitionDuration[index]} transitionOnMount={true}>
                                             <Segment size="small">
                                                 <div className="event-image" style={{overflow: 'hidden'}}>
                                                     <CustomImage src={event.imageUrl + "?"} width="260px" height="160px" alt={event.title}/>
                                                     <div className="location  max-1-line" title={this.getAddress(event.address)}>
                                                         <Icon name="marker" size="small"/><span>{this.getAddress(event.address)}</span>
                                                     </div>
                                                 </div>
                                                 <div className="event-date text-uppercase">
                                                     {this.getFormattedTime(event.createdAt) + " | " + event.createdBy}
                                                 </div>
                                                 <div className="event-title">
                                                     <Link to={this.redirectLink(event)}>
                                                         {event.title}
                                                     </Link>
                                                 </div>
                                                 <div className="event-seprator-line"/>
                                                 <p className="event-description max-4-lines">
                                                     { event.description }
                                                 </p>
                                            </Segment>
                                        </Transition>
                                    </LazyLoad>
                                </Grid.Column>
                            })
                        }
                    </Grid.Row>
                </Grid>
            </div>
        )
    }
}

export default RecentEvents;