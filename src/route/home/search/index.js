import React from "react";
import './style.scss';
import Slider from "react-slick";
import HomeSearch from "../../../component/home-search/index";

const searchSliderSettings = {
    autoPlaySpeed: 8000,
    autoplay: true,
    speed: 1000,
    arrows: false,
    pauseOnHover: false,
    fade: true,
    swipeToSlide: false,
    draggable: false,
    swipe: false
};
const fakeData = [
    {
        title: "schools",
        description: 'Search Verified schools in india',
        value: 2604,
        titleColor: "red",
        numberColor: "green"
    },
    {
        title: "colleges",
        description: 'Search Best college of your preference in india',
        value: 2145,
        titleColor: "teal",
        numberColor: "violet"
    },
    {
        title: "coachings",
        description: 'Find best coaching centers in your city',
        value: 354,
        titleColor: "blue",
        numberColor: "orange"
    },
    {
        title: "exams",
        description: 'Find upcoming and latest exams in india',
        value: 5462,
        titleColor: "olive",
        numberColor: "purple"
    },
    {
        title: "events",
        description: 'Browse the various educational events in india',
        value: 7541,
        titleColor: "pink",
        numberColor: "grey"
    },
    {
        title: "results",
        description: 'Search results from various universities in india',
        value: 6254,
        titleColor: "purple",
        numberColor: "orange"
    },
];

class SearchSection extends React.Component {

    getSliderPosition() {
        let eleHeight = window.screen.width <= 710 ? 300 : 380;
        return ((window.screen.height) / 2) - (eleHeight / 2);

    }

    render() {
        return (
            <div className="upper-container">
                <div className="search-text-slider-wrapper" style={{textAlign: 'center'}}>
                    <Slider {...searchSliderSettings}>
                        {
                            fakeData.map((item, index) => {
                                return <div key={"slider-container-" + index}>
                                    <div className="search-text-wrapper text-uppercase"
                                         style={{margin: "1em 0em 0.3em 0em"}}>
                                        <span className="desc"
                                            style={{color: '#022e44'}}>&nbsp;{item.description}&nbsp;</span>
                                    </div>
                                </div>
                            })
                        }
                    </Slider>
                </div>
                <div className="search-container">
                    <HomeSearch/>
                </div>
            </div>
        )
    }
}

export default SearchSection;