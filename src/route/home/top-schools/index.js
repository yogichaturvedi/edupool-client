import React from "react";
import {Grid, Icon, Transition} from "semantic-ui-react";
import {Link} from "react-router";
import CustomImage from "../../../component/image/index";
import LazyLoad from 'react-lazyload';
import './style.scss';

class TopSchools extends React.Component {

    transitionDuration = [1000, 1000, 2500, 2500];

    redirectLink = (basicDetail) => {
        return `/school/${basicDetail.id}`;
    };

    getFormattedTime(date) {
        return new Date(date).toLocaleDateString();
    }

    getAddress = address => (address.city ? address.city + ", " + address.state : address.state) || 'Not Available';

    render() {
        return (
            <div className="schools-list-container">
                <Grid textAlign='center' className="heading-wrapper">
                    <Grid.Row textAlign="center" centered>
                        <Grid.Column computer={12} mobile={16} textAlign="center">
                            <div className="heading text-extra-dark-gray">Top Featured Schools Of India</div>
                            <div className="description">
                                The best search engine to find the best and top-rated schools in India. Find the
                                detailed information regarding the admission process,
                                fee structure, verified rating in India.
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <Grid columns={2} className="schools-list" padded>
                    <Grid.Row columns={2} textAlign="center" centered>
                        {
                            this.props.schools.map((school, index) => {
                                return <Grid.Column className="school-column" key={index} computer={8} mobile={16}>
                                    <LazyLoad once>
                                        <Transition animation='horizontal flip'
                                                    duration={this.transitionDuration[index]} transitionOnMount={true}>
                                            <Grid columns={2}>
                                                <Grid.Column mobile={16} computer={6} className="school-image-column">
                                                    <div className="school-image">
                                                        <CustomImage src={school.imageUrl + "?"} width="100%"
                                                                     height="160px" alt={school.title}/>
                                                        <div className="location max-1-line" title={this.getAddress(school.address)}>
                                                            <Icon name="marker" size="small"/><span>{this.getAddress(school.address)}</span>
                                                        </div>
                                                    </div>
                                                </Grid.Column>
                                                <Grid.Column mobile={16} computer={10}
                                                             className="school-description-column">
                                                    <div className="school-title max-1-line">
                                                        <Link to={this.redirectLink(school)} title={school.title}>
                                                            {school.title}
                                                        </Link>
                                                    </div>
                                                    <div className="school-date text-uppercase">
                                                        {this.getFormattedTime(school.createdAt) + " | " + school.createdBy}
                                                    </div>
                                                    <p className="school-description computerOnly max-4-lines">
                                                        {school.description}
                                                    </p>
                                                </Grid.Column>
                                            </Grid>
                                        </Transition>
                                    </LazyLoad>
                                </Grid.Column>
                            })
                        }
                    </Grid.Row>
                </Grid>
            </div>
        )
    }
}

export default TopSchools;