/**
 * Created by Yogesh Chaturvedi on 11-06-2017.
 */
import React, {Component} from 'react';
import {Button, Grid, Icon, Segment} from "semantic-ui-react";
import {browserHistory} from "react-router";
import AppFeaturesInfo from "../../../static-data/home/app-features.json";
import _ from "lodash";
import "./styles.scss";

class AppFeatures extends Component {
    render() {
        return (
            <div>
                <Grid textAlign='center' style={{padding: '10px 40px', margin: 0}}>
                    <Grid.Row>
                        <h2 style={{color: "black"}} id="to-focus">{AppFeaturesInfo.label}</h2>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column computer={5} mobile={16} style={{margin: 'auto'}}>
                            {AppFeaturesInfo.description}
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <Grid columns={4} padded className="grid-column-container">
                    {
                        _.map(AppFeaturesInfo.sections, (section, index)=>{
                            return <Grid.Column computer={4} mobile={16} key={index}>
                                <Segment size="small" className="app-features-slide-container" style={{textAlign: 'center'}}>
                                    <div className="image-container">
                                        <Icon name={section.icon} size="huge" color="black" circular bordered inverted/>
                                    </div>
                                    <h3 className="app-features-column-heading">{section.label}</h3>
                                    <p className="description-container">
                                        {section.description}
                                    </p>
                                    <div className="more-details">
                                        <Button size="tiny" content='View Details' color="black" onClick={() => {
                                            localStorage.setItem("activeTab", section.link.split("/")[1]);
                                            browserHistory.push(section.link)
                                        }}/>
                                    </div>
                                </Segment>
                            </Grid.Column>
                        })
                    }
                </Grid>
            </div>
        )
    }
}

export default AppFeatures;

