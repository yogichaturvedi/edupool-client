/**
 * Created by Yogesh Chaturvedi on 11-06-2017.
 */
import React, {Component} from 'react';
import {observable} from 'mobx';
import {observer} from 'mobx-react';
import {Sticky} from "semantic-ui-react";
import UiStore from "../../route/store/ui-store";
import App_Const from "../../constants/constants";
import {errorMessage} from "../../util/error-formatter";
import AppError from "../../component/error/index";
import SEO from "../../component/seo/index";
import RecentEvents from "./recent-events/index";
import ContactForm from "./contact-us/index";
import SearchSection from "./search/index";
import AppFeatures from "./app-features/index";
import TopSlider from "./top-slider";
import JoinUs from "./join-us/index";
import SEO_INFO from "../../static-data/seo";
import TopColleges from "./top-colleges";
import HomeStore from "../store/home-store";
import TopSchools from "./top-schools/index";
import LazyLoad from 'react-lazyload';
import TopCoachings from "./top-coachings/index";
import TopExams from "./top-exams/index";
import TopResults from "./top-results/index";
import Footer from "./footer/index";
import ArticleNews from "./article-news";
import _ from "lodash";
import "./home.scss";

@observer
class Home extends Component {
    @observable activeMenuItem = '';
    @observable homePageData = {
        school: [],
        college: [],
        coaching: [],
        exam: [],
        event: [],
        result: [],
        article: [],
        news: []
    };
    handleContextRef = (contextRef) => this.setState({contextRef});
    onStick = () => {
        let menu = document.getElementById("computer-app-menu");
        menu.style.background = "#0a0a0b";
        const items = menu.getElementsByClassName('item');
        if(items) {
            _.each(items, item => {
                item.style.color = "white";
            });
        }
        this.setState({searchSectionTopPadding: 100})
    };
    onUnStick = () => {
        let menu = document.getElementById("computer-app-menu");
        menu.style.background = "transparent";
        const items = menu.getElementsByClassName('item');
        if(items) {
            _.each(items, item => {
                item.style.color = "#022e44";
            });
        }
        this.setState({searchSectionTopPadding: 0});
    };

    constructor(props) {
        super(props);
        this.onMenuItemSelect = this.onMenuItemSelect.bind(this);
        this.retry = this.retry.bind(this);
        this.fetchHomeInfo = this.fetchHomeInfo.bind(this);
        this.state = {
            showAddDetailsForm: false,
            user: {name: '', email: '', institute_name: '', mobile: '', institute_type: 'school'},
            error: {email: false, mobile: false},
            searchSectionTopPadding: 206.5
        };
    }

    componentWillMount() {
        localStorage.setItem("activeTab", "home");
        this.fetchHomeInfo();
    }

    componentWillUnmount() {
       this.onStick();
    }

    async fetchHomeInfo() {
        try {
            UiStore.loading = true;
            await this.fetchHomePageData();
            // UiStore.loading = false;
            UiStore.error.present = false;
            UiStore.error.message = "";
        }
        catch (error) {
            UiStore.loading = false;
            UiStore.error.present = true;
            if (error.message === App_Const.failedToFetch) {
                UiStore.error.message = App_Const.failedToFetch;
            }
            else {
                UiStore.error.message = errorMessage(error);
            }
        }
    }

    async retry() {
        UiStore.loading = true;
        UiStore.error.present = false;
        UiStore.error.message = "";
        await this.fetchHomeInfo();
    }

    async fetchHomePageData() {
        this.homePageData = await HomeStore.fetchHomePageData();
    }

    componentWillReceiveProps(props) {
        this.activeMenuItem = props.activeMenuItem;
    }

    onMenuItemSelect(key) {
        this.activeMenuItem = key;
    }

    render() {
        return (
            <div className="context-ref" ref={this.handleContextRef}>
                {
                    UiStore.error.present ?
                        <div>
                            <AppError retry={this.retry} error={UiStore.error}/>
                        </div>
                        :
                        <div>
                            <section className="top-slider" id="top-slider">
                                <TopSlider/>
                                <div className="home-search-section" id="home-search-section">
                                    <Sticky context={this.state.contextRef} style={{zIndex: 222}} onStick={this.onStick}
                                            onUnstick={this.onUnStick}>
                                        <div style={{height: "1px"}}/>
                                    </Sticky>
                                    <div id="search-section" className="search-section">
                                        <SearchSection topPadding={this.state.searchSectionTopPadding}
                                                       contextRef={this.state.contextRef}/>
                                    </div>
                                </div>
                            </section>

                            <section className="app-features" id="app-features">
                                <AppFeatures/>
                            </section>
                            <div id="join-us" className="join-us">
                                <JoinUs/>
                            </div>
                            <div className="latest-news-article">
                                <ArticleNews article={this.homePageData.article} news={this.homePageData.news}/>
                            </div>
                            <div className="latest-events">
                                <RecentEvents events={this.homePageData.event}/>
                            </div>
                            <div className="latest-schools">
                                <TopSchools schools={this.homePageData.school}/>
                            </div>
                            <div className="latest-colleges">
                                <LazyLoad>
                                    <TopColleges colleges={this.homePageData.college}/>
                                </LazyLoad>
                            </div>
                            <div className="latest-coachings">
                                <TopCoachings coachings={this.homePageData.coaching}/>
                            </div>
                            <div className="latest-exams">
                                <LazyLoad>
                                    <TopExams exams={this.homePageData.exam}/>
                                </LazyLoad>
                            </div>
                            <div className="latest-results">
                                <TopResults results={this.homePageData.result}/>
                            </div>
                            <ContactForm/>
                            <Footer/>
                        </div>
                }
                <SEO page="home"
                     title={SEO_INFO.home.title}
                     description={SEO_INFO.home.description}
                     image={SEO_INFO.home.image}/>
            </div>
        );
    }

    getSliderPosition() {
        let eleHeight = window.screen.width <= 710 ? 210 : 370;
        return ((window.screen.height + 15) / 2) - (eleHeight / 2);

    }
}

export default Home;

