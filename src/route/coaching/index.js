/**
 * Created by Yogesh Chaturvedi on 11-06-2017.
 */
import React, {Component} from 'react';
import {observable} from 'mobx';
import {observer} from 'mobx-react';
import List from "../../component/list";
import BasicStore from "../store/basic-store";
import Loading from "../../component/loading/index";
import AppError from "../../component/error/index";
import CommonApiService from "../../service/common-api";
import {errorMessage} from "../../util/error-formatter";
import UiStore from '../store/ui-store'
import App_Const from "../../constants/constants";
import _ from "lodash";
import SEO from "../../component/seo/index";
import SEO_INFO from "../../static-data/seo";
import { ListHeader } from '../../component/component';
import '../index.scss';
import ConfirmDelete from "../../component/modal/confirm-delete";
import ApplyNowCoaching from "../../component/modal/coaching-apply";
import {redirectToPage} from "../../util/util";

@observer
class Coaching extends Component {

    @observable basicDetails = [];

    constructor(props) {
        super(props);
        this.retry = this.retry.bind(this);
        this.fetchBasicData = this.fetchBasicData.bind(this);
        this.deleteDetail = this.deleteDetail.bind(this);
        this.state = {
            confirmDelete: {
                show: false, title: "", coachingToDelete: {}
            },
            // courses: [],
            showCourseModal: false,
            applyNow: false,
            appliedCoaching: {}
        };
        UiStore.error.present = false;
        UiStore.error.message = "";
    }

    componentWillMount() {
        localStorage.setItem("activeTab", "coaching");
        this.basicStore = new BasicStore("coaching");
        this.fetchBasicData(this.props.location.query.search);
        // this.getCourses();
    }

    componentWillReceiveProps(props) {
        this.fetchBasicData(props.location.query.search);
    }

    retry() {
        UiStore.loading = false;
        UiStore.error.present = false;
        UiStore.error.message = "";
        this.fetchBasicData(this.props.location.query.search);
    }

    deleteDetail = async (id) => {
        let coachingToDelete = _.find(this.basicDetails, detail => detail.id === id);
        this.setState({
            confirmDelete : {
                show: true,
                title: `Delete Coaching`,
                coachingToDelete: coachingToDelete
            }
        });
    };


    deleteCoaching = async (id) => {
        try {
            await new CommonApiService('coaching').deleteData(id);
            this.props.showToastr('success', 'Delete', 'Coaching deleted successfully.');
            return await this.fetchBasicData(this.props.location.query.search);
        }
        catch (error) {
            UiStore.loading = false;
            this.props.showToastr('error', 'Delete', errorMessage(error));
        }
    };

    async fetchBasicData(queryString = "") {
        try {
            UiStore.loading = true;
            let coachings = await  this.basicStore.fetchBasicData(queryString);
            this.basicDetails = coachings && _.map(coachings, (coaching) => {
                let result = _.extend(coaching, coaching.basicDetails);
                delete result.basicDetails;
                return result;
            });
            UiStore.loading = false;
        }
        catch (error) {
            if (error.message === App_Const.failedToFetch) {
                UiStore.loading = false;
                UiStore.error.present = true;
                UiStore.error.message = App_Const.failedToFetch;
            }
        }
    }

    showHideCourseModal = (e) => {
        redirectToPage("/manage-course?type=coaching");
    };

    applyNow = (coaching) => {
        this.setState({applyNow: true, appliedCoaching: coaching});
    };

    render() {
        return (
            <div>
                {
                    UiStore.error.present ?
                        <div>
                            <AppError retry={this.retry} error={UiStore.error}/>
                        </div>
                    :
                    <div>
                        <div className={"app-background page-coaching"}/>
                        {
                            UiStore.loading ? <Loading/> :
                                <div>
                                    { ListHeader('coaching') }
                                    <ConfirmDelete
                                        show={this.state.confirmDelete.show}
                                        dataToDelete={this.state.confirmDelete.coachingToDelete}
                                        title={this.state.confirmDelete.title}
                                        deleteData={this.deleteCoaching}
                                        close={() => this.setState({confirmDelete: { show: false, title: "", coachingToDelete: {}}})}/>

                                    <List title="Top coaching classes..."
                                          icon="numbered list"
                                          type="coaching"
                                          admin={this.props.admin}
                                          deleteDetail={this.deleteDetail}
                                          basicDetails={this.basicDetails}
                                          showHideCourseModal={this.showHideCourseModal}
                                          applyNow={this.applyNow}
                                          {...this.props}/>
                                </div>
                        }
                    </div>
                }
                <SEO page="coaching"
                     title={SEO_INFO.coaching.title}
                     description={SEO_INFO.coaching.description}
                     image={SEO_INFO.coaching.image}/>
            </div>
        );
    }
}

export default Coaching;

