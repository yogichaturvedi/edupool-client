/**
 * Created by Amit on 16-03-2018.
 */
import React, {Component} from 'react';
import {observer} from 'mobx-react';
import AppError from "../../component/error/index";
import UiStore from '../store/ui-store'
import ArticleNewsStore from '../store/article-news-store'
import App_Const from "../../constants/constants";
import {errorMessage} from "../../util/error-formatter";
import Loading from "../../component/loading/index";
import ArticleNewsList from "../../component/list/article-news";
import SEO from "../../component/seo/index";
import SEO_INFO from "../../static-data/seo";
import { ListHeader } from '../../component/component';
import {toTitleCase} from "../../util/util";
import ConfirmDelete from "../../component/modal/confirm-delete";
import _ from 'lodash';
import './style.scss';


@observer
class ArticleAndNews extends Component {

    constructor(props){
        super(props);
        this.state ={
            type: props.location.pathname.split("/")[1],
            articleNewsList : [],
            confirmDelete: {
                show: false, title: "", articleNewsToDelete: {}
            }
        };
        ArticleNewsStore.initializeType(this.state.type)
    }

    componentWillMount() {
        localStorage.setItem("activeTab", this.state.type);
        this.fetchAll(this.props.location.query.search);
    }

    componentWillReceiveProps(props) {
        this.fetchAll(props.location.query.search);
    }


    retry = () => {
        UiStore.loading = false;
        UiStore.error.present = false;
        UiStore.error.message = "";
        this.fetchAll(this.props.location.query.search);
    };

    async fetchAll(queryString) {
        try {
            UiStore.loading = true;
            let articleNewsList = await ArticleNewsStore.fetchAll({string: queryString});
            this.setState({articleNewsList : articleNewsList});
            UiStore.loading = false;
        }
        catch (error) {
            console.log(error);
            UiStore.loading = false;
            UiStore.error.present = true;
            if (error.message === App_Const.failedToFetch) {
                UiStore.error.message = App_Const.failedToFetch;
            }
            else {
                UiStore.error.message = errorMessage(error);
            }
        }
    }

    deleteDetail = async (id) => {
        let articleNewsToDelete = _.find(this.state.articleNewsList, articleNews => articleNews.id === id);
        this.setState({
            confirmDelete : {
                show: true,
                title: `Delete ${toTitleCase(this.state.type)}`,
                articleNewsToDelete
            }
        });
    };

    deleteArticleNews = async (id) => {
        try {
            UiStore.loading = true;
            await ArticleNewsStore.deleteArticleNews(id);
            this.props.showToastr('success', 'Delete', toTitleCase(this.state.type) + ' deleted successfully.');
            return await this.fetchAll();
        }
        catch (error) {
            UiStore.loading = false;
            this.props.showToastr('error', 'Delete', errorMessage(error));
        }
    };



    render(){
        return(
            <div className='blog-list-wrapper'>
                <div className={"app-background page-blog"}/>

                {
                    UiStore.error.present ?
                        <div>
                            <AppError retry={this.retry} error={UiStore.error}/>
                        </div>
                        :
                        <div>
                            {/*<div className={"app-background page-school"}/>*/}
                            {
                                UiStore.loading ? <Loading/> :
                                    <div>
                                        {ListHeader(this.state.type)}
                                        <ConfirmDelete
                                            show={this.state.confirmDelete.show}
                                            dataToDelete={this.state.confirmDelete.articleNewsToDelete}
                                            title={this.state.confirmDelete.title}
                                            deleteData={this.deleteArticleNews}
                                            close={() => this.setState({confirmDelete: { show: false, title: "", articleNewsToDelete: {}}})}/>

                                        <ArticleNewsList
                                            type={this.state.type}
                                            admin={this.props.admin}
                                            showToastr={this.props.showToastr}
                                            deleteArticleNews={this.deleteDetail}
                                            articleNewsList={this.state.articleNewsList}
                                            {...this.props}/>
                                    </div>
                            }
                        </div>
                }
                <SEO page={this.state.type}
                     title={SEO_INFO[this.state.type].title}
                     description={SEO_INFO[this.state.type].description}
                     image={SEO_INFO[this.state.type].image}/>
            </div>
        )
    }
}

export default ArticleAndNews;

