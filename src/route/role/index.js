/**
 * Created by Yogesh Chaturvedi on 12-06-2017.
 */
import React, {Component} from 'react';

import {observable} from 'mobx';
import {observer} from 'mobx-react';
import {Segment} from 'semantic-ui-react';
import College from "../college";
import School from "../school";
import Coaching from "../coaching";
import Exam from "../exam";
import Result from "../result";
import Event from "../event";
import StudyMaterial from "../study";
import Home from "../home";

@observer
class Role extends Component {
    @observable role = '';

    constructor(props) {
        super(props);
        const activeTab = localStorage.getItem("activeTab");
        this.admin  = localStorage.getItem("user");
        this.role = activeTab && activeTab !== '' ? activeTab : 'home';
    }

    componentWillMount(){
        console.log(this.props);
    }
    componentWillReceiveProps() {
        this.role = localStorage.getItem("activeTab");
    }

    getBaseContent() {
        switch (this.role) {
            case 'school' :
                return <School admin={this.admin} showToastr={this.props.showToastr} showLoading={this.props.showLoading} />;
            case 'college':
                return <College admin={this.admin} showToastr={this.props.showToastr} showLoading={this.props.showLoading} />;
            case 'coaching' :
                return <Coaching admin={this.admin} showToastr={this.props.showToastr} showLoading={this.props.showLoading} />;
            case 'exam' :
                return <Exam admin={this.admin} showToastr={this.props.showToastr} showLoading={this.props.showLoading} />;
            case 'result' :
                return <Result admin={this.admin} showToastr={this.props.showToastr} showLoading={this.props.showLoading} />;
            case 'event' :
                return <Event admin={this.admin} showToastr={this.props.showToastr} showLoading={this.props.showLoading} />;
            case 'study-material':
                return <StudyMaterial admin={this.admin} showToastr={this.props.showToastr} showLoading={this.props.showLoading} />;
            default :
                return <Home admin={this.admin} showToastr={this.props.showToastr} showLoading={this.props.showLoading} />;
        }
    }

    render() {
        return (
            <Segment basic className="body-container no-padding-segment">
                {this.getBaseContent()}
            </Segment>
        );
    }

}

export default Role;
