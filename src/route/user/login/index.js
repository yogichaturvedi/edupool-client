/**
 * Created by Yogesh Chaturvedi on 11-06-2017.
 */
import React, {Component} from 'react';
import {Button, Divider, Form, Icon, Segment} from 'semantic-ui-react'
import {browserHistory} from 'react-router';
import LogoLogin from '../../../assets/image/logo-login.png';
import {observer} from "mobx-react";
import UserService from "../../../service/user-service";
import '../user.scss';

@observer
class Login extends Component {

    constructor(props) {
        super(props);
        this.handleSignIn = this.handleSignIn.bind(this);
        this.onFormDataChange = this.onFormDataChange.bind(this);
        this.state = {
            user: {
                email: '',
                password: '',
            }
        }
    }

    async handleSignIn(e) {
        e.preventDefault();
        let response = await UserService.authenticate(this.state.user.email.trim(), this.state.user.password);
        if (response.code === 401) {
            this.props.showToastr('error', 'Login Failed !', 'Incorrect username or password');
        }
        else {
            localStorage.setItem("user", JSON.stringify(response));
            localStorage.setItem("isUserLoggedIn", true);
            browserHistory.push('/home');
            this.props.showToastr('success', 'Login Successful !', response[0].username + ' has been logged in');
        }
    }

    onFormDataChange(path, value) {
        let user = this.state.user;
        user[path] = value;
        this.setState({user: user});
    }

    render() {
        return (
            <div className="login-form-container">
                <div className="app-background"/>
                <Segment className={"login-box"}>
                    <div className="app-logo-container">
                        <img className="login-logo" alt="login-logo" src={LogoLogin}/>
                    </div>
                    <Form>
                        <Form.Input value={this.userName}
                                    onChange={(e) => this.onFormDataChange("email", e.target.value)} fluid
                                    label='Email ID' placeholder='Enter email' icon="mail" iconPosition='left'/>
                        <Form.Input value={this.password}
                                    onChange={(e) => this.onFormDataChange("password", e.target.value)} fluid
                                    type="Password" label='Password' placeholder='Password' icon="lock"
                                    iconPosition='left'/>
                        <Form.Field>
                            <a href onClick={(event) => {
                                event.preventDefault();
                                browserHistory.push('/verify-user')
                            }}>
                                Forgot Password ?
                            </a>
                        </Form.Field>
                        <Button.Group vertical className="login-buttons-container">
                            <Button positive animated fluid onClick={(e) => this.handleSignIn(e)}>
                                <Button.Content visible>Sign In</Button.Content>
                                <Button.Content hidden>
                                    <Icon name='right arrow'/>
                                </Button.Content>
                            </Button>
                            <Divider horizontal>Or</Divider>
                            <Button secondary animated fluid
                                    onClick={(e) => {
                                        e.preventDefault();
                                        browserHistory.push('/signup')
                                    }}>
                                <Button.Content visible>Sign Up</Button.Content>
                                <Button.Content hidden>
                                    <Icon name='compose'/>
                                </Button.Content>
                            </Button>
                        </Button.Group>
                    </Form>
                </Segment>
            </div>
        );
    }
}

export default Login;
