/**
 * Created by Yogesh Chaturvedi on 11-06-2017.
 */
import React, {Component} from 'react';
import {browserHistory} from 'react-router';

class Fake extends Component {

    componentDidMount() {
        localStorage.setItem("activeTab", "home");
        browserHistory.push("/home");
    }  

    render() {
        return (
            <div>
            </div>
        );
    }
}

export default Fake;
