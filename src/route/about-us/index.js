import React, {Component} from "react";
import {Button, Divider, Form, Header, Icon, Segment} from "semantic-ui-react";
import HomeStore from "../../route/store/home-store";
import _ from "lodash";
import AppError from "../../component/error/index";
import UiStore from "../store/ui-store";
import Loading from "../../component/loading/index";
import {observer} from "mobx-react";
import SEO from "../../component/seo/index";
import SEO_INFO from "../../static-data/seo";
import App_Const from "../../constants/constants";
import {errorMessage} from "../../util/error-formatter";

const iconColors = [
    {name: "facebook", "color": "blue"},
    {name: "twitter", "color": "teal"},
    {name: "google plus", "color": "red"},
    {name: "vk", "color": "green"},
    {name: "linkedin", "color": "blue"},
    {name: "instagram", "color": "red"},
    {name: "youtube", "color": "red"}
];

@observer
class AboutUs extends Component {

    constructor(props) {
        super(props);
        this.getIconColor = this.getIconColor.bind(this);
        this.fetchHomeInfo = this.fetchHomeInfo.bind(this);
        this.retry = this.retry.bind(this);
    }

    componentWillMount(){
        if (!HomeStore.homeInfo.aboutUs) {
            this.fetchHomeInfo();
        }
    }

    async fetchHomeInfo() {
        try {
            UiStore.loading = true;
            if (!HomeStore.homeInfo.aboutUs) {
                await HomeStore.fetchHomeInfo();
            }
            UiStore.loading = false;
            UiStore.error.present = false;
            UiStore.error.message = "";
        }
        catch (error) {
            UiStore.loading = false;
            UiStore.error.present = true;
            if (error.message === App_Const.failedToFetch) {
                UiStore.error.message = App_Const.failedToFetch;
            }
            else {
                UiStore.error.message = errorMessage(error);
            }
        }
    }

    getIconColor(iconName) {
        return _.find(iconColors, (icon) => iconName === icon.name).color;
    }

    async retry() {
        UiStore.loading = true;
        UiStore.error.present = false;
        UiStore.error.message = "";
        await this.fetchHomeInfo();
    }

    render() {
        return (
            <Segment basic className="body-container no-padding-segment">
                <div>
                    {
                        UiStore.error.present ?
                            <div>
                                <AppError retry={this.retry} error={UiStore.error}/>
                            </div>
                            :
                            <div>
                                <div className="app-background about-us"/>
                                {
                                    !HomeStore.homeInfo.aboutUs ? <Loading/> :
                                        <div className="body-content">
                                            <Header as="h2" style={{color: "deepskyblue"}}>About Us</Header>
                                            <Divider/>
                                            <p style={{
                                                color: "white",
                                                fontSize: "16px"
                                            }}>
                                                {HomeStore.homeInfo.aboutUs}
                                            </p>

                                            <Divider/>
                                            <Header as="h3" style={{color: "deepskyblue"}}>Get in touch</Header>
                                            <Form.Group style={{textAlign: "left"}} inline>
                                                {
                                                    HomeStore.homeInfo.socialLinks.map((socialLink) => {
                                                        return <Button icon={socialLink.name} as="a"
                                                                       href={socialLink.link}
                                                                       key={socialLink.name}
                                                                       target="_blank" circular inverted
                                                                       color={this.getIconColor(socialLink.name)}/>
                                                    })
                                                }
                                            </Form.Group>
                                            <Header as="h4" style={{color: "deepskyblue"}}>To list your organisation you
                                                can
                                                mail us
                                                on : </Header>
                                            <a href={"mailto:" + HomeStore.homeInfo.queryEmail + "?subject=I have read your article from https://www.edupool.in"}>
                                                <Header as='h5'>
                                                    <Icon style={{fontSize: "1.2em"}} size="mini" circular color="black"
                                                          inverted
                                                          name='mail'/>
                                                    <Header.Content as="p"
                                                                    style={{
                                                                        color: "white",
                                                                        textAlign: "left",
                                                                        wordBreak: "break-all",
                                                                        fontSize: "1.3em"
                                                                    }}>{HomeStore.homeInfo.queryEmail}
                                                    </Header.Content>
                                                </Header>
                                            </a>
                                            <Header as="h4" style={{color: "deepskyblue"}}>For general query : </Header>
                                            <a href={"mailto:" + HomeStore.homeInfo.infoEmail + "?subject=I have read your article from https://www.edupool.in"}>
                                                <Header as='h5'>
                                                    <Icon style={{fontSize: "1.2em"}} size="mini" circular color="black"
                                                          inverted
                                                          name='mail'/>
                                                    <Header.Content as="p"
                                                                    style={{
                                                                        color: "white",
                                                                        textAlign: "left",
                                                                        wordBreak: "break-all",
                                                                        fontSize: "1.3em"
                                                                    }}>{HomeStore.homeInfo.infoEmail}
                                                    </Header.Content>
                                                </Header>
                                            </a>
                                            <Divider hidden section/>
                                        </div>
                                }
                            </div>
                    }
                </div>
                <SEO page="about_us"
                     title={SEO_INFO.aboutUs.title}
                     description={SEO_INFO.aboutUs.description}
                     image={SEO_INFO.aboutUs.image}/>
                <SEO/>
            </Segment>
        );
    }
}

export default AboutUs;