/**
 * Created by Yogesh Chaturvedi on 11-06-2017.
 */
import React, {Component} from 'react';
import List from "../../component/list";
import {observer} from 'mobx-react';
import EventStore from "../store/event-store";
import _ from "lodash";
import {observable} from 'mobx';
import Loading from "../../component/loading/index";
import CommonApiService from "../../service/common-api";
import UiStore from "../store/ui-store";
import App_Const from "../../constants/constants";
import {errorMessage} from "../../util/error-formatter";
import AppError from "../../component/error/index";
import SEO from "../../component/seo/index";
import SEO_INFO from "../../static-data/seo";
import { ListHeader } from '../../component/component';
import '../index.scss';
import ConfirmDelete from "../../component/modal/confirm-delete";

@observer
class Event extends Component {

    @observable basicDetails = [];

    constructor(props) {
        super(props);
        this.retry = this.retry.bind(this);
        this.fetchBasicData = this.fetchBasicData.bind(this);
        this.deleteDetail = this.deleteDetail.bind(this);
        this.state = {
            confirmDelete: {
                show: false, title: "", eventToDelete: {}
            }
        };
        UiStore.error.present = false;
        UiStore.error.message = "";
    }

    componentWillMount() {
        localStorage.setItem("activeTab", "event");
        this.fetchBasicData(this.props.location.query.search);
    }

    componentWillReceiveProps(props) {
        this.fetchBasicData(props.location.query.search);
    }

    retry() {
        UiStore.loading = false;
        UiStore.error.present = false;
        UiStore.error.message = "";
        this.fetchBasicData(this.props.location.query.search);
    }
    async deleteDetail(id) {
        let eventToDelete = _.find(this.basicDetails, detail => detail.id === id);
        this.setState({
            confirmDelete : {
                show: true,
                title: `Delete Event`,
                eventToDelete
            }
        });
    }

    deleteEvent = async (id) => {
        try {
            await new CommonApiService('event').deleteData(id);
            this.props.showToastr('success', 'Delete', 'Event deleted successfully.');
            return await this.fetchBasicData(this.props.location.query.search);
        }
        catch (error) {
            this.props.showToastr('error', 'Delete', errorMessage(error));
        }
    };

    async fetchBasicData(queryString = "") {
        try {
            UiStore.loading = true;
            let response;
            if (queryString) {
                response = await EventStore.searchBasicData(queryString);
            }
            else {
                response = await EventStore.fetchBasicData();
            }
            this.basicDetails = response && _.map(response, (res) => {
                return {
                    id: res.id,
                    createdAt: res.createdAt,
                    createdBy: res.createdBy,
                    tags: res.tags,
                    ...res.basicDetails
                };
            });
            UiStore.loading = false;
        }
        catch (error) {
            if (error.message === App_Const.failedToFetch) {
                UiStore.loading = false;
                UiStore.error.present = true;
                UiStore.error.message = App_Const.failedToFetch;
            }
        }
    }

    render() {
        return (
            <div>
                {
                    UiStore.error.present ?
                        <div>
                            <AppError retry={this.retry} error={UiStore.error}/>
                        </div>
                        :
                        <div>
                            <div className={"app-background page-event"}/>
                            {
                                UiStore.loading ? <Loading/> :
                                    <div>
                                        { ListHeader('event') }

                                        <ConfirmDelete
                                            show={this.state.confirmDelete.show}
                                            dataToDelete={this.state.confirmDelete.eventToDelete}
                                            title={this.state.confirmDelete.title}
                                            deleteData={this.deleteEvent}
                                            close={() => this.setState({confirmDelete: { show: false, title: "", eventToDelete: {}}})}/>

                                        <List title="Latest Events..."
                                              admin={this.props.admin}
                                              icon="announcement"
                                              type="event"
                                              deleteDetail={this.deleteDetail}
                                              basicDetails={this.basicDetails}
                                              {...this.props}/>
                                    </div>
                            }
                        </div>
                }
                <SEO page="event"
                     title={SEO_INFO.event.title}
                     description={SEO_INFO.event.description}
                     image={SEO_INFO.event.image}/>
            </div>
        );
    }


}

export default Event;
