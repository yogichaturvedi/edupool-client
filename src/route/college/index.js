/**
 * Created by Yogesh Chaturvedi on 11-06-2017.
 */
import React, {Component} from 'react';
import {observable} from 'mobx';
import {observer} from 'mobx-react';
import List from "../../component/list";
import BasicStore from "../store/basic-store";
import Loading from "../../component/loading/index";
import CommonApiService from "../../service/common-api";
import UiStore from "../store/ui-store";
import App_Const from "../../constants/constants";
import {errorMessage} from "../../util/error-formatter";
import AppError from "../../component/error/index";
import SEO from "../../component/seo/index";
import SEO_INFO from "../../static-data/seo";
import {ListHeader} from '../../component/component';
import ConfirmDelete from "../../component/modal/confirm-delete";
import ManageCourse from "../../component/admin-component/component/manage-course";
import ApplyNow from "../../component/modal/college-apply";
import {redirectToPage} from "../../util/util";
import _ from "lodash";
import '../index.scss';

@observer
class College extends Component {

    @observable basicDetails = [];
    deleteDetail = async (id) => {
        let collegeToDelete = _.find(this.basicDetails, detail => detail.id === id);
        this.setState({
            confirmDelete: {
                show: true,
                title: `Delete College`,
                collegeToDelete: collegeToDelete
            },
            applyNow: false,
            collegeData: {}
        });
    };
    deleteCollege = async (id) => {
        try {
            await new CommonApiService('college').deleteData(id);
            this.props.showToastr('success', 'Delete', 'College deleted successfully.');
            return await this.fetchBasicData(this.props.location.query.search);
        }
        catch (error) {
            this.props.showToastr('error', 'Delete', errorMessage(error));
        }
    };
    getSearchKeyWord = () => {
        const keyword = this.props.location.query.keyword;
        if (keyword) {
            return keyword.split("-").join(" ");
        }
        else if (this.props.location.query.admissionOpen === "true") {
            return "AdmissionOpen";
        }
        return null;
    };
    getKeyword = () => {
        return this.props.location.query.keyword.split("-").join("");
    };
    applyNow = (college) => {
        this.setState({applyNow: true, appliedCollege: college});
    };
    // getCourses = async () => {
    //     let courses = await this.basicStore.getCourses();
    //     courses = _.sortBy(courses, 'name');
    //     this.setState({courses});
    // };
    // saveCourses = async (courses) => {
    //     try {
    //         await this.basicStore.saveCourses(courses);
    //         this.setState({courses, showCourseModal: false});
    //         this.props.showToastr('success', "Save Course", "Successfully saved courses");
    //     }
    //     catch (error) {
    //         this.props.showToastr('success', "Save Course", "Something Went Wrong");
    //     }
    // };
    showHideCourseModal = (e) => {
        // this.setState({showCourseModal: !this.state.showCourseModal})
        redirectToPage("/manage-course?type=college");
    };

    constructor(props) {
        super(props);
        this.retry = this.retry.bind(this);
        this.fetchBasicData = this.fetchBasicData.bind(this);
        this.deleteDetail = this.deleteDetail.bind(this);
        this.state = {
            confirmDelete: {
                show: false, title: "", collegeToDelete: {}
            },
            // courses: [],
            showCourseModal: false,
            applyNow: false,
            appliedCollege: {}
        };
        UiStore.error.present = false;
        UiStore.error.message = "";
    }

    componentWillMount() {
        localStorage.setItem("activeTab", "college");
        this.basicStore = new BasicStore("college");
        if (this.props.location.query.admissionOpen && this.props.location.query.admissionOpen === "true") {
            this.fetchBasicData(this.props.location.query.search, true);
            return;
        }
        this.fetchBasicData(this.props.location.query.search);
        // this.getCourses();
    }

    componentWillReceiveProps(props, nextState) {
        localStorage.setItem("activeTab", "college");
        if (!_.isEqual(props.location.query, this.props.location.query)) {
            // if (props.location.query.admissionOpen === "true") {
            //     this.fetchBasicData(this.props.location.query.search, true);
            //     return;
            // }
            let admissionOpen = props.location.query.admissionOpen === "true";
            this.fetchBasicData(this.props.location.query.search, admissionOpen);
            // this.getCourses();
        }
    }

    retry() {
        UiStore.loading = false;
        UiStore.error.present = false;
        UiStore.error.message = "";
        this.fetchBasicData(this.props.location.query.search);
    }

    async fetchBasicData(queryString = "", admissionOpen = false) {
        try {
            UiStore.loading = true;
            let colleges = [];
            if (admissionOpen) {
                colleges = await  this.basicStore.fetchAdmissionOpenBasicData();
            }
            else {
                colleges = await  this.basicStore.fetchBasicData(queryString);
            }
            this.basicDetails = colleges && _.map(colleges, (college) => {
                let result = _.extend(college, college.basicDetails);
                delete result.basicDetails;
                return result;
            });
            UiStore.loading = false;
        }
        catch (error) {
            if (error.message === App_Const.failedToFetch) {
                UiStore.loading = false;
                UiStore.error.present = true;
                UiStore.error.message = App_Const.failedToFetch;
            }
        }
    }

    render() {
        return (
            <div>
                {
                    !_.isEmpty(this.state.appliedCollege) &&
                    <ApplyNow
                        collegeData={this.state.appliedCollege || {}}
                        show={this.state.applyNow}
                        close={() => this.setState({applyNow: false})}/>
                }
                {
                    UiStore.error.present ?
                        <div>
                            <AppError retry={this.retry} error={UiStore.error}/>
                        </div>
                        :
                        <div>
                            <div className={"app-background page-college"}/>
                            {
                                UiStore.loading ? <Loading/> :
                                    <div>
                                        {ListHeader('college', this.getSearchKeyWord())}
                                        <ConfirmDelete
                                            show={this.state.confirmDelete.show}
                                            dataToDelete={this.state.confirmDelete.collegeToDelete}
                                            title={this.state.confirmDelete.title}
                                            deleteData={this.deleteCollege}
                                            close={() => this.setState({confirmDelete: { show: false, title: "", collegeToDelete: {}}})}/>

                                        <List title="Top colleges..."
                                              icon="angellist"
                                              type="college"
                                              admin={this.props.admin}
                                              showHideCourseModal={this.showHideCourseModal}
                                              applyNow={this.applyNow}
                                              deleteDetail={this.deleteDetail}
                                              basicDetails={this.basicDetails}
                                              {...this.props}/>
                                    </div>
                            }
                        </div>
                }

                {

                    this.props.location.query.keyword ?
                        <SEO page="college"
                             title={SEO_INFO.college[this.getKeyword()].title}
                             description={SEO_INFO.college[this.getKeyword()].description}
                             image={SEO_INFO.college.image}/>
                        :
                        <SEO page="college"
                             title={SEO_INFO.college.title}
                             description={SEO_INFO.college.description}
                             image={SEO_INFO.college.image}/>
                }
            </div>
        );
    }
}

export default College;

