/**
 * Created by Yogesh Chaturvedi on 11-06-2017.
 */
import React, {Component} from 'react';
import {observer} from 'mobx-react';
import List from "../../component/list";
import StudyStore from "../store/study-store";
import {observable} from "mobx";
import Loading from "../../component/loading/index";
import CommonApiService from "../../service/common-api";
import UiStore from "../store/ui-store";
import App_Const from "../../constants/constants";
import {errorMessage} from "../../util/error-formatter";
import AppError from "../../component/error/index";
import SEO from "../../component/seo/index";
import SEO_INFO from "../../static-data/seo";
import { ListHeader } from '../../component/component';
import ConfirmDelete from "../../component/modal/confirm-delete";
import _ from 'lodash';
import '../index.scss';

@observer
class Study extends Component {

    @observable basicDetails = [];

    constructor(props) {
        super(props);
        this.retry = this.retry.bind(this);
        this.fetchBasicData = this.fetchBasicData.bind(this);
        this.deleteDetail = this.deleteDetail.bind(this);
        this.state = {
            confirmDelete: {
                show: false, title: "", materialToDelete: {}
            }
        };
        UiStore.error.present = false;
        UiStore.error.message = "";
    }

    componentWillMount() {
        localStorage.setItem("activeTab", "study-material");
        this.fetchBasicData(this.props.location.query.search);
    }

    componentWillReceiveProps(props) {
        this.fetchBasicData(props.location.query.search);
    }

    async fetchBasicData(queryString = "") {
        try {
            UiStore.loading = true;
            this.basicDetails = await StudyStore.fetchBasicData(queryString);
            UiStore.loading = false;
        }
        catch (error) {
            if (error.message === App_Const.failedToFetch) {
                UiStore.loading = false;
                UiStore.error.present = true;
                UiStore.error.message = App_Const.failedToFetch;
            }
        }
    }

    async deleteDetail(id) {
        let materialToDelete = _.find(this.basicDetails, detail => detail.id === id);
        this.setState({
            confirmDelete : {
                show: true,
                title: `Delete Study-Material`,
                materialToDelete
            }
        });
    }

    deleteMaterial = async (id) => {
        try {
            await new CommonApiService('study').deleteData(id);
            this.props.showToastr('success', 'Delete', 'Study Material deleted successfully.');
            return await this.fetchBasicData(this.props.location.query.search);
        }
        catch (error) {
            this.props.showToastr('error', 'Delete', errorMessage(error));
        }
    };

    retry() {
        UiStore.loading = false;
        UiStore.error.present = false;
        UiStore.error.message = "";
        this.fetchBasicData(this.props.location.query.search);
    }

    render() {
        return (
            <div>
                {
                    UiStore.error.present ?
                        <div>
                            <AppError retry={this.retry} error={UiStore.error}/>
                        </div>
                        :
                        <div>
                            <div className={"app-background page-study"}/>
                            {
                                UiStore.loading ? <Loading/> :
                                    <div className="body-content">
                                        { ListHeader('study') }
                                        <ConfirmDelete
                                            show={this.state.confirmDelete.show}
                                            dataToDelete={this.state.confirmDelete.materialToDelete}
                                            title={this.state.confirmDelete.title}
                                            deleteData={this.deleteMaterial}
                                            close={() => this.setState({confirmDelete: { show: false, title: "", materialToDelete: {}}})}/>

                                        <List title="Download E-Books from here"
                                              icon="book"
                                              type="study-material"
                                              admin={this.props.admin}
                                              showToastr={this.props.showToastr}
                                              deleteDetail={this.deleteDetail}
                                              basicDetails={this.basicDetails}
                                              {...this.props}/>
                                    </div>
                            }
                        </div>
                }
                <SEO page="study"
                     title={SEO_INFO.study.title}
                     description={SEO_INFO.study.description}
                    image={SEO_INFO.study.image}/>
            </div>
        );
    }
}

export default Study;