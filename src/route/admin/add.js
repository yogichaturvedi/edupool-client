/**
 * Created by Yogesh Chaturvedi on 21-08-2017.
 */
import React, {Component} from 'react';
import {observable} from 'mobx';
import {observer} from 'mobx-react';
import BasicAdd from "../../component/admin-component/add-edit/basic";
import {Segment} from "semantic-ui-react";
import AddExam from "../../component/admin-component/add-edit/exam";
import AddResult from "../../component/admin-component/add-edit/result";
import AddEvent from "../../component/admin-component/add-edit/event";
import AddStudyMaterial from "../../component/admin-component/add-edit/study";
import AddHomeInfo from "../../component/admin-component/add-edit/home-info";

@observer
class Add extends Component {

    @observable type = "";
    @observable id = "";

    constructor() {
        super();
        this.type = "";
        this.id = "";
    }

    componentWillMount() {
        this.type = this.props.location.query.type;
        this.id = this.props.location.query.type;
    }

    render() {
        return (
            <Segment basic className="body-container no-padding-segment">
                <div className={`app-background page-${this.type}`}/>
                <div>
                    {
                        this.type && ( this.type === "school" || this.type === "coaching" || this.type === "college")
                            ? <BasicAdd type={this.type} admin={this.props.admin}/> : null
                    }
                    {
                        this.type && this.type === "exam" && <AddExam type={this.type}/>
                    }
                    {
                        this.type && this.type === "result" && <AddResult/>
                    }
                    {
                        this.type && this.type === "event" && <AddEvent/>
                    }
                    {
                        this.type && this.type === "study-material" && <AddStudyMaterial/>
                    }
                    {
                        this.type && this.type === 'home-info' && <AddHomeInfo showToastr={this.props.showToastr}/>
                    }
                </div>
            </Segment>
        );
    }


}

export default Add;

