/**
 * Created by Yogesh Chaturvedi on 21-08-2017.
 */
import React, {Component} from 'react';
import {observable} from 'mobx';
import {observer} from 'mobx-react';
import BasicEdit from "../../component/admin-component/add-edit/basic";
import {Segment} from "semantic-ui-react";
import ExamEdit from "../../component/admin-component/add-edit/exam";
import ResultEdit from "../../component/admin-component/add-edit/result";
import StudyMaterialEdit from "../../component/admin-component/add-edit/study";
import EventEdit from "../../component/admin-component/add-edit/event";

@observer
class Edit extends Component {

    @observable type = "";
    @observable id = "";

    constructor() {
        super();
        this.type = "";
        this.id = "";
    }

    componentWillMount() {
        this.type = this.props.location.query.type;
        this.id = this.props.location.query.id;
    }

    render() {
        return (
            <Segment basic className="body-container no-padding-segment">
                <div className={`app-background page-${this.type}`}/>
                <div>
                    {
                        this.type && ( this.type === "school" || this.type === "coaching" || this.type === "college")
                            ? <BasicEdit type={this.type} id={this.id} mode="edit" admin={this.props.admin}/> : null
                    }
                    {
                        this.type && this.type === "exam" && <ExamEdit type="exam" id={this.id} mode="edit"/>
                    }
                    {
                        this.type && this.type === "result" &&
                        <ResultEdit type={this.type} id={this.id} mode="edit"/>
                    }
                    {
                        this.type && this.type === "event" &&
                        <EventEdit type={this.type} id={this.id} mode="edit"/>
                    }
                    {
                        this.type && this.type === "study-material" &&
                        <StudyMaterialEdit type={this.type} id={this.id} mode="edit"/>
                    }
                </div>
            </Segment>
        );
    }


}

export default Edit;

