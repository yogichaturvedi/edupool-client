/**
 * Created by Yogesh Chaturvedi on 11-06-2017.
 */
import React, {Component} from 'react';
import List from "../../component/list";
import {observer} from 'mobx-react';
import ResultStore from "../store/result-store";
import _ from "lodash";
import {observable} from 'mobx';
import Loading from "../../component/loading/index";
import CommonApiService from "../../service/common-api";
import UiStore from "../store/ui-store";
import App_Const from "../../constants/constants";
import {errorMessage} from "../../util/error-formatter";
import AppError from "../../component/error/index";
import SEO from "../../component/seo/index";
import SEO_INFO from "../../static-data/seo";
import { ListHeader } from '../../component/component';
import '../index.scss';
import ConfirmDelete from "../../component/modal/confirm-delete";

@observer
class Result extends Component {

    @observable basicDetails = [];

    constructor(props) {
        super(props);
        this.retry = this.retry.bind(this);
        this.fetchBasicData = this.fetchBasicData.bind(this);
        this.deleteDetail = this.deleteDetail.bind(this);
        this.state = {
            confirmDelete: {
                show: false, title: "", resultToDelete: {}
            }
        };
        UiStore.error.present = false;
        UiStore.error.message = "";
    }

    componentWillMount() {
        localStorage.setItem("activeTab", "result");
        this.fetchBasicData(this.props.location.query.search);
    }

    componentWillReceiveProps(props) {
        this.fetchBasicData(props.location.query.search);
    }

    retry() {
        UiStore.loading = false;
        UiStore.error.present = false;
        UiStore.error.message = "";
        this.fetchBasicData(this.props.location.query.search);
    }


    async fetchBasicData(queryString = "") {
        try {
            UiStore.loading = true;
            let response ;
            if(queryString){
                response = await  new ResultStore("result").searchBasicData(queryString);
            }
            else {
                response = await  new ResultStore("result").fetchBasicData();
            }
            this.basicDetails = response && _.map(response, (res) => {
                return {
                    id: res.id,
                    createdAt: res.createdAt,
                    createdBy: res.createdBy,
                    updatedAt: res.updatedAt,
                    ...res.basicDetails
                };
            });
            UiStore.loading = false;
        }
        catch (error) {
            if (error.message === App_Const.failedToFetch) {
                UiStore.loading = false;
                UiStore.error.present = true;
                UiStore.error.message = App_Const.failedToFetch;
            }
        }
    }

    async deleteDetail(id) {
        let resultToDelete = _.find(this.basicDetails, detail => detail.id === id);
        this.setState({
            confirmDelete : {
                show: true,
                title: `Delete Result`,
                resultToDelete
            }
        });
    }

    deleteResult = async (id) => {
        try {
            await new CommonApiService('result').deleteData(id);
            this.props.showToastr('success', 'Delete', 'Result deleted successfully.');
            return await this.fetchBasicData(this.props.location.query.search);
        }
        catch (error) {
            this.props.showToastr('error', 'Delete', errorMessage(error));
        }
    };

    render() {
        return (
            <div>
                {
                    UiStore.error.present ?
                        <div>
                            <AppError retry={this.retry} error={UiStore.error}/>
                        </div>
                        :
                        <div>
                            <div className="app-background page-result"/>
                            {
                                UiStore.loading ? <Loading/> :
                                    <div>
                                        { ListHeader('result') }
                                        <ConfirmDelete
                                            show={this.state.confirmDelete.show}
                                            dataToDelete={this.state.confirmDelete.resultToDelete}
                                            title={this.state.confirmDelete.title}
                                            deleteData={this.deleteResult}
                                            close={() => this.setState({confirmDelete: { show: false, title: "", resultToDelete: {}}})}/>

                                        <List title="Latest Results"
                                              icon="checkmark box"
                                              admin={this.props.admin}
                                              basicDetails={this.basicDetails}
                                              deleteDetail={this.deleteDetail}
                                              type="result"
                                              {...this.props}/>
                                    </div>
                            }
                        </div>
                }
                <SEO page="result"
                     title={SEO_INFO.result.title}
                     description={SEO_INFO.result.description}
                     image={SEO_INFO.result.image}/>
            </div>
        );
    }
}

export default Result;