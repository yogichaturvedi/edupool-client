import {observable} from "mobx";
import ResultModal from "../../model/result";
import CommonApiService from "../../service/common-api";

class ResultStore {
    @observable resultModal;

    constructor() {
        this.commonApiService = new CommonApiService('result');
        this.resultModal = new ResultModal();
    }

    reInitializeModal() {
        this.resultModal = new ResultModal();
    }

    async fetchBasicData(limit = "") {
        return await this.commonApiService.fetchBasicData(limit);
    }
    async searchBasicData(queryString = "") {
        return await this.commonApiService.search(queryString);
    }

    async saveBasicData() {
        let dataToSave = {
            ...this.resultModal
        };
        return await this.commonApiService.saveData(dataToSave);
    }

    async updateBasicData(id) {
        let dataToUpdate = {
            ...this.resultModal
        };
        return await this.commonApiService.updateData(dataToUpdate, id);
    }

    async fetchBasicDataById(id) {
        this.resultModal = await this.commonApiService.getById(id);
        return this.resultModal;
    }
}

export default ResultStore;