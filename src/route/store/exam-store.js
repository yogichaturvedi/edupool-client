import {observable} from "mobx";
import ExamModal from "../../model/exam";
import CommonApiService from "../../service/common-api";

class BasicStore {
    @observable examModal;
    commonApiService = new CommonApiService("exam");

    constructor() {
        this.examModal = new ExamModal();
        this.commonApiService = new CommonApiService("exam");
    }

    reInitializeModal() {
        this.examModal = new ExamModal();
    }

    async fetchBasicData(queryString) {
        if(queryString){
            return this.commonApiService.search(queryString);
        }
        return this.commonApiService.fetchBasicData();
    }

    async saveBasicData() {
        let dataToSave = {
            ...this.examModal
        };
        return this.commonApiService.saveData(dataToSave);
    }

    async updateBasicData(id) {
        let dataToUpdate = {
            ...this.examModal
        };
        return this.commonApiService.updateData(dataToUpdate, id);
    }

    async fetchBasicDataById(id) {
        this.examModal = await this.commonApiService.getById(id);
    }
}

export default new BasicStore();