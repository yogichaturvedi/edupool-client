/**
 * Created by Amit on 10-09-2017.
 */
import {observable} from "mobx";

class UiStore {
    @observable loading = false;
    @observable error = {present: false, message: ''};

    reset(){
        this.loading = false;
        this.error.present = false;
        this.error.message = "";
    }
}

export default new UiStore();