/**
 * Created by Amit on 24-09-2017.
 */

import {observable} from "mobx";

class Store {
    @observable auditInfo = {
        name: "",
        email : "",
        institute : "",
        mobile : ""
    };

    update(userDetails = null) {
        if(userDetails){
            this.auditInfo.name = userDetails.name;
            this.auditInfo.email = userDetails.email;
            this.auditInfo.institute = userDetails.instituteName;
            this.auditInfo.mobile = userDetails.mobile;
        }
    }
}

export default new Store();