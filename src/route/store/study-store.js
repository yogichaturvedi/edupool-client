import {observable} from "mobx";
import StudyModal from "../../model/study";
import CommonApiService from "../../service/common-api";

class StudyStore {
    @observable studyModal;
    commonApiService = new CommonApiService("study");

    constructor() {
        this.studyModal = new StudyModal();
        this.commonApiService = new CommonApiService("study");

    }

    reInitializeModal() {
        this.studyModal = new StudyModal();
    }

    async fetchBasicData(queryString = "") {
        let query = "?$sort[createdAt]=-1";
        if(queryString){
            return await this.commonApiService.search(queryString);
        }
        return await this.commonApiService.fetchBasicDataWithQuery(query);
    }

    async saveBasicData() {
        let dataToSave = {
            ...this.studyModal
        };
        return await this.commonApiService.saveData(dataToSave);
    }

    async updateBasicData(id) {
        let dataToUpdate = {
            ...this.studyModal
        };
        return await  this.commonApiService.updateData(dataToUpdate, id);
    }

    async fetchBasicDataById(id) {
        this.studyModal = await this.commonApiService.getById(id);
    }
}

export default new StudyStore();