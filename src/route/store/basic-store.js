import {observable} from "mobx";
import BasicModal from "../../model/basic";
import CommonApiService from "../../service/common-api";

class BasicStore {
    @observable basicModal;
    type = "school";
    commonApiService = new CommonApiService(this.type);

    constructor(type) {
        this.type = type;
        this.commonApiService = new CommonApiService(this.type);
        this.basicModal = new BasicModal();
    }

    reInitializeModal() {
        this.basicModal = new BasicModal();
    }

    async fetchBasicData(queryString = "") {
        if (queryString) {
            return await this.commonApiService.search(queryString);
        }
        return await this.commonApiService.fetchBasicData();
    }

    async getCourses(){
        return await this.commonApiService.getCourses();
    }

    async getCoachingCourses(){
        return await this.commonApiService.getCoachingCourses();
    }

    async saveCourses(courses){
        return await this.commonApiService.saveCourses(courses);
    }


    async fetchAdmissionOpenBasicData() {
        return await this.commonApiService.fetchAdmissionOpenBasicData();
    }

    async saveBasicData() {
        let dataToSave = {
            createdBy: this.basicModal.auditInfo ? this.basicModal.auditInfo.name : "Yogesh Chaturvedi",
            ...this.basicModal
        };
        return await this.commonApiService.saveData(dataToSave);
    }

    async updateBasicData(id) {
        let dataToUpdate = {
            ...this.basicModal
        };
        return await this.commonApiService.updateData(dataToUpdate, id);
    }

    async fetchBasicDataById(id) {
        this.basicModal = await this.commonApiService.getById(id);
        if(!this.basicModal.course.courses && this.type === 'college'){
            this.basicModal.course.courses = [];
            // let $this = this;
            // _.each(Courses, (course) => {
            //     $this.basicModal.course.courses.push(new Course(course));
            // });
        }
    }

    async updateRating(rateValue, id) {
        this.basicModal.rating.value += rateValue;
        this.basicModal.rating.totalUsers += 1;
        return await  this.updateBasicData(id);
    }

    getSocialOptions() {
        return [
            {key: "facebook", "text": "Facebook", "value": "facebook"},
            {key: "twitter", "text": "Twitter", "value": "twitter"},
            {key: "google-plus", "text": "Google Plus", "value": "google plus"},
            {key: "vk", "text": "VK", "value": "vk"},
            {key: "linkedin", "text": "Linkedin", "value": "linkedin"},
            {key: "instagram", "text": "Instagram", "value": "instagram"},
            {key: "youtube", "text": "Youtube", "value": "youtube"}
        ];
    }
}

export default BasicStore;
