import {observable} from "mobx";
import RatingReview from "../../model/rating-review";

class GlobalStore {
    @observable ratingReviewModal;

    constructor() {
        this.ratingReviewModal = new RatingReview();
    }
}

export default new GlobalStore();