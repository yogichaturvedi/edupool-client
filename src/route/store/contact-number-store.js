import {action, observable} from "mobx";
import _ from "lodash";

class ContactNumberStore{

    @observable message = "";

    @action validateContactNumbers(images){
        let invalidNumbersIndex = this.getInvalidContactNumbers(images);
        if(invalidNumbersIndex.length > 0){
            let numbersIndex = invalidNumbersIndex.join(", ");
            this.message = "Contact Number " + numbersIndex + " are invalid";
        }
        else{
            this.message = "";
        }
    }

    @action getInvalidContactNumbers(images){
        let invalidNumbersIndexes = [];
        _.forEach(images, (image, index)=>{
            if(!this.isAddedContactNumberValid(image)){
                invalidNumbersIndexes.push(index+1);
            }
        });
        return invalidNumbersIndexes;
    }

    @action isAddedContactNumberValid(number){
        return number !== "" && number.length === 10;
    }

}

export default new ContactNumberStore();