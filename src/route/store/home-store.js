import {observable} from "mobx";
import CommonApiService from "../../service/common-api";
import HomeInfoModel from "../../model/home";
import TopRated from '../../fake-data/top-rated';

class HomeStore {
    infoId = '4ac21075-6d21-48c0-acbf-dc2da4218a5c';
    type = "home-info";

    @observable homeInfo = new HomeInfoModel();

    @observable topRatedContent = {
        school: [],
        college: [],
        coaching: []
    };

    constructor() {
        this.commonApiService = new CommonApiService(this.type);
    }

    reInitializeModal() {
        this.homeInfo = new HomeInfoModel();
    }

    async fetchHomePageData() {
        try {
            return await new CommonApiService('basic-details').fetchBasicData();
        }
        catch (error) {
            console.log(error);
            throw error;
        }
    }

    async fetchHomeInfo() {
        try {
            let response = await this.commonApiService.getById(this.infoId);
            console.log(this.homeInfo);
            this.homeInfo = response;
        }
        catch (error) {
            console.log(error);
            throw error;
        }
    }

    async updateHomeInfo() {
        let dataToUpdate = {
            ...this.homeInfo
        };
        return await this.commonApiService.updateData(dataToUpdate, this.infoId);
    }

    async saveHomeInfo() {
        let dataToSave = {
            id: this.infoId,
            createdBy: "Amit Sirohiya",
            ...this.homeInfo
        };
        return await this.commonApiService.saveData(dataToSave);
    }

    async fetchTopRatedContent() {
        try {
            this.topRatedContent = await new Promise(async (resolve, reject) => {
                resolve(TopRated);
            });
        }
        catch (error) {
            console.log(error);
            throw error;
        }
    }
}

export default new HomeStore();
