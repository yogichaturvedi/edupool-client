import {observable} from "mobx";
import EventModal from "../../model/event";
import CommonApiService from "../../service/common-api";
import Moment from "moment";

class EventStore {
    @observable eventModal;

    constructor() {
        this.commonApiService = new CommonApiService('event');
        this.eventModal = new EventModal();
    }

    reInitializeModal() {
        this.eventModal = new EventModal();
    }

    async fetchBasicData(limit = "") {
        return await this.commonApiService.fetchBasicData(limit);
    }

    async searchBasicData(queryString = "") {
        return await this.commonApiService.search(queryString);
    }

    async saveBasicData() {
        let dataToSave = {
            ...this.eventModal
        };
        return await this.commonApiService.saveData(dataToSave);
    }

    async updateBasicData(id) {
        let dataToUpdate = {
            ...this.eventModal
        };
        return await this.commonApiService.updateData(dataToUpdate, id);
    }

    async fetchBasicDataById(id) {
        let eventModal = await this.commonApiService.getById(id);
        eventModal.basicDetails.venue = eventModal.basicDetails.venue || {
            line1: "", line2: "", city: "", state: "", pincode: ""
        };
        eventModal.activities = eventModal.activities || [];
        eventModal.socialLinks = eventModal.socialLinks || [];
        eventModal.termsAndConditions = eventModal.termsAndConditions || [];
        eventModal.faqs = eventModal.faqs || [];
        if (eventModal.basicDetails.startDate) {
            eventModal.basicDetails.startDate.date = eventModal.basicDetails.startDate.date ? Moment(new Date(eventModal.basicDetails.startDate.date)) : Moment();
        }
        else {
            eventModal.basicDetails.startDate = {
                date: Moment(), time: ""
            };
        }
        if (eventModal.basicDetails.endDate) {
            eventModal.basicDetails.endDate.date = eventModal.basicDetails.endDate.date ? Moment(new Date(eventModal.basicDetails.endDate.date)) : Moment();
        }
        else {
            eventModal.basicDetails.endDate = {
                date: eventModal.basicDetails.startDate.date, time: ""
            };
        }
        this.eventModal = eventModal;
        return this.eventModal;
    }
}

export default new EventStore();