/**
 * Created by Amit on 16-03-2018.
 */
import {observable} from "mobx";
import ArticleNews from "../../model/article-news";
import ArticleNewsService from "../../service/article-news-service";
import {MONTHS, toTitleCase} from "../../util/util";
import _ from 'lodash';

class ArticleNewsStore {

    @observable articleNews = {};
    constructor() {
        this.articleNewsList = [];
        this.articleNews = new ArticleNews();
        this.articleNewsService = new ArticleNewsService();
    }

    initializeType(type){
        this.articleNews.type = type;
        this.articleNews.category = toTitleCase(type);
    }

    reInitialize(){
        this.articleNews = new ArticleNews();
    }

    async fetchAll(query) {
        if (query.string) {
            this.articleNewsList = await this.articleNewsService.search(this.articleNews.type, query.string);
        }
        else{
            this.articleNewsList = await this.articleNewsService.all(this.articleNews.type, query.limit);
        }
        return this.articleNewsList;
    }

    async fetch(id){
        this.articleNews = await this.articleNewsService.get(id);
    }

    async save() {
        let dataToSave = {
            createdBy: this.articleNews.auditInfo ? this.articleNews.auditInfo.name : "Edupool India",
            ...this.articleNews
        };
        return await this.articleNewsService.save(dataToSave);
    }

    async update(id) {
        let dataToUpdate = {
            ...this.articleNews
        };
        return await this.articleNewsService.update(dataToUpdate, id);
    }

    async deleteArticleNews(id){
        this.articleNewsService.delete(id)
    }

    getDate = (date) => {
        let dateObject = new Date(date);
        let month = MONTHS[dateObject.getMonth()];
        return `${dateObject.getDate()} ${month}, ${dateObject.getFullYear()}`;
    };

    getTime = (date) => {
        return new Date(date).toLocaleTimeString();
    };

    filter(filterType, filterValue){
        if(filterType === 'category' && filterValue !== 'All'){
            return _.filter(this.articleNewsList, item => item.category === filterValue);
        }
        return this.articleNewsList;
    }

    getTags = () => {
        return _.chain(this.articleNewsList)
            .map(item => item.tags)
            .flatten()
            .uniq()
            .value();
    };

    getCategories = () => {
        let $this = this;
        return _.chain(this.articleNewsList)
            .map(item => {
                let count = _.countBy($this.articleNewsList, b => b.category === item.category);
                return { name: item.category, count: count.true }
            })
            .uniqBy('name')
            .value();
    };


}

export default new ArticleNewsStore();