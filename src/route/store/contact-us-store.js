import ApiService from "../../service/api-service";

class ContactUsStore {

    async send(dataToSave) {
        let endPoint = "https://edupool.in/mail.php";
        return await ApiService.callApi({
            method: 'POST',
            authentication: false,
            endPoint: endPoint,
            body: dataToSave,
            mode:"no-cors"
        }).then(function (res) {
            return res;
        });
    }
}

export default new ContactUsStore();