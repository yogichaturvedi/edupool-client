import {action, observable} from "mobx";
import _ from "lodash";

class GalleryStore{

    @observable message = "";

    @action validateGalleryImages(images){
        let invalidImagesIndex = this.getInvalidGalleryImages(images);
        if(invalidImagesIndex.length > 0){
            let imagesIndex = invalidImagesIndex.join(", ");
            this.message = "Image " + imagesIndex + " URL's are invalid";
        }
        else{
            this.message = "";
        }
    }

    @action getInvalidGalleryImages(images){
        let invalidImageIndexes = [];
        _.forEach(images, (image, index)=>{
            if(!this.isAddedImageUrlValid(image)){
                invalidImageIndexes.push(index+1);
            }
        });
        return invalidImageIndexes;
    }

    @action isAddedImageUrlValid(image){
        return image !== "" && (image.endsWith('.jpg') || image.endsWith('.jpeg')  || image.endsWith('.png') || image.endsWith('.svg') || image.endsWith('.gif') );
    }

}

export default new GalleryStore();