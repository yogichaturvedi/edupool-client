import React from 'react';
import ReactDOM from 'react-dom';
import Routes from './route';
import registerServiceWorker from './registerServiceWorker';
import './index.scss';
import './assets/css/animate.css';
import './assets/css/toastr.min.css';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

let root = document.createElement('div');
root.id = "root";
document.body.appendChild( root );

ReactDOM.render(<div><Routes />
</div>, document.getElementById('root'));
registerServiceWorker(); 
  