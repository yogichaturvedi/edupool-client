/**
 * Created by Amit on 11-09-2017.
 */

export const APP_CONST = {
    failedToFetch: 'Failed to fetch',
    dates : {

        day: [
            {key: 0, text: 'Sunday', value: "SUN"},
            {key: 1, text: 'Monday', value: "MON"},
            {key: 2, text: 'Tuesday', value: "TUE"},
            {key: 3, text: 'Wednesday', value: "WED"},
            {key: 4, text: 'Thursday', value: "THU"},
            {key: 5, text: 'Friday', value: "FRI"},
            {key: 6, text: 'Saturday', value: "SAT"},

        ],
        month: [
            {key: 0, text: 'January', value: "JAN"},
            {key: 1, text: 'February', value: "FEB"},
            {key: 2, text: 'March', value: "MAR"},
            {key: 3, text: 'April', value: "APR"},
            {key: 4, text: 'May', value: "MAY"},
            {key: 5, text: 'June', value: "JUN"},
            {key: 6, text: 'July', value: "JUL"},
            {key: 7, text: 'August', value: "AUG"},
            {key: 8, text: 'September', value: "SEP"},
            {key: 9, text: 'October', value: "OCT"},
            {key: 10, text: 'November', value: "NOV"},
            {key: 11, text: 'December', value: "DEC"}
        ]
    }
};

export default APP_CONST;