import React, {Component} from 'react';
import {Button, Form, Header, Modal, Rating, TextArea} from 'semantic-ui-react'
import _ from 'lodash';
import {observable} from 'mobx';
import {observer} from 'mobx-react';
import GlobalStore from "../../route/store/global-store";
import FirebaseService from "../../service/firebase-service";
import "./style.scss"

@observer
class RatingModal extends Component {

    @observable codeSent = false;
    @observable user = {};

    componentWillReceiveProps(props, nextProps) {
        console.log(props);
        GlobalStore.ratingReviewModal.rating.value = props.rating
    }

        onFormDataChange(e) {
        e.preventDefault();
        if (e.target.name) {
            GlobalStore.ratingReviewModal[e.target.name].value = GlobalStore.ratingReviewModal[e.target.name].value ? e.target.value : e.target.value.trim();
        }
    }

    onFormFieldBlur(e) {
        e.preventDefault();
        if (e.target.name) {
            if (e.target.value === "" && e.target.value.trim() === "") {
                GlobalStore.ratingReviewModal[e.target.name].error = "Cannot be blank"
                GlobalStore.ratingReviewModal[e.target.name].valid = false;
                return;
            }
            if (e.target.name === "mobileNumber" && GlobalStore.ratingReviewModal[e.target.name].value.length !== 10) {
                GlobalStore.ratingReviewModal[e.target.name].error = "Enter valid phone number";
                return;
            }
            GlobalStore.ratingReviewModal[e.target.name].valid = true;
            GlobalStore.ratingReviewModal[e.target.name].error = "";
            GlobalStore.ratingReviewModal[e.target.name].value = GlobalStore.ratingReviewModal[e.target.name].value.trim();
        }
    }

    async onSubmitForm(e) {
        e.preventDefault();
        let ratingReviewModal = GlobalStore.ratingReviewModal;
        return this._isFormValidated().then((response) => {
            console.log(response);
            response && this.props.onClose(e, true, ratingReviewModal.name.value,
                ratingReviewModal.rating.value);
        }).catch((error) => {
            this.props.onClose(e, true);
        });
    }

    async _isFormValidated() {
        let fields = ['name', 'mobileNumber', 'otp'];
        let ratingReviewModal = GlobalStore.ratingReviewModal;
        ratingReviewModal.name.valid = ratingReviewModal.name.value.trim() !== "";
        ratingReviewModal.mobileNumber.valid = ratingReviewModal.mobileNumber.value.trim() !== ""
            && ratingReviewModal.mobileNumber.value.length === 10;
        ratingReviewModal.otp.valid = ratingReviewModal.otp.value.trim() !== "";
        if (ratingReviewModal.mobileNumber.valid && ratingReviewModal.otp.valid) {
            this.user = await FirebaseService.verifyCode(ratingReviewModal.otp.value);
            if (this.user) {
                ratingReviewModal.otp.valid = true;
                ratingReviewModal.otp.error = "";
            }
            else {
                ratingReviewModal.otp.valid = false;
                ratingReviewModal.otp.error = "OTP mismatch";
            }
        }
        let response = _.some(fields, (field) => {
            return GlobalStore.ratingReviewModal[field].valid === false;
        });
        return !response;
    }

    onRate(value) {
        GlobalStore.ratingReviewModal.rating.value = value.rating;
    }

    async getOTP() {
        GlobalStore.ratingReviewModal.mobileNumber.valid = GlobalStore.ratingReviewModal.mobileNumber.value.trim() !== "" && GlobalStore.ratingReviewModal.mobileNumber.value.length === 10;
        try {
            if (GlobalStore.ratingReviewModal.mobileNumber.valid) {
                this.codeSent = await FirebaseService.sendCode("+91" + GlobalStore.ratingReviewModal.mobileNumber.value);
            }
        } catch (e) {
            throw new Error(e);
        }
    }

    getErrorSpan(name) {
        if (GlobalStore.ratingReviewModal[name].error) {
            return <span style={{color: "red"}}>{GlobalStore.ratingReviewModal[name].error}</span>
        }
    }

    render() {
        return (
            <div>
                <Modal open={this.props.open} size='small'
                       onClose={(e) => {
                           this.props.onClose(e, false)
                       }}
                       className="rating-modal scrolling"
                       closeOnEscape={false}
                       closeOnRootNodeClick={false}>
                    <Header icon="pencil" content="Complete the details" color="blue"/>
                    <Modal.Content>
                        <Form onChange={(e) => {
                            this.onFormDataChange(e)
                        }} onBlur={(e) => {
                            this.onFormFieldBlur(e)
                        }}>
                            <Form.Field error={!GlobalStore.ratingReviewModal.name.valid}>
                                <Form.Input name="name" placeholder='Enter name...'
                                            value={GlobalStore.ratingReviewModal.name.value}/>
                                {this.getErrorSpan("name")}
                            </Form.Field>
                            <Form.Field>
                                <Form.Group>
                                    <Form.Field width="10" error={!GlobalStore.ratingReviewModal.mobileNumber.valid}
                                                className="input-without-controls">
                                        <Form.Input type="number" step="0" name="mobileNumber"
                                                    pattern="^\d{4}\d{3}\d{4}$"
                                                    placeholder='Enter mobile number...'
                                                    value={GlobalStore.ratingReviewModal.mobileNumber.value}/>
                                    </Form.Field>
                                    <Form.Field width="6">
                                        <Button fluid content="Get OTP" color="blue" onClick={() => {
                                            this.getOTP()
                                        }} icon="checkmark"/>
                                    </Form.Field>
                                </Form.Group>
                                {this.getErrorSpan("mobileNumber")}
                            </Form.Field>
                            <Form.Field>
                                <div id="recaptcha-container"/>
                            </Form.Field>
                            <Form.Field error={!GlobalStore.ratingReviewModal.otp.valid}>
                                <Form.Input name="otp" placeholder="Enter OTP code..."
                                            value={GlobalStore.ratingReviewModal.otp.value}/>
                                {this.getErrorSpan("otp")}
                            </Form.Field>
                            <Form.Field>
                                <Rating maxRating={5} onRate={(a, b) => this.onRate(b)}
                                        rating={this.props.rating} icon='star'
                                        size='massive'/>
                            </Form.Field>
                            <TextArea name="review" onChange={(e) => this.onFormDataChange(e)}
                                      style={{height: "75px"}}
                                      label='Review' rows={3} placeholder='Tell us more'/>
                        </Form>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button secondary content="Close"
                                onClick={(e) => this.props.onClose(e, false)}/>
                        <Button positive content="Submit" onClick={(e) => {
                            this.onSubmitForm(e)
                        }}/>
                    </Modal.Actions>
                </Modal>
            </div>
        );
    }
}

export default RatingModal;