/**
 * Created by Amit on 28-05-2018.
 */
import React, {Component} from 'react';
import {Form, Button, Dropdown, Input, Modal, Header, Icon} from 'semantic-ui-react'
import {observer} from 'mobx-react';
import UserService from '../../service/user-service';
import "./style.scss"
import _ from 'lodash';
import {getCities, getStates} from "../../util/states-cities";
import {isEmailValid} from "../../util/util";
import CommonApiService from "../../service/common-api";

@observer
class ApplyNow extends Component {

    constructor(props) {
        super(props);
        this.apiService = new CommonApiService();
        this.state = {
            show: false,
            name: {value: "", error: {present: false, message: ""}},
            email: {value: "", error: {present: false, message: ""}},
            mobile: {value: "", error: {present: false, message: ""}},
            course: "",
            location: "",
            stream: {name: "", value: "", error: {present: false, message: ""}},
            subStream: {name: "", value: "", error: {present: false, message: ""}},
            state: {name: "", value: "", error: {present: false, message: ""}},
            city: {name: "", value: "", error: {present: false, message: ""}},
            submitting: false,
            states: getStates(),
            cities: [],
            streams: this.getStreams(this.props.collegeData.course.courses),
            subStreams: []
        }
    }

    componentWillReceiveProps(nexrProps, nextState) {
        if (nexrProps.collegeData.course.courses) {
            this.setState({streams: this.getStreams(nexrProps.collegeData.course.courses)})
        }
    }

    componentWillUnmount() {
        this.close();
    }


    apply = async () => {
        let dataToSend = {
            name: this.state.name.value,
            email: this.state.email.value,
            mobile: this.state.mobile.value,
            college: this.props.collegeData.title,
            collegeEmail: this.props.collegeData.email,
            course: this.state.stream.name  + " ( " + this.state.subStream.name + " )",
            location: this.state.city.name + ", " + this.state.state.name,
            date: new Date().toLocaleDateString()
        };
        this.apiService.applyNow(dataToSend);
        this.close();
    };

    onFormFieldChange = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        let error = {present: false, message: ""};
        if (value === "") {
            error.present = true;
            error.message = "Cannot be blank";
        }
        if (name === 'mobile' && !(value).match(/^\d{10}$/)) {
            error.present = true;
            error.message = "Enter valid mobile number";
        }
        if (name === 'email' && !isEmailValid(value)) {
            error.present = true;
            error.message = "Enter valid email";
        }
        this.setState({[name]: {value: value, error: error}});
    };

    onLocalityChange = (name, value) => {
        this.setState({
            [name]: {
                value,
                name: value,
                error: {present: false, message: ""}
            }
        });
        if (name === 'state') {
            this.setState({
                cities: getCities(value),
                city: {
                    value: "",
                    name: "",
                    error: {present: true, message: "Select City"}
                }
            });
        }
    };

    onCourseChange = (name, value, text) => {
        if (name === 'stream') {
            let parts = value.split("-");
            let subStreams = [];
            this.setState({course: parts[0]});
            let matchedCourse = _.find( this.props.collegeData.course.courses, {value : parts[0]});
            if(matchedCourse){
                if(!_.isEmpty(matchedCourse.stream[parts[1]].subStream)){
                    subStreams = Object.keys(matchedCourse.stream[parts[1]].subStream).map((subName) => {
                        let subStream = matchedCourse.stream[parts[1]].subStream[subName];
                        return {key: subName, value: subStream.value, text: subStream.name}
                    });
                }
                this.setState({
                    stream: {value: value, name: text, error: {present: value === "", message: "Select Stream"}},
                    subStream: {name: "", value: "", error: {present: true, message: "Select Sub Stream"}},
                    subStreams: _.sortBy(subStreams, 'text')
                });
            }
        }
        if (name === 'subStream') {
            this.setState({
                subStream: {
                    value: value,
                    name: text,
                    error: {present: value === "", message: "Select City"}
                }
            });
        }
    };

    getStreams = (courses) => {
        let streams = [];
        if (courses) {
            courses.forEach((course) => {
                _.each(Object.keys(course.stream), (streamName) => {
                    streams.push({
                        key: course.value + "-" + streamName,
                        text: course.stream[streamName].name + " ( " + course.name + " )",
                        value: course.value + "-" + course.stream[streamName].value
                    });
                });
            });
        }
        return _.sortBy(streams, 'text');
    };

    close = () =>{
        this.setState({
            show: false,
            name: {value: "", error: {present: false, message: ""}},
            email: {value: "", error: {present: false, message: ""}},
            mobile: {value: "", error: {present: false, message: ""}},
            course: "",
            location: "",
            stream: {name: "", value: "", error: {present: false, message: ""}},
            subStream: {name: "", value: "", error: {present: false, message: ""}},
            state: {name: "", value: "", error: {present: false, message: ""}},
            city: {name: "", value: "", error: {present: false, message: ""}},
            submitting: false,
            states: getStates(),
            cities: [],
            streams: this.getStreams(this.props.collegeData.course.courses),
            subStreams: []
        });
        this.props.close();
    };

    isVaildForm = () => {
        let fields = ['name', 'email', 'mobile', 'state', 'city', 'stream', 'subStream'];
        return !_.some(fields, (field) => {
            return (this.state[field].value.trim() === "" || this.state[field].error.present);
        })
    };

    render() {
        return <Modal size='tiny'
                      open={this.props.show}
                      onClose={this.close}
                      className="scrolling"
                      closeOnEscape={false}
                      closeOnRootNodeClick={false}>
            <Modal.Header>
                <Header as="h4">
                    <Icon name='pencil' color="blue"/>
                    <Header.Content>
                        ADMISSION 2018
                        <Header.Subheader>{this.props.collegeData.title}</Header.Subheader>
                    </Header.Content>
                </Header>
            </Modal.Header>
            <Modal.Content>
                <h5>Please Enter the Following Details</h5>
                <Form autoComplete="off" onChange={this.onFormFieldChange} size="small">
                    <Form.Field
                        id='form-input-control-name'
                        control={Input}
                        name="name"
                        label="Name"
                        error={this.state.name.error.present}
                        value={this.state.name.value}
                        placeholder='Enter your name' required/>
                    <Form.Field
                        id='form-input-control-email'
                        name="email"
                        label='Email'
                        control={Input}
                        error={this.state.email.error.present}
                        value={this.state.email.value}
                        placeholder='Enter email id' required/>
                    <Form.Field
                        id='form-input-control-mobile'
                        control={Input}
                        name="mobile"
                        type='number'
                        label='Mobile'
                        error={this.state.mobile.error.present}
                        value={this.state.mobile.value}
                        placeholder='Enter your mobile number' required/>
                    <Form.Group widths="equal">
                        <Form.Dropdown
                            placeholder='Select Degree'
                            label='Select Degree'
                            selectOnBlur={false}
                            value={this.state.stream.value}
                            options={this.state.streams}
                            onChange={(e, data) => this.onCourseChange('stream', data.value, e.target.textContent)}
                            fluid search selection required/>
                        <Form.Dropdown
                            placeholder='Select Stream'
                            label='Select Stream'
                            selectOnBlur={false}
                            value={this.state.subStream.value}
                            options={this.state.subStreams}
                            disabled={!this.state.stream.value}
                            onChange={(e, data) => this.onCourseChange('subStream', data.value, e.target.textContent)}
                            fluid search selection required/>
                    </Form.Group>
                    <Form.Group widths="equal">
                        <Form.Dropdown
                            id="state"
                            placeholder='Select State'
                            label='Select State'
                            selectOnBlur={false}
                            options={this.state.states}
                            value={this.state.state.value}
                            onChange={(e, data) => this.onLocalityChange('state', data.value)}
                            fluid search selection required/>
                        <Form.Dropdown
                            id="city"
                            placeholder='Select City'
                            label='Select City'
                            selectOnBlur={false}
                            options={this.state.cities}
                            disabled={!this.state.state.value}
                            value={this.state.city.value}
                            onChange={(e, data) => this.onLocalityChange('city', data.value)}
                            fluid search selection required/>
                    </Form.Group>
                </Form>
                <div style={{color: 'red'}}>{this.state.message}</div>
            </Modal.Content>
            <Modal.Actions>
                <Button onClick={this.apply} loading={this.state.submitting}    
                         disabled={!this.isVaildForm()}
                        positive>Apply</Button>
                <Button onClick={this.close} disabled={this.state.submitting} negative>Cancel</Button>
            </Modal.Actions>
        </Modal>
    }
}

export default ApplyNow;