/**
 * Created by Amit on 28-05-2018.
 */
import React, {Component} from 'react';
import {Button, Input, Modal} from 'semantic-ui-react'
import {observer} from 'mobx-react';
import UserService from '../../service/user-service';
import "./style.scss"


@observer
class ConfirmDelete extends Component {

    constructor(props) {
        super(props);
        this.state = {
            show: false,
            message: "",
            password: "",
            dataToDelete: {},
            deletingData: false
        }
    }

    componentWillReceiveProps(nexrProps, nextState) {
        // if (nexrProps.dataToDelete.id !== this.state.dataToDelete.id) {
            this.setState({dataToDelete: nexrProps.dataToDelete})
        // }
    }

    componentWillUnmount() {
        this.close();
    }


    deleteData = async () => {
        this.setState({deletingData : true});
        let user = localStorage.getItem('user');
        user = JSON.parse(user)[0];
        let response = await UserService.authenticate(user.email, this.state.password);
        if (response.code === 401) {
            this.setState({message: 'Incorrect password', deletingData : false});
        }
        else {
            let deleted = await this.props.deleteData(this.state.dataToDelete.id);
            if (deleted) {
                this.close();
            }
        }

    };

    passwordChange = (e) =>        this.setState({password: e.target.value});

    close = () => {
        this.setState({
            show: false,
            message: "",
            password: "",
            dataToDelete: {},
            deletingData : false
        });
        this.props.close();
    }

    render() {
        const {title} = this.props;

        return <Modal size='tiny'
                      open={this.props.show}
                      onClose={this.close}
                      className="scrolling"
                      closeOnEscape={false}
                      closeOnRootNodeClick={false}>
            <Modal.Header icon="pencil" content={title} color="blue"/>
            <Modal.Content>
                <p>To delete "<span style={{fontWeight:'bold'}}>{this.state.dataToDelete.title}</span>" permanently enter your profile password</p>
                <Input error={this.state.message !== ""} value={this.state.password} type="password" onChange={this.passwordChange}/>
                <div style={{color:'red'}}>{this.state.message}</div>
            </Modal.Content>
            <Modal.Actions>
                <Button onClick={this.deleteData} loading={this.state.deletingData} disabled={this.state.deletingData} positive>Delete</Button>
                <Button onClick={this.close} disabled={this.state.deletingData} negative>Cancel</Button>
            </Modal.Actions>
        </Modal>
    }
}

export default ConfirmDelete;