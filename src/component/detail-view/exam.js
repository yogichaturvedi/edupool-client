/**
 * Created by Amit on 12-12-2017.
 */

import React, {Component} from "react";
import {Button, Grid, Header, Segment, Table} from "semantic-ui-react";
import {observer} from "mobx-react";
import ExamStore from "../../route/store/exam-store";
import SEO from "../seo/index";
import UiStore from '../../route/store/ui-store'
import "./style.scss";
import RelatedInfo from "../related-info/index";
import {browserHistory} from "react-router";
import Loading from "../loading/index";

@observer
class ExamDetail extends Component {

    constructor(props) {
        super(props);
        this.redirect = this.redirect.bind(this);
    }

    componentWillMount() {
        let id = this.props.router.params.id;
        this.fetchBasicData(id);
    }

    async fetchBasicData(id) {
        UiStore.loading = true;
        await ExamStore.fetchBasicDataById(id);
        UiStore.loading = false;
    }

    redirect(id) {
        browserHistory.push(`/exam/${id}`);
        this.fetchBasicData(id);
    }


    render() {
        // const website = <a target="_blank" href={ ExamStore.examModal.officialWebsite.link }>Official Website</a>;

        return (
            <div className="detail-container">
                <div className={`app-background page-exam`}/>
                {
                    UiStore.loading ? <Loading/> :
                        <Grid className="detail-wrapper">
                            <Grid.Column computer={12} mobile={16}>

                                <section id="basic-details-section">
                                    <div className="border-bottom-colored"/>
                                    <Segment className="basic-detail-wrapper margin-top-zero exam">
                                        <Grid columns={3}>
                                            <Grid.Column computer={4} mobile={16}>
                                                <div className="image-wrapper" width="100%">
                                                    <img src={ExamStore.examModal.basicDetails.imageUrl}
                                                         alt={ExamStore.examModal.basicDetails.title}
                                                         height={150}
                                                         width="100%"/>
                                                </div>
                                            </Grid.Column>
                                            <Grid.Column computer={12} mobile={16}>
                                                <Header as='h1' className="institute-title">
                                                    <Header.Content>
                                                        {ExamStore.examModal.basicDetails.title}
                                                    </Header.Content>
                                                </Header>
                                                <p>{ExamStore.examModal.basicDetails.description}</p>
                                                {/*<div>*/}
                                                {/*<Icon name="globe" color="blue"/>*/}
                                                {/*<CopyPopup triggerElement={website} content={ExamStore.examModal.officialWebsite.link}*/}
                                                {/*textToCopy={ExamStore.examModal.officialWebsite.link}/>*/}
                                                {/*</div>*/}
                                            </Grid.Column>
                                        </Grid>

                                    </Segment>
                                </section>

                                <section id="notice-section">
                                    <h2 className="section-heading">Notice</h2>
                                    <div className="border-bottom-colored"/>
                                    <Segment className="notice-wrapper margin-top-zero">
                                        <p>{ExamStore.examModal.notice.message1}</p>
                                        <p>{ExamStore.examModal.notice.message2}</p>
                                    </Segment>
                                </section>

                                <section id="dates-section">
                                    <h2 className="section-heading">Important Dates</h2>
                                    <div className="border-bottom-colored"/>
                                    <Segment className="dates-wrapper margin-top-zero">
                                        <Table basic='very' celled selectable unstackable>
                                            <Table.Header>
                                                <Table.Row>
                                                    <Table.HeaderCell>Title</Table.HeaderCell>
                                                    <Table.HeaderCell>Date</Table.HeaderCell>
                                                </Table.Row>
                                            </Table.Header>
                                            <Table.Body>
                                                {

                                                    ExamStore.examModal.importantDates.length > 0 ?
                                                        ExamStore.examModal.importantDates.map((dateInfo, index) => {
                                                            return <Table.Row key={"row-" + index}>
                                                                <Table.Cell>{dateInfo.title ? dateInfo.title : "-"}</Table.Cell>
                                                                <Table.Cell>{dateInfo.date ? dateInfo.date : "-"}</Table.Cell>
                                                            </Table.Row>
                                                        })
                                                        :
                                                        <Table.Row><Table.Cell>-</Table.Cell></Table.Row>
                                                }
                                            </Table.Body>
                                        </Table>
                                    </Segment>
                                </section>

                                <section id="vacancies-section">
                                    <h2 className="section-heading">Vacancies</h2>
                                    <div className="border-bottom-colored"/>
                                    <Segment className="vacancies-wrapper margin-top-zero">
                                        <p>{ExamStore.examModal.numberOfVacancies}</p>
                                    </Segment>
                                </section>

                                <section id="criteria-section">
                                    <h2 className="section-heading">Criteria</h2>
                                    <div className="border-bottom-colored"/>
                                    <Segment className="criteria-wrapper margin-top-zero">
                                        {
                                            !ExamStore.examModal.criteria.educational && !ExamStore.examModal.criteria.age &&
                                            <p>No information available right now, You may ask directly to
                                                authorities </p>
                                        }
                                        {
                                            ExamStore.examModal.criteria.educational && <div>
                                                <h3>Educational Qualification</h3>
                                                <p>{ExamStore.examModal.criteria.educational}</p>
                                            </div>
                                        }
                                        <br/>
                                        {
                                            ExamStore.examModal.criteria.age && <div>
                                                <h3>Age Limit</h3>
                                                <p style={{margin: "2px"}}>{ExamStore.examModal.criteria.age}</p>
                                            </div>
                                        }

                                    </Segment>
                                </section>

                                <section id="fees-section">
                                    <h2 className="section-heading">Fees</h2>
                                    <div className="border-bottom-colored"/>
                                    <Segment className="fees-wrapper margin-top-zero">
                                        <p>{ExamStore.examModal.fees}</p>
                                    </Segment>
                                </section>

                                <section id="procedure-section">
                                    <h2 className="section-heading">Selection Procedure</h2>
                                    <div className="border-bottom-colored"/>
                                    <Segment className="procedure-wrapper margin-top-zero">
                                        <p>{ExamStore.examModal.selectionProcedure}</p>
                                    </Segment>
                                </section>

                                <section id="apply-section">
                                    <h2 className="section-heading">How To Apply</h2>
                                    <div className="border-bottom-colored"/>
                                    <Segment className="apply-wrapper margin-top-zero">
                                        <h3>Follow these steps to apply for this post</h3>
                                        {
                                            ExamStore.examModal.howToApply.message &&
                                            <p>{ExamStore.examModal.howToApply.message}</p>
                                        }
                                        <div>
                                            <ol>
                                                {
                                                    ExamStore.examModal.howToApply.instructions.map((item, index) => {
                                                        return <li key={index}><p>{item}</p></li>
                                                    })
                                                }
                                            </ol>
                                        </div>
                                    </Segment>
                                </section>

                                <section id="website-section">
                                    <h2 className="section-heading">Official Website</h2>
                                    <div className="border-bottom-colored"/>
                                    <Segment className="website-wrapper margin-top-zero">
                                        <h3>Press the bellow button to check more details on official website</h3>
                                        {
                                            ExamStore.examModal.officialWebsite.message &&
                                            <p>{ExamStore.examModal.officialWebsite.message}</p>
                                        }
                                        <Button icon="globe" size="mini" target="_blank"
                                                className="animated-button"
                                                href={ExamStore.examModal.officialWebsite.link}
                                                color="black" content="Go to official website"/>
                                    </Segment>
                                </section>
                            </Grid.Column>

                            <Grid.Column computer={4} mobile={16}>
                                {
                                    ExamStore.examModal.similar.organizer.length > 0 &&
                                    <div className="related-info-wrapper">
                                        <RelatedInfo
                                            type="exam"
                                            title={`Exam's in `}
                                            data={ExamStore.examModal.similar.organizer}
                                            redirectTo={this.redirect}/>
                                    </div>
                                }
                            </Grid.Column>
                        </Grid>
                }

                <SEO page="exam_detail"
                     title={ExamStore.examModal.basicDetails.title}
                     image={ExamStore.examModal.basicDetails.imageUrl}
                     description={ExamStore.examModal.basicDetails.description}/>
            </div>
        );
    }
}

export default ExamDetail;