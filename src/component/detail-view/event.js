/**
 * Created by Yogesh on 19-12-2017.
 */

import React, {Component} from "react";
import {observer} from "mobx-react";
import EventStore from "../../route/store/event-store";
import SEO from "../seo/index";
import UiStore from '../../route/store/ui-store'
import "./event-detail.scss";
import {browserHistory} from "react-router";
import {getParsedTime, toTitleCase} from "../../util/util";
import Loading from "../loading/index";
import {Accordion, Button, Grid, Icon} from "semantic-ui-react";
import APP_CONST from "../../constants/constants";
import Slider from "react-slick";
import EventDetailCard from "../card/event-card";
import _ from "lodash";

const iconColors = [
    {name: "facebook", "color": "blue"},
    {name: "twitter", "color": "teal"},
    {name: "google plus", "color": "red"},
    {name: "vk", "color": "green"},
    {name: "linkedin", "color": "blue"},
    {name: "instagram", "color": "red"},
    {name: "youtube", "color": "red"}
];

const CarouselSettings = {
    infinite: false,
    arrows: false,
    dots: true
};

@observer
class EventDetail extends Component {

    handleContextRef = contextRef => this.setState({contextRef});
    getSimilarCard = type => {
        let data = type === 'category' ? EventStore.eventModal.similar[type] : _.uniqBy(_.union(EventStore.eventModal.similar.city, EventStore.eventModal.similar.state), 'id');
        if (data.length <= 3 && window.screen.width > 768) {
            return <Grid columns={3}>
                {
                    data.map((item, index) => {
                        return <Grid.Column computer={5} mobile={16} key={index}>
                            <EventDetailCard
                                key={index}
                                basicDetail={{...item.basicDetails, tags: item.tags, id: item.id}}
                                redirectTo={this.redirect}/>
                        </Grid.Column>
                    })
                }
            </Grid>
        }
        else {
            return <Slider {...CarouselSettings} slidesToShow={window.screen.width > 768 ? 3 : 1}>
                {
                    data.map((item, index) => {
                        return <div>
                            <EventDetailCard
                                key={index}
                                basicDetail={{...item.basicDetails, tags: item.tags, id: item.id}}
                                redirectTo={this.redirect}/>
                        </div>
                    })
                }
            </Slider>
        }
    }
    redirect = (id) => {
        browserHistory.push(`/event/${id}`);
        this.fetchBasicData(id);
    }

    constructor(props) {
        super();
        this.handleTandCAccordian = this.handleTandCAccordian.bind(this);
        this.state = {openTandC: false, contextRef: null};
    }

    componentWillMount() {
        let id = this.props.router.params.id;
        this.fetchBasicData(id);
    }

    async fetchBasicData(id) {
        UiStore.loading = true;
        await EventStore.fetchBasicDataById(id);
        UiStore.loading = false;
    }

    handleTandCAccordian() {
        this.setState({'openTandC': !this.state.openTandC})
    }

    goToSocialSite(link) {
        window.open(link, '_blank');
    }

    getIconColor(iconName) {
        return _.find(iconColors, (icon) => iconName === icon.name).color;
    }

    getDate(date) {
        if (date) {
            return new Date(date).getDate();
        }
        return "-";
    }

    getMonth(date) {
        if (date) {
            return APP_CONST.dates.month[new Date(date).getMonth()].text;
        }
        return "-";
    }

    getFormattedDateTime() {
        let dateTime = this.getMonth(EventStore.eventModal.basicDetails.startDate.date)
            + " " + this.getDate(EventStore.eventModal.basicDetails.startDate.date);
        dateTime += EventStore.eventModal.basicDetails.startDate.time ? " | " +
            getParsedTime(EventStore.eventModal.basicDetails.startDate.time) : "";
        return dateTime;
    }

    getFormattedVenue() {
        let venue = EventStore.eventModal.basicDetails.venue.line1;
        venue += EventStore.eventModal.basicDetails.venue.line2 ? (", " + EventStore.eventModal.basicDetails.venue.line2) : "";
        venue += EventStore.eventModal.basicDetails.venue.city ? (", " + EventStore.eventModal.basicDetails.venue.city) : "";
        venue += EventStore.eventModal.basicDetails.venue.state ? (", " + EventStore.eventModal.basicDetails.venue.state) : "";
        venue += EventStore.eventModal.basicDetails.venue.pincode ? (", " + EventStore.eventModal.basicDetails.venue.pincode) : "";
        return venue;
    }

    isVenueEmpty() {
        return !_.some(Object.keys(EventStore.eventModal.basicDetails.venue), (key) => EventStore.eventModal.basicDetails.venue[key] !== "");
    }

    render() {
        return (
            <div className="event-detail-container">
                <div className={`app-background page-event detail`}/>
                {
                    UiStore.loading ? <Loading/> :
                        <div>
                            <Grid className="detail-wrapper">
                                <Grid.Row>
                                    <Grid.Column computer={11} mobile={16}>
                                        <div ref={this.handleContextRef}>
                                            {
                                                EventStore.eventModal.coverImage && <div className="image-wrapper">
                                                    <img src={EventStore.eventModal.coverImage}
                                                         alt={EventStore.eventModal.basicDetails.title}
                                                         width="100%" height="100%"/>
                                                </div>
                                            }
                                            <section id="about-section">
                                                <h2 className="section-heading">About</h2>
                                                <div className="about-wrapper">
                                                    <p>{EventStore.eventModal.basicDetails.description}</p>
                                                </div>
                                            </section>
                                            {
                                                EventStore.eventModal.faqs.length !== 0 &&
                                                <section id="faq-section">
                                                    <h2 className="section-heading">FAQs</h2>
                                                    <div className="faq-wrapper">
                                                        <ol>
                                                            {
                                                                EventStore.eventModal.faqs.map((faq, index) =>
                                                                    <li><p>{faq}</p></li>
                                                                )
                                                            }
                                                        </ol>
                                                    </div>
                                                </section>
                                            }
                                            {
                                                !this.isVenueEmpty() &&
                                                <section id="venue-section">
                                                    <h2 className="section-heading">Venue</h2>
                                                    <div className="venue-wrapper">
                                            <span className="heading-2">
                                                {`${EventStore.eventModal.basicDetails.venue.line1} ${EventStore.eventModal.basicDetails.venue.city}`}
                                            </span>
                                                        <p>
                                                            {this.getFormattedVenue()}
                                                        </p>
                                                    </div>
                                                </section>
                                            }

                                            {
                                                EventStore.eventModal.activities.length !== 0 &&
                                                <section id="activities-section">
                                                    <h2 className="section-heading">Activities</h2>
                                                    <div className="activities-wrapper">
                                                        {
                                                            EventStore.eventModal.activities.map((activity, index) =>
                                                                <div className="event-activity">
                                                                    <div className="heading-2">{activity.title}</div>
                                                                    <div className="date-fees-wrapper">
                                                                        <Icon color="blue"
                                                                              name="calendar"/><span>{activity.date}</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <Icon color="green"
                                                                              name="rupee"/><span>{activity.fees}</span>
                                                                    </div>
                                                                    <p>{activity.description}</p>
                                                                </div>
                                                            )
                                                        }

                                                    </div>
                                                </section>

                                            }
                                            {
                                                EventStore.eventModal.termsAndConditions.length !== 0 &&
                                                <section id="terms-condition-section">
                                                    <Accordion>
                                                        <Accordion.Title active={this.state.openTandC}
                                                                         onClick={this.handleTandCAccordian}
                                                                         className="section-heading" as='h2'
                                                                         style={{borderBottom: this.state.openTandC ? '1px solid #c20000' : 'none'}}>
                                                            <Icon name={this.state.openTandC ? 'minus' : 'plus'}
                                                                  color="red"/>
                                                            Terms & Conditions
                                                        </Accordion.Title>
                                                        <Accordion.Content>
                                                            <div className="terms-conditions-wrapper">
                                                                <ol>
                                                                    {
                                                                        EventStore.eventModal.termsAndConditions.map((tAndC, index) =>
                                                                            <li><p>{tAndC}</p></li>
                                                                        )
                                                                    }
                                                                </ol>
                                                            </div>
                                                        </Accordion.Content>
                                                    </Accordion>
                                                </section>
                                            }
                                            <section id="tags-section">
                                                <div className="tag-wrapper">
                                                    {
                                                        EventStore.eventModal.tags.map(tag =>
                                                            <span className="_tag"
                                                                  title={toTitleCase(tag)}>{tag}</span>
                                                        )}
                                                </div>
                                            </section>
                                        </div>
                                    </Grid.Column>

                                    <Grid.Column computer={5} mobile={16}>
                                        <Grid padded className="basic-detail-wrapper">
                                            <Grid.Row>
                                                <Grid.Column>
                                                    <h1 className="event-title">{EventStore.eventModal.basicDetails.title}</h1>
                                                </Grid.Column>
                                            </Grid.Row>

                                            <Grid.Row className="date-time">
                                                <Grid.Column width={2}><Icon name="calendar" color="red"/></Grid.Column>
                                                <Grid.Column width={14}>
                                                    {this.getFormattedDateTime()}
                                                </Grid.Column>
                                            </Grid.Row>
                                            {
                                                !this.isVenueEmpty() &&
                                                <Grid.Row className="venue">
                                                    <Grid.Column width={2}><Icon name="point"
                                                                                 color="red"/></Grid.Column>
                                                    <Grid.Column
                                                        width={14}>{`${EventStore.eventModal.basicDetails.venue.line1} ${EventStore.eventModal.basicDetails.venue.city}`}</Grid.Column>
                                                </Grid.Row>
                                            }
                                            {
                                                EventStore.eventModal.basicDetails.fees &&
                                                <Grid.Row className="fees">
                                                    <Grid.Column width={2}><Icon name="rupee"
                                                                                 color="red"/></Grid.Column>
                                                    <Grid.Column
                                                        width={14}>{EventStore.eventModal.basicDetails.fees}</Grid.Column>
                                                </Grid.Row>
                                            }
                                            <Grid.Row>
                                                <Grid.Column className="website-btn">
                                                    <Button color="blue" as="a" fluid
                                                            href={EventStore.eventModal.basicDetails.website}
                                                            target="_blank" content="GO TO OFFICIAL WEBSITE"
                                                            icon='globe' labelPosition='left'/>
                                                </Grid.Column>
                                            </Grid.Row>
                                        </Grid>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                            {
                                (EventStore.eventModal.socialLinks.length !== 0) &&
                                <Grid className="social-links-wrapper">
                                    <Grid.Row>
                                        <Grid.Column textAlign='center' verticleAlign="middle">
                                            On social media:&nbsp;&nbsp;&nbsp;
                                            {
                                                EventStore.eventModal.socialLinks.map((socialLink, index) => {
                                                    return <Button icon={socialLink.name} as="a"
                                                                   href={socialLink.link}
                                                                   key={index}
                                                                   target="_blank" circular inverted
                                                                   color={this.getIconColor(socialLink.name)}/>
                                                })
                                            }
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                            }
                            {
                                (EventStore.eventModal.similar.category.length !== 0) &&
                                <div className="similar-category-wrapper">
                                    <h2 className="title">Popular
                                        in {toTitleCase(EventStore.eventModal.basicDetails.category)}</h2>
                                    {this.getSimilarCard('category')}
                                </div>
                            }
                            {
                                (EventStore.eventModal.similar.city.length !== 0 || EventStore.eventModal.similar.state.length !== 0) &&
                                <div className="similar-nearby-wrapper">
                                    <h2 className="title">Nearby You</h2>
                                    {this.getSimilarCard('nearby')}
                                </div>
                            }
                        </div>
                }
                <SEO page="event_detail"
                     title={EventStore.eventModal.basicDetails.title}
                     image={EventStore.eventModal.basicDetails.imageUrl}
                     description={EventStore.eventModal.basicDetails.description}/>
            </div>
        );
    }
}

export default EventDetail;