import React, {Component} from "react";
import {observer} from "mobx-react";
import EventResultStore from "../../route/store/result-store";
import {Button, Grid, Header, Image, Segment} from "semantic-ui-react";
import {browserHistory} from "react-router";
import Loading from "../loading/index";
import UiStore from "../../route/store/ui-store";
import "../../../node_modules/react-image-carousel/lib/css/main.min.css";
import "./style.scss";
import SubscriptionForm from "../form/subscription/subscription";
import SEO from "../seo/index";
import RelatedInfo from "../related-info/index";


@observer
class ResultDetailView extends Component {
    type = "";

    constructor(props) {
        super(props);
        this.type = 'result';
        this.fetchBasicDetail = this.fetchBasicDetail.bind(this);
        this.redirectToResult = this.redirectToResult.bind(this);
        this.eventStore = new EventResultStore(this.type);
    }

    componentWillMount() {
        this.fetchBasicDetail();
    }

    async fetchBasicDetail() {
        UiStore.loading = true;
        await this.eventStore.fetchBasicDataById(this.props.router.params.id);
        UiStore.loading = false;
    }

    redirectToResult(id) {
        browserHistory.push(`/result/${id}`);
        this.fetchBasicDetail();
    }

    render() {
        let eventResultModal = this.eventStore.resultModal;
        return (
            <div className="result-detail-container">
                {
                    UiStore.loading ? <Loading/> : <div>
                        {
                            eventResultModal.basicDetails.title &&
                            <div className="title-wrapper">
                                <h1 className="title">{eventResultModal.basicDetails.title}</h1>
                            </div>
                        }
                        <div className="content-wrapper">
                            <Grid style={{padding: "0", margin: "0"}}>
                                <Grid.Column computer={12} mobile={16}>
                                    {
                                        eventResultModal.basicDetails.description &&
                                        <div className="description-wrapper">
                                <span
                                    className="first-letter bg-extra-dark-gray">{eventResultModal.basicDetails.description.substr(0, 1)}</span>
                                            <p>{eventResultModal.basicDetails.description.substr(1, eventResultModal.basicDetails.description.length)}</p>
                                        </div>
                                    }
                                    <Button size="tiny" className="result-animated-button"
                                            content="Click here to check result" color="black" onClick={() => {
                                        window.open(eventResultModal.basicDetails.website, '_blank')
                                    }}/>
                                    <SubscriptionForm showToastr={this.props.showToastr} place="result"/>
                                </Grid.Column>
                                <Grid.Column computer={4} mobile={16}>
                                    {
                                        eventResultModal.basicDetails.imageUrl &&
                                        <Segment style={{textAlign: 'center'}}>
                                            <Image size="tiny" src={eventResultModal.basicDetails.imageUrl}
                                                   style={{margin: 'auto'}}/>
                                            <Header as="h4">{eventResultModal.basicDetails.boardOrUniversity}
                                            </Header>
                                        </Segment>
                                    }
                                    {
                                        eventResultModal.similar.boardOrUniversity.length > 0 &&
                                        <RelatedInfo type="result" title="Related Results"
                                                     data={eventResultModal.similar.boardOrUniversity}
                                                     redirectTo={this.redirectToResult}/>
                                    }
                                </Grid.Column>
                            </Grid>
                        </div>
                    </div>
                }
                <SEO page="result_detail"
                     title={eventResultModal.basicDetails.title}
                     image={eventResultModal.basicDetails.imageUrl}
                     description={eventResultModal.basicDetails.description}/>
            </div>
        );
    }
}

export default ResultDetailView;