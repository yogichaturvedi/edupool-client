import React, {Component} from "react";
import Carousel from 'react-image-carousel';
import {Button, Grid, Header, Icon, Image, Item, Rating, Segment, Table} from "semantic-ui-react";
import {observable} from "mobx";
import {observer} from "mobx-react";
import RatingModal from "../modal/rating-modal";
import BasicStore from "../../route/store/basic-store"
import {browserHistory} from "react-router";
import SEO from "../seo/index";
import "react-image-carousel/lib/css/main.min.css";
import "./style.scss";
import RelatedInfo from "../related-info/index";
import UiStore from "../../route/store/ui-store";
import Loading from "../loading/index";
import CopyPopup from "../popup/copy-popup";
import _ from 'lodash';
import ApplyNowCoaching from "../modal/coaching-apply";

@observer
class DetailView extends Component {
    @observable ratingSuccess = false;
    @observable openRatingModal = false;
    rating = 0;
    type = "";

    enquiryNow = () => {
        this.setState({enquiry: true});
    };

    constructor(props) {
        super();
        this.openRatingModal = false;
        this.type = props.router.location.pathname.split("/")[1];
        this.basicStore = new BasicStore(this.type);
        this.openRatingModalClose = this.openRatingModalClose.bind(this);
        this.getRatingDetail = this.getRatingDetail.bind(this);
        this.redirect = this.redirect.bind(this);
        this.state = {copyTextMessage: 'Click to copy', enquiry: false}
    }

    componentWillMount() {
        if (this.type !== 'school' && this.type !== 'college' && this.type !== 'coaching') {
            browserHistory("/");
        }
        this.fetchBasicDetail();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({copyTextMessage: 'Click to copy'});
    }

    async fetchBasicDetail() {
        UiStore.loading = true;
        await this.basicStore.fetchBasicDataById(this.props.router.params.id);
        UiStore.loading = false;
    }

    onRate(event) {
        this.rating = event.rating;
        this.openRatingModal = true
    }

    getRatingValue() {
        let rating = this.basicStore.basicModal.rating;
        let ratingValue = rating.value ? rating.value : 0;
        return !rating.totalUsers ? 0 : Math.ceil(ratingValue / (rating.totalUsers));

    }

    getRatingDetail() {
        let message = "Average User Rating ";
        let rating = this.getRatingValue();
        if (Number(rating) === 0) {
            return "Be the first one to rate here";
        }
        return message + rating + "/5 by " + this.basicStore.basicModal.rating.totalUsers + " users"
    }

    async openRatingModalClose(e, state, customerName = "", rateValue = 0) {
        this.openRatingModal = false;
        this.ratingSuccess = state;

        if (state) {
            this.basicStore.updateRating(rateValue, this.props.router.params.id);
        }

        if (customerName) {
            this.customerName = customerName;
        }
    }

    goToSocialSite(link) {
        window.open(link, '_blank');
    }

    redirect(id, title) {
        const uri = "/" + this.type + "/" + id;
        browserHistory.push(uri);
        this.fetchBasicDetail();
    }

    render() {
        const basicDetails = this.basicStore.basicModal.basicDetails;

        const email = <a id="email-element"
                         href={"mailto:" + basicDetails.email + "?subject=I have read your article from www.edupool.in"}>Mail
            Us</a>;

        const website = <a target="_blank" href={basicDetails.website}>Go To Website</a>;

        return (
            <div className="detail-container">
                <div className={`app-background page-${this.type}`}/>
                {
                    this.state.enquiry &&
                    <ApplyNowCoaching
                        coachingData={{...this.basicStore.basicModal.basicDetails, course : this.basicStore.basicModal.course}}
                        show={this.state.enquiry}
                        close={() => this.setState({enquiry: false})}/>
                }
                {
                    UiStore.loading ? <Loading/> :
                        <Grid className="detail-wrapper">
                            <Grid.Column computer={12} mobile={16}>
                                <div>
                                    <RatingModal open={this.openRatingModal} onClose={this.openRatingModalClose}
                                                 rating={this.rating}/>

                                    <section id="basic-details-section">
                                        <div className="border-bottom-colored"/>
                                        <Segment className="basic-detail-wrapper margin-top-zero">
                                            <Grid columns={3}>
                                                <Grid.Column computer={4} mobile={16}>
                                                    <div className="image-wrapper" width="100%">
                                                        <img src={basicDetails.imageUrl} alt={basicDetails.title}
                                                             height={200}
                                                             width={200}/>
                                                    </div>
                                                </Grid.Column>
                                                <Grid.Column computer={12} mobile={16}>
                                                    <Header as='h1' className="institute-title">
                                                        <Header.Content>
                                                            {basicDetails.title}
                                                            <span className="computerOnly">&nbsp;{<Rating
                                                                rating={this.getRatingValue()} maxRating={5} icon='star'
                                                                disabled/>}</span>
                                                        </Header.Content>
                                                        <Header.Subheader>
                                                            {basicDetails.address.line1},&nbsp;{basicDetails.address.line2},&nbsp;
                                                            {basicDetails.address.city},&nbsp;{basicDetails.address.state},&nbsp;{basicDetails.address.pincode}
                                                            <div>
                                                        <span className="mobileOnly"
                                                              style={{textAlign: 'center', marginTop: 5}}>
                                                            <Rating rating={this.getRatingValue()} maxRating={5}
                                                                    icon='star' disabled/>
                                                        </span>
                                                            </div>
                                                        </Header.Subheader>
                                                    </Header>
                                                    <Grid>
                                                        <Grid.Row columns={3}>
                                                            <Grid.Column computer={5} mobile={9}>
                                                                <Icon name="phone" color="green"/> <a
                                                                href={"tel:" + basicDetails.mobileNumber}>{basicDetails.mobileNumber}</a>
                                                            </Grid.Column>
                                                            <Grid.Column computer={5} mobile={7}>
                                                                <Icon name="mail" color="red"/>
                                                                <CopyPopup triggerElement={email}
                                                                           content={basicDetails.email}
                                                                           textToCopy={basicDetails.email}/>
                                                            </Grid.Column>
                                                            <Grid.Column computer={5} mobile={9}>
                                                                <Icon name="globe" color="blue"/>
                                                                <CopyPopup triggerElement={website}
                                                                           content={basicDetails.website}
                                                                           textToCopy={basicDetails.website}/>
                                                            </Grid.Column>
                                                            {
                                                                this.type === 'school' &&
                                                                <Grid.Column computer={5} mobile={7}>
                                                                    <Icon name="home" color="teal"/>{basicDetails.level}
                                                                </Grid.Column>
                                                            }
                                                        </Grid.Row>
                                                        <Grid.Row>
                                                            <Grid.Column>
                                                                <div className="social-icons-wrapper">
                                                                    {
                                                                        this.basicStore.basicModal.socialLinks.length > 0 &&
                                                                        <div>
                                                                    <span
                                                                        style={{fontSize: 13}}>On social networks&nbsp;&nbsp;</span>
                                                                            {
                                                                                this.basicStore.basicModal.socialLinks.map((socialLink, index) => {
                                                                                    return <Icon key={index}
                                                                                                 className={"social-link-" + socialLink.name}
                                                                                                 name={socialLink.name === 'facebook' ? 'facebook f' : socialLink.name}
                                                                                                 onClick={(e) => this.goToSocialSite(socialLink.link)}/>
                                                                                })
                                                                            }
                                                                        </div>
                                                                    }
                                                                </div>
                                                            </Grid.Column>
                                                        </Grid.Row>
                                                        <Grid.Row>
                                                            <Grid.Column>
                                                                <div className="">
                                                                    {
                                                                        this.type === "coaching" && this.basicStore.basicModal.admissionOpen &&
                                                                        <Button size="tiny"
                                                                                className="scc-card-continue-button animated-button"
                                                                                content="Apply Now" color="black"
                                                                                onClick={this.enquiryNow}/>
                                                                    }
                                                                </div>
                                                            </Grid.Column>
                                                        </Grid.Row>
                                                    </Grid>
                                                </Grid.Column>
                                            </Grid>

                                        </Segment>
                                    </section>

                                    <section id="about-us-section">
                                        <h2 className="section-heading">About Us</h2>
                                        <div className="border-bottom-colored"/>
                                        <Segment className="about-us-wrapper margin-top-zero">
                                            <p>{this.basicStore.basicModal.aboutUs.message1}</p>
                                            {
                                                this.basicStore.basicModal.aboutUs.message2 &&
                                                <p>{this.basicStore.basicModal.aboutUs.message2}</p>
                                            }
                                        </Segment>
                                    </section>

                                    {

                                        (this.type === 'school' || !this.basicStore.basicModal.course.courses || this.basicStore.basicModal.course.courses.length === 0) &&
                                        <section id="courses-section">
                                            <h2 className="section-heading">Courses & Fees</h2>
                                            <div className="border-bottom-colored"/>
                                            <Segment className="courses-wrapper margin-top-zero">
                                                <p>{this.basicStore.basicModal.course.message}</p>
                                                <Table basic='very' celled selectable unstackable>
                                                    <Table.Header>
                                                        <Table.Row>
                                                            <Table.HeaderCell>Name</Table.HeaderCell>
                                                            <Table.HeaderCell>Fees</Table.HeaderCell>
                                                            <Table.HeaderCell>Duration</Table.HeaderCell>
                                                        </Table.Row>
                                                    </Table.Header>
                                                    <Table.Body>
                                                        {

                                                            this.basicStore.basicModal.course.courseList.length > 0 ?
                                                                this.basicStore.basicModal.course.courseList.map((course, index) => {
                                                                    return <Table.Row key={"row-" + index}>
                                                                        <Table.Cell>{course.name ? course.name : "-"}</Table.Cell>
                                                                        <Table.Cell>{course.fee ? course.fee : "-"}</Table.Cell>
                                                                        <Table.Cell>{course.duration ? course.duration : "-"}</Table.Cell>
                                                                    </Table.Row>
                                                                })
                                                                :
                                                                <Table.Row><Table.Cell>-</Table.Cell></Table.Row>
                                                        }
                                                    </Table.Body>
                                                </Table>
                                            </Segment>
                                        </section>
                                    }

                                    {
                                        (this.type === 'college' || this.type === 'coaching') && this.basicStore.basicModal.course.courses && this.basicStore.basicModal.course.courses.length > 0 &&
                                        <section id="courses-section">
                                            <h2 className="section-heading">Courses & Fees</h2>
                                            <div className="border-bottom-colored"/>
                                            <Segment className="courses-wrapper margin-top-zero">
                                                <Table size="small" basic="very"
                                                       celled>
                                                    <Table.Header>
                                                        <Table.Row>
                                                            <Table.HeaderCell>Stream</Table.HeaderCell>
                                                            <Table.HeaderCell>Fee</Table.HeaderCell>
                                                            <Table.HeaderCell>Duration</Table.HeaderCell>
                                                        </Table.Row>
                                                    </Table.Header>

                                                    <Table.Body>
                                                        {
                                                            this.basicStore.basicModal.course.courses && this.basicStore.basicModal.course.courses.map((course, courseIndex) => {
                                                                return !_.isEmpty(course.stream) && Object.keys(course.stream).map((streamName) => {
                                                                    let stream = course.stream[streamName];
                                                                    return !_.isEmpty(stream.subStream) && Object.keys(stream.subStream).map((subStreamName) => {
                                                                        let subStream = stream.subStream[subStreamName];
                                                                        return subStream.subjects.length > 0 && subStream.subjects.map((subject, index) => {
                                                                            return <Table.Row key={index}>
                                                                                <Table.Cell>
                                                                                    {stream.name} - {subStream.name} {subject.name ? `( ${subject.name} )` : ""}
                                                                                </Table.Cell>
                                                                                <Table.Cell>{subject.fee}</Table.Cell>
                                                                                <Table.Cell>{subject.duration}</Table.Cell>
                                                                            </Table.Row>
                                                                        })
                                                                    })
                                                                })

                                                            })
                                                        }
                                                    </Table.Body>
                                                </Table>
                                            </Segment>
                                        </section>
                                    }


                                    <section id="hostels-section">
                                        <h2 className="section-heading">Hostel & Buses</h2>
                                        <div className="border-bottom-colored"/>
                                        <Segment className="courses-wrapper margin-top-zero">
                                            {
                                                !this.basicStore.basicModal.hostelAndBuses.hostel && !this.basicStore.basicModal.hostelAndBuses.buses &&
                                                <p>No information available right now, You may ask
                                                    directly {this.props.type} </p>
                                            }
                                            {
                                                this.basicStore.basicModal.hostelAndBuses.hostel &&
                                                <div>
                                                    <h3 style={{color: "teal", margin: "2px"}}>Hostel</h3>
                                                    <p style={{
                                                        color: "grey",
                                                        margin: "2px"
                                                    }}
                                                       className="content-wrapper">{this.basicStore.basicModal.hostelAndBuses.hostel}</p>
                                                    <br/>
                                                    <br/>
                                                </div>
                                            }
                                            {
                                                this.basicStore.basicModal.hostelAndBuses.buses &&
                                                <div>
                                                    <h3 style={{color: "teal", margin: "2px"}}>Buses</h3>
                                                    <p style={{color: "grey", margin: "2px"}}
                                                       className="content-wrapper">{this.basicStore.basicModal.hostelAndBuses.buses}</p>
                                                    <br/>
                                                </div>
                                            }
                                        </Segment>
                                    </section>


                                    <section id="events-section">
                                        <h2 className="section-heading">Events</h2>
                                        <div className="border-bottom-colored"/>
                                        <Segment className="events-wrapper margin-top-zero">
                                            {
                                                this.basicStore.basicModal.events.length > 0
                                                    ?
                                                    <div>
                                                        {
                                                            this.basicStore.basicModal.events.map((event) => {
                                                                return <Item.Group>
                                                                    <Item>
                                                                        <Image style={{height: "100px"}} size='small'
                                                                               src={event.imageUrl} bordered/>
                                                                        <Item.Content>
                                                                            <Item.Header className="content-wrapper">
                                                                                {event.title}
                                                                            </Item.Header>
                                                                            <Item.Meta as='p'
                                                                                       className="content-wrapper">{event.description}</Item.Meta>
                                                                            {
                                                                                event.link &&
                                                                                <Item.Extra style={{color: "blue"}}
                                                                                            as="p" target="_blank"
                                                                                            href={event.link}>
                                                                                    Click here to know more
                                                                                </Item.Extra>
                                                                            }
                                                                        </Item.Content>
                                                                    </Item>
                                                                </Item.Group>
                                                            })
                                                        }
                                                    </div>
                                                    :
                                                    <div>
                                                        <h4 style={{color: "grey"}}>There is no current event right
                                                            now</h4>
                                                        <a style={{color: "blue"}} target="_blank"
                                                           href={basicDetails.website}>Click here to know more</a>
                                                    </div>
                                            }
                                        </Segment>
                                    </section>

                                    <section id="rating-section">
                                        <h2 className="section-heading">Rating & Reviews</h2>
                                        <div className="border-bottom-colored"/>
                                        <Segment className="rating-wrapper margin-top-zero">
                                            <h2>{this.getRatingDetail()}<h3 style={{display: 'inline'}}>, Rate here to
                                                help new users</h3></h2>
                                            {
                                                this.ratingSuccess ?
                                                    <h3 style={{color: "green"}}>Thanks for your valuable rating</h3>
                                                    :
                                                    <div>
                                                        <Rating onRate={(a, event) => {
                                                            this.onRate(event)
                                                        }} maxRating={5} defaultRating={0} icon='star' size='massive'/>
                                                        <h5 style={{
                                                            color: "red",
                                                            display: "inline",
                                                            verticalAlign: 'super',
                                                            margin: "0 10px"
                                                        }}>1 star - Not Happy &nbsp; 5 star - Excellent</h5>

                                                    </div>
                                            }
                                        </Segment>
                                    </section>

                                    <section id="rating-section">
                                        <h2 className="section-heading">Gallery</h2>
                                        <div className="border-bottom-colored"/>
                                        <Segment className="rating-wrapper margin-top-zero">
                                            <div className="carousel-detail">
                                                <Carousel images={this.basicStore.basicModal.gallery.slice()}
                                                          thumb={true}
                                                          loop={true}
                                                          className="carousel-detail"
                                                          autoplay={3000}/>
                                            </div>
                                        </Segment>
                                    </section>
                                </div>
                            </Grid.Column>
                            <Grid.Column computer={4} mobile={16}>
                                <div className="related-info-wrapper">
                                    {
                                        this.basicStore.basicModal.similar.city.length > 0 &&
                                        <RelatedInfo type={this.type}
                                                     instance={1}
                                                     title={`${this.type}s in ${basicDetails.address.city}`}
                                                     data={this.basicStore.basicModal.similar.city}
                                                     redirectTo={this.redirect}/>
                                    }
                                    {
                                        this.basicStore.basicModal.similar.state.length > 0 &&
                                        <RelatedInfo type={this.type}
                                                     instance={2}
                                                     title={`${this.type}s in ${basicDetails.address.state}`}
                                                     data={this.basicStore.basicModal.similar.state}
                                                     redirectTo={this.redirect}/>
                                    }
                                </div>

                            </Grid.Column>
                        </Grid>
                }
                <SEO page={`${this.type}_detail`}
                     title={basicDetails.title}
                     image={basicDetails.imageUrl}
                     description={this.basicStore.basicModal.aboutUs.message1}/>
            </div>
        );
    }
}

export default DetailView;
