/**
 * Created by Amit on 21-03-2018.
 */
import React, {Component} from "react";
import {Grid} from "semantic-ui-react";
import {observer} from "mobx-react";
import ArticleNewsStore from "../../route/store/article-news-store";
import UiStore from '../../route/store/ui-store'
import {Link} from "react-router";
import Loading from "../loading/index";
import "./style.scss";
import ArticleNewsFilters from "../filters/article-news";
import App_Const from "../../constants/constants";
import {errorMessage} from "../../util/error-formatter";
import FirebaseService from '../../service/firebase-service';
import SEO from "../seo/index";
import _ from 'lodash';
import CommentForm from "../form/comment/index";


@observer
class ArticleNewsDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            comment: '',
            allArticleNews : [],
            user: {name: '', email:'', imageUrl: '', profileLink: ''}
        };
        ArticleNewsStore.initializeType(props.location.pathname.split("/")[1])
    }

    componentWillMount() {
        this.fetchArticleNews(this.props.params.id);
        this.fetchArticleNewsList();
    }

    componentWillReceiveProps(nextProps){
        if (nextProps.params.id !== this.props.params.id) {
            this.fetchArticleNews(nextProps.params.id);
        }
    }

    componentDidMount(){
       const $this = this;
        setTimeout(function () {
            const user = FirebaseService.getCurrentUser();
            if(user) {
                $this.setState({user: {name: user.displayName, email: user.email, imageUrl: user.photoURL, profileLink: user.profileLink}})
            }
            // const contentImages = document.querySelectorAll(".blog-content .content img");
            // _.each(contentImages,(element, index)=>{
            //     element.classList.add('blog-content-image-'+index);
            //     $this.wrap(element);
            // });
        },1200);
    }

    // wrap(el) {
    //     const wrapper = document.createElement('div');
    //     wrapper.classList.add('content-image-wrapper');
    //     el.parentNode.insertBefore(wrapper,el);
    //     wrapper.appendChild(el);
    // }

    componentWillUnmount() {
        ArticleNewsStore.reInitialize();
    }


    fetchArticleNews = async (id) => {
        UiStore.loading = true;
        await ArticleNewsStore.fetch(id);
        UiStore.loading = false;
    };

    fetchArticleNewsList = async() => {
        try {
            UiStore.loading = true;
            let allArticleNews = await ArticleNewsStore.fetchAll({limit: 5});
            this.setState({allArticleNews : allArticleNews});
            UiStore.loading = false;
        }
        catch (error) {
            UiStore.loading = false;
            UiStore.error.present = true;
            if (error.message === App_Const.failedToFetch) {
                UiStore.error.message = App_Const.failedToFetch;
            }
            else {
                UiStore.error.message = errorMessage(error);
            }
        }
    };

    googleSignIn = async () => {
        try {
            let user = await FirebaseService.googleSignIn();
            if (user) {
                this.setState({user});
                this.props.showToastr('success', 'Google SignIn', 'Successfully signed in through google.');
            }
        }
        catch(error){
            this.props.showToastr('error', 'Google SignIn', 'Something went wrong while signing in through google.');
        }

    };

     googleSignOut = async () => {
         try {
             await FirebaseService.googleSignOut();
             this.setState({user: {name: '', email: '', imageUrl: '', profileLink: ''}});
             this.props.showToastr('success', 'Google Sign out', 'Successfully signed out of google.');
         }
         catch(error){
             this.props.showToastr('error', 'Google Sign out', 'Something went wrong while signing out of google.');
         }
     };

    onCommentChange = message => this.setState({comment: message.slice(0,200)});

    postComment = async () => {
        if(this.state.comment) {
            ArticleNewsStore.articleNews.comment.push({
                author: this.state.user,
                message: this.state.comment,
                postedAt: new Date().toISOString()
            });
            this.setState({comment: ''});
            await ArticleNewsStore.update(this.props.params.id);
        }
    };

    render() {
        return (
            <div className="detail-container">
                <div className={`app-background page-blog`}/>
                {
                    UiStore.loading ? <Loading/> :
                        <div className="blog-detail">
                            <div className="blog-header">
                                <div className="container">
                                    <Grid>
                                        <Grid.Row columns={2}>
                                            <Grid.Column computer={10} mobile={16}>
                                                <div className="blog-title-wrapper">
                                                    <h1 className="alt-font text-extra-dark-gray text-uppercase">{ArticleNewsStore.articleNews.title}</h1>
                                                </div>
                                            </Grid.Column>
                                            <Grid.Column computer={6} mobile={16}>
                                                <div className="breadcrumb text-small alt-font">
                                                    <ul className="xs-text-center text-uppercase">
                                                        <li><Link className="text-dark-gray">{ArticleNewsStore.getDate(ArticleNewsStore.articleNews.createdAt)}</Link></li>
                                                        <li><span className="text-dark-gray">by {ArticleNewsStore.articleNews.createdBy}</span></li>
                                                        <li><Link className="text-dark-gray">{ArticleNewsStore.articleNews.category}</Link></li>
                                                    </ul>
                                                </div>
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                </div>
                            </div>
                            <Grid columns={2} style={{margin: 0}}>
                                <Grid.Column computer={12} mobile={16} className="blog-content">
                                    <div className="content" dangerouslySetInnerHTML={{__html: ArticleNewsStore.articleNews.content}}/>
                                    <CommentForm
                                        user={this.state.user}
                                        comment={this.state.comment}
                                        onCommentChange={this.onCommentChange}
                                        comments={ArticleNewsStore.articleNews.comment.reverse()}
                                        signIn={this.googleSignIn}
                                        signOut={this.googleSignOut}
                                        postComment={this.postComment}/>
                                </Grid.Column>

                                <Grid.Column  className="filters-wrapper" computer={4} mobile={16}>
                                    <ArticleNewsFilters
                                        type={ArticleNewsStore.articleNews.type}
                                        tags={ArticleNewsStore.getTags()}
                                        categories={ArticleNewsStore.getCategories()}
                                        articleNewsList={_.slice(this.state.allArticleNews, 0, 6)}
                                        filterArticleNewsList={()=>{}}/>
                                </Grid.Column>
                            </Grid>
                        </div>
                }
                <SEO page="article_detail"
                     title={ArticleNewsStore.articleNews.title}
                     image={ArticleNewsStore.articleNews.imageUrl}
                     description={ArticleNewsStore.articleNews.shortDescription}/>
            </div>
        )
    }
}

export default ArticleNewsDetail;