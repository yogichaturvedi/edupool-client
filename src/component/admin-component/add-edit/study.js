import React, {Component} from "react";
import {Button, Divider, Form, Input, Segment} from "semantic-ui-react";
import {observer} from "mobx-react";
import StudyStore from "../../../route/store/study-store";
import {browserHistory} from "react-router";
import {redirectToPage} from "../../../util/util";
import _ from "lodash";
import TagsForm from "../../form/tags";

@observer
class AddStudyMaterial extends Component {

    constructor(props) {
        super();
        this.handleSaveUpdateData = this.handleSaveUpdateData.bind(this);
        this.addTag = this.addTag.bind(this);
        this.removeTag = this.removeTag.bind(this);
        this.removeAllTags = this.removeAllTags.bind(this);

        this.emptyLinksModal = {
            title: "",
            link: ""
        };
        if (props.mode === "edit" && props.type && props.id) {
            //Fetch data relevant to id
            StudyStore.fetchBasicDataById(props.id);
        }
    }

    onFormDataChange(path, value) {
        StudyStore.studyModal[path] = value;
    }

    studyMaterialLinkChanges(path, index, value) {
        StudyStore.studyModal.links[index][path] = value;
    }

    addRemoveLink(action, linkIndex) {
        if (action === 'add') {
            StudyStore.studyModal.links.push(this.emptyLinksModal);
        }
        else {
            _.remove(StudyStore.studyModal.links, (linkInfo, index) => index === linkIndex);
        }
    }

    async handleSaveUpdateData() {
        try {
            if (this.props.mode === "edit") {
                await StudyStore.updateBasicData(this.props.id);
            }
            else {
                await StudyStore.saveBasicData();
            }
            StudyStore.reInitializeModal();
            browserHistory.push('/study-material');
        }
        catch (error) {

        }
    }

    addTag(tag) {
        StudyStore.studyModal.tags.push(tag);
    }

    removeTag(tagIndex) {
        _.remove(StudyStore.studyModal.tags, (tag, index) => index === tagIndex);
    }

    removeAllTags() {
        StudyStore.studyModal.tags = [];
    }

    render() {
        const studyDetails = StudyStore.studyModal;
        return (
            <div className="add-edit-wrapper">
                <h2 style={{color: "teal"}}>
                    {
                        this.props.mode === "edit" ?  StudyStore.studyModal.title : "Enter the details below"
                    }
                </h2>

                <section id="basic-details-section">
                    <Segment>
                        <h3>Basic Details</h3>
                        <Divider/>
                        <Form>
                            <Form.Input
                                label="Title" placeholder="Enter title"
                                value={studyDetails.title}
                                onChange={e => this.onFormDataChange("title", e.target.value)}/>
                            <Form.TextArea
                                label="Description" placeholder="Enter description"
                                value={studyDetails.description}
                                onChange={(e) => this.onFormDataChange("description", e.target.value)}/>
                        </Form>
                    </Segment>
                </section>

                <section id="study-materials-section">
                    <Segment>
                        <h3>Study Materials</h3>
                        <Divider/>
                        <Form>
                            {
                                studyDetails.links.map((data, index) => {
                                    return <Form.Group key={index}>
                                        <Form.Field width={7}>
                                            <Input
                                                placeholder={"Enter title"}
                                                label={"Title " + (index + 1)}
                                                value={studyDetails.links[index].title}
                                                onChange={ e => this.studyMaterialLinkChanges("title", index, e.target.value)}/>
                                        </Form.Field>
                                        <Form.Field width={8}>
                                            <Input
                                                placeholder={"Enter download link"}
                                                label={"Link"}
                                                value={studyDetails.links[index].link}
                                                onChange={e => this.studyMaterialLinkChanges("link", index, e.target.value)}/>
                                        </Form.Field>
                                        <Form.Button icon="trash" color="red" inverted circular
                                                     className="form-delete-button"
                                                     onClick={event => this.addRemoveLink('remove', index)}/>
                                    </Form.Group>
                                })
                            }
                        </Form>
                        <Form.Button icon="plus" color="blue" inverted circular onClick={() => this.addRemoveLink("add")}/>
                    </Segment>
                </section>

                <section id="basic-details-section">
                    <Segment>
                        <h3>Tags</h3>
                        <Divider/>
                        <Form>
                            <Form.Field>
                                <TagsForm
                                    tags={StudyStore.studyModal.tags}
                                    addTag={this.addTag}
                                    removeTag={this.removeTag}
                                    removeAllTags={this.removeAllTags}/>
                            </Form.Field>
                        </Form>
                    </Segment>
                </section>

                <section id="buttons-section">
                    <Form>
                            <Form.Group style={{marginTop: "10px"}}>
                                <Form.Field>
                                    <Button color={this.props.mode === "edit" ? "orange" : "green"}
                                            onClick={this.handleSaveUpdateData}
                                            disabled={!StudyStore.studyModal.title}
                                            content={this.props.mode === "edit" ? "Update" : "Save"} inverted/>
                                </Form.Field>
                                <Form.Field>
                                    <Button color="red" content="Close" as="a"
                                            onClick={(e) => redirectToPage("/study-material")} inverted/>
                                </Form.Field>
                            </Form.Group>
                        </Form>
                </section>
            </div>
        )
    }
}

export default AddStudyMaterial;