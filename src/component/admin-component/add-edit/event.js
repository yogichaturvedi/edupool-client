import React, {Component} from "react";
import {Button, Divider, Form, Input, Segment} from "semantic-ui-react";
import {observer} from "mobx-react";
import EventStore from "../../../route/store/event-store"
import {browserHistory} from "react-router";
import {redirectToPage} from "../../../util/util";
import TagsForm from '../../form/tags';
import './style.scss';
import _ from "lodash";
import AddressForm from "../../form/address";
import SocialLinksForm from "../../form/social-links";
import {observable} from "mobx";
import Grid from "semantic-ui-react/dist/es/collections/Grid/Grid";
import DatePicker from "react-datepicker";
import Moment from "moment";

const socialOptions = [
    {key: "facebook", "text": "Facebook", "value": "facebook"},
    {key: "twitter", "text": "Twitter", "value": "twitter"},
    {key: "google-plus", "text": "Google Plus", "value": "google plus"},
    {key: "vk", "text": "VK", "value": "vk"},
    {key: "linkedin", "text": "Linkedin", "value": "linkedin"},
    {key: "instagram", "text": "Instagram", "value": "instagram"},
    {key: "youtube", "text": "Youtube", "value": "youtube"}
];

@observer
class AddEvent extends Component {

    constructor(props) {
        super();
        this.handleSaveUpdateData = this.handleSaveUpdateData.bind(this);
        this.addTag = this.addTag.bind(this);
        this.removeTag = this.removeTag.bind(this);
        this.removeAllTags = this.removeAllTags.bind(this);
        this.fetchData = this.fetchData.bind(this);
        this.onVenueUpdate = this.onVenueUpdate.bind(this);
        this.onFormDataChange = this.onFormDataChange.bind(this);
        this.onFormBasicDataChange = this.onFormBasicDataChange.bind(this);
        this.onSocialMediaChange = this.onSocialMediaChange.bind(this);
        this.onSocialMediaOptionSelect = this.onSocialMediaOptionSelect.bind(this);
        this.addItem = this.addItem.bind(this);
        this.removeItem = this.removeItem.bind(this);
        this.onItemChange = this.onItemChange.bind(this);
        this.onActivityChange = this.onActivityChange.bind(this);
        this.addActivity = this.addActivity.bind(this);
    }

    componentWillMount() {
        this.fetchData();
    }

    async fetchData() {
        //check mode and if mode is edit then fetch data
        if (this.props.mode === "edit" && this.props.id) {
            //Fetch data relevant to id
            await EventStore.fetchBasicDataById(this.props.id);
            console.log(EventStore.eventModal);
        }
    }

    onFormBasicDataChange(path, value) {
        EventStore.eventModal.basicDetails[path] = value;
    }

    onFormDataChange(path, value) {
        EventStore.eventModal[path] = value;
    }

    async handleSaveUpdateData() {
        try {
            if (this.props.mode === "edit") {
                await EventStore.updateBasicData(this.props.id);
            }
            else {
                await EventStore.saveBasicData();
            }
            EventStore.reInitializeModal();
            browserHistory.push('/event');
        } catch (error) {

        }
    }

    addTag(tag) {
        EventStore.eventModal.tags.push(tag);
    }

    removeTag(tagIndex) {
        _.remove(EventStore.eventModal.tags, (tag, index) => index === tagIndex);
    }

    removeAllTags() {
        EventStore.eventModal.tags = [];
    }

    onVenueUpdate(field, value) {
        EventStore.eventModal.basicDetails.venue[field] = value;
        if (field === 'state') {
            EventStore.eventModal.basicDetails.venue.city = "";
        }
    }

    onSocialMediaOptionSelect(b) {
        _.delete(socialOptions, ((val) => {
            return val.value === b.value
        }))
    }

    onSocialMediaChange(index, path, value) {
        EventStore.eventModal.socialLinks[index][path] = value;
    }

    addItem(field) {
        EventStore.eventModal[field].push("");
    }

    removeItem(field, itemIndex) {
        _.remove(EventStore.eventModal[field], (value, index) => index === itemIndex);
    }

    onItemChange(field, index, value) {
        EventStore.eventModal[field][index] = value;
    }

    onActivityChange(field, index, value) {
        EventStore.eventModal.activities[index][field] = value;
    }

    addActivity() {
        EventStore.eventModal.activities.push({
            title: "",
            fees: "",
            description: "",
            date: "",
        });
    }

    handleDateTimeChange(field, type, value) {
        EventStore.eventModal.basicDetails[type + "Date"][field] = value;
        if (type === "start") {
            let sd = JSON.parse(JSON.stringify(EventStore.eventModal.basicDetails.startDate));
            EventStore.eventModal.basicDetails.endDate = _.clone(EventStore.eventModal.basicDetails.startDate);
        }
    }

    render() {
        let eventModal = EventStore.eventModal;
        const basicDetails = eventModal.basicDetails;
        return (
            <div className="add-edit-wrapper">
                <h2 style={{color: "teal"}}>Fill the details</h2>
                <Form>

                    <section id="basic-details-section">
                        <Segment className="basic-details">
                        <h3>Basic Details</h3>
                        <Divider/>
                        <Form.Input label="Title"
                                    placeholder="Enter title"
                                    value={basicDetails.title}
                                    onChange={(e) => {
                                        this.onFormBasicDataChange("title", e.target.value)
                                    }}/>
                        <Form.Input label="Thumbnail"
                                    placeholder="Enter thumbnail image url"
                                    icon='image'
                                    iconPosition='left'
                                    value={basicDetails.imageUrl}
                                    onChange={(e) => {
                                        this.onFormBasicDataChange("imageUrl", e.target.value)
                                    }}/>
                        <Form.Input size="small" label='Cover Image'
                                    placeholder='Enter Cover Image'
                                    icon='image'
                                    iconPosition='left'
                                    value={eventModal.coverImage}
                                    onChange={(e) => {
                                        this.onFormDataChange("coverImage", e.target.value)
                                    }}/>
                        <Form.Input label="Website Address"
                                    placeholder="Enter website address"
                                    value={basicDetails.website}
                                    onChange={(e) => {
                                        this.onFormBasicDataChange("website", e.target.value)
                                    }}/>
                        <Form.TextArea label="Description"
                                       placeholder="Enter description"
                                       value={basicDetails.description}
                                       onChange={(e) => {
                                           this.onFormBasicDataChange("description", e.target.value)
                                       }}/>
                        <Form.Group widths="equal" inline>
                            <Form.Field label="Starts on"/>
                            <Form.Field>
                                <DatePicker required
                                            selected={eventModal.basicDetails.startDate.date}
                                            onChange={(date) => this.handleDateTimeChange('date', 'start', date)}
                                            isClearable={true}
                                            dateFormat="DD/MM/YYYY"
                                            minDate={Moment()}
                                            placeholderText="DD/MM/YY"
                                />
                            </Form.Field>
                            <Form.Field>
                                <Form.Input type="time"
                                            value={eventModal.basicDetails.startDate.time}
                                            onChange={(e) => this.handleDateTimeChange('time', 'start', e.target.value)}/>
                            </Form.Field>

                            <Form.Field label="Ends on"/>
                            <Form.Field>
                                <DatePicker required
                                            selected={eventModal.basicDetails.endDate.date
                                            || eventModal.basicDetails.startDate.date}
                                            onChange={(date) => this.handleDateTimeChange('date', 'end', date)}
                                            isClearable={true}
                                            dateFormat="DD/MM/YYYY"
                                            minDate={eventModal.basicDetails.startDate.date}
                                            placeholderText="DD/MM/YY"
                                />
                            </Form.Field>
                            <Form.Field>
                                <Form.Input type="time"
                                            value={eventModal.basicDetails.endDate.time}
                                            onChange={(e) => this.handleDateTimeChange('time', 'end', e.target.value)}/>
                            </Form.Field>
                        </Form.Group>
                        <Form.Input size="small" label='Organizer'
                                    placeholder='Organizer'
                                    icon='user'
                                    iconPosition='left'
                                    value={basicDetails.organizer}
                                    onChange={(e) => {
                                        this.onFormBasicDataChange("organizer", e.target.value)
                                    }}/>
                        <Form.Group>
                            <Form.Field width={13}>
                                <Form.Input size="small" label='Category'
                                            placeholder='category'
                                            icon='tag'
                                            iconPosition='left'
                                            value={basicDetails.category}
                                            onChange={(e) => {
                                                this.onFormBasicDataChange("category", e.target.value)
                                            }}/>
                            </Form.Field>
                            <Form.Field width={3}>
                                <Form.Input size="small" label='Fees'
                                            placeholder='Fees'
                                            icon='rupee'
                                            iconPosition='left'
                                            value={basicDetails.fees}
                                            onChange={(e) => {
                                                this.onFormBasicDataChange("fees", e.target.value)
                                            }}/>
                            </Form.Field>
                        </Form.Group>
                    </Segment>
                    </section>

                    <Segment className="address-details">
                        <h3>Venue</h3>
                        <Divider/>
                        <AddressForm
                            address={basicDetails.venue}
                            onAddressUpdate={this.onVenueUpdate}/>
                    </Segment>

                    <Segment className="faqs-details">
                        <h3>FAQ's</h3>
                        <Divider/>
                        <Form.Field>
                            <CustomItem
                                for="faqs"
                                title="FAQs"
                                items={eventModal.faqs || []}
                                add={this.addItem}
                                remove={this.removeItem}
                                onChange={this.onItemChange}/>
                        </Form.Field>
                    </Segment>

                    <Segment className="terms-conditions-details">
                        <h3>Terms & Conditions</h3>
                        <Divider/>
                        <Form.Field>
                            <CustomItem
                                for="termsAndConditions"
                                title="Terms & Conditions"
                                items={eventModal.termsAndConditions || []}
                                add={this.addItem}
                                remove={this.removeItem}
                                onChange={this.onItemChange}/>
                        </Form.Field>
                    </Segment>

                    <Segment className="activities-details">
                        <Form.Field>
                            <h3>Activities</h3>
                            <Divider/>
                            <Activity activities={eventModal.activities || []}
                                      add={this.addActivity}
                                      remove={this.removeItem}
                                      onChange={this.onActivityChange}/>
                        </Form.Field>
                    </Segment>

                    <Segment className="social-links-details">
                        <Form.Field>
                            <h3>Social Links</h3>
                            <Divider/>
                            <SocialLinksForm
                                socialLinks={eventModal.socialLinks || []}
                                onSocialMediaOptionSelect={this.onSocialMediaOptionSelect}
                                onSocialMediaChange={this.onSocialMediaChange}
                                remove={(index) => this.removeItem('socialLinks', index)}/>
                        </Form.Field>
                    </Segment>

                    <Segment className="tags-details">
                        <h3>Tags</h3>
                        <Divider/>
                        <Form.Field>
                            <TagsForm
                                tags={eventModal.tags}
                                addTag={this.addTag}
                                removeTag={this.removeTag}
                                removeAllTags={this.removeAllTags}/>
                        </Form.Field>
                    </Segment>

                    <Form.Group>
                        <Form.Field>
                            <Button inverted color={this.props.mode === "edit" ? "orange" : "green"}
                                    onClick={this.handleSaveUpdateData}
                                    disabled={!basicDetails.title}
                                    content={this.props.mode === "edit" ? "Update" : "Save"}/>
                        </Form.Field>
                        <Form.Field>
                            <Button inverted color="red" content="Close" as="a"
                                    onClick={(e) => redirectToPage("/event")}/>
                        </Form.Field>
                    </Form.Group>
                </Form>
            </div>
        )
    }
}

@observer
class CustomItem extends Component {
    @observable items = [];

    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.items = props.items
    }

    componentWillReceiveProps(props) {
        this.items = props.items
    }

    onChange(index, value) {
        this.items[index] = value;
        this.props.onChange(this.props.for, index, value);
    }

    render() {
        return (
            <div>
                {/*<h4>{this.props.title}</h4>*/}
                {/*<h5 style={{color: 'red'}}>{this.props.message}</h5>*/}
                <Form>
                    {
                        this.items.map((item, index) => {
                            return <Form.Group key={index}>
                                <Form.Field width={15}>
                                    <Input placeholder={`Enter ${this.props.title} ${index + 1}`}
                                           value={this.items[index]}
                                           onChange={(e) => {
                                               this.onChange(index, e.target.value)
                                           }}
                                           label={`${this.props.title} ${index + 1}`}/>
                                </Form.Field>
                                <Form.Button icon="trash" color="red" inverted circular
                                             onClick={(event) => this.props.remove(this.props.for, index)}/>
                            </Form.Group>
                        })
                    }
                    <Button icon="plus" color="green" inverted circular
                            onClick={(event) => this.props.add(this.props.for)}/>
                </Form>
            </div>);
    }
}

@observer
class Activity extends Component {
    @observable activities = [];

    constructor(props) {
        super(props);
        this.activities = props.activities
    }

    componentWillReceiveProps(props) {
        this.activities = props.activities
    }

    render() {
        return (
            <div>
                <h4>{this.props.title}</h4>
                <h5 style={{color: 'red'}}>{this.props.message}</h5>
                <Form>
                    {
                        this.activities.map((item, index) => {
                            return <Grid key={index}>
                                <Grid.Column width="8">
                                    <Form.Input placeholder={`Enter title ${index + 1}`}
                                                value={this.activities[index].title}
                                                onChange={(e) => {
                                                    this.props.onChange('title', index, e.target.value)
                                                }}
                                                label={`Title ${index + 1}`}/>
                                    <Form.Group>
                                        <Form.Field width="8">
                                            <Input placeholder={`Enter fees ${index + 1}`}
                                                   value={this.activities[index].fees}
                                                   onChange={(e) => {
                                                       this.props.onChange('fees', index, e.target.value)
                                                   }}
                                                   label="Fees" icon="rupee"/>
                                        </Form.Field>
                                        <Form.Field width="8">
                                            <Input placeholder="DD/MM/YYYY"
                                                   value={this.activities[index].date}
                                                   onChange={(e) => {
                                                       this.props.onChange('date', index, e.target.value)
                                                   }}
                                                   label="Date" icon="calendar"/>
                                        </Form.Field>
                                    </Form.Group>
                                </Grid.Column>
                                <Grid.Column width="7">
                                    <Form.TextArea value={this.activities[index].description}
                                                   placeholder={`Enter Description ${index + 1}`}
                                                   rows={4}
                                                   onChange={(e) => {
                                                       this.props.onChange('description', index, e.target.value)
                                                   }}
                                                   label={`Description ${index + 1}`}/>
                                </Grid.Column>
                                <Grid.Column width="1" style={{paddingRight: 0, paddingBottom: 0, paddingLeft: 0}}>
                                    <Form.Button
                                        icon="trash"
                                        color="red"
                                        inverted
                                        circular
                                        onClick={(event) => this.props.remove('activities', index)}/>
                                </Grid.Column>
                            </Grid>
                        })
                    }
                    <Button icon="plus" color="green" inverted circular
                            onClick={(event) => this.props.add()}/>
                </Form>
            </div>);
    }
}

export default AddEvent;