import React, {Component} from "react";
import "../../../../node_modules/react-image-carousel/lib/css/main.min.css";
import {
    Button,
    Checkbox,
    Divider,
    Dropdown,
    Form,
    Header,
    Icon,
    Message,
    Responsive,
    Segment,
    Table
} from "semantic-ui-react";
import {action, toJS} from "mobx";
import {observer} from "mobx-react";
import BasicStore from "../../../route/store/basic-store";
import Store from "../../../route/store/store";
import {updateObjectProperty} from "../../../util/collection";
import {browserHistory} from 'react-router';
import Gallery from "../../form/gallery";
import GalleryStore from "../../../route/store/gallery-store";
import {admin_details, redirectToPage, schoolLevels} from "../../../util/util";
import TagsForm from '../../form/tags';
import AddressForm from "../../form/address";
import SocialLinksForm from "../../form/social-links";
// import Courses from "../../../static-data/courses.json";
import Course from "../../../model/course";
import AddCourseSection from "../component/course";
import _ from "lodash";
import './style.scss'

const SocialOptions = [
    {key: "facebook", "text": "Facebook", "value": "facebook"},
    {key: "twitter", "text": "Twitter", "value": "twitter"},
    {key: "google-plus", "text": "Google Plus", "value": "google plus"},
    {key: "vk", "text": "VK", "value": "vk"},
    {key: "linkedin", "text": "Linkedin", "value": "linkedin"},
    {key: "instagram", "text": "Instagram", "value": "instagram"},
    {key: "youtube", "text": "Youtube", "value": "youtube"}
];

@observer
class BasicAdd extends Component {


    getCourses = async () => {
        let courses = [];
        if(this.type === "college"){
            courses = await this.basicStore.getCourses();
        }
        else{
            courses = await this.basicStore.getCoachingCourses();
        }
        // let courses = await this.basicStore.getCourses();
        courses = _.sortBy(courses, 'name');
        this.setState({courses});
        this.streamOptions();
    };
    removeAllTags = () => {
        this.basicStore.basicModal.tags = [];
    }
    /* Course section functions */
    updateCourse = (type, value) => {
        let data = {
            [type]: value
        };
        this.setState(data);
        if (type === 'stream') {
            this.subStreamOptions(value);
        }
    };
    streamOptions = () => {
        let streams = [];
        _.each(this.state.courses, course => {
            let courseStreams = _.map(Object.keys(course.stream), (streamName) => {
                return {
                    key: `${course.name}-${streamName}`,
                    text: `${course.stream[streamName].name}    ( ${course.name} )`,
                    value: `${course.value}-${streamName}`
                };
            });
            streams = _.union(streams, courseStreams);
        });
        streams = _.sortBy(streams, ['text']);
        this.setState({streams});
    };
    subStreamOptions = (stream) => {
        let parts = stream.split("-");
        let matchedCourse = _.find(this.state.courses, (course) => course.value === parts[0] && !_.isEmpty(course.stream[parts[1]]));
        if (matchedCourse && !_.isEmpty(matchedCourse.stream[parts[1]].subStream)) {
            let stream = matchedCourse.stream[parts[1]];
            let subStreams = _.map(Object.keys(stream.subStream), (subStreamName) => {
                return {key: subStreamName, text: stream.subStream[subStreamName].name, value: subStreamName};
            });
            subStreams = _.sortBy(subStreams, ['text']);
            this.setState({subStreams});
        }
        else {
            this.setState({subStreams: []});
        }
    };
    updateSubject = (courseIndex, stream, subStream, subject) => {
        console.log(this.basicStore.basicModal.course.courses[courseIndex].stream[stream]);
        let matchedSubStream = this.basicStore.basicModal.course.courses[courseIndex].stream[stream].subStream[subStream];
        if (matchedSubStream) {
            matchedSubStream.updateSubjects(subject);
        }
    };
    addSubjects = () => {
        let parts = this.state.stream.split("-");
        let course = _.find(this.state.courses, course => course.value === parts[0]);
        if (course) {
            let stream = _.find(course.stream, stream => stream.value === parts[1]);
            if (stream) {
                let $this = this;
                let subStreams = _.filter(stream.subStream, subStream => {
                    return _.indexOf($this.state.subStream, subStream.value) !== -1
                });
                let newCourse = {
                    id: course.id,
                    name: course.name,
                    value: course.value,
                    stream: {
                        [stream.value]: {
                            id: stream.id,
                            name: stream.name,
                            value: stream.value,
                            subStream: {}
                        }
                    }
                };
                _.each(subStreams, (subStream) => {
                    newCourse.stream[stream.value].subStream[subStream.value] = {
                        id: subStream.id,
                        name: subStream.name,
                        value: subStream.value,
                        subjects: []
                    };
                });
                this.basicStore.basicModal.course.courses.push(new Course(newCourse));
            }
        }
        this.setState({stream: "", subStream: []});
    };
    @action
    removeSubStream = (course, stream, subStream) => {
        let courses = this.basicStore.basicModal.course.courses;
        let matchedCourse = _.find(courses, c => c.value === course);
        if (matchedCourse) {
            console.log(this.basicStore.basicModal.course.courses);
            matchedCourse.stream[stream].removeSubStream(subStream);
            // delete matchedCourse.stream[stream].subStream[subStream];
        }
        this.setState({stream: "", subStream: []});
    };
    removeSubject = (course, stream, subStreamValue, subject) => {
        let courses = this.basicStore.basicModal.course.courses;
        let matchedCourse = _.find(courses, c => c.value === course);
        if (matchedCourse) {
            let subStream = matchedCourse.stream[stream].subStream[subStreamValue];
            let matchedSubjectIndex = _.findIndex(subStream.subjects, sub => sub.name === subject.name);
            if (matchedSubjectIndex !== -1) {
                subStream.subjects.splice(matchedSubjectIndex, 1);
            }
        }
    };
    addSubStream = (courseName, stream, subStreamName) => {
        let matchedCourse = _.find(this.basicStore.basicModal.course.courses, course => course.value === courseName);
        if (matchedCourse) {
            matchedCourse.stream[stream].addSubStream(subStreamName);
        }
    };

    constructor(props) {
        super(props);
        this.onFormDataChange = this.onFormDataChange.bind(this);
        this.onSocialMediaOptionSelect = this.onSocialMediaOptionSelect.bind(this);
        this.handleSaveUpdateData = this.handleSaveUpdateData.bind(this);
        this.addNewImageUrl = this.addNewImageUrl.bind(this);
        this.removeImageUrl = this.removeImageUrl.bind(this);
        this.onGalleryDataChange = this.onGalleryDataChange.bind(this);
        this.validateImageUrls = this.validateImageUrls.bind(this);
        this.addTag = this.addTag.bind(this);
        this.removeTag = this.removeTag.bind(this);
        this.removeAllTags = this.removeAllTags.bind(this);
        this.fetchData = this.fetchData.bind(this);
        this.initializeAuditInfo = this.initializeAuditInfo.bind(this);
        this.onAddressUpdate = this.onAddressUpdate.bind(this);
        this.review = this.review.bind(this);
        this.removeSocialLink = this.removeSocialLink.bind(this);
        this.onSocialMediaChange = this.onSocialMediaChange.bind(this);
        this.onSocialMediaOptionSelect = this.onSocialMediaOptionSelect.bind(this);

        this.state = {
            stream: "",
            streams: [],
            subStream: [],
            subStreams: [],
            courses: []
        };

        GalleryStore.message = "";
        //Add empty course object to state
        this.emptyCourseData = {name: "", fee: "", duration: ""};
        this.emptyEventData = {title: "", imageUrl: "", description: "", link: ""};
        this.emptySocialMediaData = {"name": "", "link": ""};
        this.basicStore = new BasicStore(props.type);
    }

    componentWillMount() {
        this.getCourses();
    }

    componentDidMount() {
        if (this.props.mode === "edit" && this.props.type && this.props.id) {
            this.fetchData();
        }
        else {
            this.initializeAuditInfo();
        }
    }

    async fetchData() {
        await this.basicStore.fetchBasicDataById(this.props.id);
        if (this.props.mode === "edit") {
            let $this = this;
            _.each(this.basicStore.basicModal.course.courses, (course, index) => {
                $this.basicStore.basicModal.course.courses[index] = new Course(course);
            });
        }
        this.initializeAuditInfo();
    }

    initializeAuditInfo() {
        // Initialize audit info with user details
        if (!this.props.admin) {
            if (Store.auditInfo.name) {
                this.basicStore.basicModal.auditInfo = Store.auditInfo;
            }
            else {
                browserHistory.push("/home");
            }
        }
        else {
            this.basicStore.basicModal.auditInfo = admin_details;
        }
    }

    onFormDataChange(path, value) {
        updateObjectProperty(this.basicStore.basicModal, path, value);
    }

    onGalleryDataChange(index, value) {
        this.basicStore.basicModal.gallery[index] = value;
    }

    validateImageUrls() {
        GalleryStore.validateGalleryImages(this.basicStore.basicModal.gallery);
    }

    addNewImageUrl() {
        this.basicStore.basicModal.gallery.push("");
        this.validateImageUrls();
    }

    removeImageUrl(imageIndex) {
        _.remove(this.basicStore.basicModal.gallery, (image, index) => index === imageIndex);
        this.validateImageUrls();
    }

    onCourseListDataChange(index, property, value) {
        this.basicStore.basicModal.course.courseList[index][property] = value;
    }

    onAddressUpdate(field, value) {
        this.basicStore.basicModal.basicDetails.address[field] = value;
        if (field === 'state') {
            this.basicStore.basicModal.basicDetails.address.city = "";
        }
    }

    onAddressListChange(index, value) {
        this.basicStore.basicModal.contactUs.addresses[index] = value;
    }

    onEventListChanges(index, path, value) {
        this.basicStore.basicModal.events[index][path] = value;
    }

    onSocialMediaOptionSelect(b) {
        _.delete(SocialOptions, ((val) => {
            return val.value === b.value
        }))
    }

    onSocialMediaChange(index, path, value) {
        this.basicStore.basicModal.socialLinks[index][path] = value;
    }

    async handleSaveUpdateData() {
        if (GalleryStore.getInvalidGalleryImages(this.basicStore.basicModal.gallery.length) > 0) {
            alert("Can't save the form. Gallery image URL's are invalid.");
            return;
        }

        try {
            if (this.props.mode === "edit") {
                await this.basicStore.updateBasicData(this.props.id);
            }
            else {
                await this.basicStore.saveBasicData();
            }
            this.basicStore.reInitializeModal();
            browserHistory.push('/' + this.props.type);
        }
        catch (e) {
        }
    }

    addTag(tag) {
        this.basicStore.basicModal.tags.push(tag);
    }

    removeTag(tagIndex) {
        _.remove(this.basicStore.basicModal.tags, (tag, index) => index === tagIndex);
    }

    review(e) {
        this.basicStore.basicModal.reviewed = !this.basicStore.basicModal.reviewed;
    }

    removeSocialLink(itemIndex) {
        _.remove(this.basicStore.basicModal.socialLinks, (value, index) => index === itemIndex);
    }

    getButtonsSection(position) {
        return <section id="buttons-section" className={`${position}-placement`}>
            <Form>
                <Form.Group inline>
                    <Form.Field>
                        <Button inverted color={this.props.mode === "edit" ? "orange" : "green"}
                                onClick={(e) => this.handleSaveUpdateData()}
                                disabled={!this.basicStore.basicModal.basicDetails.title}
                                content={this.props.mode === "edit" ? "Update" : "Save"}/>
                    </Form.Field>
                    <Form.Field>
                        <Button inverted color="red" content="Close" as="a"
                                onClick={(e) => redirectToPage("/" + this.props.type)}/>
                    </Form.Field>
                </Form.Group>
            </Form>
        </section>
    }

    render() {
        let basicModal = this.basicStore.basicModal;
        const renderLabel = label => ({
            color: 'blue',
            content: `${label.text}`,
            icon: 'check',
        });
        return (
            <div className="add-edit-wrapper">
                <h2 style={{color: "teal"}}>
                    {
                        this.props.mode === "edit" ? basicModal.basicDetails.title : "Enter the details below"
                    }
                </h2>

                {/*{ this.getButtonsSection('top') }*/}

                <section id="basic-details-section">
                    <Segment>
                        <h3>Basic Details</h3>
                        <Divider/>
                        <Form>
                            {
                                this.props.admin &&
                                <Form.Group>
                                    <Form.Field>
                                        <Checkbox name="review"
                                                  checked={this.basicStore.basicModal.reviewed}
                                                  onChange={this.review}
                                                  label="Make it visible for all users"/>
                                    </Form.Field>
                                    {
                                        this.props.admin && (this.props.type === 'college' || this.props.type === 'coaching') && [
                                            <Form.Field key="featured">
                                                <Checkbox
                                                    name="featured"
                                                    checked={this.basicStore.basicModal.featured}
                                                    onChange={() => this.basicStore.basicModal.featured = !this.basicStore.basicModal.featured}
                                                    label="Featured"/>
                                            </Form.Field>,
                                            <Form.Field key="admission-open">
                                                <Checkbox name="admission-open"
                                                          checked={this.basicStore.basicModal.admissionOpen}
                                                          onChange={() => this.basicStore.basicModal.admissionOpen = !this.basicStore.basicModal.admissionOpen}
                                                          label="Admissions Open"/>
                                            </Form.Field>
                                        ]
                                    }
                                </Form.Group>

                            }
                            <Form.Input placeholder='Enter title'
                                        value={basicModal.basicDetails.title}
                                        onChange={(e) => {
                                            this.onFormDataChange("basicDetails.title", e.target.value)
                                        }} label="Title"/>
                            <Form.TextArea placeholder='Enter description'
                                           value={basicModal.basicDetails.description}
                                           onChange={(e) => {
                                               this.onFormDataChange("basicDetails.description", e.target.value)
                                           }} label="Description"/>
                            <Form.Input placeholder='Enter image url'
                                        value={basicModal.basicDetails.imageUrl}
                                        onChange={(e) => {
                                            this.onFormDataChange("basicDetails.imageUrl", e.target.value)
                                        }} label="Image URL"/>
                            <Form.Input placeholder='Enter website address'
                                        value={basicModal.basicDetails.website} onChange={(e) => {
                                this.onFormDataChange("basicDetails.website", e.target.value)
                            }} label="Website"/>
                            <Form.Input placeholder='Enter mobile number'
                                        value={basicModal.basicDetails.mobileNumber}
                                        onChange={(e) => {
                                            this.onFormDataChange("basicDetails.mobileNumber", e.target.value)
                                        }} label="Mobile Number"/>
                            <Form.Input placeholder='Enter email'
                                        value={basicModal.basicDetails.email}
                                        onChange={(e) => {
                                            this.onFormDataChange("basicDetails.email", e.target.value)
                                        }} label="Email Address"/>
                            {
                                this.props.type === 'school' &&
                                <Form.Field>
                                    <label>School Level</label>
                                    <Dropdown
                                        width={5}
                                        size="mini"
                                        minCharacters={0}
                                        placeholder="Select"
                                        value={basicModal.basicDetails.level}
                                        onChange={(e, value) => this.onFormDataChange('basicDetails.level', value.value)}
                                        options={toJS(schoolLevels)} selection search fluid/>
                                </Form.Field>
                            }
                        </Form>
                    </Segment>
                </section>

                <section id="address">
                    <Segment>
                        <h3>Address</h3>
                        <Divider/>
                        <AddressForm address={this.basicStore.basicModal.basicDetails.address}
                                     onAddressUpdate={this.onAddressUpdate}/>
                    </Segment>
                </section>

                <section id="gallery">
                    <Gallery
                        images={this.basicStore.basicModal.gallery}
                        onImageUrlChange={this.onGalleryDataChange}
                        onBlur={this.validateImageUrls}
                        removeImageUrl={this.removeImageUrl}
                        addNewImageUrl={this.addNewImageUrl}/>
                </section>

                <section id="about-us">
                    <Segment>
                        <h3>About Us</h3>
                        <Divider/>
                        <Form>
                            <Form.TextArea placeholder='Enter message 1' label="Message 1"
                                           value={basicModal.aboutUs.message1}
                                           onChange={(e) => {
                                               this.onFormDataChange("aboutUs.message1", e.target.value)
                                           }}/>
                            <Form.TextArea placeholder='Enter message 2' label="Message 2"
                                           value={basicModal.aboutUs.message2}
                                           onChange={(e) => {
                                               this.onFormDataChange("aboutUs.message2", e.target.value)
                                           }}/>
                        </Form>
                    </Segment>
                </section>

                {
                    (this.props.type === 'college' || this.props.type === 'coaching' )&& <section id="courses-and-fees">
                        <Segment>
                            <h3>Courses & Fees</h3>
                            <Divider/>
                            <Form>
                                <Form.TextArea
                                    placeholder='Enter message'
                                    label="Message"
                                    value={this.basicStore.basicModal.course.message} onChange={(e) => {
                                    this.onFormDataChange("course.message", e.target.value)
                                }}/>
                                <Responsive as="h4" size="mini" color="blue">
                                    <Icon name='book'/>
                                    Add Course
                                </Responsive>
                                <Divider/>
                                <Form.Group>
                                    <Form.Field width={5}>
                                        <Dropdown
                                            placeholder="Select Stream"
                                            renderLabel={renderLabel}
                                            label="Select Degree"
                                            onChange={(e, value) => this.updateCourse('stream', value.value)}
                                            value={this.state.stream}
                                            options={toJS(this.state.streams)}
                                            selectOnBlur={false}
                                            fluid search selection/>
                                    </Form.Field>

                                    <Form.Field width={9}>
                                        <Dropdown
                                            placeholder="Select Sub Streams"
                                            renderLabel={renderLabel}
                                            onChange={(e, value) => this.updateCourse('subStream', value.value)}
                                            value={this.state.subStream}
                                            options={toJS(this.state.subStreams)}
                                            selectOnBlur={false}
                                            // allowAdditions={true}
                                            // onAddItem={this.addSubStream}
                                            disabled={!this.state.stream}
                                            fluid multiple search selection/>
                                    </Form.Field>

                                    <Form.Button size="small" color="blue" width={2} onClick={this.addSubjects}>
                                        Add
                                    </Form.Button>

                                </Form.Group>
                            </Form>

                            {

                                this.basicStore.basicModal.course.courses && this.basicStore.basicModal.course.courses.map((course, courseIndex) => {
                                    return <AddCourseSection
                                        key={courseIndex}
                                        courseIndex={courseIndex}
                                        course={course}
                                        courseList={this.basicStore.basicModal.course.courseList}
                                        removeSubStream={this.removeSubStream}
                                        removeSubject={this.removeSubject}
                                        updateSubject={this.updateSubject}/>
                                })
                            }

                            {
                                this.basicStore.basicModal.course.courseList.length > 0 && [
                                    <Message size="tiny" attached="top" negative icon>
                                        <Icon name="info circle"/>
                                        <Message.Content>
                                            <Message.Header>To Be Removed</Message.Header>
                                            This section will be removed once new section updates
                                        </Message.Content>
                                    </Message>,
                                    <Segment attached>
                                        <Table size="small" basic="very" celled>
                                            <Table.Header>
                                                <Table.Row>
                                                    <Table.HeaderCell>Subject</Table.HeaderCell>
                                                    <Table.HeaderCell>Fee</Table.HeaderCell>
                                                    <Table.HeaderCell>Duration</Table.HeaderCell>
                                                </Table.Row>
                                            </Table.Header>

                                            <Table.Body>
                                                {
                                                    this.basicStore.basicModal.course.courseList.map((c, index) => {
                                                        return <Table.Row key={index}>
                                                            <Table.Cell>{c.name}</Table.Cell>
                                                            <Table.Cell>{c.fee}</Table.Cell>
                                                            <Table.Cell>{c.duration}</Table.Cell>
                                                        </Table.Row>
                                                    })
                                                }
                                            </Table.Body>
                                        </Table>
                                    </Segment>
                                ]
                            }
                        </Segment>
                    </section>
                }

                {

                    (this.props.type === 'school' || this.props.type === 'coaching') && <section id="courses-and-fees">
                        <Segment>
                            <h3>Courses & Fees</h3>
                            <Divider/>
                            <Form>
                                <Form.TextArea placeholder='Enter message' label="Message"
                                               value={basicModal.course.message} onChange={(e) => {
                                    this.onFormDataChange("course.message", e.target.value)
                                }}/>
                                <h3>Add your course</h3>
                                {
                                    basicModal.course.courseList.map((course, index) => {
                                        index = index + 1;
                                        return <Form.Group widths='equal'
                                                           key={"course-form-field=" + index}>
                                            <Form.Input
                                                value={basicModal.course.courseList[index - 1].name}
                                                onChange={(e) => {
                                                    this.onCourseListDataChange(index - 1, "name", e.target.value);
                                                }}
                                                placeholder={"Enter course"}
                                                label={"Course " + index + " Name"}/>
                                            <Form.Input
                                                value={basicModal.course.courseList[index - 1].fee}
                                                onChange={(e) => {
                                                    this.onCourseListDataChange(index - 1, "fee", e.target.value);
                                                }}
                                                placeholder={"Enter fee"}
                                                label={"Course " + index + " Fee"}/>
                                            <Form.Input
                                                value={basicModal.course.courseList[index - 1].duration}
                                                onChange={(e) => {
                                                    this.onCourseListDataChange(index - 1, "duration", e.target.value);
                                                }}
                                                placeholder={"Enter duration"}
                                                label={"Course " + index + " Duration"}/>
                                        </Form.Group>
                                    })
                                }
                                <Button icon="plus" color="blue" inverted circular onClick={(event) => {
                                    basicModal.course.courseList.push(this.emptyCourseData);
                                }}/>
                            </Form>
                        </Segment>
                    </section>
                }


                <section id="hostel-and-buses">
                    <Segment>
                        <h3>Hostels & Buses</h3>
                        <Divider/>
                        <Form>
                            <Form.TextArea label="Hostel info"
                                           value={basicModal.hostelAndBuses.hostel}
                                           onChange={(e) => {
                                               this.onFormDataChange("hostelAndBuses.hostel", e.target.value)
                                           }}/>
                            <br/>
                            <Form.TextArea label="Buses info"
                                           value={basicModal.hostelAndBuses.buses}
                                           onChange={(e) => {
                                               this.onFormDataChange("hostelAndBuses.buses", e.target.value)
                                           }}/>
                        </Form>
                        <br/>
                    </Segment>
                </section>

                <section id="contact-us">
                    <Segment>
                        <h3>Contact Us</h3>
                        <Divider/>
                        <Form>
                            <Form.Input placeholder="Enter phone number"
                                        label="Phone Number"
                                        value={basicModal.contactUs.mobileNumber}
                                        onChange={(e) => {
                                            this.onFormDataChange("contactUs.mobileNumber", e.target.value)
                                        }}/>
                            <Form.Input placeholder="Enter email address"
                                        label="Email Address"
                                        value={basicModal.contactUs.email}
                                        onChange={(e) => {
                                            this.onFormDataChange("contactUs.email", e.target.value)
                                        }}/>
                            <Form.Input placeholder="Enter website address"
                                        label="Website Address"
                                        value={basicModal.contactUs.website}
                                        onChange={(e) => {
                                            this.onFormDataChange("contactUs.website", e.target.value)
                                        }}/>
                            <h3>Add your all address here</h3>
                            {
                                basicModal.contactUs.addresses.map((address, index) => {
                                    index = index + 1;
                                    return <Form.Input
                                        key={"contact-number-" + index}
                                        placeholder={"Enter address " + index}
                                        label={"Address " + index}
                                        value={basicModal.contactUs.addresses[index - 1]}
                                        onChange={(e) => {
                                            this.onAddressListChange(index - 1, e.target.value,)
                                        }}/>
                                })
                            }
                            <Button icon="plus" color="blue" inverted circular onClick={() => {
                                basicModal.contactUs.addresses.push("")
                            }}/>
                        </Form>
                    </Segment>
                </section>

                <section id="events">
                    <Segment>
                        <h3>Events</h3>
                        <Divider/>
                        <Form>
                            <h3>Add your event here</h3>
                            {
                                basicModal.events.map((eventt, index) => {
                                    index = index + 1;
                                    return <div>
                                        <Header as="h4">{"Event " + index + " Info"}</Header>
                                        <Form.Input placeholder="Enter title" label="Title"
                                                    value={basicModal.events[index - 1].title}
                                                    onChange={(e) => {
                                                        this.onEventListChanges(index - 1, "title", e.target.value,)
                                                    }}/>
                                        <Form.Input placeholder="Enter image url"
                                                    label="Image URL"
                                                    value={basicModal.events[index - 1].imageUrl}
                                                    onChange={(e) => {
                                                        this.onEventListChanges(index - 1, "imageUrl", e.target.value,)
                                                    }}/>
                                        <Form.TextArea placeholder="Enter description"
                                                       label="Description"
                                                       value={basicModal.events[index - 1].description}
                                                       onChange={(e) => {
                                                           this.onEventListChanges(index - 1, "description", e.target.value,)
                                                       }}/>
                                        <Form.Input placeholder="Enter link" label="Link"
                                                    value={basicModal.events[index - 1].link}
                                                    onChange={(e) => {
                                                        this.onEventListChanges(index - 1, "link", e.target.value,)
                                                    }}/>
                                        <Divider/>
                                    </div>
                                })
                            }
                            <Button icon="plus" color="blue" inverted circular onClick={() => {
                                basicModal.events.push(this.emptyEventData)
                            }}/>
                        </Form>
                    </Segment>
                </section>

                <section id="social-links">
                    <Segment>
                        <h3>Social Links</h3>
                        <Divider/>
                        <SocialLinksForm
                            socialLinks={this.basicStore.basicModal.socialLinks}
                            onSocialMediaOptionSelect={this.onSocialMediaOptionSelect}
                            onSocialMediaChange={this.onSocialMediaChange}
                            remove={this.removeSocialLink}/>
                    </Segment>
                </section>

                <section id="tags">
                    <Segment>
                        <h3>Tags</h3>
                        <Divider/>
                        <Form>
                            <TagsForm
                                tags={this.basicStore.basicModal.tags}
                                addTag={this.addTag}
                                removeTag={this.removeTag}
                                removeAllTags={this.removeAllTags}/>
                        </Form>
                    </Segment>
                </section>

                {this.getButtonsSection('bottom')}

            </div>
        );
    }
}

export default BasicAdd;

