import React, {Component} from "react";
import {Button, Divider, Form, Input, Segment} from "semantic-ui-react";
import {observer} from "mobx-react";
import ExamStore from "../../../route/store/exam-store";
import {updateObjectProperty} from "../../../util/collection";
import {browserHistory} from "react-router";
import {redirectToPage} from "../../../util/util";
import TagsForm from "../../form/tags";
import _ from "lodash";
import './style.scss';

@observer
class AddExam extends Component {

    constructor(props) {
        super(props);
        this.onImportantDatesChanges = this.onImportantDatesChanges.bind(this);
        this.addRemoveImportantDates = this.addRemoveImportantDates.bind(this);
        this.addTag = this.addTag.bind(this);
        this.removeTag = this.removeTag.bind(this);
        this.removeAllTags = this.removeAllTags.bind(this);

        this.emptyImportantDateModal = {
            title: "",
            date: ""
        };
    }

    componentDidMount(){
        this.fetchBasicData();
    }

    componentWillUnmount(){
        ExamStore.reInitializeModal();
    }

    async fetchBasicData(){
        if (this.props.mode === "edit" && this.props.type && this.props.id) {
            //Fetch data relevant to id
            await ExamStore.fetchBasicDataById(this.props.id);
        }

        // In create mode add one empty instruction (At least one instruction is required)
        if(ExamStore.examModal.howToApply.instructions.length === 0){
            ExamStore.examModal.howToApply.instructions.push("");
        }
    }


    onFormDataChange(path, value) {
        updateObjectProperty(ExamStore.examModal, path, value);
    }

    onImportantDatesChanges(path, index, value) {
        ExamStore.examModal.importantDates[index][path] = value;
    }

    onInstructionChange(index, value) {
        ExamStore.examModal.howToApply.instructions[index] = value;
    }

    addRemoveImportantDates(action, dateInfoIndex){
        if(action === 'add') {
            ExamStore.examModal.importantDates.push(this.emptyImportantDateModal)
        }
        else{
            _.remove(ExamStore.examModal.importantDates, (dateInfo, index) => index === dateInfoIndex);
        }
    }


    addRemoveInstruction(action, instructionIndex){
        if(action === 'add'){
            ExamStore.examModal.howToApply.instructions.push("");
        }
        else{
            _.remove(ExamStore.examModal.howToApply.instructions, (instruction, index) => index === instructionIndex);
        }
    }

    async handleSaveUpdateData() {
        try {
            if (this.props.mode === "edit") {
                await ExamStore.updateBasicData(this.props.id);
            }
            else {
                await ExamStore.saveBasicData();
            }
            ExamStore.reInitializeModal();
            browserHistory.push('/exam');
        }
        catch (error) {

        }
    }

    addTag(tag) {
        ExamStore.examModal.tags.push(tag);
    }

    removeTag(tagIndex) {
        _.remove(ExamStore.examModal.tags, (tag, index) => index === tagIndex);
    }

    removeAllTags(){
        ExamStore.examModal.tags = [];
    }

    getButtonsSection(position){
        return <section id="buttons-section" className={`${position}-placement`}>
            <Form>
                <Form.Group inline>
                    <Form.Field>
                        <Button inverted color={this.props.mode === "edit" ? "orange" : "green"}
                                onClick={() => this.handleSaveUpdateData()}
                                disabled={!ExamStore.examModal.basicDetails.title}
                                content={this.props.mode === "edit" ? "Update" : "Save"}/>
                    </Form.Field>
                    <Form.Field>
                        <Button color="red" content="Close"
                                as="a" onClick={(e) => redirectToPage("/" + this.props.type)} inverted/>
                    </Form.Field>
                </Form.Group>
            </Form>
        </section>
    }

    render() {
        const examData = ExamStore.examModal;
        return (
            <div className="add-edit-wrapper">
                <div>
                    <h2 style={{color: "teal"}}>
                        {
                            this.props.mode === "edit" ?  examData.basicDetails.title : "Enter the details below"
                        }
                    </h2>

                    {/*{this.getButtonsSection('top')}*/}

                    <section id="basic-details-section">
                        <Segment>
                            <h3>Basic Details </h3>
                            <Divider/>
                            <Form>
                                <label>Title</label>
                                <Form.Input
                                        placeholder='Enter title'
                                        value={examData.basicDetails.title}
                                        onChange={ e => this.onFormDataChange("basicDetails.title", e.target.value)}/>

                                <label>Image URL</label>
                                <Form.Input
                                    placeholder='Enter image url'
                                    value={examData.basicDetails.imageUrl}
                                    onChange={ e => this.onFormDataChange("basicDetails.imageUrl", e.target.value)}/>

                                <label>Organizer</label>
                                <Form.Input
                                    placeholder='Enter Organizer'
                                    value={examData.basicDetails.organizer}
                                    onChange={ e => this.onFormDataChange("basicDetails.organizer", e.target.value)}/>

                                <label>Description</label>
                                <Form.TextArea
                                    placeholder='Enter description'
                                    value={examData.basicDetails.description}
                                    onChange={ e => this.onFormDataChange("basicDetails.description", e.target.value)}/>

                            </Form>
                        </Segment>
                    </section>

                    <section id="notice">
                        <Segment>
                            <h3>Notice</h3>
                            <Divider/>
                            <Form>
                                <label>Notice 1</label>
                                <Form.TextArea
                                    placeholder='Enter notice info'
                                    value={examData.notice.message1}
                                    onChange={e => this.onFormDataChange("notice.message1", e.target.value)}/>

                                <label>Notice 2</label>
                                <Form.TextArea
                                    placeholder='Enter notice info'
                                    value={examData.notice.message2}
                                    onChange={e => this.onFormDataChange("notice.message2", e.target.value)}/>
                            </Form>
                        </Segment>
                    </section>

                    <section id="number-of-vacancies">
                        <Segment>
                            <h3>Vacancies</h3>
                            <Divider/>
                            <Form>
                                <Form.TextArea
                                    placeholder='Enter vacancy info'
                                    value={examData.numberOfVacancies}
                                    onChange={ e => this.onFormDataChange("numberOfVacancies", e.target.value)}/>
                            </Form>
                        </Segment>
                    </section>

                    <section id="important-dates">
                        <Segment>
                            <h3>Important Dates</h3>
                            <Divider/>
                            <ImportantDates
                                importantDates={ExamStore.examModal.importantDates}
                                onImportantDatesChanges={this.onImportantDatesChanges}
                                addRemoveImportantDates={this.addRemoveImportantDates}/>
                        </Segment>
                    </section>

                    <section id="criteria">
                        <Segment>
                            <h3>Criteria</h3>
                            <Divider/>
                            <Form>
                                <label>Educational Qualification</label>
                                <Form.TextArea
                                    placeholder='Enter educational criteria info'
                                    value={examData.criteria.educational}
                                    onChange={ e => this.onFormDataChange("criteria.educational", e.target.value)}/>

                                <label>Age Limit</label>
                                <Form.TextArea
                                    placeholder='Enter age criteria info'
                                    value={examData.criteria.age}
                                    onChange={ e => this.onFormDataChange("criteria.age", e.target.value)}/>
                            </Form>
                        </Segment>
                    </section>

                    <section id="fees">
                        <Segment>
                            <h3>Fees</h3>
                            <Divider/>
                            <Form>
                                <Form.TextArea
                                    placeholder='Enter exam fee info'
                                    value={examData.fees}
                                    onChange={e => this.onFormDataChange("fees", e.target.value)}/>
                            </Form>
                        </Segment>
                    </section>

                    <section id="selection-procedure">
                        <Segment>
                            <h3>Selection Procedure</h3>
                            <Divider/>
                            <Form>
                                <Form.TextArea
                                    placeholder='Enter selection procedure info'
                                    value={examData.selectionProcedure}
                                    onChange={ e => this.onFormDataChange("selectionProcedure", e.target.value)}/>
                            </Form>
                        </Segment>
                    </section>

                    <section id="how-to-apply">
                        <Segment>
                            <h3>How To Apply</h3>
                            <Divider/>
                            <Form>
                                <Form.TextArea
                                    placeholder='Enter message for apply instruction'
                                    value={examData.howToApply.message}
                                    onChange={e => this.onFormDataChange("howToApply.message", e.target.value)}/>
                                {
                                    examData.howToApply.instructions.map((date, index) => {
                                        return <Form.Group key={index}>
                                            <Form.Field width={15}>
                                                <Input label={"Instruction " + (index + 1)}
                                                    placeholder={"Enter instruction"}
                                                       value={examData.howToApply.instructions[index]}
                                                       onChange={e => this.onInstructionChange(index, e.target.value)}/>
                                            </Form.Field>
                                            {
                                                index !==0 && <Form.Button icon="trash" color="red" inverted circular
                                                                      onClick={(event) => this.addRemoveInstruction('remove', index)}/>
                                            }
                                        </Form.Group>
                                    })
                                }
                                <Button icon="plus" color="blue" inverted circular onClick={() => {this.addRemoveInstruction('add')}}/>
                            </Form>
                        </Segment>
                    </section>

                    <section id="official-website">
                        <Segment>
                            <h3>Official Website</h3>
                            <Divider/>
                            <Form>
                                <label>Message</label>
                                <Form.TextArea
                                    placeholder='Enter message'
                                    value={examData.officialWebsite.message}
                                    onChange={ e => this.onFormDataChange("officialWebsite.message", e.target.value)}/>

                                <label>Enter the link of official website</label>
                                <Form.Input placeholder='Enter link here'
                                    value={examData.officialWebsite.link}
                                    onChange={ e => this.onFormDataChange("officialWebsite.link", e.target.value)}/>
                            </Form>
                        </Segment>
                    </section>

                    <section id="tags">
                        <Segment>
                            <h3>Tags</h3>
                            <Divider/>
                            <Form>
                                <TagsForm
                                    tags={ExamStore.examModal.tags}
                                    addTag={this.addTag}
                                    removeTag={this.removeTag}
                                    removeAllTags={this.removeAllTags}/>
                            </Form>
                        </Segment>
                    </section>

                    { this.getButtonsSection('bottom')}
                </div>
            </div>
        );
    }
}


@observer
class ImportantDates extends Component{
    render(){
        return(
            <Form>
                {
                    this.props.importantDates.map((date, index) => {
                        return <Form.Group widths='16' key={index}>
                            <Form.Field width={9}>
                                <Input placeholder={`Enter Title ${index+1}`}
                                       label="Title"
                                       value={this.props.importantDates[index].title}
                                       onChange={(e) => { this.props.onImportantDatesChanges("title", index, e.target.value)}}/>
                            </Form.Field>
                            <Form.Field width={5}>
                                <Input placeholder={`Enter Date ${index+1}`}
                                        label={"Date"}
                                        value={this.props.importantDates[index].date}
                                        onChange={(e) =>{this.props.onImportantDatesChanges("date", index, e.target.value)}}/>
                            </Form.Field>
                            <Form.Button icon="trash" color="red" inverted circular
                                         onClick={(event) => this.props.addRemoveImportantDates('remove', index)}/>
                        </Form.Group>
                    })
                }
                <Button icon="plus" color="blue" inverted circular onClick={() => {this.props.addRemoveImportantDates('add')}}/>
            </Form>
        );
    }
}

export default AddExam;

