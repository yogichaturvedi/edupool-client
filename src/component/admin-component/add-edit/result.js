import React, {Component} from "react";
import {Button, Divider, Form, Segment} from "semantic-ui-react";
import {observer} from "mobx-react";
import {observable} from 'mobx'
import ResultStore from "../../../route/store/result-store"
import {browserHistory} from "react-router";
import {redirectToPage} from "../../../util/util";
import TagsForm from '../../form/tags';
import './style.scss';
import _ from "lodash";

@observer
class AddResult extends Component {

    constructor(props) {
        super();
        this.handleSaveUpdateData = this.handleSaveUpdateData.bind(this);
        this.addTag = this.addTag.bind(this);
        this.removeTag = this.removeTag.bind(this);
        this.removeAllTags = this.removeAllTags.bind(this);
        this.eventStore = new ResultStore();
        this.fetchData = this.fetchData.bind(this);
        this.addItem = this.addItem.bind(this);
        this.removeItem = this.removeItem.bind(this);
        this.onItemChange = this.onItemChange.bind(this);
    }

    componentWillMount() {
        this.fetchData();
    }

    async fetchData() {
        //check mode and if mode is edit then fetch data
        if (this.props.mode === "edit" && this.props.id) {
            //Fetch data relevant to id
            await this.eventStore.fetchBasicDataById(this.props.id);
        }
    }

    onFormDataChange(path, value) {
        this.eventStore.resultModal.basicDetails[path] = value;
    }

    async handleSaveUpdateData() {
        try {
            if (this.props.mode === "edit") {
                await this.eventStore.updateBasicData(this.props.id);
            }
            else {
                await this.eventStore.saveBasicData();
            }
            this.eventStore.reInitializeModal();
            browserHistory.push('/result');
        } catch (error) {

        }
    }

    addTag(tag) {
        this.eventStore.resultModal.tags.push(tag);
    }

    removeTag(tagIndex) {
        _.remove(this.eventStore.resultModal.tags, (tag, index) => index === tagIndex);
    }

    removeAllTags() {
        this.eventStore.resultModal.tags = [];
    }

    addItem(field) {
        this.eventResultStore.eventResultModal[field].push("");
    }

    removeItem(field, itemIndex) {
        _.remove(this.eventResultStore.eventResultModal[field], (value, index) => index === itemIndex);
    }

    onItemChange(field, index, value) {
        this.eventResultStore.eventResultModal[field][index] = value;
    }

    getButtonsSection(position){
        return <section id="buttons-section" className={`${position}-placement`}>
            <Form>
                <Form.Group inline>
                    <Form.Field>
                        <Button inverted color={this.props.mode === "edit" ? "orange" : "green"}
                                onClick={this.handleSaveUpdateData}
                                disabled={!this.eventStore.resultModal.basicDetails.title}
                                content={this.props.mode === "edit" ? "Update" : "Save"}/>
                    </Form.Field>
                    <Form.Field>
                        <Button inverted color="red" content="Close" as="a"
                                onClick={(e) => redirectToPage("/result")}/>
                    </Form.Field>
                </Form.Group>
            </Form>
        </section>
    }

    render() {
        const basicDetails = this.eventStore.resultModal.basicDetails;
        return (
            <div className="add-edit-wrapper">
                <h2 style={{color: "teal"}}>
                    {
                        this.props.mode === "edit" ? basicDetails.title : "Enter the details below"
                    }
                </h2>

                {/*{this.getButtonsSection('top')}*/}

                <section id="basic-details-section">
                    <Segment>
                        <h3>Basic Details</h3>
                        <Divider/>
                        <Form>
                            <Form.Input label="Title"
                                        placeholder="Enter title"
                                        value={basicDetails.title}
                                        onChange={(e) => {
                                            this.onFormDataChange("title", e.target.value)
                                        }}/>
                            {
                                <Form.Input label="Board / University"
                                            placeholder="Enter Board / University"
                                            value={basicDetails.boardOrUniversity}
                                            onChange={(e) => {
                                                this.onFormDataChange("boardOrUniversity", e.target.value)
                                            }}/>
                            }
                            <Form.Input label="Image URL"
                                        placeholder="Enter image url"
                                        value={basicDetails.imageUrl}
                                        onChange={(e) => {
                                            this.onFormDataChange("imageUrl", e.target.value)
                                        }}/>
                            <Form.Input label="Website Address"
                                        placeholder="Enter website address"
                                        value={basicDetails.website}
                                        onChange={(e) => {
                                            this.onFormDataChange("website", e.target.value)
                                        }}/>
                            <Form.TextArea label="Description"
                                           placeholder="Enter description"
                                           value={basicDetails.description}
                                           onChange={(e) => {
                                               this.onFormDataChange("description", e.target.value)
                                           }}/>
                        </Form>
                    </Segment>
                </section>

                <section id="tags">
                    <Segment>
                        <h3>Tags</h3>
                        <Divider/>
                        <Form>
                            <TagsForm
                                tags={this.eventStore.resultModal.tags}
                                addTag={this.addTag}
                                removeTag={this.removeTag}
                                removeAllTags={this.removeAllTags}/>
                        </Form>
                    </Segment>
                </section>

                {this.getButtonsSection('bottom')}

            </div>

        )
    }
}

export default AddResult;