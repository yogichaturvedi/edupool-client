/**
 * Created by Amit on 16-03-2018.
 */
import React, {Component} from "react";
import {observer} from "mobx-react";
import ArticleNewsStore from "../../../route/store/article-news-store";
import {Button, Divider, Form, Segment} from "semantic-ui-react";
import {admin_details, redirectToPage} from "../../../util/util";
import TagsForm from "../../form/tags";
import Store from "../../../route/store/store";
import {browserHistory} from "react-router";
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import _ from 'lodash';
import './style.scss';



@observer
class AddEditArticleNews extends Component {
    constructor(props) {
        super(props);
        this.state = {
            type: this.props.location.pathname.split("/")[1],
            mode : this.props.params.id ? 'edit' : 'create', editorState : EditorState.createEmpty()
        };
        ArticleNewsStore.initializeType(this.state.type)
    }

    componentWillMount(){
        if (this.state.mode === "edit") {
            this.fetchData();
        }
    }

    componentWillUnmount(){
        ArticleNewsStore.reInitialize();
    }

    fetchData = async () => {
        await ArticleNewsStore.fetch(this.props.params.id);
        const content = htmlToDraft(ArticleNewsStore.articleNews.content);
        if (content) {
            const contentState = ContentState.createFromBlockArray(content.contentBlocks);
            const editorState = EditorState.createWithContent(contentState);
            this.setState({editorState: editorState});
        }
        this.initializeAuditInfo();
    };

    initializeAuditInfo = () => {
        // Initialize audit info with user details
        if (!this.props.admin) {
            if (Store.auditInfo.name) {
                ArticleNewsStore.articleNews.auditInfo = Store.auditInfo;
            }
            else {
                browserHistory.push(`/${this.state.type}`);
            }
        }
        else {
            ArticleNewsStore.articleNews.auditInfo = admin_details;
        }
    };

    onContentChange = editorState => {
        this.setState({editorState: editorState});
        ArticleNewsStore.articleNews.content = draftToHtml(convertToRaw(editorState.getCurrentContent()));
    };

    getButtonsSection(position){
        return <section id="buttons-section" className={`${position}-placement`}>
            <Form>
                <Form.Group inline>
                    <Form.Field>
                        <Button
                            color={this.state.mode === "edit" ? "orange" : "green"}
                            onClick={(e) => this.saveUpdateData()}
                            disabled={!ArticleNewsStore.articleNews.title}
                            content={this.state.mode === "edit" && ArticleNewsStore.articleNews.show ? "Update" : "Save"} inverted/>
                    </Form.Field>
                    <Form.Field>
                        <Button
                            color="yellow"
                            onClick={this.saveUpdateDraft}
                            disabled={!ArticleNewsStore.articleNews.title}
                            content={this.state.mode === "edit" && !ArticleNewsStore.articleNews.show ? "Update Draft" : "Save Draft"} inverted/>
                    </Form.Field>
                    <Form.Field>
                        <Button
                            color="red" content="Close" as="a"
                            onClick={(e) => redirectToPage(`/${this.state.type}`)} inverted/>
                    </Form.Field>
                </Form.Group>
            </Form>
        </section>
    }

    onFormDataChange = event => ArticleNewsStore.articleNews[event.target.name] = event.target.value;

    show = () => ArticleNewsStore.articleNews.show = !ArticleNewsStore.articleNews.show;

    addTag = tag => ArticleNewsStore.articleNews.tags.push(tag);

    removeTag = tagIndex => _.remove(ArticleNewsStore.articleNews.tags, (tag, index) => index === tagIndex);

    removeAllTags = () => ArticleNewsStore.articleNews.tags = [];

     saveUpdateData = async () => {
        try {
            ArticleNewsStore.articleNews.show = true;
            if (this.state.mode === "edit") {
                await ArticleNewsStore.update(this.props.params.id);
            }
            else {
                ArticleNewsStore.articleNews.comment = [];
                await ArticleNewsStore.save();
            }
            ArticleNewsStore.reInitialize();
            browserHistory.push(`/${this.state.type}`);
        }
        catch (e) {
            console.log(e);
        }
    };

    saveUpdateDraft = async () =>{
        try {
            ArticleNewsStore.articleNews.show = false;
            if (this.state.mode === "edit") {
                await ArticleNewsStore.update(this.props.params.id);
            }
            else{
                ArticleNewsStore.articleNews.comment = [];
                await ArticleNewsStore.save();
            }
            ArticleNewsStore.reInitialize();
            browserHistory.push(`/${this.state.type}`);
        }
        catch (e) {
            console.log(e);
        }
    }
    render(){
        return(
            <Segment basic className="body-container no-padding-segment">
                <div className={`app-background page-${this.type}`}/>
                <div className='add-edit-wrapper'>

                    <h2 style={{color: "teal"}}>Enter the details below</h2>

                    <section id="basic-details-section">
                        <Segment>
                            <Form>
                                <Form.Input
                                    placeholder='Enter title'
                                    value={ArticleNewsStore.articleNews.title}
                                    name="title"
                                    onChange={this.onFormDataChange}
                                    label="Title"/>
                                <Form.TextArea
                                    placeholder='Enter Short Description'
                                    value={ArticleNewsStore.articleNews.shortDescription}
                                    name="shortDescription"
                                    onChange={this.onFormDataChange}
                                    label="Description"/>
                                <Form.Input
                                    placeholder='Enter image url'
                                    value={ArticleNewsStore.articleNews.thumbnail}
                                    name="thumbnail"
                                    onChange={ this.onFormDataChange }
                                    label="Thumbnail"/>
                                <Form.Input
                                    placeholder='Enter Category'
                                    value={ArticleNewsStore.articleNews.category}
                                    name="category"
                                    onChange={ this.onFormDataChange }
                                    label="Category"/>

                            </Form>
                        </Segment>
                    </section>

                    <section id="content-section">
                        <Segment>
                        <Form>
                            <Editor
                                editorState={this.state.editorState}
                                wrapperClassName="content-wrapper"
                                editorClassName="content-editor"
                                toolbarClassName="content-toolbar"
                                onEditorStateChange={this.onContentChange}
                            />
                        </Form>
                        </Segment>
                    </section>

                    <section id="tags">
                        <Segment>
                            <h3>Tags</h3>
                            <Divider/>
                            <Form>
                                <TagsForm
                                    tags={ArticleNewsStore.articleNews.tags}
                                    addTag={this.addTag}
                                    removeTag={this.removeTag}
                                    removeAllTags={this.removeAllTags}/>
                            </Form>
                        </Segment>
                    </section>

                    {this.getButtonsSection('bottom')}
                </div>
            </Segment>
        );
    }
}

export default AddEditArticleNews