import React, {Component} from 'react';
import {Button, Divider, Form, Grid, Segment} from "semantic-ui-react";
import {observer} from "mobx-react";
import HomeStore from "../../../route/store/home-store";
import Gallery from "../../form/gallery";
import GalleryStore from "../../../route/store/gallery-store";
import ContactNumberForm from "../../form/contact";
import ContactNumberStore from "../../../route/store/contact-number-store";
import _ from 'lodash';
import SocialLinksForm from "../../form/social-links";
import {browserHistory} from "react-router";
import UiStore from "../../../route/store/ui-store";
import AppError from "../../error/index";
import App_Const from "../../../constants/constants";
import {errorMessage} from "../../../util/error-formatter";


const socialOptions = [
    {key: "facebook", "text": "Facebook", "value": "facebook"},
    {key: "twitter", "text": "Twitter", "value": "twitter"},
    {key: "google-plus", "text": "Google Plus", "value": "google plus"},
    {key: "vk", "text": "VK", "value": "vk"},
    {key: "linkedin", "text": "Linkedin", "value": "linkedin"},
    {key: "instagram", "text": "Instagram", "value": "instagram"},
    {key: "youtube", "text": "Youtube", "value": "youtube"}
];


@observer
class HomeInfo extends Component {

    constructor(props) {
        super(props);
        this.addNewImageUrl = this.addNewImageUrl.bind(this);
        this.removeImageUrl = this.removeImageUrl.bind(this);
        this.onGalleryDataChange = this.onGalleryDataChange.bind(this);
        this.validateImageUrls = this.validateImageUrls.bind(this);
        this.addNewContactNumber = this.addNewContactNumber.bind(this);
        this.removeContactNumber = this.removeContactNumber.bind(this);
        this.onContactNumberChange = this.onContactNumberChange.bind(this);
        this.validateContactNumbers = this.validateContactNumbers.bind(this);
        this.onSocialMediaChange = this.onSocialMediaChange.bind(this);
        this.onSocialMediaOptionSelect = this.onSocialMediaOptionSelect.bind(this);
        this.removeSocialLink = this.removeSocialLink.bind(this);
        GalleryStore.message = "";
        UiStore.error.present = false;
        UiStore.error.message = "";
    }

    componentWillMount() {
        HomeStore.reInitializeModal();
        this.fetchInfo();
    }

    async fetchInfo() {
        try {
            await HomeStore.fetchHomeInfo();
        }
        catch (error) {
            UiStore.error.present = true;
            if (error.message === App_Const.failedToFetch) {
                UiStore.error.message = App_Const.failedToFetch;
            }
            else {
                UiStore.error.message = errorMessage(error);
            }
        }
    }

    onGalleryDataChange(index, url) {
        HomeStore.homeInfo.images[index] = url;
    }

    validateImageUrls() {
        GalleryStore.validateGalleryImages(HomeStore.homeInfo.images);
    }

    addNewImageUrl() {
        HomeStore.homeInfo.images.push("");
        this.validateImageUrls();
    }

    removeImageUrl(imageIndex) {
        _.remove(HomeStore.homeInfo.images, (image, index) => index === imageIndex);
        this.validateImageUrls();
    }

    onContactNumberChange(index, url) {
        HomeStore.homeInfo.contactNumbers[index] = url;
    }

    validateContactNumbers() {
        ContactNumberStore.validateContactNumbers(HomeStore.homeInfo.contactNumbers);
    }

    addNewContactNumber() {
        HomeStore.homeInfo.contactNumbers.push("");
        this.validateContactNumbers();
    }

    removeContactNumber(numberIndex) {
        _.remove(HomeStore.homeInfo.contactNumbers, (image, index) => index === numberIndex);
        this.validateContactNumbers();
    }

    onSocialMediaOptionSelect(b) {
        _.delete(socialOptions, ((val) => {
            return val.value === b.value
        }))
    }

    onSocialMediaChange(index, path, value) {
        HomeStore.homeInfo.socialLinks[index][path] = value;
    }

    onAddressChange(address) {
        HomeStore.homeInfo.address = address;
    }

    onAboutUsChange(aboutUs) {
        HomeStore.homeInfo.aboutUs = aboutUs;
    }

    onEmailChange(emailType, email) {
        HomeStore.homeInfo[emailType] = email;
    }

    async saveOrUpdate() {
        try {
            await HomeStore.updateHomeInfo();
            browserHistory.push("/home");
        }
        catch (error) {
            this.props.showToastr('error', 'Home Info Update', 'Error in updating home info');
        }
    }

    async retry() {
        try {
            UiStore.loading = true;
            await HomeStore.fetchHomeInfo();
            UiStore.error.present = false;
            UiStore.error.message = "";
            UiStore.loading = false;
        }
        catch (error) {
            UiStore.loading = false;
            UiStore.error.present = true;
            if (error.message === App_Const.failedToFetch) {
                UiStore.error.message = App_Const.failedToFetch;
            }
            else {
                UiStore.error.message = errorMessage(error);
            }
        }
    }

    removeSocialLink(itemIndex) {
        _.remove(HomeStore.homeInfo.socialLinks, (value, index) => index === itemIndex);
    }

    render() {
        const contextRef = this.state && this.state.contextRef ? this.state.contextRef : null;
        let scrollItems = [];
        return (
            <div className="add-edit-wrapper">
                {
                    UiStore.error.present ?
                        <div>
                            <AppError retry={this.retry} error={UiStore.error}/>
                        </div>
                        :
                        <Segment basic className="no-padding-segment">
                            <div className="app-background"/>
                            <div>
                                <Grid>
                                    <Grid.Column>
                                        <section id="contact-info">
                                            <h2 style={{color: "teal"}}>Enter the details below</h2>
                                            <Segment>
                                                <h3>Contact Info</h3>
                                                <Divider/>
                                                <Form>
                                                    <Form.Input placeholder='Enter Info Email' label="Info Email"
                                                                value={HomeStore.homeInfo.infoEmail}
                                                                onChange={(e) => {
                                                                    this.onEmailChange("infoEmail", e.target.value)
                                                                }}/>
                                                    <Form.Input placeholder='Enter Query Email' label="Query Email"
                                                                value={HomeStore.homeInfo.queryEmail}
                                                                onChange={(e) => {
                                                                    this.onEmailChange("queryEmail", e.target.value)
                                                                }}/>
                                                    <Form.TextArea placeholder='Address' label="Address"
                                                                   value={HomeStore.homeInfo.address}
                                                                   onChange={(e) => {
                                                                       this.onAddressChange(e.target.value)
                                                                   }}/>
                                                </Form>
                                            </Segment>
                                        </section>

                                        <section id="contact-numbers">
                                            <ContactNumberForm
                                                contactNumbers={HomeStore.homeInfo.contactNumbers}
                                                onContactNumberChange={this.onContactNumberChange}
                                                onBlur={this.validateContactNumbers}
                                                removeContactNumber={this.removeContactNumber}
                                                addNewContactNumber={this.addNewContactNumber}/>
                                        </section>

                                        <section id="social-links">
                                            <Segment>
                                                <h3>Social Media Links</h3>
                                                <Divider/>
                                                <SocialLinksForm
                                                    socialLinks={HomeStore.homeInfo.socialLinks}
                                                    onSocialMediaOptionSelect={this.onSocialMediaOptionSelect}
                                                    onSocialMediaChange={this.onSocialMediaChange}
                                                    remove={this.removeSocialLink}
                                                />
                                            </Segment>
                                        </section>

                                        <section id="gallery">
                                            <Gallery
                                                images={HomeStore.homeInfo.images}
                                                onImageUrlChange={this.onGalleryDataChange}
                                                onBlur={this.validateImageUrls}
                                                removeImageUrl={this.removeImageUrl}
                                                addNewImageUrl={this.addNewImageUrl}/>
                                        </section>

                                        <section id="about-us">
                                            <Segment>
                                                <h3>About Us</h3>
                                                <Divider/>
                                                <Form>
                                                    <Form.TextArea
                                                        rows="10"
                                                        placeholder='About Us'
                                                        value={HomeStore.homeInfo.aboutUs}
                                                        onChange={(e) => this.onAboutUsChange(e.target.value)}/>
                                                </Form>
                                            </Segment>
                                        </section>

                                        <section id="buttons">
                                            <Form>
                                                <Form.Group>
                                                    <Form.Field>
                                                        <Button
                                                            color="orange"
                                                            onClick={() => this.saveOrUpdate()}
                                                            content="Update" inverted/>
                                                    </Form.Field>
                                                    <Form.Field>
                                                        <Button inverted color="red" content="Close" as="a" onClick={(e) => browserHistory.push("/home")}/>
                                                    </Form.Field>
                                                </Form.Group>
                                            </Form>
                                        </section>
                                    </Grid.Column>

                                </Grid>
                            </div>
                        </Segment>
                }
            </div>
        );
    }
}

export default HomeInfo;