/**
 * Created by Amit on 19-06-2018.
 */

import React, {Component} from 'react';
import {observer} from 'mobx-react';
import {Link} from "react-router";
import {Form, Grid, Icon, Segment, Table} from "semantic-ui-react";
import SubStreamRow from "./row";
import _ from 'lodash';

@observer
class AddCourseSection extends Component {

    constructor(props) {
        super(props);
        this.state = {
            course: props.course,
            subjectToEdit: null
        };
    }

    componentWillReceiveProps(nextProps, nextState){
        this.setState({course: nextProps.course});
    }

    // editSubject = (subStream, subject) => {
    //     this.setState({subjectToEdit: {...subject, subStream}});
    // };

    updateSubject = (courseIndex, stream, subStream, subject) => {
        this.props.updateSubject(courseIndex, stream, subStream, subject);
        if(this.state.subjectToEdit){
            this.setState({subjectToEdit: null});
        }
    };

    removeSubStream = (course, streamName, subStreamName) => {
        this.props.removeSubStream(course, streamName, subStreamName);
    };

    render() {
        let $this = this;
        const { courseIndex, courseList } = this.props;

        return <div>
            <Grid columns={2} style={{margin : 0}}>
                {
                    !_.isEmpty(this.state.course.stream) && Object.keys(this.state.course.stream).map((streamName) => {
                        let stream = $this.state.course.stream[streamName];
                        return !_.isEmpty(stream.subStream) ? Object.keys(stream.subStream).map((subStreamName) => {
                            return <Grid.Row className="course" key={subStreamName}>
                                <Grid.Column computer={16} mobile={16}>
                                    <Segment color="blue">
                                        <span className="course-name" style={{fontWeight: 'bold'}}>
                                            <span>{stream.name} - {stream.subStream[subStreamName].name}</span>
                                            <span style={{float: 'right', fontSize: 18, cursor: 'pointer'}}
                                                  onClick={(e) => this.removeSubStream($this.state.course.value, streamName, subStreamName)}>
                                                <Icon name="trash" color="red" size="small"/>
                                            </span>
                                        </span> &nbsp;
                                        <Form>
                                            <SubStreamRow
                                                courseIndex={courseIndex}
                                                stream={streamName}
                                                subStream={subStreamName}
                                                subjectToEdit={this.state.subjectToEdit}
                                                updateSubject={this.updateSubject}/>
                                            {
                                                stream.subStream[subStreamName].subjects.length > 0 &&
                                                    <Table size="small" basic="very" celled>
                                                        <Table.Header>
                                                            <Table.Row>
                                                                <Table.HeaderCell>Subject</Table.HeaderCell>
                                                                <Table.HeaderCell>Fee</Table.HeaderCell>
                                                                <Table.HeaderCell>Duration</Table.HeaderCell>
                                                                <Table.HeaderCell/>
                                                            </Table.Row>
                                                        </Table.Header>

                                                        <Table.Body>
                                                            {
                                                                stream.subStream[subStreamName].subjects.map((subject, index) => {
                                                                    return <Table.Row key={index}>
                                                                        <Table.Cell>{subject.name}</Table.Cell>
                                                                        <Table.Cell>{subject.fee}</Table.Cell>
                                                                        <Table.Cell>{subject.duration}</Table.Cell>
                                                                        <Table.Cell>
                                                                            {/*<Link*/}
                                                                                {/*style={{textDecoration: 'underline', cursor: 'pointer'}}*/}
                                                                                {/*onClick={(e) => this.editSubject(subStreamName, subject)}>*/}
                                                                                {/*Edit*/}
                                                                            {/*</Link>*/}
                                                                            &nbsp;&nbsp;
                                                                            <Link
                                                                                style={{textDecoration: 'underline', cursor: 'pointer'}}
                                                                                onClick={
                                                                                    (e) => this.props.removeSubject($this.state.course.value, streamName, subStreamName, subject)
                                                                                }>
                                                                                Delete
                                                                            </Link>
                                                                        </Table.Cell>
                                                                    </Table.Row>
                                                                })
                                                            }
                                                        </Table.Body>
                                                    </Table>
                                            }
                                        </Form>
                                    </Segment>
                                </Grid.Column>
                            </Grid.Row>
                        }) : null
                    })
                }
            </Grid>
        </div>
    }
}

export default AddCourseSection;