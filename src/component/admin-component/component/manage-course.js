/**
 * Created by Amit on 06-07-2018.
 */
import React, {Component} from 'react';
import {Button, Card, Divider, Form, Header, Icon, Input, Label, Message, Segment} from 'semantic-ui-react'
import {observer} from 'mobx-react';
import CommonApiService from "../../../service/common-api";
import {toTitleCase} from "../../../util/util";
import {browserHistory} from "react-router";
import _ from 'lodash';
import './style.scss';

@observer
class ManageCourse extends Component {

    constructor(props) {
        super(props);
        this.state = {
            show: false,
            message: "",
            savingCourse: false,
            savedCourses: [],
            selectedCourse: {},
            courseInput: '',
            selectedStream : '',
            streamInput: '',
            selectedSubStream : '',
            subStreamInput: ''
        };
        this.apiService = new CommonApiService();
        this.addNewStream = this.addNewStream.bind(this);
        this.type = (this.props.location.query.type && this.props.location.query.type === "coaching") ? "coaching" : 'college';
    }

    componentWillReceiveProps(nextProps, nextState) {
        this.setState({savedCourses : nextProps.courses})
    }

    componentWillMount(){
        this.getCourses();
    }

    componentWillUnmount() {
        this.clearModal();
    }

    getCourses = async () => {
        let savedCourses = [];
        if(this.type === "college"){
            savedCourses = await this.apiService.getCourses();
        }
        else{
            savedCourses = await this.apiService.getCoachingCourses();
        }
        savedCourses = _.sortBy(savedCourses, 'name');
        this.setState({savedCourses});
    };

    saveCourses = async () => {
        this.setState({savingCourse : true});
        try {
            await this.apiService.saveCourses(this.type, this.state.savedCourses);
            this.props.showToastr('success', "Save Course", "Successfully saved courses");
            this.close();
        }
        catch(error){
            console.log(error);
            this.props.showToastr('error', "Save Course", "Something Went Wrong");
            this.setState({
                savingCourse : false
            });
        }
    };

    clearModal = ( ) =>{
        this.setState({
            message: "",
            savingCourse : false,
            savedCourses: [],
            selectedCourse: {},
            courseInput: '',
            selectedStream : '',
            streamInput: '',
            selectedSubStream : '',
            subStreamInput: ''
        });
    };

    close = () => {
        this.clearModal();
        browserHistory.push("/" + this.type);
    };

    addNewCourse = () => {
        if(this.state.courseInput.trim()) {
            let savedCourses = _.cloneDeep(this.state.savedCourses);
            savedCourses.push({
                id: _.random(100, 200),
                name: this.state.courseInput,
                value: toTitleCase(this.state.courseInput),
                stream: {}
            });
            this.setState({courseInput: "", savedCourses});
        }
    };

    addNewStream = () => {
        if(this.state.streamInput.trim()) {
            let savedCourses = _.cloneDeep(this.state.savedCourses);
            let matchedCourse = _.find(savedCourses, {id: this.state.selectedCourse.id});
            matchedCourse.stream[toTitleCase(this.state.streamInput)] = {
                id: _.random(200, 300),
                name: this.state.streamInput,
                value: toTitleCase(this.state.streamInput),
                subStream: {}
            };
            this.setState({selectedCourse: matchedCourse, savedCourses, streamInput: ""});
        }
    };

    addNewSubStream = () => {
        if(this.state.subStreamInput.trim()) {
            let savedCourses = _.cloneDeep(this.state.savedCourses);
            let matchedCourse = _.find(savedCourses, {id: this.state.selectedCourse.id});
            matchedCourse.stream[this.state.selectedStream].subStream[toTitleCase(this.state.subStreamInput)] = {
                id: _.random(300, 400),
                name: this.state.subStreamInput,
                value: toTitleCase(this.state.subStreamInput),
                subjects: {}
            };
            this.setState({selectedCourse: matchedCourse, savedCourses, subStreamInput: ""});
        }
    };


    render() {
        return  <Segment basic className="body-container no-padding-segment">
                <div className={`app-background page-${this.type}`}/>
                <div style={{padding : '0 250px'}}>
                    <div style={{color:'red'}}>{this.state.message}</div>

                    <section className="course" style={{marginBottom: 10}}>
                        <Segment>
                            <Header as="h4" color="blue"><Icon name="graduation cap"/>Available Courses</Header>
                            <Divider/>
                            <Card.Group itemsPerRow={4}  style={{marginBottom: 10}}>
                            {
                                this.state.savedCourses.map((course, i) => {
                                    return <Card
                                        key={i}
                                        title={course.name}
                                        className={!_.isEmpty(this.state.selectedCourse) &&  this.state.selectedCourse.name === course.name? 'selected' : ''}
                                        onClick={() => this.setState({selectedCourse: course, selectedStream: ""})} raised>
                                        <Card.Content>
                                            <Card.Header>
                                                {course.name}
                                            </Card.Header>
                                        </Card.Content>
                                    </Card>
                                })
                            }
                                <Card raised>
                                    <Card.Content>
                                        <Card.Header>
                                            <Input
                                                name="add-course"
                                                size="mini"
                                                style={{marginTop: 5}}
                                                value={this.state.courseInput}
                                                placeholder="Add New"
                                                onChange={(e) => this.setState({courseInput: e.target.value})} transparent/>
                                            <Icon color='blue' name='add' onClick={this.addNewCourse}/>
                                        </Card.Header>
                                    </Card.Content>
                                </Card>
                            </Card.Group>

                            {
                                !_.isEmpty(this.state.selectedCourse) && <div>
                                    <Form>
                                        <h5>Update / Delete selected course</h5>
                                        <Divider/>
                                        <Form.Group inline>
                                            <Form.Input
                                                width={12}
                                                name="edit-stream"
                                                value={this.state.selectedCourse.name}
                                                placeholder="Edit"
                                                onChange={(e) => {
                                                    let c = this.state.selectedCourse;
                                                    c.name = e.target.value;
                                                    this.setState({selectedCourse: c})
                                                }}/>
                                            <Form.Button
                                                width={2}
                                                onClick={() => {
                                                    let courses = this.state.savedCourses;
                                                    _.remove(courses, {id: this.state.selectedCourse.id});
                                                    this.setState({
                                                        selectedCourse: {},
                                                        courseInput: '',
                                                        selectedStream: '',
                                                        streamInput: '',
                                                        selectedSubStream: '',
                                                        subStreamInput: '',
                                                        savedCourses: courses
                                                    });
                                                }} negative>
                                                Delete
                                            </Form.Button>
                                        </Form.Group>
                                    </Form>
                                </div>
                            }
                        </Segment>
                    </section>

                    {
                        !_.isEmpty(this.state.selectedCourse) && <section className="stream" style={{marginTop: 25}}>
                            <Segment>
                                <Header as="h4" color="blue"><Icon name="graduation cap"/>Available Degrees </Header>
                                <Divider/>
                                {
                                    !_.isEmpty(this.state.selectedCourse.stream) && Object.keys(this.state.selectedCourse.stream).sort().map((streamName, streamIndex) => {
                                        return <Label
                                                    as='a'
                                                    key={streamIndex}
                                                    size="large"
                                                    className={!_.isEmpty(this.state.selectedCourse) &&  this.state.selectedStream === streamName? 'selected' : ''}
                                                    style={{marginTop: 5}}
                                                    onClick={() => this.setState({selectedStream: streamName})} basic>
                                            {this.state.selectedCourse.stream[streamName].name}
                                        </Label>
                                    })
                                }
                                {
                                    _.isEmpty(this.state.selectedCourse.stream) && <Message size="tiny" attached="top" info icon>
                                        <Icon name="info circle"/>
                                        <Message.Content>
                                            No Degree Available
                                        </Message.Content>
                                    </Message>
                                }

                                <Input
                                    name="add-stream"
                                    size="mini"
                                    style={{marginTop: 5, marginRight: 5}}
                                    action={{color: 'blue', icon: 'add', onClick: this.addNewStream}}
                                    value={this.state.streamInput}
                                    placeholder="Add New"
                                    onChange={(e) => this.setState({streamInput: e.target.value})}/>
                                {
                                    !_.isEmpty(this.state.selectedStream) && !_.isEmpty(this.state.selectedCourse.stream) && [
                                        <Input
                                            name="edit-stream"
                                            size="mini"
                                            value={this.state.selectedCourse.stream[this.state.selectedStream].name}
                                            placeholder="Edit"
                                            style={{marginTop: 5}}
                                            action={{color: 'red', icon: 'trash', onClick: () => {
                                                let courses = this.state.savedCourses;
                                                let matchedCourse = _.find(courses, {id: this.state.selectedCourse.id});
                                                if(matchedCourse && !_.isEmpty(matchedCourse.stream[this.state.selectedStream])) {
                                                    delete matchedCourse.stream[this.state.selectedStream];
                                                    this.setState({
                                                        selectedStream: '',
                                                        streamInput: '',
                                                        selectedSubStream: '',
                                                        subStreamInput: ''
                                                    });
                                                }
                                            }}}
                                            onChange={(e) => {
                                                let c = this.state.selectedCourse;
                                                c.stream[this.state.selectedStream].name = e.target.value;
                                                this.setState({selectedCourse: c})
                                            }}/>
                                    ]
                                }
                            </Segment>
                        </section>
                    }

                    {
                        !_.isEmpty(this.state.selectedCourse) && !_.isEmpty(this.state.selectedStream) &&
                        <section className="sub-stream" style={{marginTop: 25}}>
                            <Segment>
                                <Header as="h4" color="blue"><Icon name="book"/>Available Streams</Header>
                                <Divider/>
                                {
                                    !_.isEmpty(this.state.selectedCourse.stream[this.state.selectedStream].subStream) &&
                                            Object.keys(this.state.selectedCourse.stream[this.state.selectedStream].subStream).sort().map((ss, index) => {
                                                return <Label
                                                    as='a'
                                                    key={index}
                                                    size="large"
                                                    className={!_.isEmpty(this.state.selectedCourse) &&  this.state.selectedSubStream === ss? 'selected' : ''}
                                                    onClick={() => this.setState({selectedSubStream: ss})}
                                                    style={{marginTop: 5}} basic>
                                                    {this.state.selectedCourse.stream[this.state.selectedStream].subStream[ss].name}
                                                </Label>
                                            })
                                }
                                {
                                    _.isEmpty(this.state.selectedCourse.stream[this.state.selectedStream].subStream) && <Message size="tiny" attached="top" info icon>
                                        <Icon name="info circle"/>
                                        <Message.Content>
                                            No Streams Available
                                        </Message.Content>
                                    </Message>
                                }
                                <Input
                                    name="add-subStream"
                                    size="mini"
                                    style={{marginTop: 5, marginRight: 5}}
                                    action={{color: 'blue', icon: 'add', onClick: this.addNewSubStream}}
                                    value={this.state.subStreamInput}
                                    placeholder="Add New"
                                    onChange={(e) => this.setState({subStreamInput: e.target.value})}/>
                                {
                                    !_.isEmpty(this.state.selectedSubStream) && !_.isEmpty(this.state.selectedCourse.stream[this.state.selectedStream].subStream) && [
                                        <Input
                                            name="edit-subStream"
                                            size="mini"
                                            value={this.state.selectedCourse.stream[this.state.selectedStream].subStream[this.state.selectedSubStream].name}
                                            placeholder="Edit"
                                            style={{marginTop: 5}}
                                            action={{color: 'red', icon: 'trash', onClick: () => {
                                                let courses = this.state.savedCourses;
                                                let matchedCourse = _.find(courses, {id: this.state.selectedCourse.id});
                                                if(matchedCourse) {
                                                    delete matchedCourse.stream[this.state.selectedStream].subStream[this.state.selectedSubStream];
                                                    this.setState({
                                                        selectedSubStream: '',
                                                        subStreamInput: ''
                                                    });
                                                }
                                            }}}
                                            onChange={(e) => {
                                                let c = this.state.selectedCourse;
                                                c.stream[this.state.selectedStream].subStream[this.state.selectedSubStream].name = e.target.value;
                                                this.setState({selectedCourse: c})
                                            }}/>
                                    ]
                                }
                                </Segment>
                        </section>
                    }

                    <section className="buttons-section" style={{marginTop: 20}}>
                        <Button onClick={this.saveCourses} loading={this.state.savingCourse} disabled={this.state.savingCourse} positive>Save</Button>
                        <Button onClick={this.close} disabled={this.state.savingCourse} negative>Cancel</Button>
                    </section>
                </div>
            </Segment>
    }
}

export default ManageCourse;
