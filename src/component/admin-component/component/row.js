/**
 * Created by Amit on 14-06-2018.
 */

import React, {Component} from "react";
import {observer} from "mobx-react";
import {Form} from "semantic-ui-react";

@observer
class SubStreamRow extends Component {

    constructor(props){
        super(props);
        this.state = {
            name: "",
            fee: "",
            duration: ""
        }
    }

    // componentWillReceiveProps(nextProps){
    //     if(nextProps.subjectToEdit && this.props.subStream === nextProps.subjectToEdit.subStream) {
    //         this.setState({ name: nextProps.subjectToEdit.name, fee: nextProps.subjectToEdit.fee, duration: nextProps.subjectToEdit.duration})
    //     }
    // }

    onSubStreamUpdate = (key, value) => {
        this.setState({[key]: value});
    };

    updateSubjects = () => {
        const {courseIndex, stream, subStream} = this.props;
        this.props.updateSubject(courseIndex, stream, subStream, {...this.state})
        this.setState({name: "", fee: "", duration: ""});
    };

    render(){
        const { subStreamIndex } = this.props;
        return <Form.Group widths='equal' key={subStreamIndex} style={{marginTop: 5}}>
            <Form.Input
                value={this.state.name}
                onChange={(e) => { this.onSubStreamUpdate("name", e.target.value);}}
                placeholder={"Enter Subject"}/>
            <Form.Input
                value={this.state.fee}
                onChange={(e) => {this.onSubStreamUpdate("fee", e.target.value);}}
                placeholder={"Enter fee"}
                icon="rupee"/>
            <Form.Input
                value={this.state.duration}
                onChange={(e) => { this.onSubStreamUpdate("duration", e.target.value);}}
                placeholder="Course Duration"/>
            <Form.Button size="tiny" color="blue" onClick={this.updateSubjects}>
                Add
            </Form.Button>
        </Form.Group>
    }
}

export default SubStreamRow;