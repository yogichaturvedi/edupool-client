/**
 * Created by Amit on 14-06-2018.
 */


import React, {Component} from "react";
import {observer} from "mobx-react";
import {Form} from "semantic-ui-react";
import _ from 'lodash';

@observer
class SubStreamSearch extends Component {

    constructor(props){
        super(props);
        this.state = {
            subStreams: [],
        };
        // In edit mode update multi-select subStream input with available subStreams
        let availableSubStreams = _.filter(props.name, {available: true});
        this.state.subStreams = _.map(availableSubStreams, subStream => subStream.value);
    }

    updateSubStreams = subStreams => {
        this.setState({subStreams: subStreams});
        this.props.showSubStream(this.props.courseIndex, subStreams);
    };

    render(){
        const { subStreamOptions } = this.props;
        const renderLabel = label => ({
            color: 'blue',
            content: `${label.text}`,
            icon: 'check',
        });
        return <Form.Dropdown
                    placeholder="SubStream"
                    renderLabel={renderLabel}
                    onChange={(e, value) => this.updateSubStreams(value.value)}
                    value={this.state.subStreams}
                    options={subStreamOptions}
                    selectOnBlur={false}
                    fluid selection/>
    }
}

export default SubStreamSearch;

