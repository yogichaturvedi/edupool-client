/**
 * Created by Amit on 06-12-2017.
 */
import React, {Component} from 'react';
import {observer} from 'mobx-react';
import {Popup, Icon, Button} from "semantic-ui-react";
import {copyToClipboard} from "../../util/util";

@observer
class CopyPopup extends Component {

    constructor(props) {
        super(props);
        this.copy = this.copy.bind(this);
    }

    copy(){
        copyToClipboard("email-popup", this.props.textToCopy);
    }


    render() {
        const icon = <Icon name="copy" bordered={false} onClick={this.copy}/>

        return (
            <Popup id="email-popup" trigger={this.props.triggerElement} position={this.props.position} size={this.props.size} hoverable flowing>
                <Popup.Content>
                    {this.props.content}&nbsp;&nbsp;
                    <Button color="blue" size="mini"
                            icon={icon} circular inverted/>
                </Popup.Content>
            </Popup>
        );
    }
}
CopyPopup.defaultProps = {
    size: 'mini',
    position: 'top center',
};

export default CopyPopup;