/**
 * Created by Yogesh Chaturvedi on 11-06-2017.
 */
import React, {Component} from 'react';
import {observer} from 'mobx-react';
// import "./styles.css";
import "./error.scss";
import {Button} from "semantic-ui-react";
import UiStore from "../../route/store/ui-store";
import App_Const from "../../constants/constants";


@observer
class AppError extends Component {

    render() {
        return (
            <div style={{textAlign: 'center'}}>
                <div className="no-internet-background" style={{borderTop:'50px solid #0a0a0b'}}/>
                {
                    <div>
                        <h3 style={{color: 'red'}} className="error-message">
                            {
                                UiStore.error.message === App_Const.failedToFetch ? 'No Internet Connection' : this.props.error.message
                            }
                        </h3>
                        <Button color="blue" className="retry-button" as="a" onClick={(e) => this.props.retry()}>
                            Click here to retry</Button>
                    </div>
                }
            </div>
        );
    }
}

export default AppError;
