import React from "react";
import {Grid, Icon} from 'semantic-ui-react';
import ActionButtons from "./action-buttons";
import DefaultImage from '../../assets/image/broken-image.png';
// import './card.css';
import './card.scss';
import {browserHistory} from "react-router";

class ResultDetailCard extends React.Component {

    constructor(props) {
        super(props);
        this.getFormattedTime = this.getFormattedTime.bind(this);
    }

    getFormattedTime(date) {
        return new Date(date).toLocaleString();
    }

    onImageError(event) {
        event.target.setAttribute("src", DefaultImage);
    }


    render() {
        const {basicDetail, type, admin, deleteDetail} = this.props;
        const uri = "/" + type  + "/" + basicDetail.id;
        return (
            <div>
                <Grid>
                    <Grid.Row style={{margin: 'auto'}}>
                        <Grid.Column mobile={16} computer={16}>
                            <Grid>
                                <Grid.Row className="margin-auto">
                                    <Grid.Column>
                                        <h2 className="result-card-title margin-bottom-zero" onClick={() => {
                                            browserHistory.push(uri)
                                        }}>{basicDetail.title}
                                        </h2>
                                        <span style={{marginRight: '10px', color: '#013A6B'}}>
                                            <Icon name="calendar"
                                                  size="small"/> Declared At - {this.getFormattedTime(basicDetail.createdAt)}
                                        </span>
                                        {
                                            this.props.admin && <div className="result-card-action-buttons-wrapper">
                                                <ActionButtons type={type} basicDetail={basicDetail} admin={admin}
                                                               deleteDetail={deleteDetail}/>
                                            </div>
                                        }
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }
}

export default ResultDetailCard;