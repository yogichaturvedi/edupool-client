import React from "react";
import {Button, Grid, Segment} from 'semantic-ui-react';
import DefaultImage from '../../assets/image/broken-image.png';
import ActionButtons from "./action-buttons";
// import './card.css';
import './card.scss';
import {Link} from "react-router";
import CustomImage from "../image/index";
import LazyLoad from 'react-lazyload';

class BasicDetailCard extends React.Component {

    constructor(props) {
        super(props);
        this.getFormattedTime = this.getFormattedTime.bind(this);
    }

    getFormattedTime(date) {
        return new Date(date).toLocaleDateString();
    }

    onImageError(event) {
        event.target.setAttribute("src", DefaultImage);
    }

    render() {
        const {basicDetail, type, admin, deleteDetail} = this.props;
        const uri = "/" + type + "/" + basicDetail.id;
        return (
            <div>
                <div className={`scc-list-card ${basicDetail.reviewed ? 'verified' : 'not-verified'}`}>
                    <Grid doubling reversed={"mobile vertically"}>
                        <Grid.Row style={{margin: 'auto'}}>
                            <Grid.Column mobile={16} computer={5} verticalAlign="middle">
                                <Segment size="small" style={{background: "none", padding: 0}} basic>
                                    <div className="scc-card-image-wrapper">
                                        <Link to={uri}>
                                            <LazyLoad height={140} offset={100}>
                                                <CustomImage src={basicDetail.imageUrl + "?"}
                                                             width="100%"
                                                             height="140"
                                                             alt={basicDetail.title} circular/>
                                            </LazyLoad>
                                        </Link>
                                    </div>
                                </Segment>

                            </Grid.Column>
                            <Grid.Column mobile={16} computer={11}>
                                <Segment size="small" basic style={{background: "none", padding: 0}}>
                                    <div
                                        className="scc-card-title-wrapper text-extra-dark-gray alt-font text-extra-large">
                                        <Link to={uri}><h2 className="title">{basicDetail.title}</h2></Link>
                                        <div
                                            className="ssc-card-audit-info-wrapper text-medium-gray text-extra-small text-uppercase alt-font">
                                            By {basicDetail.createdBy} | {this.getFormattedTime(basicDetail.createdAt)}
                                        </div>
                                    </div>
                                    <p className="scc-card-description-wrapper">
                                        {basicDetail.description}
                                    </p>
                                    <Link to={uri}>
                                        <Button size="tiny" className="scc-card-continue-button animated-button"
                                                content="Read More" color="black"/>
                                    </Link>
                                    {
                                        type === "college" && basicDetail.admissionOpen &&
                                        <Button size="tiny" className="scc-card-continue-button animated-button"
                                                content="Apply Now" color="black" onClick={()=>this.props.applyNow(basicDetail)}/>
                                    }
                                    <ActionButtons
                                        type={type}
                                        basicDetail={basicDetail} admin={admin}
                                        deleteDetail={deleteDetail}/>

                                </Segment>
                            </Grid.Column>
                        </Grid.Row>
                        {/*{*/}
                        {/*basicDetail.address &&*/}
                        {/*<Grid.Row style={{margin: 'auto', padding : 0}}>*/}
                        {/*<div className="scc-card-address-wrapper">*/}
                        {/*<h5><Icon name="marker" color="red"/>{basicDetail.address}</h5>*/}
                        {/*</div>*/}
                        {/*</Grid.Row>*/}
                        {/*}*/}
                    </Grid>
                </div>
                {/*<div style={{marginBottom: 20}} className={"card-container " + type}>*/}
                {/*<Link to={uri}><Header color="blue" attached="top" as='h3'>{basicDetail.title}</Header></Link>*/}
                {/*<Segment attached>*/}
                {/*<Grid doubling reversed={"mobile vertically"}>*/}
                {/*<Grid.Row style={{margin:'auto'}}>*/}
                {/*<Grid.Column mobile={16} computer={6}>*/}
                {/*<div className="published-date">Published On*/}
                {/*: {this.getFormattedTime(basicDetail.createdAt)}</div>*/}
                {/*<div className={"card-image-container"}>*/}
                {/*<Link to={uri}>*/}
                {/*<img src={basicDetail.imageUrl + "?"}*/}
                {/*width="100%"*/}
                {/*height="200"*/}
                {/*alt={basicDetail.title}*/}
                {/*onError={this.onImageError}/>*/}
                {/*</Link>*/}
                {/*</div>*/}

                {/*</Grid.Column>*/}
                {/*<Grid.Column mobile={16} computer={10}>*/}
                {/*<Item>*/}
                {/*<Item.Content>*/}
                {/*<Item.Meta as='h5' className="basic-item-extra-content">*/}
                {/*{basicDetail.description}*/}
                {/*</Item.Meta>*/}

                {/*{*/}
                {/*basicDetail.address &&*/}
                {/*<Item.Extra as="h5">*/}
                {/*<Icon name="marker" color="red"/>{basicDetail.address}*/}
                {/*</Item.Extra>*/}
                {/*}*/}
                {/*</Item.Content>*/}
                {/*</Item>*/}
                {/*<ActionButtons type={type} basicDetail={basicDetail} admin={admin}*/}
                {/*deleteDetail={deleteDetail}/>*/}
                {/*</Grid.Column>*/}
                {/*</Grid.Row>*/}
                {/*</Grid>*/}
                {/*</Segment>*/}
                {/*<Button.Group attached="bottom" widths='3'>*/}
                {/*<Button as="a"*/}
                {/*href={"tel:" + basicDetail.mobileNumber}*/}
                {/*icon="call"*/}
                {/*color="blue"*/}
                {/*className="basic-detail-contact-button"*/}
                {/*content={basicDetail.mobileNumber}/>*/}

                {/*<Button*/}
                {/*href={"mailto:" + basicDetail.email + "?subject=I have read your article from http://www.edupool.in"}*/}
                {/*icon="mail"*/}
                {/*color="blue"*/}
                {/*className="basic-detail-contact-button"*/}
                {/*content={'Send Mail'}/>*/}

                {/*<Button href={basicDetail.website}*/}
                {/*icon="globe"*/}
                {/*target="_blank"*/}
                {/*color="blue"*/}
                {/*className="basic-detail-contact-button"*/}
                {/*content="Go to website"/>*/}

                {/*</Button.Group>*/}
                {/*</div>*/}
            </div>
        )
    }
}


export default BasicDetailCard;
