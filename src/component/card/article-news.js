/**
 * Created by Amit on 16-03-2018.
 */
import React from "react";
import {observer} from "mobx-react";
import {Link} from "react-router";
import CustomImage from "../image/index";
import {Icon} from "semantic-ui-react";
import { redirectToPage} from "../../util/util";
import ArticleNewsStore from '../../route/store/article-news-store';
import './card.scss';

@observer
class ArticleNewsCard extends React.Component {

    getTitleClass = () => (this.props.column === 1 || this.props.column === 3) ? 'max-2-lines' : 'max-3-lines';

    render(){
        const { articleNews, type } =  this.props;
        const viewUri = `/${type}/view/${articleNews.id}`;
        return(
            <div className={`blog-card ${articleNews.show ? '' : 'draft'}`}>
                <div className="image-wrapper" style={{height: this.props.column === 2 ? 220 : 180 }}>
                    <Link to={viewUri}>
                    <div className="overlay">
                        <span  style={{ top: '45%',right: 0, left: 0, position: 'absolute', fontSize: '20px'}} >{articleNews.category}</span>
                    </div>
                    </Link>
                    <CustomImage src={articleNews.thumbnail} width="100%" height="100%" alt={articleNews.title}/>
                </div>
                <div className="content">
                    <Link to={viewUri}>
                        <div className={`blog-title alt-font text-extra-dark-gray ${this.getTitleClass()}`}>{articleNews.title}</div>
                    </Link>
                    <p className={(this.props.column === 1 || this.props.column === 3) ? 'max-3-lines' : 'max-4-lines'}>{articleNews.shortDescription}</p>

                    <div className="separator-line"/>

                    <div className="author text-medium-gray text-uppercase text-extra-small display-inline-block">{`By ${articleNews.createdBy} | ${ArticleNewsStore.getDate(articleNews.createdAt)}`}</div>
                    {
                        this.props.admin &&
                        <div className="basic-card-action-buttons" style={{bottom: 10}}>
                            <Icon name="pencil" color="blue"
                                  onClick={() => redirectToPage(`/${type}/edit/${this.props.articleNews.id}`)}/>
                            <Icon name="trash" color="red" onClick={(e, id) => this.props.deleteArticleNews(articleNews.id)}/>
                        </div>
                    }
                </div>
            </div>
        )
    }

}

export default ArticleNewsCard