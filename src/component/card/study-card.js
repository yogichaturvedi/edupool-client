import React from "react";
import {Grid, Header, Icon, Item, Segment} from 'semantic-ui-react';
import ActionButtons from "./action-buttons";
// import './card.css';
import './card.scss';

class StudyDetailCard extends React.Component {

    constructor(props) {
        super(props);
        this.getFormattedTime = this.getFormattedTime.bind(this);
    }

    getFormattedTime(date) {
        return new Date(date).toLocaleString();
    }


    render() {
        const {basicDetail, type, admin, deleteDetail} = this.props;
        // const imageUri = "/detail/" + type + "/" + basicDetail.id;
        return (
            <div style={{marginBottom: 20}} className={"card-container " + type}>
                <a>
                    <Header color="blue" attached="top" as='h2' style={{fontSize:'16px'}}>{basicDetail.title}</Header>
                </a>
                <Segment attached>
                    <Grid>
                        <Grid.Row  style={{margin:'auto'}}>
                            <Grid.Column>
                                <Item>
                                    <Item.Content>
                                        <Item.Extra style={{color: "orange"}}>
                                            {"Published on : " + this.getFormattedTime(basicDetail.createdAt)}
                                        </Item.Extra>
                                        <Item.Meta as='h3' style={{fontSize: '15px'}}>{basicDetail.description}</Item.Meta>
                                        <div style={{color: "red", fontSize:'14px'}}>Download Links</div>
                                        {
                                            basicDetail.links.map((data, index) => {
                                                return <div key={index} >
                                                    <Header as='h4' style={{fontSize:'14px', margin:0, display:'inline'}}>
                                                        <a target="_blank" href={data.link}>{index + 1 + ". " + data.title}</a>
                                                    </Header>
                                                    <Icon color="grey" name="download"/><br/>
                                                </div>
                                            })
                                        }
                                    </Item.Content>
                                </Item>
                                <ActionButtons type={type} basicDetail={basicDetail} admin={admin}
                                               deleteDetail={deleteDetail}/>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>

                </Segment>
            </div>
        )
    }
}

export default StudyDetailCard;