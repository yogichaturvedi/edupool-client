import React from "react";
import {Grid, Icon} from 'semantic-ui-react';
import DefaultImage from '../../assets/image/broken-image.png';
import ActionButtons from "./action-buttons";
import {browserHistory} from "react-router";
import CustomImage from "../image/index";
import LazyLoad from 'react-lazyload';
import './card.scss';


class ExamDetailCard extends React.Component {
    constructor(props) {
        super(props);
        this.onImageError = this.onImageError.bind(this);
        this.getFormattedTime = this.getFormattedTime.bind(this);
    }

    getFormattedTime(date) {
        return new Date(date).toLocaleString();
    }


    onImageError(event) {
        event.target.setAttribute("src", DefaultImage);
    }

    render() {
        const {basicDetail, type, admin, deleteDetail} = this.props;
        const uri = "/exam/" + basicDetail.id;
        return (
            <div className="exam-list-card">
                <Grid style={{margin: 0}}>
                    <Grid.Column mobile={16} computer={3}>
                        <div className="exam-card-image-container">
                            <LazyLoad height={140} offset={100}>
                                <CustomImage src={basicDetail.imageUrl + "?"}
                                             width="100%"
                                             height="100%"
                                             alt={basicDetail.title}/>
                            </LazyLoad>
                        </div>
                    </Grid.Column>
                    <Grid.Column mobile={16} computer={this.props.admin ? 11 : 12}>
                        <h2 className="exam-card-title margin-bottom-zero"
                            onClick={() => browserHistory.push(uri)}>
                            {basicDetail.title}
                        </h2>
                        <span style={{marginRight: '10px', color: '#013A6B'}}>
                            <Icon name="user"
                                  size="small"/> Organized By - {basicDetail.organizer ? basicDetail.organizer : 'Other'}
                        </span>

                    </Grid.Column>
                    {
                        this.props.admin && <Grid.Column mobile={16} computer={2}>
                            <div className="exam-card-action-buttons-wrapper">
                                <ActionButtons type={type} basicDetail={basicDetail} admin={admin}
                                               deleteDetail={deleteDetail}/>
                            </div>

                        </Grid.Column>
                    }
                </Grid>
            </div>
        )
    }
}

export default ExamDetailCard;