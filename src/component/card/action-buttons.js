import React from "react";
import {Button} from "semantic-ui-react";
import {redirectToPage} from "../../util/util";

class ActionButtons extends React.Component{
    render() {
        const { type, basicDetail, deleteDetail} = this.props;
        let queryParams = "type=" + type + "&id=" + basicDetail.id;
        return <div style={{position:'relative'}}>
            {
                this.props.admin &&
                <div className="basic-card-action-buttons">
                    <Button circular
                            as="a" onClick={()=>redirectToPage("/edit", queryParams)}
                            icon="pencil" color="blue" inverted={this.props.inverted === undefined || this.props.inverted}/>
                    <Button circular icon="trash" color="red"
                            onClick={(e, id) => deleteDetail(basicDetail.id)} inverted={this.props.inverted === undefined || this.props.inverted}/>
                </div>
            }
        </div>
    };
}

export default ActionButtons;