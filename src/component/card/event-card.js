import React from "react";
import {observer} from "mobx-react";
import DefaultImage from '../../assets/image/broken-image.png';
import './card.scss';
import {Button, Grid, Header, Icon, Segment} from "semantic-ui-react";
import APP_CONST from "../../constants/constants";
import {toTitleCase} from "../../util/util";
import {browserHistory} from "react-router";
import ActionButtons from "./action-buttons";
import _ from "lodash";
import CustomImage from "../image/index";
import LazyLoad from 'react-lazyload';

@observer
class EventDetailCard extends React.Component {

    redirect = uri => {
        if (this.props.redirectTo) {
            this.props.redirectTo(this.props.basicDetail.id, this.props.basicDetail.title);
        }
        else {
            browserHistory.push(uri)
        }
    };

    constructor(props) {
        super(props);
        this.getFormattedTime = this.getFormattedTime.bind(this);
        this.getDay = this.getDay.bind(this);
        this.getMonth = this.getMonth.bind(this);
        this.getDate = this.getDate.bind(this);
    }

    getFormattedTime(date) {
        return new Date(date).toLocaleString();
    }

    onImageError(event) {
        event.target.setAttribute("src", DefaultImage);
    }

    getDay(date) {
        return APP_CONST.dates.day[new Date(date).getDay()].value;
    }

    getDate(date) {
        return new Date(date).getDate();
    }

    getMonth(date) {
        return APP_CONST.dates.month[new Date(date).getMonth()].value;
    }

    getFormattedVenue(address) {
        if (!address) {
            return "-";
        }
        let venue = "";
        venue += address.line1 ? (address.line1 + ", ") : "";
        venue += address.line2 ? (address.line2 + ", ") : "";
        venue += address.city + ", " + address.state;
        venue += address.pincode ? (", " + address.pincode) : "";
        return venue;
    }

    getMoreTags(tags) {
        let formattedTags = _.map(tags, (tag) => {
            return toTitleCase(tag);
        });
        return formattedTags.splice(2, formattedTags.length).join(", ");
    }

    render() {
        const {basicDetail, admin, deleteDetail} = this.props;
        const uri = "/event/" + basicDetail.id;
        return (
            <Segment className="event-card no-padding-segment">
                <div className="image-wrapper">
                    {/*<img src={basicDetail.imageUrl + "?"} height={200} width="100%;" alt={basicDetail.title}/>*/}
                    <LazyLoad height={200} offset={100}>
                        <CustomImage src={basicDetail.imageUrl} title={basicDetail.title} height={200} width="100%"/>
                    </LazyLoad>
                    <ActionButtons type='event' basicDetail={basicDetail} admin={admin} inverted={false}
                                   deleteDetail={deleteDetail}/>
                </div>
                <Grid style={{margin: 0}}>
                    <Grid.Row className="content-wrapper">
                        <Grid.Column width={4} textAlign="center">
                            <div className="date-wrapper">
                                <div className="month text-uppercase">
                                    {basicDetail.startDate && basicDetail.startDate.date ? this.getMonth(basicDetail.startDate.date) : '-'}
                                </div>
                                <div className="date">
                                    {basicDetail.startDate && basicDetail.startDate.date ? this.getDate(basicDetail.startDate.date) : '-'}
                                </div>
                                <div className="day">
                                    {basicDetail.startDate && basicDetail.startDate.date ? this.getDay(basicDetail.startDate.date) : '-'}
                                </div>
                            </div>
                        </Grid.Column>
                        <Grid.Column width={12}>
                            <Header as="h2">
                                <Header.Content className="title max-2-lines" onClick={() => this.redirect(uri)}>
                                    {basicDetail.title}
                                </Header.Content>
                                <Header.Subheader className="sub-title">
                                    <div className="venue max-2-lines">
                                        {this.getFormattedVenue(basicDetail.venue)}
                                    </div>
                                    {
                                        basicDetail.fees && <div className="fees">
                                            <Icon style={{marginRight: 0}}
                                                  name="rupee"/>{basicDetail.fees}
                                        </div>
                                    }
                                </Header.Subheader>
                            </Header>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row style={{margin: 0}} verticalAlign="middle">
                        <Grid.Column width={10} style={{paddingLeft: 10, paddingRight: 4}}>
                            {basicDetail.tags[0] && <span className="_tag"
                                                          title={toTitleCase(basicDetail.tags[0])}>{basicDetail.tags[0]}</span>}
                            {basicDetail.tags[1] && <span className="_tag"
                                                          title={toTitleCase(basicDetail.tags[1])}>{basicDetail.tags[1]}</span>}
                            {basicDetail.tags.length > 2 &&
                            <span className="_tag" style={{marginRight: 0}}
                                  title={this.getMoreTags(basicDetail.tags)}>
                                +{basicDetail.tags.length - 2}
                                </span>}
                        </Grid.Column>
                        <Grid.Column width={6} textAlign="right" style={{paddingLeft: 0, paddingRight: 10}}>
                            <Button size="tiny" className="more-detail" content="More Details"
                                    onClick={() => this.redirect(uri)}/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Segment>
        )
    }
}

export default EventDetailCard;