/**
 * Created by Yogesh Chaturvedi on 18-06-2017.
 */

import React, {Component} from 'react'
import {Header, Icon, Image} from "semantic-ui-react";
import Warning from "../../assets/image/warn.png";

class PageUnderConstruction extends Component {

    render() {
        return (
            <div>
                <Header as='h2' color="grey">
                    <Header.Content>
                        This Page is under construction
                    </Header.Content>
                </Header>
                <Image size="medium"
                       src={Warning}/>
                <Header as='h3' color="grey">
                    <Icon color="grey" name='home'/>
                    <Header.Content>
                        <a href="/">Go to home page</a>
                    </Header.Content>
                </Header>
            </div>
        );
    }
}

export default PageUnderConstruction;

