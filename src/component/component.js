/**
 * Created by Amit on 23-01-2018.
 */

import React from 'react'
import {Grid, Icon, Segment} from "semantic-ui-react";
import {Link} from "react-router";
import LazyLoad from 'react-lazyload';
import './component.scss';
import _ from "lodash";
import CustomImage from "./image/index";

export const ListHeader = (listType, title) => {
    const listData = require("../../src/static-data/list/list.json");
    let subTitle;
    if (title) {
        subTitle = title.split(' ').join('');
    }
    return <div className="list-heading-container">
        <h1 className="list-heading-title">
            {title ? listData[listType][subTitle].title : listData[listType].title}
        </h1>
        <div className="list-heading-content">
            <Icon name="quote left" size="small"/>
            {title ? listData[listType][subTitle].content : listData[listType].content}
            <Icon name="quote right" size="small"/>
        </div>
    </div>
};

export const ListTopContentSection = (listType, basicDetails, exploreAll) => {
    return <section className="list-top-content-section">
        <h2 className="section-title">Top featured {`${listType}s`} in India</h2>
        <Grid stackable>
            <Grid.Row columns={4}>
                {
                    _.slice(basicDetails, 0, 4).map((basicDetail, index) => {
                        const uri = "/" + listType + "/" + basicDetail.id;
                        return <Grid.Column key={index}>
                            <Segment className="section-content" size="small">
                                <div className="list-top-content-image-wrapper">
                                    <Link to={uri}>
                                        <LazyLoad height={140} offset={100}>
                                            <CustomImage src={basicDetail.imageUrl + "?"}
                                                         width="100%"
                                                         height="140"
                                                         alt={basicDetail.title}/>
                                        </LazyLoad>
                                    </Link>
                                </div>
                                <div className="list-top-content-title max-1-lines"><Link
                                    to={uri}>{basicDetail.title}</Link></div>
                            </Segment>
                        </Grid.Column>
                    })
                }
            </Grid.Row>
        </Grid>
        <div className="explore" onClick={() => exploreAll()}>
            <div>Explore more</div>
            <img src="https://media.giphy.com/media/JROrvHnpJis4U/giphy.gif" height={30} width={30} alt="Explore All"/>
        </div>
        {/*<span className="explore" style={{float : 'right'}} onClick={()=>exploreAll()}>Explore All...</span>*/}
    </section>
};