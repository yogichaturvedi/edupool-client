/**
 * Created by Yogesh Chaturvedi on 16-06-2017.
 */
import React, {Component} from 'react'
import {Link} from 'react-router'
import {observer} from "mobx-react";
import {Grid, Icon} from "semantic-ui-react";
import './footer.scss';
import SubscriptionForm from "../form/subscription/subscription";
import FooterInfo from "../../static-data/footer.json";


@observer
class Footer extends Component {

    goToSocialSite(link) {
        window.open(link, '_blank');
    }

    render() {
        return (
            <div className="footer-container bg-extra-dark-gray">
                {/*<div className="opacity-extra-medium bg-black"/>*/}
                <div className="padding-three-tb">
                    <div className="container footer-top-container">
                        <Grid columns={4} style={{margin:0}}>
                            <Grid.Row className="row equalize xs-equalize-auto">
                                <Grid.Column computer={5} mobile={16} className="">
                                    <Link to="/">
                                        <div className="footer-logo margin-20px-bottom display-inline-block">EDUPOOL
                                        </div>
                                    </Link>
                                    <div className="footer-text text-small">
                                        {FooterInfo.description}
                                    </div>
                                    <div className="display-inline-block vertical-align-middle social-icons-wrapper">
                                        {FooterInfo.socialSites.map((socialSite, index) => {
                                            return <Icon key={index} className={"social-link-" + socialSite.name}
                                                         name={socialSite.icon} size="large"
                                                         onClick={(e) => this.goToSocialSite(socialSite.link)}
                                                         inverted/>
                                        })
                                        }
                                    </div>
                                </Grid.Column>
                                <Grid.Column computer={5} mobile={16} className="contact-info">
                                    <div
                                        className="alt-font text-small text-medium-gray text-uppercase footer-column-title">
                                        Contact Info
                                    </div>
                                    <div className="address">{FooterInfo.address.line1}<br/>{FooterInfo.address.line2}
                                    </div>
                                    <div className="email">
                                        <a href={`mailto:${FooterInfo.infoEmail}?subject=I have read your article from https://www.edupool.in`}>
                                            {FooterInfo.infoEmail}
                                        </a>
                                    </div>
                                    <div className="phone">
                                        <a href={`tel:${FooterInfo.contactNumbers[0]}`}>{FooterInfo.contactNumbers[0]}</a>,
                                        <a href={`tel:${FooterInfo.contactNumbers[1]}`}>{FooterInfo.contactNumbers[1]}</a>
                                    </div>
                                </Grid.Column>
                                <Grid.Column computer={5} mobile={16}
                                             className="border-right border-color-medium-dark-gray site-pages">
                                    <SubscriptionForm showToastr={this.props.showToastr} place="footer"/>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </div>
                </div>


                <div className="bg-dark-footer padding-50px-tb text-center xs-padding-30px-tb">
                    <div className="container footer-bottom-container">
                        <Grid columns={2} style={{margin:0}}>
                            <Grid.Row>
                                <Grid.Column computer={8} mobile={16} className="footer-text-left text-small">
                                    © 2017 EDUPOOL is Proudly Powered by <Link to="/about-us">Edupool India</Link>
                                </Grid.Column>
                                <Grid.Column computer={8} mobile={16} className="footer-text-right text-small">
                                    <Link to="/about-us">About Us</Link>&nbsp;&nbsp;|&nbsp;&nbsp;
                                    <a>Term and Condition</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                    <a>Privacy Policy</a>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </div>
                </div>
            </div>
        )
    }
}

export default Footer;
