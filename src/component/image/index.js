import React from 'react'
import DefaultImage from '../../assets/image/broken-image.png';
import DefaultCircularImage from '../../assets/image/broken-image-circular.png'

function onImageError(event, circularImage) {
    event.target.setAttribute("src", circularImage ? DefaultCircularImage : DefaultImage);
}

const CustomImage = (props) =>{
    return <img src={props.src}
         width={props.width}
         height={props.height}
         alt={props.title}
         onError={(e)=>onImageError(e, props.circular)}/>};

export default CustomImage;