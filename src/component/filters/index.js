/**
 * Created by Amit on 15-11-2017.
 */

import React, {Component} from 'react'
import {observer} from 'mobx-react';
import {toJS} from "mobx";
import {Button, Dropdown, Form, Icon, Input, Label, Segment} from "semantic-ui-react";
import Moment from "moment";
import DatePicker from "react-datepicker";
import FiltersStore from './store';
import './style.scss';

@observer
class FiltersForm extends Component {

    constructor(props) {
        super(props);
        this.onFormFieldChange = this.onFormFieldChange.bind(this);
        this.filterData = this.filterData.bind(this);
        this.resetFilters = this.resetFilters.bind(this);
    }

    componentDidMount() {
        if (this.props.page === 'result') {
            FiltersStore.populateBoardOrUniversities(this.props.rawData);
        }
        else if(this.props.page === 'exam'){
            FiltersStore.populateOrganizer(this.props.rawData);
        }
        else {
            FiltersStore.populateStates();
            FiltersStore.filter.boardOrUniversity.value = "";
            FiltersStore.filter.boardOrUniversity.list = [];
        }
    }

    componentWillUnmount(){
        FiltersStore.resetFilters();
    }

    onFormFieldChange(field, value) {
        FiltersStore.updateForm(field, value);
        this.props.filterData();
    }

    filterData() {
        this.props.filterData();
        // this.setState({activeIndex: -1})
    }

    resetFilter(type){
        this.props.resetFilter(type);
    }

    resetFilters() {
        this.props.resetFilters();
    }

    render() {
        const {page} = this.props;
        return (
            <Segment className="filters-form-wrapper">
                <Label attached="top" className="filters-form-header">
                    Apply Filters
                </Label>

                <div className="appliedFilters">
                    <Label.Group>
                        {
                            FiltersStore.filter.date.value &&
                            <Label size="medium" as='a' onClick={() => this.resetFilter('date')}>
                                {FiltersStore.filter.date.value.toDate().toLocaleDateString()}
                                <Icon name='close'/>
                            </Label>
                        }
                        {
                            FiltersStore.filter.state.value &&
                            <Label size="medium" as='a'
                                   onClick={() => this.resetFilter('state')}>
                                {FiltersStore.filter.state.value}
                                <Icon name='close'/>
                            </Label>
                        }
                        {
                            FiltersStore.filter.city.value &&
                            <Label size="medium" as='a'
                                   onClick={() => this.resetFilter('city')}>
                                {FiltersStore.filter.city.value}
                                <Icon name='close'/>
                            </Label>
                        }
                        {
                            FiltersStore.filter.schoolLevel.value &&
                            <Label size="medium" as='a'
                                   onClick={() => this.resetFilter('schoolLevel')}>
                                {FiltersStore.filter.schoolLevel.value}
                                <Icon name='close'/>
                            </Label>
                        }
                        {
                            FiltersStore.filter.boardOrUniversity.value &&
                            <Label size="medium" as='a'
                                   onClick={() => this.resetFilter('boardOrUniversity')}>
                                {FiltersStore.filter.boardOrUniversity.value}
                                <Icon name='close'/>
                            </Label>
                        }
                        {
                            FiltersStore.filter.organizer.value &&
                            <Label size="medium" as='a'
                                   onClick={() => this.resetFilter('organizer')}>
                                {FiltersStore.filter.organizer.value}
                                <Icon name='close'/>
                            </Label>
                        }
                    </Label.Group>
                </div>
                <Form className="filters-form" size="small" widths="equal">
                    {
                        page === 'result' && <Form.Field>
                            <label>Published Date</label>
                            <DatePicker
                                size="small"
                                selected={FiltersStore.filter.date.value}
                                dateFormat="MM/DD/YY"
                                maxDate={Moment()}
                                isClearable={true}
                                className="small"
                                customInput={<Input size="mini"/>}
                                placeholderText="MM/DD/YY"
                                onChange={(value) => this.onFormFieldChange('date', value)}/>
                        </Form.Field>
                    }
                    {
                        page !== 'result' && page !== 'exam' && <Form.Field>
                            <label>State</label>
                            <Dropdown
                                width={5}
                                size="small"
                                minCharacters={0}
                                placeholder="Select"
                                selectOnBlur={false}
                                value={FiltersStore.filter.state.value}
                                onChange={(e, value) => this.onFormFieldChange('state', value.value)}
                                options={toJS(FiltersStore.filter.state.list)} selection search fluid/>
                        </Form.Field>
                    }
                    {
                        page !== 'result' && page !== 'exam' && <Form.Field>
                            <label>City</label>
                            <Dropdown
                                width={5}
                                size="small"
                                minCharacters={0}
                                placeholder="Select"
                                selectOnBlur={false}
                                disabled={!FiltersStore.filter.state.value}
                                options={toJS(FiltersStore.filter.city.list)}
                                onChange={(e, value) => this.onFormFieldChange('city', value.value)}
                                value={FiltersStore.filter.city.value} search selection fluid/>
                        </Form.Field>
                    }
                    {
                        page === 'result' && <Form.Field>
                            <label>Board / University</label>
                            <Dropdown
                                width={5}
                                size="small"
                                minCharacters={0}
                                placeholder='Select'
                                selectOnBlur={false}
                                options={toJS(FiltersStore.filter.boardOrUniversity.list)}
                                value={FiltersStore.filter.boardOrUniversity.value}
                                onChange={(e, value) => this.onFormFieldChange('boardOrUniversity', value.value)}
                                search selection fluid/>
                        </Form.Field>
                    }
                    {
                        page === 'school' &&
                        <Form.Field>
                            <label>School Level</label>
                            <Dropdown
                                width={5}
                                size="mini"
                                minCharacters={0}
                                placeholder="Select"
                                selectOnBlur={false}
                                value={FiltersStore.filter.schoolLevel.value}
                                onChange={(e, value) => this.onFormFieldChange('schoolLevel', value.value)}
                                options={toJS(FiltersStore.filter.schoolLevel.list)} selection search fluid/>
                        </Form.Field>
                    }
                    {
                        page === 'exam' &&
                        <Form.Field>
                            <label>Organizer</label>
                            <Dropdown
                                width={5}
                                size="mini"
                                minCharacters={0}
                                placeholder="Select"
                                selectOnBlur={false}
                                value={FiltersStore.filter.organizer.value}
                                onChange={(e, value) => this.onFormFieldChange('organizer', value.value)}
                                options={toJS(FiltersStore.filter.organizer.list)} selection search fluid/>
                        </Form.Field>
                    }
                    <Form.Field className="filters-form-buttons-wrapper">
                        <Button icon="refresh" color="black" content="Reset" className="animated-button"
                                onClick={this.resetFilters} size="mini"/>
                    </Form.Field>
                </Form>
            </Segment>
        )
    }

}

export default FiltersForm;