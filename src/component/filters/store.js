/**
 * Created by Amit on 15-11-2017.
 */

import {observable, toJS} from "mobx";
import {getCities, getStates} from "../../util/states-cities";
import _ from "lodash";
import {schoolLevels} from "../../util/util";

class FiltersStore {

    @observable filter = {
        date : {
            value : null,
        },
        city : {
            value : '',
            list : []
        },
        state : {
            value : '',
            list : []
        },
        boardOrUniversity :{
            value : '',
            list : []
        },
        organizer:{
            value : '',
            list : []
        },
        schoolLevel :{
            value : '',
            list : schoolLevels
        }
    };

    @observable filteredData = [];

    updateForm(field, value){
        this.filter[field].value = value;

        // On state change update city dropdown
        if(field === 'state'){
            this.filter.city.list = getCities(this.filter.state.value);
            this.filter.city.value = "";
        }
    }

    populateStates(){
        this.filter.state.list = getStates();
    }

    populateBoardOrUniversities(rawData){
        this.filter.boardOrUniversity.list = _.chain(rawData)
            .uniqBy((data) => data.boardOrUniversity)
            .map((basicDetail, index) => {
                let boardOrUniversity = basicDetail.boardOrUniversity ? basicDetail.boardOrUniversity : 'Other';
                let count = _.countBy(rawData, (data) => data.boardOrUniversity === basicDetail.boardOrUniversity);
                return {key: index, value: boardOrUniversity, text: `${boardOrUniversity} (${count.true})`}
            })
            .sortBy(['value'])
            .value();
    }

    populateOrganizer(rawData){
        this.filter.organizer.list = _.chain(rawData)
            .uniqBy((data) => data.organizer)
            .map((basicDetail, index) => {
                let organizer = basicDetail.organizer ? basicDetail.organizer : 'Other';
                let count = _.countBy(rawData, (data) => data.organizer === basicDetail.organizer);
                return {key: index, value: organizer, text: `${organizer} (${count.true})`}
            })
            .sortBy(['value'])
            .value();
    }

    filterData(page, dataToFilter) {
        let $this = this;
        let filteredData = page === 'result' ? _.cloneDeep(dataToFilter) : toJS(dataToFilter);
        if(this.filter.date.value) {
            if(page === 'result') {
                filteredData = _.filter(filteredData, (data) => {
                    let resultDate = new Date(data.date).setHours(0,0,0,0);
                    let filterDate = Date.parse($this.filter.date.value.toDate().toISOString());
                    return resultDate === filterDate;
                });
            }
            else{
                filteredData = _.filter(filteredData, (data) => {
                    let resultDate = new Date(data.createdAt).setHours(0,0,0,0);
                    let filterDate = Date.parse($this.filter.date.value.toDate().toISOString());
                    return resultDate === filterDate;
                });
            }
        }
        if(this.filter.state.value) {
            let field = page === 'event' ? 'venue' : 'address';
            filteredData = _.filter(filteredData, (data)=>{
                return  data[field] && (data[field].state === $this.filter.state.value);
            });
        }
        if(this.filter.city.value) {
            let field = page === 'event' ? 'venue' : 'address';
            filteredData = _.filter(filteredData, (data)=>{
                return  data[field] && (data[field].city === $this.filter.city.value);
            });
        }
        if(this.filter.schoolLevel.value){
            filteredData = _.filter(filteredData, (data)=>{
                return data.level === $this.filter.schoolLevel.value
            });
        }
        if (this.filter.organizer.value) {
            filteredData = _.filter(filteredData, (data) => {
                return data.organizer === $this.filter.organizer.value || ($this.filter.organizer.value === 'Other' && !data.organizer)
            });
        }
        if (this.filter.boardOrUniversity.value) {
            filteredData = _.map(filteredData, (data) => {
                data.results = _.filter(data.results, (r) => r.boardOrUniversity === this.filter.boardOrUniversity.value);
                return data;
            });
            filteredData = _.filter(filteredData, (result) => result.results.length > 0)
        }

        return filteredData
    }

    resetFilter(type){
        this.filter[type].value = type === 'date' ? null : "";
    }

    resetFilters(){
        this.filter.date.value = null;
        this.filter.city.value = '';
        this.filter.state.value = '';
        this.filter.schoolLevel.value = '';
        this.filter.organizer.value = '';
        this.filter.boardOrUniversity.value = '';
    }

}

export default new FiltersStore();