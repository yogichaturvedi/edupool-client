/**
 * Created by Amit on 22-03-2018.
 */
import React, {Component} from 'react';
import {observer} from 'mobx-react';
import {Link} from "react-router";
import CustomImage from "../image/index";
import {Icon} from "semantic-ui-react";
import ArticleNewsStore from '../../route/store/article-news-store';
import { toTitleCase} from "../../util/util";
import './article-news.scss';

@observer
class ArticleNewsFilters extends Component {
    render(){
        return(
            <div style={{position: 'sticky', top: 'calc(1vw + 60px)'}}>
                <div className="filter-top-box bg-deep-pink padding-30px-all text-white text-center margin-45px-bottom xs-margin-25px-bottom">
                    <Icon name="quote left" size="large"/>
                    <span className="text-extra-large display-block">The future belongs to those who believe in the beauty of their dreams.</span>
                </div>

                <section className="popular-list-section">
                    <div className="section-heading text-extra-dark-gray alt-font text-uppercase text-small">
                        <span>Popular {toTitleCase(this.props.type)}</span>
                    </div>
                    <div className="section-content">
                        <ul className="latest-blog position-relative">
                            {
                                this.props.articleNewsList.map((item, index) => {
                                    const viewUri = `/${item.type}/view/${item.id}`;
                                    return <li key={index}>
                                        <figure>
                                            <Link to={viewUri}>
                                                <CustomImage src={item.thumbnail} alt={item.title} height={55} width={75}/>
                                            </Link>
                                        </figure>
                                        <div className="display-table-cell vertical-align-top text-small">
                                            <span className="title display-inline-block  max-2-lines">
                                                <Link to={viewUri} className="text-extra-dark-gray">{item.title}</Link>
                                            </span>
                                            <span className="blog-date clearfix text-medium-gray text-small">{ArticleNewsStore.getDate(item.createdAt)}</span>
                                        </div>
                                    </li>
                                })
                            }
                        </ul>
                    </div>
                </section>

                <section className="category-section">
                    <div className="section-heading text-extra-dark-gray alt-font text-uppercase text-small">
                        <span>Categories</span></div>
                    <div className="section-content">
                        <ul className="category-list position-relative text-small">
                            <li>
                                <div className="display-inline-block" onClick={() => this.props.filterArticleNewsList('category', 'All')}>
                                    <span className="category">All</span>
                                    <span className="count">{this.props.articleNewsList.length}</span>
                                </div>
                            </li>
                            {
                                this.props.categories.map((category, index) => {
                                    return <li key={index}>
                                        <div className="display-inline-block" onClick={() => this.props.filterArticleNewsList('category', category.name)}>
                                            <span className="category">{category.name}</span>
                                            <span className="count">{category.count}</span>
                                        </div>
                                    </li>
                                })
                            }
                        </ul>
                    </div>
                </section>

                <section className="tags-section">
                    <div className="section-heading text-extra-dark-gray alt-font text-uppercase text-small">
                        <span>Tags</span></div>
                    <div className="section-content">
                        {
                            this.props.tags.map((tag, i) => {
                                return <span className="tag" key={i}>{tag}</span>
                            })
                        }
                    </div>
                </section>
            </div>
        )
    }
}
export default ArticleNewsFilters;


