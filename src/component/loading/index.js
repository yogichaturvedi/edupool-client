/**
 * Created by Yogesh Chaturvedi on 11-06-2017.
 */
import React, {Component} from 'react';
import {observer} from 'mobx-react';
// import "../error/styles.css";
import "../error/error.scss";

@observer
class Loading extends Component {

    render() {
        return (
            <div>
                <div className="loader loading-book-icon">
                    <figure className="loading-book-page"/>
                    <figure className="loading-book-page"/>
                    <figure className="loading-book-page"/>
                </div>
                <h1 className="loading-text">Loading</h1>
            </div>
        );
    }
}

export default Loading;
