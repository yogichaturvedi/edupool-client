/**
 * Created by Amit on 04-12-2017.
 */

import React, {Component} from "react";
import {observer} from 'mobx-react';
import {Divider, Grid, Label, Segment} from "semantic-ui-react";
import {Link} from "react-router";
import Slider from "react-slick";
import "./style.scss";

const CarouselSettings = {
    autoPlaySpeed: 7000,
    autoplay: true,
    speed: 700,
    arrows: false,
    vertical: true,
    infinite: true
};

@observer
class RelatedInfo extends Component {

    render() {
        return (
            <Segment attached="top" className={`related-info-container  ${this.props.type}`}>
                <Label color="black" attached="top" className="title text-uppercase">{this.props.title}</Label>
                <div className="related-info-wrapper">
                    <Slider {...CarouselSettings}
                            slidesToShow={this.props.data.length < 4 ? this.props.data.length : 4}>
                        {
                            this.props.data.map((item, index) => {
                                return <div className="info" key={index}>
                                    <Link onClick={() => this.props.redirectTo(item.id, item.title)}>
                                        <p>
                                            <Grid>
                                                {
                                                    this.props.type !== 'result' && <Grid.Column width={3}>
                                                        <img style={{display: 'inline'}}
                                                             height={40} width={40}
                                                             alt={item.title}
                                                             src={item.imageUrl}/>
                                                    </Grid.Column>
                                                }
                                                <Grid.Column width={this.props.type === 'result' ? 16 : 13}>
                                                    <span className="title">{item.title}</span>
                                                    {
                                                        this.props.type === 'result' &&
                                                        <img style={{display: 'inline'}}
                                                             height={15} width={20}
                                                             alt={item.title}
                                                             src="https://media.giphy.com/media/hJQHFv5mdIU6Y/200.gif"/>
                                                    }
                                                </Grid.Column>
                                            </Grid>
                                        </p>
                                    </Link>
                                    <Divider/>
                                </div>

                            })
                        }
                    </Slider>
                </div>
            </Segment>
        )
    }
}

export default RelatedInfo