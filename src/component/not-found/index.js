/**
 * Created by Amit on 07-10-2017.
 */
import React from 'react';
// import './style.css';
import {Button} from "semantic-ui-react";
import {Link} from "react-router";
import './not-found.scss';

const NotFound = () =>
    <div className="not-found">
        <h1>404</h1>
        <p>Oops! Something is wrong.</p>
        <Link to="/"><Button icon="home" content="Go back to home"/></Link>
    </div>

export default NotFound;
