/**
 * Created by Yogesh Chaturvedi on 11-06-2017.
 */
import React, {Component} from 'react';
import {Dropdown, Grid, Icon, Image, Menu, Segment} from 'semantic-ui-react';
import 'semantic-ui-css/semantic.css';
import {observable} from 'mobx';
import {observer} from 'mobx-react';
import {browserHistory, Link} from 'react-router';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Footer from '../footer';
import LogoImage from '../../assets/image/logo-small.png';
import {reveal as BurgerMenu} from 'react-burger-menu';
import SearchExampleStandard from "../search/index";
import './style.scss';

const options = [
    {key: 'profile', text: "", value: ''},
    // {key: 'profile', text: 'Profile', value: 'profile'},
    {key: 'editAboutUs', text: 'Edit Home Info', value: 'editAboutUs'},
    // {key: 'changePassword', text: 'Change Password', value: 'changePassword'},
    {key: 'logout', text: 'Logout', value: 'logout'}
];

@observer
class Header extends Component {
    @observable active = "";
    @observable loading = false;
    @observable searchString = "";
    @observable isMenuOpen = false;
    rightMenuTrigger = () => {
        if (this.state.currentUser[0].profilePhoto) {
            return <Image size="mini" src={this.state.currentUser[0].profilePhoto}/>
        }
        else {
            return <Icon size="big" name="user circle outline" style={{color: "white"}}/>
        }
    };
    onAdmissionOpenClicked = () => {
        localStorage.setItem("activeTab", "college");
    }

    constructor(props) {
        super(props);
        const activeTab = localStorage.getItem("activeTab");
        this.active = activeTab && activeTab !== "/" ? activeTab : "home";
        this.menuClicked = this.menuClicked.bind(this);
        this.onMenuItemSelection = this.onMenuItemSelection.bind(this);
        this.onSearchChange = this.onSearchChange.bind(this);
        this.searchString = "";
        this.COMMON_MENU_ITEMS = ["school", "college", "coaching", "exam", "event", "result", "article", "news"];
        let user = localStorage.getItem("user");
        if (user) {
            user = JSON.parse(user);
            // Set username to first option of right menu list
            options[0].text = user[0].username;
            options[0].value = user[0].username;
        }
        this.state = {currentUser: user ? user : ''};
    }

    componentWillReceiveProps() {
        this.searchString = "";
        const activeTab = localStorage.getItem("activeTab");
        this.setState({currentUser: JSON.parse(localStorage.getItem("user"))});
        this.active = activeTab && activeTab !== "/" ? activeTab : "home";
        if (this.active !== 'home') {
            document.getElementById("computer-app-menu").style.background = "#0a0a0b"
        }
    }

    menuClicked(event, tab) {
        let selectedTab = tab.name;
        this.isMenuOpen = false;
        this.active = tab.name;
        localStorage.setItem("activeTab", selectedTab);
        browserHistory.push("/" + selectedTab);
    }

    onMenuItemSelection(event, menuItem) {
        event.preventDefault();
        if (menuItem.value === 'logout') {
            try {
                if (window.localStorage) {
                    localStorage.clear();
                }
                browserHistory.replace('/');
            }
            catch (err) {
                console.log('error occurred while logout', err);
            }
        }
        else if (menuItem.value === 'editAboutUs') {
            browserHistory.push("/add?type=home-info");
        }
    }

    getPathName() {
        let path = this.props.location.pathname;
        return path.split("/")[1];
    }

    onSearchChange(event) {
        this.searchString = event.target.value;
    }

    render() {
        const pathName = this.getPathName();
        const pageTransitionName = "page";
        const transitionClassName = "transition-group";
        const transitionDuration = 300;
        const transitionEnterTimeout = 2 * transitionDuration;
        const childrenWithProps = React.Children.map(this.props.children,
            (child) => React.cloneElement(child, {
                key: pathName,
                admin: this.state.currentUser ? this.state.currentUser[0] : "",
                showToastr: this.props.showToastr,
                confirmDelete: this.props.confirmDelete,
                showLoading: this.showLoading
            })
        );


        return (
            <Segment id="outer-container" basic className="no-padding-segment" style={{position: 'relative'}}>
                <section className={"app-header " + pathName}>
                    <Grid className="header-grid" style={{width: "100%", marginRight: 0, marginLeft: 0}}>
                        <Grid.Row only="mobile" style={{paddingTop: 0}}>
                            <Grid.Column width={16}>
                                <Menu fluid stackable={false} className="app-header" borderless={true} inverted
                                      style={{margin: 0, borderRadius: 0, background: '#0a0a0b', position: "fixed"}}>
                                    <Menu.Item name="home" style={{padding: '5p 10px', flex: 'unset'}}>
                                        <Grid columns={4} style={{width: '100%'}}>
                                            <Grid.Column width={2} textAlign="center"
                                                         style={{paddingRight: 0}}>
                                                <Image alt="app-logo" src={LogoImage} height={35} style={{marginTop: 6}}
                                                       onClick={(event) => this.menuClicked(event, {name: "home"})}/>
                                            </Grid.Column>
                                            <Grid.Column width={5} textAlign="left"
                                                         style={{paddingLeft: 0, paddingRight: 0, textAlign: "left"}}>
                                                <h3 onClick={(event) => this.menuClicked(event, {name: "home"})}
                                                    style={{lineHeight: "2.5em", fontWeight: 'bold'}}>
                                                    &nbsp;&nbsp;EDUPOOL</h3>
                                            </Grid.Column>
                                            <Grid.Column width={7} textAlign="center"
                                                         className="header-search-input"
                                                         style={{marginRight: 0, padding: 0}} verticalAlign="middle">
                                                {
                                                    this.active !== "home" ?
                                                        <SearchExampleStandard id="search-in-burger" type={this.active}
                                                                               updateBurgerMenu={() => {
                                                                                   this.isMenuOpen = false
                                                                               }} style={{lineHeight: "3.4em"}}/>
                                                        : <div style={{width: 220}}/>
                                                }
                                            </Grid.Column>
                                            <Grid.Column width={1} textAlign="right" verticalAlign="middle">
                                                <Icon name="bars" size="big"
                                                      onClick={() => {
                                                          this.isMenuOpen = !this.isMenuOpen
                                                      }}/>
                                            </Grid.Column>
                                        </Grid>
                                    </Menu.Item>
                                </Menu>
                                <div style={{width: "100%", marginTop: '58px'}}>
                                    <BurgerMenu noOverlay className="burger-menu-style" pageWrapId={"page-wrap"}
                                                outerContainerId={"outer-container"} width={"100%"} style={{left: 0}}
                                                isOpen={this.isMenuOpen} animation="elastic" customBurgerIcon={false}
                                                customCrossIcon={false}>
                                        <Menu stackable className="app-header" borderless={true} inverted
                                              style={{width: "100%"}}>
                                            {
                                                this.COMMON_MENU_ITEMS.map((menuItem, index) => {
                                                    return <Menu.Item name={menuItem} as={Link} to={`/${menuItem}`}
                                                                      onClick={this.menuClicked}
                                                                      active={this.active === menuItem} key={index}/>
                                                })
                                            }

                                            <Menu.Menu position='right'>
                                                {
                                                    this.state.currentUser && <Menu.Item name='logout'>
                                                        <Dropdown item onChange={this.onMenuItemSelection}
                                                                  options={options} trigger={this.rightMenuTrigger()}
                                                                  icon={null}/>
                                                    </Menu.Item>
                                                }

                                            </Menu.Menu>
                                        </Menu>
                                    </BurgerMenu>
                                </div>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row only="computer tablet">
                            <Segment basic className="no-padding-segment" style={{width: "100%"}}>
                                <Menu stackable className="app-computer-menu" id="computer-app-menu"
                                      style={{maxHeight: "52px"}} pointing borderless={true} inverted>
                                    <Menu.Item name="home" className="app-logo" onClick={this.menuClicked}>
                                        <img alt="app-logo" src={LogoImage}/>
                                        <span style={{
                                            fontSize: "20px",
                                            fontWeight: 'bold'
                                        }}> &nbsp;&nbsp;EDUPOOL</span>
                                    </Menu.Item>
                                    {
                                        this.COMMON_MENU_ITEMS.map((menuItem, index) => {
                                            return <Menu.Item name={menuItem} as={Link} to={`/${menuItem}`}
                                                              onClick={this.menuClicked}
                                                              active={this.active === menuItem} key={index}/>
                                        })
                                    }
                                    <Menu.Menu position='right'>
                                        <Menu.Item style={{paddingTop: 0, paddingBottom: 0}}>
                                            {
                                                this.active !== "home" &&
                                                <div className="header-search-input">
                                                    <SearchExampleStandard type={this.active}/></div>
                                            }
                                        </Menu.Item>
                                        <Menu.Item name="admisson-open" className="admission-open"
                                                   style={{paddingTop: 0, paddingBottom: 0, color: 'white !important'}}
                                                   as={Link} onClick={this.onAdmissionOpenClicked}
                                                   to={"/college" + "?admissionOpen=true"}>
                                            {
                                                <div
                                                    style={{color: 'white'}}>Admissions Open</div>

                                            }
                                        </Menu.Item>
                                        {
                                            this.state.currentUser && <Menu.Item name='logout'>
                                                <Dropdown item onChange={this.onMenuItemSelection} options={options}
                                                          trigger={this.rightMenuTrigger()} icon={null}/>
                                            </Menu.Item>
                                        }
                                    </Menu.Menu>
                                </Menu>
                            </Segment>
                        </Grid.Row>
                    </Grid>
                </section>
                <section className={"app-body " + pathName}>
                    <div id="page-wrap">
                        <ReactCSSTransitionGroup
                            component="div"
                            className={transitionClassName}
                            transitionName={pageTransitionName}
                            transitionEnterTimeout={transitionEnterTimeout}
                            transitionLeaveTimeout={transitionDuration}>
                            <Segment basic className="body-container no-padding-segment">
                                {childrenWithProps}
                            </Segment>
                        </ReactCSSTransitionGroup>
                    </div>
                </section>
                {
                    pathName !== 'home' && pathName !== "about-us" && <section className="app-footer">
                        <Footer showToastr={this.props.showToastr}/>
                    </section>
                }
            </Segment>
        )
    }
}

export default Header;
