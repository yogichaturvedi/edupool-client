/**
 * Created by Amit on 18-09-2017.
 */
import React from 'react';
import {Helmet} from "react-helmet";
import {socialSitesInfo} from "../../util/util";

const SEO_IMAGE = {
    home: "https://image.ibb.co/dPXgvG/home_seo.jpg",
    about_us:  'https://image.ibb.co/draehw/about_us.png',
    school:  "https://image.ibb.co/ehM8aG/school_seo.jpg",
    college:  "https://image.ibb.co/nzBKhw/college_seo.jpg",
    coaching:  'https://image.ibb.co/eV7s2w/coaching_seo.jpg',
    exam:  "https://image.ibb.co/iXPuFG/exam_seo.jpg",
    event:  "https://image.ibb.co/eQioaG/event_seo.jpg",
    result:  "https://image.ibb.co/cPQC2w/result_seo.jpg",
    study:  "https://image.ibb.co/eNhEFG/study_seo.jpg",
};

const SEO = (props) => {

    let content = {
        title : props.title ? "Edupool | " + props.title : "Edupool",
        pageUrl: window.location.href.split("?")[0],
        description : props.description ? props.description.substr(0,160) : "Edupool India",
        image : props.image || SEO_IMAGE[props.page] || "https://image.ibb.co/mvxRf6/logo.png",
        canonicalUrl: `http://edupool.in${window.location.pathname}${window.location.search ? window.location.search : ""}`,
        keywords : 'School, College, Coaching, Exam, Event, Result, Study Material',
    };

    return (
        <Helmet defaultTitle="Edupool">
            <title>{content.title }</title>
            <meta name="description" content={ content.description } />
            <meta name="keywords" content={ content.keywords } />

            {/*Twitter Card data*/}
            <meta name="twitter:card" content="list"/>
            <meta name="twitter:site" content={socialSitesInfo.twitter.link}/>
            <meta name="twitter:title" content={content.title}/>
            <meta name="twitter:description" content={content.description}/>
            <meta name="twitter:creator" content="@Chaturvediyo"/>
            <meta name="twitter:url" content={socialSitesInfo.twitter.link}/>
            <meta name="twitter:image" content={content.image}/>)


            {/*Facebook Card data*/}
            <meta name="facebook:card" content="summary"/>
            <meta name="facebook:site" content={socialSitesInfo.facebook.link}/>
            <meta name="facebook:title" content={content.title}/>
            <meta name="facebook:description" content={content.description}/>
            <meta name="facebook:creator" content="@Chaturvediyo"/>
            <meta name="facebook:url" content={socialSitesInfo.facebook.link}/>
            <meta name="facebook:image" content={content.image}/>)

            {/*Open graph tags*/}
            <meta property="og:locale" content="en_IN" />
            <meta property="og:type" content="article" />
            <meta property="og:title" content={content.title}/>
            <meta property="og:description" content={content.description}/>
            <meta property="og:url" content={content.pageUrl}/>
            <meta property="og:site_name" content="Edupool India"/>
            <meta property="og:image" content={content.image}/>
            <meta property="fb:app_id" content="136115847097236" />

            <link rel="canonical" href={content.canonicalUrl}/>
        </Helmet>
    );

};


export default SEO;
