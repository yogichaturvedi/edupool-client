/**
 * Created by Amit on 03-11-2017.
 */

import React, {Component} from 'react';
import {Grid, Icon} from 'semantic-ui-react';
import {observer} from 'mobx-react';
import ResultDetailCard from "../card/result-card";
import './style.scss';

@observer
class ResultList extends Component {
    constructor(props) {
        super(props);
        this.updatePages = this.updatePages.bind(this);

    }

    updatePages(pages){
        this.props.updatePages(pages);
    }

    render() {
        return (
            <div>
                {this.props.results.map((groupedData, index) => {
                    return index < this.props.currentPage * 5 && index >= (this.props.currentPage * 5 - 5) &&
                        <div key={index} className="result-list-container">
                            <div className="result-card-created-date">
                                <Icon name="calendar" size="large"/>{groupedData.date}</div>
                            {
                                groupedData.results.map((dataGroupedByDate, i) => {
                                    return <div key={i}>
                                        <div className="result-card-boardOrUniversity">
                                            <Icon name="building" size="small"
                                                  style={{color: 'white'}}/>{dataGroupedByDate.boardOrUniversity}
                                        </div>
                                        <div className="border-bottom-colored"/>
                                        <Grid style={{margin: '5px 0 0 0'}} padded>
                                            <Grid.Row columns={2}>
                                                {
                                                    dataGroupedByDate.results.map((basicDetails, key) => {
                                                        return <Grid.Column  key={key} computer={dataGroupedByDate.results.length === 1 ? 10 :8} mobile={16}
                                                                            className="result-card-wrapper">
                                                            <ResultDetailCard
                                                                key={key}
                                                                admin={this.props.admin}
                                                                type="result"
                                                                deleteDetail={this.props.deleteDetail}
                                                                basicDetail={basicDetails}/>
                                                        </Grid.Column>
                                                    })
                                                }
                                            </Grid.Row>
                                        </Grid>
                                    </div>
                                })
                            }
                        </div>
                })
                }
            </div>
        )
    }
}

export default ResultList