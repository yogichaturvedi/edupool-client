/**
 * Created by Amit on 16-03-2018.
 */
import React, {Component} from 'react';
import {observer} from 'mobx-react';
import {redirectToPage} from "../../util/util";
import {Button, Grid, Message, Pagination} from "semantic-ui-react";
import ArticleNewsCard from "../card/article-news";
import ArticleNewsFilter from "../filters/article-news";
import ArticleNewsStore from '../../route/store/article-news-store';
import _ from 'lodash';
import './style.scss';
import FirebaseService from '../../service/firebase-service';


@observer
class ArticleNewsList extends Component {

    populateArticleAndNews = (articleNewsList) => {
        let i = 1;
        let stateData = {list1: [], list2: [], list3: []};
        _.each(articleNewsList, (articleNews) => {
            let string = 'list' + i;
            stateData[string] = _.union(stateData[string] || [], [articleNews]);
            i++;
            if (i > 3) {
                i = 1;
            }
        });
        this.setState(stateData);
        console.log(FirebaseService.getCurrentUser());
    };
    filterArticleNewsList = (filterType, filterValue) => {
        const filteredList = ArticleNewsStore.filter(filterType, filterValue);
        this.populateArticleAndNews(filteredList);
        window.scrollTo(0, 0);
    };
    handlePaginationChange = (e, data) => {
        this.setState({activePage: data.activePage});
        this.handlePagination(data.activePage);
    };
    handlePagination = (activePage) => {
        const $this = this;
        let filteredList = _.filter(this.props.articleNewsList, (item, index) => {
            return index < (activePage * $this.pageItems) && index >= (activePage * $this.pageItems - $this.pageItems)
        });
        this.populateArticleAndNews(filteredList);
    };
    getMessage = () => {
        if (this.props.location.query.search) {
            return `Sorry no results found for your query "${this.props.location.query.search}". Please be more specific.`
        }
        else {
            return "Sorry No results found. Please try again after some time."
        }
    };

    constructor(props) {
        super(props);
        this.pageItems = 9;
        this.state = {list1: [], list2: [], list3: [], activePage: 1, totalPages: 0}
    }

    componentDidMount() {
        this.setState({totalPages: Math.ceil(this.props.articleNewsList.length / this.pageItems)});
        this.handlePagination(1);
    }

    componentWillReceiveProps(props) {
        if (props.articleNewsList) {
            this.populateArticleAndNews(props.articleNewsList);
        }
    }

    render() {
        return <div>
            {
                this.props.admin ?
                    <div className='basic-detail-add-new-button' style={{float: 'right'}}>
                        <Button
                            className="computerOnly"
                            content='Add New'
                            onClick={(e) => redirectToPage(`/${this.props.type}/add`)}
                            color="green"
                            icon="plus" inverted/>
                        <Button
                            className="mobileOnly"
                            onClick={(e) => redirectToPage(`/${this.props.type}/add`)}
                            color="green"
                            icon="plus" inverted circular/>
                    </div>
                    :
                    null
            }

            <Grid className="blogs-list-content">
                <Grid.Row columns={2}>
                    <Grid.Column className="blogs-wrapper" computer={12} mobile={16}>
                        {
                            this.props.articleNewsList.length === 0 ?
                                <Message size='large' negative floating>
                                    <Message.Header>No Results Found</Message.Header>
                                    <p>{this.getMessage()}</p>
                                </Message>
                                :
                                <Grid className="blogs">

                                    <Grid.Column computer={5} mobile={16} className='blog column-1'>
                                        {
                                            this.state.list1.map((item, index) => {
                                                return <ArticleNewsCard
                                                    key={index}
                                                    column={1}
                                                    type={this.props.type}
                                                    articleNews={item}
                                                    admin={this.props.admin}
                                                    deleteArticleNews={this.props.deleteArticleNews}/>
                                            })
                                        }
                                    </Grid.Column>
                                    <Grid.Column computer={5} mobile={16} className='blog column-2'>
                                        {
                                            this.state.list2.map((item, index) => {
                                                return <ArticleNewsCard
                                                    key={index}
                                                    column={2}
                                                    type={this.props.type}
                                                    articleNews={item}
                                                    admin={this.props.admin}
                                                    deleteArticleNews={this.props.deleteArticleNews}/>
                                            })
                                        }
                                    </Grid.Column>
                                    <Grid.Column computer={5} mobile={16} className='blog column-3'>
                                        {
                                            this.state.list3.map((item, index) => {
                                                return <ArticleNewsCard
                                                    key={index}
                                                    column={3}
                                                    type={this.props.type}
                                                    articleNews={item}
                                                    admin={this.props.admin}
                                                    deleteArticleNews={this.props.deleteArticleNews}/>
                                            })
                                        }
                                    </Grid.Column>
                                </Grid>
                        }

                        {
                            this.state.totalPages > 1 && <div style={{textAlign: 'center'}}>
                                <Pagination
                                    size='mini'
                                    boundaryRange={2}  className="article-news-pagination"
                                    siblingRange={1}
                                    activePage={this.state.activePage}
                                    onPageChange={this.handlePaginationChange}
                                    totalPages={this.state.totalPages}/>
                            </div>
                        }
                    </Grid.Column>

                    <Grid.Column className="filters-wrapper" computer={4} mobile={16}>
                        <ArticleNewsFilter
                            type={this.props.type}
                            tags={ArticleNewsStore.getTags()}
                            categories={ArticleNewsStore.getCategories()}
                            articleNewsList={_.slice(this.props.articleNewsList, 0, 5)}
                            filterArticleNewsList={this.filterArticleNewsList}/>
                    </Grid.Column>

                </Grid.Row>
            </Grid>
        </div>
    }

}

export default ArticleNewsList;