import React, {Component} from 'react';
import {Button, Grid, Message, Segment} from 'semantic-ui-react';
import {observable, toJS} from 'mobx';
import {observer} from 'mobx-react';
import ExamDetailCard from "../card/exam-card";
import EventDetailCard from "../card/event-card";
import StudyDetailCard from "../card/study-card";
import BasicDetailCard from "../card/index";
import FiltersStore from "../filters/store"
import {redirectToPage} from '../../util/util';
import _ from "lodash";
import './style.scss';
import ResultList from "./result";
import FiltersForm from "../filters/index";
import {Pagination} from "react-bootstrap";

@observer
class List extends Component {
    @observable currentPage = 1;
    @observable pageItems = 10;
    @observable groupedResults = [];
    @observable pages = [];
    @observable filteredData = [];

    constructor(props) {
        super(props);
        this.currentPage = 1;
        this.updatePages = this.updatePages.bind(this);
        this.resetFilter = this.resetFilter.bind(this);
        this.resetFilters = this.resetFilters.bind(this);
        this.filterData = this.filterData.bind(this);
    }

    componentWIllUnmount() {
        FiltersStore.resetFilters();
    }

    componentWillReceiveProps(props) {
        if (props.type === 'result') {
            this.groupedResults = this.getGroupedResults(props.basicDetails);
            this.filteredData = this.groupedResults;
        }
        else {
            this.filteredData = props.basicDetails;
        }
        this.pages = this._getPages(this.filteredData);
    }

    componentDidMount() {
        this.groupedResults = this.getGroupedResults(this.props.basicDetails);
        this.filteredData = this.props.type === 'result' ? this.groupedResults : this.props.basicDetails;
        this.pages = this._getPages(this.filteredData);
    }

    // Function to get results grouped by date
    getGroupedResults(basicDetails) {
        let resultsGroupedByDate = _.groupBy(basicDetails, (detail) => new Date(detail.createdAt).toLocaleDateString());
        return _.map(resultsGroupedByDate, (data, key) => {
            let dataGroupedByBoard = _.groupBy(data, 'boardOrUniversity');
            let mappedData = _.map(dataGroupedByBoard, (d, k) => {
                return {boardOrUniversity: k === 'undefined' ? 'Other' : k, results: d}
            });
            return {date: key, fullDate: data.createdAt, results: mappedData}
        });
    }

    _getPages(filteredData) {
        if (this.props.type === 'result') {
            this.pageItems = 5;
        }
        else if (this.props.type === 'event') {
            this.pageItems = 9;
        }
        return _.range(1, Math.ceil(filteredData.length / this.pageItems) + 1)
    }

    handlePagination(page) {
        this.currentPage = page;
    }

    updatePages(pages) {
        this.pages = pages;
    }

    getMessage() {
        if (this.props.location.query.search) {
            return `Sorry no results found for your query "${this.props.location.query.search}". Please be more specific.`
        }
        else {
            return "Sorry No results found. Please try again after some time."
        }

    }

    filterData() {
        let dataToFilter = this.props.type === 'result' ? this.groupedResults : this.props.basicDetails;
        let filteredData = FiltersStore.filterData(this.props.type, toJS(dataToFilter));
        this.filteredData = filteredData;
        this.updatePages(this._getPages(filteredData));
        this.handlePagination(1);

    }

    resetFilter(type) {
        FiltersStore.resetFilter(type);
        this.filterData();
    }

    resetFilters() {
        FiltersStore.resetFilters();
        if (this.props.type === 'result') {
            this.filteredData = this.groupedResults;
            this.updatePages(this._getPages(this.groupedResults));
        }
        else {
            this.filteredData = this.props.basicDetails;
            this.updatePages(this._getPages(this.props.basicDetails));
        }
        this.handlePagination(1);
    }

    getCard() {
        if (this.props.type === "school" || this.props.type === "college" || this.props.type === "coaching") {
            return <Grid>
                <Grid.Row columns={2}>
                    {
                        this.filteredData.map((basicDetail, index) => {
                            return index < this.currentPage * this.pageItems && index >= (this.currentPage * this.pageItems - this.pageItems) &&
                                <Grid.Column computer={8} mobile={16} key={index}>
                                    <BasicDetailCard type={this.props.type}
                                                     admin={this.props.admin}
                                                     applyNow={this.props.applyNow}
                                                     deleteDetail={this.props.deleteDetail}
                                                     basicDetail={basicDetail}/>
                                </Grid.Column>
                        })
                    }
                </Grid.Row>
            </Grid>
        }
        else if (this.props.type === "result") {
            return <ResultList
                admin={this.props.admin}
                currentPage={this.currentPage}
                results={this.filteredData}
                deleteDetail={this.props.deleteDetail}
                rawData={this.props.basicDetails}
                updatePages={this.updatePages}/>
        }
        else if(this.props.type === 'event'){
            return <Grid.Column computer={13} mobile={16}>
                <Grid>
                    <Grid.Row columns={window.screen.width <= 710 ? 1 : 3}>
                        {
                            this.filteredData.map((basicDetail, index) => {
                                return index < this.currentPage * this.pageItems && index >= (this.currentPage * this.pageItems - this.pageItems) &&
                                    <Grid.Column key={index}>
                                        <EventDetailCard admin={this.props.admin}
                                                         type={this.props.type}
                                                         deleteDetail={this.props.deleteDetail}
                                                         basicDetail={basicDetail}/>
                                    </Grid.Column>
                            })
                        }
                    </Grid.Row>
                </Grid>
            </Grid.Column>
        }
        else {
            return <Grid>
                <Grid.Row columns={2}> {
                    this.filteredData.map((basicDetail, index) => {
                        return index < this.currentPage * this.pageItems && index >= (this.currentPage * this.pageItems - this.pageItems) &&
                            <Grid.Column computer={8} mobile={16} key={index}>
                                <ExamDetailCard
                                    key={index}
                                    admin={this.props.admin}
                                    type={this.props.type}
                                    deleteDetail={this.props.deleteDetail}
                                    basicDetail={basicDetail}/>
                            </Grid.Column>
                    })
                }
                </Grid.Row>
            </Grid>
        }
    }

    render() {
        const basicDetails = this.props.basicDetails;
        return (
            <Segment className={"app-list-segment " + this.props.type} basic>
                {
                    !this.props.type ?
                        <div>
                            <div style={{textAlign: 'center'}} className="text-uppercase">
                                <h2 style={{display: 'inline-block', margin: 'auto', verticalAlign: 'middle'}}>
                                    {this.props.title}
                                </h2>
                                {
                                    this.props.admin ?
                                        <Button as="a" style={{marginLeft: "5px"}}
                                                size="mini"
                                                onClick={(e) => redirectToPage("add", "type=" + this.props.type)}
                                                color="green"
                                                icon="plus" circular inverted/>
                                        :
                                        null
                                }
                                <div className="separator-line"/>
                            </div>
                        </div>
                        :
                        <div>
                            {/*<Header as='h2' className="basic-detail-list-header" style={{float:'left'}}>*/}
                                {/*<Icon color="blue" name={this.props.icon}/>*/}
                                {/*<Header.Content>*/}
                                    {/*{this.props.title}*/}
                                {/*</Header.Content>*/}
                            {/*</Header>*/}
                            {
                                this.props.admin ?
                                    <div className='basic-detail-add-new-button' style={{float:'right'}}>
                                        <Button
                                            className="computerOnly"
                                            content='Add New'
                                            onClick={(e) => redirectToPage("add", "type=" + this.props.type)}
                                            color="green"
                                            icon="plus" inverted/>

                                        {

                                            (this.props.type === 'college' || this.props.type === 'coaching') &&
                                            <Button
                                                className="computerOnly"
                                                content='Add Course'
                                                onClick={this.props.showHideCourseModal}
                                                color="blue"
                                                icon="book" inverted/>
                                        }


                                        <Button
                                            className="mobileOnly"
                                            onClick={(e) => redirectToPage("add", "type=" + this.props.type)}
                                            color="green"
                                            icon="plus" inverted circular/>
                                    </div>
                                    :
                                    null
                            }
                        </div>
                }

                {(this.props.type !== "study-material") &&
                <Grid columns={2} style={{clear:'both'}}>
                    <Grid.Column computer={3} mobile={16}>
                        <FiltersForm
                            page={this.props.type}
                            rawData={this.props.basicDetails}
                            filterData={this.filterData}
                            resetFilter={this.resetFilter}
                            resetFilters={this.resetFilters}/>
                    </Grid.Column>
                    <Grid.Column computer={13} mobile={16}>
                        {
                            this.filteredData.length === 0 ?
                                <Message size='large' negative floating>
                                    <Message.Header>No Results Found</Message.Header>
                                    <p>{this.getMessage()}</p>
                                </Message>
                                :
                                <div>
                                    {
                                        this.getCard()
                                    }
                                </div>
                        }
                    </Grid.Column>
                </Grid>
                }

                {
                    (this.props.type === "study-material") && this.filteredData.length === 0 &&
                    <Message size='large' negative floating>
                        <Message.Header>No Results Found</Message.Header>
                        <p>{this.getMessage()}</p>
                    </Message>
                }

                {
                    this.props.type === "study-material" && <div style={{clear:'both'}}>
                        {
                            basicDetails.map((basicDetail, index) => {
                                return index < this.currentPage * this.pageItems && index >= (this.currentPage * this.pageItems - this.pageItems) &&
                                    <StudyDetailCard key={index}
                                                     admin={this.props.admin}
                                                     type={this.props.type}
                                                     deleteDetail={this.props.deleteDetail}
                                                     basicDetail={basicDetail}/>
                            })
                        }
                    </div>
                }
                {/*{*/}
                    {/*(this.props.type === "event") &&*/}
                    {/*<Grid columns={2}>*/}
                        {/*<Grid.Column computer={3} mobile={16}>*/}
                            {/*<FiltersForm*/}
                                {/*page={this.props.type}*/}
                                {/*rawData={this.props.basicDetails}*/}
                                {/*filterData={this.filterData}*/}
                                {/*resetFilter={this.resetFilter}*/}
                                {/*resetFilters={this.resetFilters}/>*/}
                        {/*</Grid.Column>*/}
                        {/*<Grid.Column computer={13} mobile={16}>*/}

                            {/*<Grid>*/}
                                {/*<Grid.Row columns={3}>*/}
                                    {/*{*/}
                                        {/*this.filteredData.map((basicDetail, index) => {*/}
                                            {/*return index < this.currentPage * this.pageItems && index >= (this.currentPage * this.pageItems - this.pageItems) &&*/}
                                                {/*<Grid.Column key={index}>*/}
                                                    {/*<EventDetailCard admin={this.props.admin}*/}
                                                                     {/*type={this.props.type}*/}
                                                                     {/*deleteDetail={this.props.deleteDetail}*/}
                                                                     {/*basicDetail={basicDetail}/>*/}
                                                {/*</Grid.Column>*/}
                                        {/*})*/}
                                    {/*}*/}
                                {/*</Grid.Row>*/}
                            {/*</Grid>*/}
                        {/*</Grid.Column>*/}
                    {/*</Grid>*/}
                {/*}*/}
                {
                    this.pages.length > 0 &&

                    <Pagination
                        prev
                        next
                        first
                        last
                        ellipsis
                        boundaryLinks
                        items={this.pages.length}
                        maxButtons={3}
                        bsClass="pagination"
                        activePage={this.currentPage}
                        style={{margin: 0, float: 'right'}}
                        onSelect={event => this.handlePagination(event)}
                    />
                }

            </Segment>

        );
    }

}

export default List;
