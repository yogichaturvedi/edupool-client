import React, {Component} from 'react';
import {Button, Divider, Form, Input, Segment} from "semantic-ui-react";
import {observable} from "mobx";
import {observer} from "mobx-react";
import ContactNumberStore from "../../route/store/contact-number-store";
import './form.scss';

@observer
class ContactNumberForm extends Component {

    @observable contactNumbers = [];

    constructor(props) {
        super(props);
        this.addNewContactNumber = this.addNewContactNumber.bind(this);
        this.removeContactNumber = this.removeContactNumber.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.onContactNumberChange = this.onContactNumberChange.bind(this);
        this.contactNumbers = props.contactNumbers;
    }

    componentWillReceiveProps(props) {
        this.contactNumbers = props.contactNumbers;
    }

    onContactNumberChange(index, value) {
        this.contactNumbers[index] = value;
        this.props.onContactNumberChange(index, value);
    }

    onBlur(index) {
        this.props.onBlur(index);
    }

    addNewContactNumber() {
        this.props.addNewContactNumber();
    }

    removeContactNumber(index) {
        this.props.removeContactNumber(index);
    }


    render() {
        return (
            <Segment>
                <h3>Contact Numbers</h3>
                <Divider/>
                <h5 style={{color: 'red'}}>{ContactNumberStore.message}</h5>
                <Form>
                    {
                        this.contactNumbers.map((number, index) => {
                            return <Form.Group key={"contact_number_" + (index + 1)} widths={8} unstackable>
                                <Form.Field width={14}>
                                    <Form.Input placeholder={"Enter Contact Number" + (index + 1)}
                                        value={this.contactNumbers[index]}
                                        onChange={(e) => this.onContactNumberChange(index, e.target.value)}
                                        onBlur={(e) => this.onBlur(index)}
                                        label={"Contact Number " + (index + 1)}/>
                                </Form.Field>
                                <Form.Field>
                                    <Form.Button className="delete-button" icon="trash" color="red" inverted circular
                                        onClick={(event) => this.removeContactNumber(index)}/>
                                </Form.Field>
                            </Form.Group>
                        })
                    }
                    <Button icon="plus" color="green" inverted circular onClick={(event) => this.addNewContactNumber()}/>
                </Form>
            </Segment>);
    }
}

export default ContactNumberForm;