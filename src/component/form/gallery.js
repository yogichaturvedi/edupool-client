import React, {Component} from 'react';
import {Button, Divider, Form, Input, Label, Segment} from "semantic-ui-react";
import {observable} from "mobx";
import {observer} from "mobx-react";
import GalleryStore from "../../route/store/gallery-store";

@observer
class GalleryForm extends Component {

    @observable images = [];

    constructor(props) {
        super(props);
        this.addNewImageUrl = this.addNewImageUrl.bind(this);
        this.removeImageUrl = this.removeImageUrl.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.onImageUrlChange = this.onImageUrlChange.bind(this);
        this.images = props.images
    }

    componentWillReceiveProps(props) {
        this.images = props.images
    }

    onImageUrlChange(index, value) {
        this.images[index] = value;
        this.props.onImageUrlChange(index, value);
    }

    onBlur(index) {
        this.props.onBlur(index);
    }

    addNewImageUrl() {
        this.props.addNewImageUrl();
    }

    removeImageUrl(index) {
        this.props.removeImageUrl(index);
    }


    render() {
        return (
            <Segment>
                <h3>Gallery</h3>
                <Divider/>
                <h5 style={{color: 'red'}}>{GalleryStore.message}</h5>
                <Form>
                    {
                        this.images.map((image, index) => {
                            return <Form.Group key={"gallery_image_" + (index + 1)}>
                                <Form.Field width={15}>
                                    <Input placeholder={"Enter Image URL" + (index + 1)}
                                           value={this.images[index]}
                                           onChange={(e) => {
                                               this.onImageUrlChange(index, e.target.value)
                                           }}
                                           onBlur={(e) => this.onBlur(index)}
                                           label={"Image " + (index + 1) + " URL"}/>
                                </Form.Field>
                                <Form.Button icon="trash" color="red" inverted circular
                                             onClick={(event) => this.removeImageUrl(index)}/>
                            </Form.Group>
                        })
                    }
                    <Button icon="plus" color="green" inverted circular onClick={(event) => this.addNewImageUrl()}/>
                </Form>
            </Segment>);
    }
}

export default GalleryForm;