/**
 * Created by Amit on 02-04-2018.
 */
import React, {Component} from 'react';
import {observer} from "mobx-react";
import {Link} from "react-router";
import {Button, Feed, Form, Icon, TextArea} from "semantic-ui-react";
import ArticleNewsStore from "../../../route/store/article-news-store";
import CustomImage from "../../image/index";
import _ from 'lodash';
import './style.scss';

@observer
class CommentForm extends Component {

    constructor(props){
        super(props);
        this.state = {
            comments: this.props.comments ? _.slice(this.props.comments, 0, 5).reverse() : []
        }
    }

    componentWillReceiveProps(nextProps, nextState){
        this.setState({comments: _.slice(nextProps.comments, 0, 5).reverse()})
    }

    viewOlderComments = () =>  this.setState({comments:  _.slice(this.props.comments, 0, this.state.comments.length + 5).reverse()});

    render() {
        const {user, signIn, signOut, comment, onCommentChange, postComment} = this.props;
        return (
            <div className="comment-wrapper" style={{marginTop: 100}}>
                <Form>
                    <Form.Field>
                        <TextArea
                            className="comment"
                            value={comment}
                            style={{minHeight: 100}}
                            onChange={(e) => onCommentChange(e.target.value)}
                            placeholder='Enter your comment here'/>
                        <span className="comment-limit">{200-comment.length} chars left</span>
                    </Form.Field>
                    <Form.Field>
                        {
                            user.name ?
                                <div>
                                    <div className="user-info-wrapper">
                                        <CustomImage
                                        width={40}
                                        height={40}
                                        src={user.imageUrl}
                                        alt="User image"/>
                                        <div className="username-wrapper">
                                            <div className="username-label">Comment as :</div>
                                            <div className="username">{user.name}</div>
                                        </div>
                                        <Link onClick={signOut} className="signOut-button" >Sign Out</Link>
                                    </div>
                                    <Button icon="pencil" content="Comment" size="mini" floated="right" onClick={postComment} className="comment-button" primary/>
                                </div>
                                :
                                <div>
                                    <Icon name="user circle" size="big" className="no-user-icon" color="black"/>
                                    <span>Not Signed In</span>
                                    <Link onClick={signIn} className="signIn-button">Sign In</Link>
                                    <Button icon="pencil" content="Comment" size="mini" floated="right" style={{marginTop: '.6%'}} disabled primary/>
                                </div>
                        }
                    </Form.Field>
                </Form>

                <div className="comments-list-wrapper">
                    {
                        this.props.comments.length > 5 && (this.state.comments.length !== this.props.comments.length) && <div>
                            <span className="older-comments-label" onClick={this.viewOlderComments}>
                                 <Icon name="angle double left" size="small" color="blue"/>View Older comments
                            </span>
                        </div>
                    }
                    {
                        this.state.comments.map((comment, index)=> {
                            return <Feed key={index}>
                                <Feed.Event>
                                    <Feed.Label image={comment.author.imageUrl}/>
                                    <Feed.Content>
                                        <Feed.Date>{`${ArticleNewsStore.getDate(comment.postedAt)} ${ArticleNewsStore.getTime(comment.postedAt)}`}</Feed.Date>
                                        <Feed.Summary>
                                            <a href={comment.author.profileLink} target="_blank">{comment.author.name}</a>
                                        </Feed.Summary>
                                        <Feed.Extra text>
                                            {comment.message}
                                        </Feed.Extra>
                                    </Feed.Content>

                                </Feed.Event>
                            </Feed>
                        })
                    }
                </div>
            </div>
        )
    }
}

export default CommentForm;