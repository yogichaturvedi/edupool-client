/**
 * Created by Amit on 16-11-2017.
 */
import React, {Component} from 'react';
import {observer} from 'mobx-react';
import {Dropdown, Form, Segment} from "semantic-ui-react";
import {getCities, getStates} from '../../util/states-cities';
import {toJS} from "../../../node_modules/mobx/lib/mobx";

@observer
class AddressForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            states: getStates(),
            cities: []
        };
        this.onFormUpdate = this.onFormUpdate.bind(this);
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.address.state){
            this.setState({cities : getCities(nextProps.address.state)});
        }
    }

    onFormUpdate(field, value) {
        this.props.onAddressUpdate(field, value);
        if(field === 'state'){
            this.setState({cities : getCities(value)});
        }
    }

    render() {
        return (
            <div className="address-form-wrapper">
                {/*<h3>Your Address here</h3>*/}
                <Form>
                    <Form.Input size="small" label='Line 1'
                                placeholder='Line 1'
                                icon='home'
                                iconPosition='left'
                                value={this.props.address.line1}
                                onChange={(e)=>this.onFormUpdate('line1', e.target.value)}
                                onBlur={(e)=>this.onFormUpdate('line1', e.target.value)}/>
                    <Form.Input size="small" label='Line 2'
                                placeholder='Line 2'
                                icon='home'
                                iconPosition='left'
                                value={this.props.address.line2}
                                onChange={(e)=>this.onFormUpdate('line2', e.target.value)}
                                onBlur={(e)=>this.onFormUpdate('line2', e.target.value)}/>
                    <Form.Group>
                        <Form.Field>
                            <label>State</label>
                            <Dropdown width={5}
                                      upward={true}
                                      minCharacters={0}
                                      placeholder="Select State"
                                      selectOnBlur={false}
                                      value={this.props.address.state}
                                      options={toJS(this.state.states)}
                                      onChange={(e, value) => this.onFormUpdate('state', value.value)} search selection/>
                        </Form.Field>
                        <Form.Field>
                            <label>City</label>
                            <Dropdown width={5}
                                      upward={true}
                                      minCharacters={0}
                                      placeholder="Select City"
                                      selectOnBlur={false}
                                      disabled={!this.props.address.state}
                                      options={toJS(this.state.cities)}
                                      value={this.props.address.city}
                                      onChange={(e, value) => this.onFormUpdate('city', value.value)} search selection/>
                        </Form.Field>
                        <Form.Field>
                            <Form.Input placeholder='Enter Pincode'
                                        value={this.props.address.pincode}
                                        onChange={(e)=>{this.onFormUpdate('pincode', e.target.value)}} label="Pincode"/>
                        </Form.Field>
                    </Form.Group>
                </Form>
            </div>
        );
    }
}

export default AddressForm;