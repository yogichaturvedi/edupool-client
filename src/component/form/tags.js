/**
 * Created by Amit on 18-09-2017.
 */
import React, {Component} from 'react';
import {Form, Header, Icon, Label} from "semantic-ui-react";
import {observer} from "mobx-react";
import _ from 'lodash';
import './tags.scss';

@observer
class TagsForm extends Component {


    constructor(props) {
        super(props);
        this.removeAllTags = this.removeAllTags.bind(this);
        this.state = {
            tag: "",
            message: ""
        };
    }

    onTagChange(tag) {
        this.setState({tag: tag.toLowerCase()});
    }

    addTag() {
        let tagToSave = this.state.tag.trim();
        if (tagToSave) {
            let duplicateTag = _.find(this.props.tags, (tag) => tag === tagToSave);
            if (duplicateTag) {
                this.setState({message: "Duplicate tag value not allowed"});
            }
            else {
                this.setState({tag: "", message: ""});
                this.props.addTag(tagToSave);
            }
        }
    }

    // removeTag(tagIndex) {
    //     this.props.removeTag(tagIndex);
    //     // let tagsToUpdate = this.state.tags;
    //     // _.remove(tagsToUpdate, (tag, index) => index === tagIndex);
    //     // this.setState({tags: tagsToUpdate});
    // }

    removeAllTags() {
        this.props.removeAllTags();
        this.setState({tag: "", message: ""});
    }

    render() {
        return (
            <div>
                <Header as="h4" color="red">{this.state.message}</Header>
                {/*<Form>*/}
                    <Form.Group>
                        <Form.Field width={10}>
                            <Form.Input placeholder={"Enter Tag"}
                                        value={this.state.tag}
                                        onChange={(e) => { this.onTagChange(e.target.value) }}/>

                        </Form.Field>
                        {
                            <Form.Button style={{display : 'none'}} icon="plus" color="green"
                                         onClick={(event) => this.addTag(event)} inverted circular/>
                        }
                        <Form.Button icon={<Icon name="trash" style={{color:"white"}}/>}
                                     className="clear-all-tag-button"
                                     color="orange" content="Clear all"
                                     onClick={(event) => this.removeAllTags(event)}/>
                    </Form.Group>
                    <Form.Field width={16}>
                        <Label.Group tag>
                            {
                                this.props.tags.map((tag, index) => {
                                    return <Label key={index} size="medium" color="teal">
                                        {tag} <Icon name='close' onClick={() => this.props.removeTag(index)}/>
                                    </Label>
                                })

                            }
                        </Label.Group>
                    </Form.Field>
                {/*</Form>*/}
            </div>);
    }
}

export default TagsForm;