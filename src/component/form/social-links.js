import React, {Component} from 'react';
import {Button, Form} from "semantic-ui-react";
import {observable} from "mobx";
import {observer} from "mobx-react";
import './form.scss';

const socialOptions = [
    {key: "facebook", "text": "Facebook", "value": "facebook"},
    {key: "twitter", "text": "Twitter", "value": "twitter"},
    {key: "google-plus", "text": "Google Plus", "value": "google plus"},
    {key: "vk", "text": "VK", "value": "vk"},
    {key: "linkedin", "text": "Linkedin", "value": "linkedin"},
    {key: "instagram", "text": "Instagram", "value": "instagram"},
    {key: "youtube", "text": "Youtube", "value": "youtube"}
];

// const hashTags = ["basic-details", "gallery", "about-us", "courses-and-fees", "hostel-and-buses", "contact-us", "events", "social-links"]

@observer
class SocialLinksForm extends Component {

    @observable socialLinks = [];

    constructor(props) {
        super(props);
        this.emptySocialMediaData = {"name": "", "link": ""};
        this.socialLinks = props.socialLinks;
    }

    componentWillMount() {
        // this.socialOptions = BasicStore.getSocialOptions();
    }

    componentWillReceiveProps(props) {
        this.socialLinks = props.socialLinks;
    }

    onSocialMediaOptionSelect(socialMediaType) {
        this.props.onSocialMediaOptionSelect(socialMediaType);
    }

    onSocialMediaChange(index, path, value) {
        this.props.onSocialMediaChange(index, path, value);
    }

    onSocialLinkChange(index, path, value) {
        this.props.onSocialMediaChange(index, path, value);
    }

    render() {
        return (
            <Form>
                {
                    this.socialLinks.map((event, index) => {
                        index = index + 1;
                        return <Form.Group widths="16" key={"social-link-form-field-" + index}>
                            <Form.Select placeholder='Select platform'
                                         width="5"
                                         label="Select Platform"
                                         onSelect={(e, b) => {
                                             this.onSocialMediaOptionSelect(b);
                                         }}
                                         value={this.socialLinks[index - 1].name}
                                         onChange={(e, b) => {
                                             this.onSocialMediaChange(index - 1, "name", b.value)
                                         }}
                                         options={socialOptions}/>
                            <Form.Input
                                width="10"
                                placeholder={"Enter link"}
                                value={this.socialLinks[index - 1].link}
                                onChange={(e) => {
                                    this.onSocialLinkChange(index - 1, "link", e.target.value)
                                }}
                                label={"Link"}/>
                            <Form.Button className="delete-button" icon="trash" color="red" inverted circular
                                         onClick={(event) => this.props.remove(index - 1)}/>
                        </Form.Group>
                    })
                }
                <Button icon="plus" color="green" inverted circular
                        onClick={() => {
                            this.socialLinks.push(this.emptySocialMediaData)
                        }}/>
            </Form>
        );
    }
}

export default SocialLinksForm