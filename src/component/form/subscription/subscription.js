/**
 * Created by Amit on 23-11-2017.
 */
import React, {Component} from 'react';
import {Button, Input} from "semantic-ui-react";
import {observer} from "mobx-react";
import {isEmailValid} from "../../../util/util";
import ContactUsStore from "../../../route/store/contact-us-store";
import './subscription.scss';

@observer
class SubscriptionForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: {value: '', valid: true}
        };
        this.onChange = this.onChange.bind(this);
        this.submit = this.submit.bind(this);
        this.onKeyUp = this.onKeyUp.bind(this);
    }

    onChange(event) {
        this.setState({email: {value: event.target.value, valid: isEmailValid(event.target.value)}});
    }

    onKeyUp(event){
        const keyCode = event.keyCode || event.which;
        if (keyCode === 13) {
            this.submit();
        }
    }

    submit() {
        let email = this.state.email;
        let isValid = isEmailValid(this.state.email.value);
        if(isValid){
            ContactUsStore.send({email:this.state.email.value,subject:"I have subscribed to EDUPOOL"});
            this.props.showToastr('success', 'Subscription', 'Thanks for subscribing with us');
            email.value = "";
        }
        email.valid = isValid;
        this.setState({email : email});
    }

    render(){
        return(
            <div className={`subscription-container ${this.props.place}`}>
                <div className="subscription-form-container">
                    <div className="alt-font text-small text-medium-gray text-uppercase footer-column-title">Subscribe Here</div>
                    <div className="footer-text">To never miss any announcement subscribe here</div>
                    <Input onChange={this.onChange}
                           error={!this.state.email.valid}
                           value={this.state.email.value}
                           className={this.state.email.valid ? "valid" : "invalid"}
                           action={<Button icon="mail" onClick={this.submit}/>}
                           onKeyUp={(e)=>this.onKeyUp(e)}
                           placeholder='Enter your email here'>
                    </Input>
                </div>
            </div>
        )
    }
}

export default SubscriptionForm;