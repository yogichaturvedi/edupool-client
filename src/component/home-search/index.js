/**
 * Created by Yogesh Chaturvedi on 02-11-2017.
 */
import React, {Component} from 'react';
import {observer} from 'mobx-react';
import {Button, Dropdown, Input} from "semantic-ui-react";
import SearchExampleStandard from "../search/index";

const searchOptions = [
    {key: 'school', text: 'School', value: 'school'},
    {key: 'college', text: 'College', value: 'college'},
    {key: 'coaching', text: 'Coaching', value: 'coaching'},
    {key: 'exam', text: 'Exam', value: 'exam'},
    {key: 'result', text: 'Result', value: 'result'},
    {key: 'event', text: 'Event', value: 'event'},
    {key: 'study-material', text: 'Study', value: 'study-material'},
];

@observer
class HomeSearch extends Component {
    constructor(props) {
        super(props);
        this.beginSearch = this.beginSearch.bind(this);
        this.state = {searchType: 'school', initiateSearch: false};
        this.onSearchOptionChange = this.onSearchOptionChange.bind(this);
    }

    beginSearch() {
        this.setState({initiateSearch: true});
    }

    onSearchOptionChange(value) {
        this.setState({searchType: value});
    }

    render() {
        return (
            <div>
                <Input type='text'
                       placeholder='Search your city ...' action>
                    <Button.Group color='blue' size="big"
                                  style={{background: "#2185d0"}}>
                        <Dropdown compact options={searchOptions} button
                                  value={this.state.searchType}
                                  onChange={(a, option) => {
                                      this.onSearchOptionChange(option.value)
                                  }}/>
                    </Button.Group>
                    <div className="home-search">
                        <SearchExampleStandard type={this.state.searchType}
                                               initiateSearch={this.state.initiateSearch}/>
                    </div>
                    <Button size="large" color="orange" icon="chevron right"
                            onClick={this.beginSearch}/>
                </Input>
            </div>
        );
    }
}

export default HomeSearch;
