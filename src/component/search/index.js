import _ from 'lodash'
// import faker from 'faker'
import React, {Component} from 'react'
import {Form, Search} from 'semantic-ui-react'
import {browserHistory} from "react-router";
import {toTitleCase} from "../../util/util";

const source = [
    {
        "title": "Central Academy Senior Secondary School, Sardarpura, Udaipur"
    },
    {
        "title": "SS College of Engineering"
    },
    {
        "title": "Ascent Career Point Udaipur"
    },
    {
        "title": "Poornima"
    },
    {
        "title": "JECRC"
    }
];

export default class SearchExampleStandard extends Component {
    componentWillMount() {
        this.resetComponent()
    }

    constructor(props) {
        super(props);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    resetComponent = () => this.setState({isLoading: false, results: [], value: ''});

    handleResultSelect = (e, {result}) => this.setState({value: result.title});

    handleSearchChange = (e, {value}) => {
        this.setState({isLoading: true, value});

        setTimeout(() => {
            if (this.state.value.length < 1) return this.resetComponent();

            const re = new RegExp(_.escapeRegExp(this.state.value), 'i');
            const isMatch = result => re.test(result.title);

            this.setState({
                isLoading: false,
                results: _.filter(source, isMatch),
            })
        }, 500)
    };

    onFormSubmit() {
        localStorage.setItem("activeTab", this.props.type);
        if(_.isFunction(this.props.updateBurgerMenu)) {
            this.props.updateBurgerMenu();
        }
        browserHistory.push("/" + this.props.type + "?search=" + this.state.value);
    }

    componentWillReceiveProps(props) {
        if(props.initiateSearch && this.state.value.trim()){
            this.onFormSubmit();
        }
        this.resetComponent();
    }

    render() {
        const {isLoading, value, results} = this.state;
        let page = this.props.type === 'study-material' ? 'Study Material' : toTitleCase(this.props.type);
        let placeHolder = `Search ${page}`;
        return (
            <Form>
                {/*<Form.Field>*/}
                <Search
                    loading={isLoading}
                    onResultSelect={this.handleResultSelect}
                    onSearchChange={this.handleSearchChange}
                    results={results}
                    placeholder={placeHolder}
                    value={value}
                    showNoResults={false}
                />
                {/*</Form.Field>*/}
                <Form.Button style={{display: 'none'}} onClick={() => this.onFormSubmit()}>Search</Form.Button>
            </Form>
        )
    }
}

