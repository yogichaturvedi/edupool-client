export default function setEnvVariable() {
  console.log('process', process.env.NODE_ENV);
  process.env.NODE_ENV = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';
  if(process.env.NODE_ENV !== 'development') {
    process.env.REACT_APP_API = '';
    console.log('process.env.REACT_APP_API in production env ', process.env.REACT_APP_API );
  } else {
    console.log('process.env.REACT_APP_API in development env ', process.env.REACT_APP_API );
    process.env.REACT_APP_API = 'http://localhost:3050';
  }
}

setEnvVariable();
