var express = require('express');
var proxy = require('http-proxy-middleware');
var cors = require('cors');

/**
 * Configure proxy middleware
 */

var eduPoolServer = proxy({
    target: "http://34.211.98.114:10101",
    changeOrigin: true,
    logLevel: 'debug'
});

var app = express();

app.use(cors({exposedHeaders: ["location"]}));

/**
* Add the proxy to express
*/

app.use('/edu-pool-server',eduPoolServer);

app.listen(3060);

console.log('Edu-Pool Server: listening on port 3060');
